# A Board 4 U To Do list #

## Roadmap

Check the [Kanban board for the project](https://pag-isep.atlassian.net/secure/RapidBoard.jspa?rapidView=2&projectKey=B4U).

## Service orientation

- aspects of service-orientation that are important to showcase:
  - autonomy
  - API gateway (security, throtling, ...)
  - monitoring
  - identity management
  - more than one (micro-)service
    - authz
    - user
    - message
    - comments
    - tags
    - recommendations (e.g., based on tags)
  - multiple (micro)services:
    - TCP/gRPC transport for the microservices, HTTP/GraphQL for the API
  - different technology stacks 
  - elasticity, scalability
  - resilience, fault-tolerance
  - event-driven
  - graphQL as an API aggregator for more than one service
  - docker/kubernetes deployment

## Tags

tags should have its own repository

- in the future it can be a microservice
- tags should be updated by events when a message is created/updated/deleted
    - message queueing / streaming (kafka)
    - https://blog.heroku.com/kafka-data-pipelines-frp-node
- tags need to be cleaned up to avoid treating smalls caps as a different tag
- leverage cache

## Toughts

- Pagination
  - if we always limit the search results to "n" how do we know if it is the last page?
  - if we use pagination with cursor "after", how do we determine the "prev" page?

- misc.
  - commit guidelines, e.g., [jquery](https://contribute.jquery.org/commits-and-pull-requests/#commit-guidelines)

## Deployment and operations

- should Swagger UI be exposed on the deployed server? in production?
- GraphQL Playground is not exposed in production but would be interesting to have a cloud deployment with it 
- we should have two deployments: `development` and `production`

## New Features ideas

- Message
    - aprove a message `[COULD]`
    - clap a post action
        - `clap` subresource (collection)
    - Comments on messages `[SHOULD]`
        - https://schema.org/Comment 
        - `comment` subresource (collection)
        - boolean to allow comments on creation, default: false
        - close/open a post for comments
        - reply/comment to a post
        - retract a post/comment `[COULD]`
    - both claps and comments might be a separate microservice from messages

- User
    - Report/block a user from commenting your posts `[COULD]` 
    - aprove a user `[COULD]`
    - follow an author `[SHOULD]`

## GraphQL

- how to handle photo upload for user's avatar?
- API subscriptions `[SHOULD]`
    - trivia augmentation completed ? (instead of a callback)
    - message posted by a specific author
    - comments on my posts
- security `[SHOULD]`
    - https://nordicapis.com/security-points-to-consider-before-implementing-graphql/
    - https://github.com/stems/graphql-depth-limit
    - https://leapgraph.com/graphql-api-security
- GraphQL support for optimistic UI thru subscriptions `[COULD]`
    - https://dev-blog.apollodata.com/tutorial-graphql-mutations-optimistic-ui-and-store-updates-f7b6b66bf0e2 
    - https://medium.com/open-graphql/save-hundreds-of-lines-of-code-in-your-next-graphql-app-with-this-one-weird-trick-3bef9ef0d45a
- modules
    - https://github.com/Urigo/graphql-modules
    - https://nordcloud.com/modular-graphql-server/
- tips
    - https://graphql.org/learn/best-practices/ 
    - https://blog.apollographql.com/full-stack-react-graphql-tutorial-582ac8d24e3b
    - https://sazzer.github.io/blog/2016/05/07/Designing-a-GraphQL-API/
    - https://hackernoon.com/graphql-tips-after-a-year-in-production-419341db52e3
    - https://www.freecodecamp.org/news/five-common-problems-in-graphql-apps-and-how-to-fix-them-ac74d37a293c/
    - https://techblog.commercetools.com/modeling-graphql-mutations-52d4369f73b1
- tips (misc)
    - https://github.com/joonhocho/graphql-input-number 
    - https://github.com/Urigo/graphql-scalars
    - https://medium.com/open-graphql/top-5-graphql-scalars-3f8a38965b53
    - https://github.com/graphql-community/graphql-directive-auth
    - https://blog.apollographql.com/reusable-graphql-schema-directives-131fb3a177d1
    - apollo engine for performance tracing
        - https://blog.apollographql.com/tutorial-building-a-graphql-server-cddaa023c035
    - https://blog.runscope.com/posts/you-might-not-need-graphql 

## RESTful Hypermedia

- partial update of messages/users with `json-patch` media type `[COULD]`
    - http://jsonpatch.com/
    - https://www.npmjs.com/package/fast-json-patch
- projections and embeddeds `[COULD]`
    - new query string parameters: 
        - `Fields` - to define which properties to return
        - `Include` - to define which subresources/linked resources to embedd 
- Web UI webforms
    - JWT authentication `[SHOULD]`
    - `edit-message.html` and `edit-user.html` `[COULD]`
        - add more JS to the form to allow `PUT` and `DELETE`?
    - list the message collection `[COULD]`
    - list the user collection `[COULD]`
    - styling: https://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-media-objects.php
- Use of `prefer` http header
    - https://www.iana.org/assignments/http-parameters/http-parameters.xhtml#preferences 

## Media types

- use `application/problem+json` media type `[COULD]`: RFC7807 https://tools.ietf.org/html/rfc7807 
- support more media-types `[SOULD]`
    - HAL
- support more media-types `[COULD]`
    - https://sookocheff.com/post/api/on-choosing-a-hypermedia-format/ 
    - BSON
        - http://bsonspec.org/
    - JSON:API
        - https://jsonapi.org/ 
    - `hCard` for user
    - JSON-LD
        - https://json-ld.org/spec/latest/json-ld-api-best-practices/
    - other, e.g., SIREN, Collection+JSON

- Schemas
    - send schema in all responses `[MUST]`
    - https://json-schema.org/learn/getting-started-step-by-step.html
    - https://apisyouwonthate.com/blog/the-many-amazing-uses-of-json-schema-client-side-validation 
    - should we send different schemas based on the media type or just the data part?
    - schemas for `messageDataIn` are diferent. schemas should be returned for client-side validation, so they should be the `messageIn` or `userIn` and not the message or user schema?
    - hyper schema
        - https://json-schema.org/draft/2019-09/json-schema-hypermedia.html
        - https://apisyouwonthate.com/blog/getting-started-with-json-hyper-schema/

- meta data
    - https://developer.ibm.com/technologies/web-development/articles/wa-schemaorg2 
    - https://developer.ibm.com/technologies/web-development/articles/wa-schemaorg3
    - schema.org
        - https://schema.org/CreativeWork 
        - https://schema.org/Comment
        - https://schema.org/Review
        - https://schema.org/Person 
        - https://schema.org/Action
    - RDF
    - microdata
    - json-ld `[COULD]`
        - `application/ld+json`
        - send json-ld `@context` in a link header, e.g., 
        - `Link: <https://example.org/contexts/imu.jsonld>; rel="https://www.w3.org/ns/json-ld#context"`
        - https://json-ld.org/spec/latest/json-ld-api-best-practices/ 

## Authz

- OAuth/OpenID Connect `[COULD]`

## Other potential topics ##

_all of these are prioritized as `[COULD]`_

- gRPC endpoint
    - ...
- publishing events
    - graphql subscriptions
        - ...
    - webhooks
        - ...
    - https://devcenter.heroku.com/articles/node-websockets
    - https://devcenter.heroku.com/articles/realtime-polyglot-app-node-ruby-mongodb-socketio
- CQRS
- HTTP/2
    - https://nodejs.org/api/http2.html
    - https://kinsta.com/learn/what-is-http2/
    - https://developers.google.com/web/fundamentals/performance/http2
    - https://www.upwork.com/hiring/development/the-http2-protocol-its-pros-cons-and-how-to-start-using-it/
    - https://www.smashingmagazine.com/2016/02/getting-ready-for-http2/
    - https://webapplog.com/http2-node/
    - server push
        - https://medium.com/the-node-js-collection/node-js-can-http-2-push-b491894e1bb1
        - https://www.smashingmagazine.com/2017/04/guide-http2-server-push/

## Application structure

- separate utility node modules
    - https://nodejs.org/api/modules.html 
    - publish to npm `[COULD]`
        - https://zellwk.com/blog/publish-to-npm/

## Testing

- what kind of tests should the user controller and user commands have that do not endup in duplication?

- unit testing `[MUST]`
    - https://buddy.works/guides/how-automate-nodejs-unit-tests-with-mocha-chai
    - https://codeburst.io/javascript-unit-testing-using-mocha-and-chai-1d97d9f18e71
    - https://hackernoon.com/a-crash-course-on-testing-with-node-js-6c7428d3da02

- mocha tests
    - https://blog.logrocket.com/a-quick-and-complete-guide-to-mocha-testing-d0e0ea09f09d/
    - https://dev.to/asciidev/testing-a-nodeexpress-application-with-mocha--chai-4lho
    - https://medium.com/kanssfer-consulting/testing-expressjs-rest-api-with-mocha-and-chai-90bf4178f15e

- POSTman collection
    - https://medium.com/distant-horizons/testing-apis-with-postman-10-common-challenges-solutions-c4674c78528d

- partial update - user-controller.spec - partial updates of different fields must **resolve***!!!!

## Documentation

- references
    - https://nordicapis.com/5-examples-of-excellent-api-documentation/
        - https://stripe.com/docs/api/errors
    - https://restfulapi.net/

- openapi
    - describe links: https://swagger.io/docs/specification/links/ `[SHOULD]`

## Persistence

- upsert
    - leverage upsert from the data layer with Mongo's `findOneAndUpdate` to do upsert `[COULD]`
- mongodb persistence layer
    - better error handling `[SHOULD]`
        - e.g., not possible to connect if no network connection

## Lingering in older versions ##

- v5, v6, v7, v8
    - postman collection 
        - upload photo example
            
