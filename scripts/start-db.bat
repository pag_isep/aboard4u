@echo off
REM *
REM * A Board 4 U
REM *
REM * Copyright (c) 2013-2022 Paulo Gandra de Sousa
REM * MIT Licensed
REM *

SETLOCAL 

start mongod --dbpath=../data --port=10050