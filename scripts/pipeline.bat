@echo off
REM *
REM * A Board 4 U
REM *
REM * Copyright (c) 2013-2022 Paulo Gandra de Sousa
REM * MIT Licensed
REM *

SETLOCAL 

cd ..
echo Linting...
call npm run lint
if errorlevel 1 (
    exit /b %errorlevel%
)

echo Installing dependencies and Auditing for vulnerabilities...
call npm install
if errorlevel 1 (
    exit /b %errorlevel%
)
call npm audit fix

echo Checking outdated dependencies...
call npm outdated --dev
if errorlevel 1 (
    exit /b %errorlevel%
)

echo Testing
call npm test
if errorlevel 1 (
    exit /b %errorlevel%
)

if "%1%"=="-run" (
    echo Starting local server...
    SET DEBUG=pag:*
    start npm start

    echo End-to-end testing...
    cd scripts
    call e2e-api-test.bat -all -dev
)