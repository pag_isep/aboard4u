@echo off
REM *
REM * A Board 4 U
REM *
REM * Copyright (c) 2013-2022 Paulo Gandra de Sousa
REM * MIT Licensed
REM *

SETLOCAL 

if "%1%" == "-h" SET SHOW_HELP=1
if "%1%" == "" SET SHOW_HELP=1

if "%1" == "-all" (
    SET RUN_REST_D=1
    SET RUN_GQL_D=1
    SET RUN_REST_S=1
    SET RUN_GQL_S=1
    SET RUN_REST_M=1
    SET RUN_GQL_M=1
    SET RUN_REST_U=1
    SET RUN_GQL_U=1
    SET RUN_REST_T=1
    SET RUN_GQL_T=1
)
if "%1" == "-demo" (
    SET RUN_REST_D=1
    SET RUN_GQL_D=1
)
if "%1" == "-signup" (
    SET RUN_REST_S=1
    SET RUN_GQL_S=1
)
if "%1" == "-messages" (
    SET RUN_REST_M=1
    SET RUN_GQL_M=1
)
if "%1" == "-users" (
    SET RUN_REST_U=1
    SET RUN_GQL_U=1
)
if "%1" == "-tags" (
    SET RUN_REST_T=1
    SET RUN_GQL_T=1
)

REM if not defined RUN_REST SET SHOW_HELP=1

if defined SHOW_HELP (
    echo Usage:
    echo    API [ENV]
    echo. 
    echo Where API is one of:
    echo    -all 
    echo    -demo 
    echo    -signup 
    echo    -messages 
    echo    -users
    echo    -tags
    echo. 
    echo and ENV is
    echo    -dev
    exit
)


SET API_COL=../message-server/assets/apitest/
SET API_PHOTO=../stock-photos


SET environment=%API_COL%ABoard4U.postman_environment.json
IF "%2" == "-dev" (
    SET environment=%API_COL%ABoard4U_dev.postman_environment.json
)

echo Running against environment %environment%
echo    loading collections from %API_COL%
echo    with working dir %API_PHOTO%
echo Executing


if "%RUN_REST_D%" == "1" (
    echo.
    echo Running Demo data
    call newman run "%API_COL%ABoard4U-Demo data.postman_collection.json" -e "%environment%" --working-dir "%API_PHOTO%"
    if errorlevel 1 (
    echo Failure Running Demo data; Reason Given is %errorlevel%
    exit /b %errorlevel%
    )
)

if "%RUN_REST_D%" == "1" (
    echo.
    echo Running API discovery
    REM temporarily disabled due to application not being deployed to heroku anymore
    REM call newman run "%API_COL%ABoard4U-API discovery.postman_collection.json" -e "%environment%"  --working-dir "%API_PHOTO%"
    if errorlevel 1 (
        echo Failure Running API discovery; Reason Given is %errorlevel%
        exit /b %errorlevel%
    )
)

if "%RUN_REST_S%" == "1" (
    echo.
    echo Running User registration + authz
    call newman run "%API_COL%ABoard4U-Authz & Signup.postman_collection.json" -e "%environment%"  --working-dir "%API_PHOTO%"
    if errorlevel 1 (
        echo Failure Running User registration + authz; Reason Given is %errorlevel%
        exit /b %errorlevel%
    )
)

if "%RUN_REST_U%" == "1" (
    echo.
    echo Running User management
    call newman run "%API_COL%ABoard4U-User.postman_collection.json" -e "%environment%"  --working-dir "%API_PHOTO%"
    if errorlevel 1 (
        echo Failure User management; Reason Given is %errorlevel%
        exit /b %errorlevel%
    )
)

if "%RUN_REST_M%" == "1" (
    echo.
    echo Running Message management
    call newman run "%API_COL%ABoard4U-Message.postman_collection.json" -e "%environment%"  --working-dir "%API_PHOTO%"
    if errorlevel 1 (
        echo Failure Running Message management; Reason Given is %errorlevel%
        exit /b %errorlevel%
    )
)

if "%RUN_REST_T%" == "1" (
    echo.
    echo Running Tag querying
    call newman run "%API_COL%ABoard4U-Tags.postman_collection.json" -e "%environment%"  --working-dir "%API_PHOTO%"
    if errorlevel 1 (
        echo Failure Running Tag querying; Reason Given is %errorlevel%
        exit /b %errorlevel%
    )
)

echo.
echo Everything OK.