# A Board 4 U Feature List #

*A [REST](https://en.wikipedia.org/wiki/Representational_state_transfer)ful Hypermedia 
and [GraphQL](http://graphql.org) API for a message board service*

Copyright (c) 2013-2022 Paulo Gandra de Sousa

[MIT Licensed](https://spdx.org/licenses/MIT.html)

---

## Terminology

**user** - authenticated user (human or agent) of the service

**guest** - unathenticated user (human or agent) of the service

## Features overview

### Exploring the API
   
| Feature                                                       | REST                            | GraphQL                       |
|---------------------------------------------------------------|---------------------------------|-------------------------------|
| User friendly homepage                                        | `/`                             |                               |
| link relationship description                                 | `/`                             |                               |
| Billboard API                                                 | `GET /api`                      |                               |
| OpenAPI specification                                         | `/api/openapi.json`             |                               |
| Swagger UI                                                    | `/api/api-docs`                 |                               |
| GraphQL schema                                                |                                 | introspection on `/graphql`   |
| GraphQL Playground                                            | `/graphql`                      |                               |
| obtain info on API version                                    | rel=`https://schema.org/softwareVersion` |                      |
| obtain info on API license                                    | rel=`license`                   |                               |
| obtain schema of user data                                    | rel=`describedBy`               |                               |
| obtain schema of message data                                 | rel=`describedBy`               |                               |
||||

### Signup

| Feature                                                                 | REST                            | GraphQL                 |
|-------------------------------------------------------------------------|---------------------------------|-------------------------|
| As a guest I want to signup for the service                             | `PUT /api/user/{username}`      | `registerUser`          |
| As a guest I want to upload my avatar while signing up for the service  | `PUT /api/user/{username}`      | n.a.                    |
| As a guest I want to signup for the service and upload my avatar in a user-friendly way | `/forms/signup.html` <br> `POST /user` | n.a. |
||||

### Authenticate

| Feature                                                                 | REST                            | GraphQL                 |
|-------------------------------------------------------------------------|---------------------------------|-------------------------|
| As guest I want to authenticate so I can use the service afterwards     | rel=`https://aboard4u.herokuapp.com/rel/login` | n.a.     |
| As guest I want to reset my password                                    | rel=`https://aboard4u.herokuapp.com/rel/password-reset` | n.a. |
| As user I want to change my password                                    | rel=`https://aboard4u.herokuapp.com/rel/password` | `changeMyPassword`  |
||||

### My User account

| Feature                                                                 | REST                            | GraphQL                  |
|-------------------------------------------------------------------------|---------------------------------|--------------------------|
| As user I want to get my user info                                      | rel=`https://aboard4u.herokuapp.com/rel/me`   | `me`                     |
| As user I want to edit my user info in a human-friendly way             | `/forms/edit-user.html`         | n.a.                     |
| As user I want to replace my account info                               | rel=`edit`                      | n.a.                     |
| As user I want to partially update my account info                      | `PATCH /api/user/{id}`          | `updateMyUser`           |
| delete my user account                                                  | rel=`edit`                      | n.a.                     |
| update my user's photo                                                  | `PUT /api/user/{id}/photo`      | n.a.                     |
| update my user's photo in a browser-friendly way                        | `POST /api/user/{id}/photo`     | n.a.                     |
| delete my user's photo                                                  | `DELETE /api/user/{id}/photo`   | n.a.                     |
||||

### Browsing messages

| Feature                                                                 | REST                            | GraphQL                  |
|-------------------------------------------------------------------------|---------------------------------|--------------------------|
| As a guest I want to browse messages                                    | `GET /api/message`              | `messages()`             |
| As a guest I want to search messages by title, author name, text, date of publishing or tag | `GET /api/message` | `messages(criteria)` |
| paginate message results (links)                                        | rel=`start` <br> rel=`next` <br> rel=`prev` | n.a.         |
| paginate message results (query string)                                 | `GET /api/message`              | `messages(paging)`       |
| get a message                                                           | `GET /api/message/{id}`         | `message(id)`            |
| get a message and its author                                            | n.a.                            | `Message.author`         |
| get messages of one user                                                | rel=`https://aboard4u.herokuapp.com/rel/messages` | `User.messages` |
| As a guest/user I want to know how many messages a user has posted      | `GET /user/:username/message/summary` | `User.hasPostedN`  |
||||

### Browsing authors

| Feature                                                                 | REST                            | GraphQL                  |
|-------------------------------------------------------------------------|---------------------------------|--------------------------|
| As a guest/user I want to get a user info                               | `GET /user/{id}`                | `user(username)`         |
| As a guest/user I want to get a user info as PDF                        | `GET /user/{id}`                | n.a.                     |
| As a guest/user I want to get a user info as PDF in a browser-friendly way | `GET /user/:username/pdf`    | n.a.                     |
| As a guest/user I want to get a user's photo                            | `GET /user/:username/photo`     | n.a.                     |
| search users by name or email                                           | `GET /user`                     | `users(criteria)`        |
| paginate user results (links)                                           | rel=`start` <br> rel=`next` <br> rel=`prev` | n.a.         |
| paginate user results (query string)                                    | `GET /user`                     | n.a.                     |
||||


### Querying tags

| Feature                                                                 | REST                            | GraphQL                  |
|-------------------------------------------------------------------------|---------------------------------|--------------------------|
| As guest I want to know all existing tags                               | `GET /tags`                     | `tags`                   |
| As guest I want to search tags                                          | `GET /tags`                     | `tags(criteria)`         |
| As guest I want to know the distribution of messages per tag (tag-cloud)| `GET /tag-cloud`                | `tagCloud`               |
| As guest I want to know an author's tag-cloud                           | `GET /tag-cloud`                | `tagCloud(criteria)`     |
||||

### Posting and manipulating messages

| Feature                                                                 | REST                            | GraphQL                  |
|-------------------------------------------------------------------------|---------------------------------|--------------------------|
| As user I want to post a message and optionaly tag it                   | `POST /api/message`             | `postMessage`            |
| As user I want to post a message in a user-friendly way                 | `/forms/post-message.html`      | n.a.                     |
| As user I want to edit one of my messages in a user-friendly way        | `/forms/edit-message.html`      | n.a.                     |
| As user I want to partialy update one of my messages                    | `PATCH /api/message/{id}`       | `updateMessage`          |
| As user I want to replace one of my messages                            | rel=`edit`                      | n.a.                     |
| As user I want to delete one of my messages                             | rel=`edit`                      | `deleteMessage`          |
| As user I want to augment one of my existing messages with trivia and be notified via callback | `POST /api/message/{id}/trivia` | n.a.   |
| As user I want to augment one of my existing messages with trivia and pool the result | `POST /api/message/{id}/trivia` | `augmentMessage` |
| As user I want to post a message for augmentation and be notified via callback | `POST /api/message-trivia` | n.a.                   |
| As user I want to post a message for augmentation and pool the result   | `POST /api/message-trivia`  | `postMessageForAugmentation` |
||||

### Other

- JSON Schema validation
- hash passwords in the persistence storage
- gzip compression
- rate limiter
- support for HTTPS
- cleanup data for representations (e.g., avoid exposing `__v` or `__id`)
- concurrency handling
    - REST: `etag` and `if-match`
    - GraphQL: `version` property and parameter to mutations
- `Link` headers in REST response
- `application/vnd.pagsousa.hyper+json`
- postman collection w/ tests
- Basic and Digest HTTP authentication
- JWT authentication
- inmemory and mongo persistence strategies
- `.env` configuration file
- use `HTTP_PROXY` and `HTTPS_PROXY` and `NO_PROXY` env variables
- cluster mode
- service status `GET /status`
