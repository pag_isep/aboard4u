/*
 * A Board 4 U client
 * asynchronous POST with callback
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */

/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// this examples POSTs a request and waits for the response in a calback endpoint
// there is no correlation between messages
//


// clients needs to be able to receive requests
const express = require('express');

const callbackApp = express();
callbackApp.disable("x-powered-by");

callbackApp.use(express.json({
    type: ["application/json", "application/vnd.pagsousa.hyper+json"]
}));
callbackApp.use(express.urlencoded({
    extended: true
}));

// enable compression
const compression = require('compression');
callbackApp.use(compression());


const PORT = parseInt(process.argv[3]) || 3007;
const HOST = require("os").hostname();
const CALLBACK_ROOT = "http://" + HOST + ":" + PORT;

// to count replies
let callbackCount = 0;

//how many postbacks should we receive - don't count the error urls!
const POSTBACKS_TO_WAIT = 7;


//
// callback routing
// uses the same callback url for all requests
//
function handleJsonResponse(body, res, req) {
    if (!body.correlation || !body.correlation.yourRef) {
        console.log("Invalid postback without correlation");
        res.status(400).send({
            error: "You must send correlation info."
        });
    } else {
        // process the response to our initial request
        console.log("myRef %s got response ", body.correlation.yourRef, body);
        //print the Link header
        console.log("Link header:", req.headers.link);

        res.status(req.params.statusToSend).end();
    }
}

function handleHyperResponse(body, res, req) {
    if (body.data) {
        if (!body.correlation || !body.correlation.yourRef) {
            console.log("Invalid postback without correlation");
            res.status(400).send({
                error: "You must send correlation info."
            });
        } else {
            // process the response to our initial request
            console.log("myRef %s got response (data) ", body.correlation.yourRef, body.data);
            //print the Link header
            console.log("Links:", body.links);

            res.status(req.params.statusToSend).end();
        }
    } else {
        //something strange has been received
        console.log("Invalid postback with unexpected/unknown content", body);
        res.status(400).send({
            error: "unexpected/unknown content"
        });
    }
}

callbackApp.route("/callback/:statusToSend")
    .post(function (req, res) {
        callbackCount++;
        const body = req.body;
        const contentType = req.header("content-type");
        console.log(`\nPOSTBACK as ${contentType} ==>`);
        if (body.error) {
            console.log("myRef: %s  got error %s", body.error.correlation.yourRef, body.error);
        } else if (contentType === "application/vnd.pagsousa.hyper+json") {
            handleHyperResponse(body, res, req);
        } else if (contentType === "application/json") {
            handleJsonResponse(body, res, req);
        } else {
            //something strange has been received
            console.log("Invalid postback with unexpected/unknown content", body);
            res.status(400).send({
                error: "unexpected/unknown content"
            });
        }

        //end process if we already received the *n* replies back
        if (callbackCount == POSTBACKS_TO_WAIT) {
            setTimeout(() => {
                console.log("Received all the postbacks we were waiting for. shuting down...");
                process.exit();
            }, 500);
        }
    });

// STARTING callback
callbackApp.listen(PORT, function () {
    console.log("Listening on " + PORT);
});


//
// CLIENT
//
const request = require('request');
const serverUrl = process.argv[2] || "http://localhost:3001/api";

/**
 * @param {string} token
 * @param {*} title 
 * @param {*} txt 
 * @param {*} ref 
 * @param {Boolean} hyper - request hyper message format instead of application/json 
 * @param {Boolean} genError - send wrong callback url to generate "error" on the server
 * @param {Boolean} sendError - callback will reply with content and status not expected on the server
 */
function postMessage(token, title, txt, ref, hyper, genError, sendError) {
    const options = {
        uri: serverUrl + "/message-trivia",
        method: "POST",
        headers: {
            Authorization: "Bearer " + token,
        },
        body: {
            title: title,
            text: txt,
            callback: CALLBACK_ROOT + "/callback/200",
            callbackRef: ref.toString()
        },
        json: true
    };
    if (hyper) {
        options.headers.accept = "application/vnd.pagsousa.hyper+json";
    }
    if (genError) {
        options.body.callback = CALLBACK_ROOT + "/unexisting-callback-url";
    }
    if (sendError) {
        options.body.callback = CALLBACK_ROOT + "/callback/400";
    }
    // POST message
    request(options,
        function (err, res, body) {
            if (!err) {
                console.log("Posted request '%s'\n\t\t got response code %s", txt, res.statusCode);
            } else {
                console.log(err);
            }
        });
}

function login(username, password, cb) {
    const options = {
        uri: serverUrl + "/authz/login",
        method: "POST",
        body: {
            username,
            password
        },
        json: true
    };
    // POST message
    request(options, function (err, res, body) {
        if (!err) {
            console.log("Loging in '%s'\n\t\t got response code %s\n\t\t%s", username, res.statusCode, body);
            cb(res.statusCode, body);
        } else {
            console.log(err);
        }
    });
}

login("Mary", "123", function (statusCode, token) {
    if (statusCode == 200) {
        // POST messages to test out of order and boundary conditions
        console.log("POST messages to test out of order and boundary conditions\n");

        postMessage(token, "hello", "test #1 of callback post", 1);
        postMessage(token, "hello", "test #2 HYPER of callback post", 2, true);
        postMessage(token, "hello", "test #3 of callback post", 3);
        postMessage(token, "hello", "test #4 WRONG URL of callback post", 4, false, true);
        postMessage(token, "hello", "test #5 WRONG URL of callback post", 5, false, true, true);
        postMessage(token, "hello", "test #6 of callback post", 6);
        postMessage(token, "hello", "test #7 WRONG STATUS of callback post", 7, false, false, true);
        postMessage(token, "hello", "test #8 HYPER of callback post", 8, true);
        postMessage(token, "hello", "test #9 HYPER + WRONG STATUS of callback post", 9, true, false, true);
    }
});