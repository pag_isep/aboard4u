/*
 * A Board 4 U client
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */

/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// an hypermedia-enabled client that uses the hypermedia controls available 
// in the user media type to GET one user from the server and checks if the 
// user has a photo (HEAD). if so it will download the photo (GET) and a PDF
// card of the user. otherwise it will POST a photo to the server.
//

const request = require('request');
const fs = require('fs');

const userURI = process.argv[2] || "http://localhost:3001/api/user/Tom";
const photo = process.argv[3] || "../../stock-photos/man.jpg";
const fnamePrefix = process.argv[4] || "tom";
const username = process.argv[5] || "Tom";
const password = process.argv[6] || "123";
const serverUrl = process.argv[7] || "http://localhost:3001/api";


function getLink(links, rel) {
    for (let link of links) {
        if (link.rel == rel) {
            return link.href;
        }
    }
    return null; // TODO better error handling
}

function getPhotoUriFromUser(user) {
    return getLink(user.links, 'https://Schema.or/image');
}

function getSelfUriFromUser(user) {
    return getLink(user.links, 'self');
}


function checkIfUserHasPhoto(userURI, user, aPhoto, aFNnamePrefix, etag) {
    const userPhotoURI = getPhotoUriFromUser(user);
    if (!userPhotoURI) {
        console.log("The requested user has no photo, let's upload it");
        // POST to the user to update the photo since we don't have the image link
        uploadPhoto(userURI, aPhoto, etag);
    } else {
        request({
                uri: userPhotoURI,
                method: "HEAD",
                json: {}
            },
            function (err, res, body) {
                if (!err) {
                    if (res.statusCode == 404) {
                        uploadPhoto(userPhotoURI, aPhoto, etag);
                    } else {
                        downloadPhotoAndPDF(user, aFNnamePrefix);
                    }
                } else {
                    console.log(err);
                }
            });
    }
}

function login(username, password, cb) {
    const options = {
        uri: serverUrl + "/authz/login",
        method: "POST",
        body: {
            username,
            password
        },
        json: true
    };
    // POST message
    request(options, function (err, res, body) {
        if (!err) {
            console.log("Loging in '%s'\n\t\t got response code %s\n\t\t%s", username, res.statusCode, body);
            cb(res.statusCode, body);
        } else {
            console.log(err);
        }
    });
}

function doUploadPhoto(token, userPhotoURI, aPhoto, etag) {
    const options={
        uri:userPhotoURI,
        method: "POST",
        headers: {
            Authorization: "Bearer " + token,
            "if-match": etag
        }
    };
    const req = request.post(options,
        function (err, resp, body) {
            if (err) {
                console.log('Error!', err);
            } else {
                console.log('URL: %s', body);
            }
        });
    const form = req.form();
    form.append('displayImage', fs.createReadStream(aPhoto));
}

function uploadPhoto(userPhotoURI, aPhoto, etag) {
    login(username, password, function (statusCode, token) {
        if (statusCode == 200) {
            doUploadPhoto(token, userPhotoURI, aPhoto, etag);
        }
        else {
            console.log("Unable to authenticate")
        }
    });
}

function downloadPhotoAndPDF(user, aFNnamePrefix) {
    const theUserPhotoURI = getPhotoUriFromUser(user);
    const theUserURI = getSelfUriFromUser(user);

    // GET photo
    request
        .get(theUserPhotoURI)
        .on('error', function (err) {
            console.log(err);
        })
        .pipe(fs.createWriteStream(aFNnamePrefix + '-photo.jpg')); // TODO give proper file extension

    // GET PDF
    request({
            uri: theUserURI,
            headers: {
                'Accept': 'application/pdf'
            },
        })
        .on('error', function (err) {
            console.log(err);
        })
        .pipe(fs.createWriteStream(aFNnamePrefix + ".pdf"));
}


// GET User
request({
        uri: userURI,
        json: {},
        headers: {
            "accept": "application/vnd.pagsousa.hyper+json"
        }
    },
    function (err, res, body) {
        if (!err) {
            console.log("received HEADERS: %s", res.headers);
            console.log("received BODY: %s", JSON.stringify(body));
            checkIfUserHasPhoto(userURI, body, photo, fnamePrefix, res.headers.etag);
        } else {
            console.log(err);
        }
    });