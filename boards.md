# A Board 4 U Project Boards

## Repository

[source code repository](https://bitbucket.org/pag_isep/aboard4u/src)

## Project Management

[Issues](https://pag-isep.atlassian.net/secure/RapidBoard.jspa?rapidView=2&projectKey=B4U)

[Ideas](TODO.md)

## Quality

[Sonar](https://sonarcloud.io/dashboard?id=pag_isep_message-service)

[LGTM](https://lgtm.com/projects/b/pag_isep/aboard4u/?mode=list)

[42Crunch](https://platform.42crunch.com/apis/234884ee-17fa-491e-a67c-0a31822eea06/api-summary)

## Pipeline

[build pipeline](https://bitbucket.org/pag_isep/aboard4u/addon/pipelines/home)

## Operations

[database](https://cloud.mongodb.com/)

[cloud hosting](https://dashboard.heroku.com/apps/aboard4u)

[runtime logs](https://dashboard.heroku.com/apps/aboard4u/logs)

[IAM Identity and Access Management](https://manage.auth0.com/dashboard/eu/aboard4u/)
