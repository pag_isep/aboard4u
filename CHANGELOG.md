# A Board 4 U Release Notes #

*A [REST](https://en.wikipedia.org/wiki/Representational_state_transfer)ful Hypermedia 
and [GraphQL](http://graphql.org) API for a message board service*

Copyright (c) 2013-2022 Paulo Gandra de Sousa

[MIT Licensed](https://spdx.org/licenses/MIT.html)

---

## 6.1.0-SNAPSHOT

**scope:** TBD

Removed/Breaking:

- TBD

Deprecated:

- TBD

What's new:

- sample request and test for large JSON payloads
- B4U-59 standardize error messages
    - 413 Payload Too Large
    - 415 Unsupported Media Type
    - 400 Bad Request
- B4U-145 unit test for Response module: more test coverage

Fixes:

- B4U-162 CSP issues on production env
- B4U-163 enable trivia web implementation in heroku deployment
- B4U-164 allow brackets in message title and text regular expression

## 6.0.0 / 2021.01.11

**scope:** Decoupling resources' implementation & self-documenting API

Removed/Breaking:

- B4U-152 decouple authz from user
    - GraphQL mutation `changeMyPassword` returns a new type `Authz` instead of `User`
        - should have been done in version 5.0.0 in the scope of #131 Authz resource MUST not return user representation
- B4U-157 use heroku deployment as custom relationship type for self-documenting and exploratory API
    - `http://pagsousa.net/me` -> `https://aboard4u.herokuapp.com/rel/me`
    - `http://pagsousa.net/login` -> `https://aboard4u.herokuapp.com/rel/login`
    - `http://pagsousa.net/password` -> `https://aboard4u.herokuapp.com/rel/password`
    - `http://pagsousa.net/password-reset` -> `https://aboard4u.herokuapp.com/rel/password-reset`
    - `http://pagsousa.net/messages` -> `https://aboard4u.herokuapp.com/rel/messages`
    - `http://pagsousa.net/trivia` -> `https://aboard4u.herokuapp.com/rel/trivia`
    - `http://pagsousa.net/users` -> `https://aboard4u.herokuapp.com/rel/users`
- B4U-155 switch to `https` prefixed custom relationship types
    - `http://schema.org/softwareVersion` -> `https://schema.org/softwareVersion`
    - `http://Schema.org/image` -> `https://Schema.org/image`
- B4U-161 set `content-type` on rel assets to `text/html`

Deprecated:

- n.a.

What's new:

- B4U-146 add `photo` child object back to user representation
- B4U-147 add `authorUsername` field back to message representation
- B4U-56 monitoring with `clinic doctor`
- B4U-54 optimize for performance
    - clustering
    - `/status` endpoint
- refactorings:
    - B4U-144 reorganize POSTMAN collections merging rest and graphql
    - better organization of postman collections to avoid test duplication
    - B4U-152 decouple authz from user
    - B4U-145 unit test for Response module
    - avoid code duplication in `Hyper` and `Response`
    - remove `console.log` from collection scripts
    - enhance testing to collection pre-request scritps
    - B4U-156 refactor limit file size and type
    - enhance random id generator to avoid collisions
    - fix regular expressions in openapi and JSON schemas

Fixes:

- duplicate link `rel` in `link` header
- missing openapi message schema pattern
- B4U-59 enhance error messages: missing `if-match` header
- B4U-108 avoid duplication in openapi schemas
- B4U-37 json schema validation failing test
- B4U-150 hypermedia collection `users` and `message` is returning implementation details: `_id`, `__v`
- B4U-158 next link in user collection `start` and `n` parameters are `NaN`
- B4U-159 search messages of today is returning results from other days


## 5.2.1 / 2020.12.28

Fixes:

- B4U-150 hypermedia collection `users` and `message` is returning implementation details: `_id`, `__v`


## 5.2.0 / 2020.12.22

**scope:** JSON schema validation

Removed/Breaking:

- n.a

Deprecated:

- n.a.

What's new:

- B4U-37 validate json schema for each request
    - messages
    - user
- B4U-57 security
    - validate JWT issuer, audience and algorithm to avoid tampering
- refactorings
    - random fake phrases in POSTMAN collection instead of fixed text
    - `n` random messages from random users in bootstrap demo data
    - use of `Collection.countDocuments` instead of deprecated `Collection.count` in MongoDB persistence

Fixes:

- in memory tag persistence layer
- B4U-96 lgtm issues: unused symbols
- B4U-140 sonar issues: limit content size
- B4U-142 42Crunch issues: openapi vulnerabilities
- B4U-108 avoid duplication in openapi


## 5.1.0 / 2020.12.07

**scope:** tags

Removed/Breaking:

- n.a.

Deprecated:

- n.a.

What's new:

- #28 As user I want to add tags to a message when posting a message 
- #136 As guest/user I want to search messages by tags 
- #137 As guest/user I want to know all existing tags
- #137 As guest/user I want to search tags
- #138 As guest/user I want to know the distribution of messages per tag (tag-cloud) 
- #138 As guest/user I want to know an author's tag-cloud 

Fixes:

- change password
- etag generation


## 5.0.1 / 2020.11.27

Fixes:

- change password
- etag generation


## 5.0.0 / 2020.11.27

**scope:** cleanup deprecated features

Removed/Breaking:

- #131 Authz resource MUST not return user representation
    - `/authz/me/password`
    - `/authz/password-reset-confirmation`
    - JWT payload does not return user representation in `http://pagsousa.net/me` property
- #132 remove deprecated link relations  
- `http://pagsousa.net/hyper/userMessages` (was replaced by `http://pagsousa.net/messages`)
- `http://pagsousa.net/hyper/user` (was replaced by `http://pagsousa.net/users`)
- `http://pagsousa.net/hyper/message` (was replaced by `http://pagsousa.net/messages`)
- User representation no longer brings links that are really related to the authentication session
    - `rel=http://pagsousa.net/me`
    - `rel=http://pagsousa.net/authz/me/password`
    - `rel=http://pagsousa.net/password-reset`
    - `rel=http://pagsousa.net/login`

Deprecated:

- n.a.

What's new:

- #133 all user endpoints handle accept header to return a PDF representation
- refactorings
    - decoupling authz from user
    - middleware to add session links according to authentication
- JWT payload has property `links`

Fixes:

- n.a.


## 4.1.1 / 2020.11.26

Fixes:

- revert breaking change #60 remove deprecated query string parameters in `GET /messages`


## 4.1.0 / 2020.11.18

**scope:** cleanup

**alternate name:** V13

Removed:

- `ABOARD4U_PORT` environment variable as it is duplicating the standard `PORT` env variable
- #60 remove deprecated query string parameters in `GET /messages`. `start` and `n` were **replaced** by `first` and `after`

Deprecated:

- `http://pagsousa.net/hyper/userMessages` will be replaced by `http://pagsousa.net/messages`
- `http://pagsousa.net/hyper/user` will be replaced by `http://pagsousa.net/users`
- `http://pagsousa.net/hyper/message` will be replaced by `http://pagsousa.net/messages`
- Authz resource responses won't return user representation to avoid coupling
    - JWT payload `http://pagsousa.net/me`
    - `PUT /authz/me/password`
    - `POST /autz/password-reset-confirmation`

What's new:

- refactorings
    - #35 replace deprecated `request` by `node-fetch`
    - add internal utility module `fetch-with-proxy`

Fixes:

- #128 `PATCH` should return `405` instead of `404` for resources that are not allowed
- #124 ensure `content-type` for `/authz` resource
- email field must be mandatory on signup form


## 4.0.1 / 2020.11.03

Fixes:

- #128 `PATCH` should return `405` instead of `404` for resources that are not allowed


## 4.0.0 / 2020.11.02

**scope:** cleanup authz

Removed:

- #80 `GET /authz/me` (breaking change) as it goes against REST principles. It represents different "resources" depending on the authorization so issuing a `GET` makes no sense (for instance, it cannot be cached). The path returns a `410 Gone` and will be completly removed in future versions of the API.

Deprecated:

- n.a.

What's new:

- #80 new link relationships
    - `http://pagsousa.net/login`

Fixes:

- n.a.


## 3.0.3 / 2020.11.03

Fixes:

- #128 `PATCH` should return `405` instead of `404` for resources that are not allowed


## 3.0.2 / 2020.10.30

What's new:

- refactorings
    - Webapp UI arrangements

Fixes:

- #126 webapp is showing `${API_VERSION}`


## 3.0.1 / 2020.10.30

What's new:

- refactorings
    - #123 reuse responses in OpenAPI

Fixes:

- #116 hash passwords (_this would required a database migration in production_)
- #124 ensure the right `content-type` is sent for each route
- avoid some security breaches by returning less info, e.g., `404` when changing the password of an unexisting user


## 3.0.0 / 2020.10.28

**scope:** JWT authentication + merge-patch

Removed:

- n.a.

Deprecated:

- n.a.

What's new:

- `deleteMessage`, `updateMessage` graphQL mutations return `null` if the message does not exist
    - previously an internal server error was being returned
    - this is a change in the contract as the schema previously mentioned a non-null return
- #118 use JWT for authentication
    - new `/authz/login` resource for obtaining the JWT token
    - new environment variable `ALLOW_BASIC_DIGEST_AUTH`
    - new environment variable `SIGN_SECRET`
    - use JWT on routes
    - updated POSTMAN collections
- #57 optimize for security
    - use `helmet`
    - force the use of HTTPS on heroku
- #48 partial update with PATCH merge
    - `PATCH /message/:id` with `application/merge-patch+json`
    - `PATCH /user/:id` with `application/merge-patch+json`
    - #119 `POST` form-data returns `415`
    - #120 `PATCH` with `merge-patch` is not updating
- refactorings
    - #117 #119 migrate `merge-graphql-schema` to `graphql-tools`

Fixes:

- n.a.


## 2.0.0 / 2020.03.04

**scope:** clean up deprecated stuff from previous versions 

Removed:

- #91 removed deprecated query string parameter `user` in `GET /messages`
    - renamed to `author` 
- #93 removed deprecated query parameters `from` and `to`
    - renamed to `created_from` and `created_to`
- #30 removed deprecated `prompt` property of `Link` and `Query` objects
    - renamed to `title`
- #23 removed deprecated `photo.path` property of `user` 
    - use the Link relation `http://schema.org/image`
- #45 removed deprecated property `description` when doing file upload
    - renamed to `caption`
- #45 removed deprecated property `title` when returning the caption of the user's photo
    - renamed to `caption`
- #24 removed parameter `latest` from GraphQL query `User.messages`
- removed `userID` form field
    - renamed to `username`
- #112 remove deprecated `senderId` property of `message` 
    - renamed database field to `authorUsername`
    - use the Link relation `author` 
- #113 removed deprecated `photo.image` property of `user`
    - use the Link relation `http://schema.org/image`
    - the `photo` property was completely removed. The link relation has the `title` and `type` attributes for conveying the mimetype and caption of the photo
- #114 removed `User.id` property 
    - renamed to `User.username`
    - GraphQL parameters renamed to `username`
- renamed `userID` form field to `username`

Deprecated:

- n.a.

What's new:

- #19 user's short description, i.e., about me 
- #22 partially update a message
    - `PATCH /message/:id` with `application/json`
    - `POST /message/:id` with same semantics as `PATCH`
- #26 partially update user
    - `PATCH /user/:username` with `application/json`
    - `POST /user/:username` with same semantics as `PATCH`
- #24 pagination in GraphQL query 
    - `messages`
    - `User.messages`
- #25 filtering in GraphQL query done thru a Type parameter
    - `messages`
    - `users`
- refactorings
    - #62 application structure
        - avoid global vars
        - express application
        - routes
        - `response` and `hyper` as util modules
        - user command classes for partial and full update, user registration and deletion
        - packages along components w/ layers inside
            - graphql schema and resolvers
            - REST handlers
            - controllers
    - #44 improve test coverage
        - `pag-hyper`

Fixes:

- message filtering by creation date in the in-memory persistence layer implementation
- validation of date format in filtering
- version label on web UI footer


## 1.7.0 / 2020.02.04

**scope:** internal refactorings

Removed:

- n.a.

Deprecated:

- n.a.

What's new:

- #101 As user/guest I want to get the user's latest messages
- #102 As a guest/user I want to know how many messages a user has posted
- #54 optimize for performance
    - enable compression (gzip)
    - set up rate limiter
- refactorings
    - #82 use native methods instead of lodash: 
        - `_.every` replaced by `Array.prototype.every`
        - `_.map` replaced by `Array.prototype.map`
        - `_.reduce` replaced by `Array.prototype.reduce`
    - #62 restructure the application
        - utility module with pagination limits
        - utility module with ID/Token generator
        - message "feature" folder
        - users "feature" folder
        - authz "feature" folder
    - #107 fix 42Crunch issues
        - `ABOARD4U_HOST` env var specifies `https` transport
        - integer fields must specify `minimum`
        - string fields must specify `maxLength`

Fixes:

- send `correlation` property on message augmentation callback when connected to MongoDB
- issue #95 `hasPostedN` does not count all messages of the user
- Internal Server Error when logging in via Digest authentication with an unknown username


## 1.6.1 / 2020.01.22

Fixes:

- send `correlation` property on message augmentation callback when connected to MongoDB


## 1.6.0 / 2020.01.20

**scope:** internal quality (refactorings & test coverage)

Removed:

- n.a.

Deprecated:

- query string parameters in `GET /messages`
    - `user` will be renamed to `author` 
    - `from` and `to` will be renamed to `created_from` and `created_to`
    - `start` and `n` will be **replaced** by `first` and `after`
- query string parameters in `GET /user/:username/messages`
    - `start` and `n` will be **replaced** by `first` and `after`

What's new:

- #89 As a user/guest I want to search messages by title 
- #90 query string parameter `author` in `GET /messages`
- #92 query parameter `created_from` and `created_to` in `GET /messages`
- #7 message's publishing date attribute (with the same value as creation date)
- #94 Messages should be sorted by publishing date descending order
    - new message attribute `publishedOn`
- #61 `first` and `after` paging query parameters 
- refactorings
    - #44 \[QA\] improve test coverage 
        - UserController
        - MessageController
        - util
    - #65 \[QA\] `lint` script in pipeline step
    - #73 \[QA\] add e2e `newman` step to the build pipeline
    - #62 \[refactor\] restructure the application
        - separate config and dependency injection to its own module
        - `pipeline` local script
    - #51 \[cloud\] store photo image outside of the filesystem
        - image storing as its own separate module
        - `photo-repo` folder created on startup if it doesn't exist
    - #77 mock trivia augmentation for local test w/o network

Fixes:

- n.a.


## 1.5.0 / 2020.01.05

**scope:** internal quality (refactorings & test coverage)

Removed:

- `PROXY` env variable (see #47)

Deprecated:

- paging query parameters `start` and `n` will be **replaced** by `first` and `after` 

What's new:

- #33 limit file size and type
- #41 edit links are only sent for authenticated requests
- #46 support hypermedia on message augmentation postback
- #47 use proxy `env` settings (i.e., `HTTP_PROXY`, `HTTPS_PROXY`, `NO_PROXY`), for making the requests to outside networks
- refactorings
    - #44 initial test coverage
    - #55 switch branch strategies
    - #62 \[refactor\] restructure the application
        - `scripts` folder
        - e2e test script in `scripts` folder

Fixes:

- issue #27 sonar issues
- issue #32 do not use `multer` as global middleware
- issue #34 refactor route parameters
- issue #35 use `request` instead of `node-fetch`
- issue #36 Replace `underscore` with `lodash`
- issue #40 take authentication into account even for unauthenticated endpoints


## 1.4.0 / 2019.12.30

**scope:** cloud deployment

Removed:

- n.a.

Deprecated:

- #29 property `prompt` of object `link` - clients should use `title` instead as according to [RFC 8288](https://tools.ietf.org/html/rfc8288#section-3.4.1)  
- #29 property `prompt` of object `query` - clients should use `title` instead
- #38 property `description` when doing file upload, and `title` when returning the caption of the user's photo - clients should use `caption` instead

What's new in this release:

- #20 rebranding service to "ABoard4U" (previously known as "Message Service")
- #20 make postman collection available in the webapp as part of the API documentation
- refactorings
    - #21 connection to mongodb cloud database (see also #20)
    - restructure postman collections
    - two environments for postman collection, localhost and heroku

Fixes:

- (reopened) issue #18 `PUT /user/:username` MUST fully update the resource
- issue #31 correctly support image media type on upload 


## 1.3.0 / 2019.12.19

**scope:** GraphQL mutations

**alternate name:** v12

Removed:

- n.a.

Deprecated:

- n.a.

What's new in this release:

- improved description
- GraphQL 
    - #1 queries return `version` of the object for optimistic concurrency control
    - #1 As user I want to update one of my messages
    - #15 As user I want to delete one of my messages
    - #16 As user I want to change my password
    - #17 As user I want to update my account info
    
Fixes:

- issue #13 `PUT /message/:id` to fully replace resource
- issue #18 `PUT /user/:username` MUST fully update the resource


## 1.2.1 / 2019.12.19

Fixes:

- issue #13 `PUT /message/:id` to fully replace resource
- issue #18 `PUT /user/:username` MUST fully update the resource


## 1.2.0 / 2019.12.17

**scope:** semver & documentation

Deprecated:

- `senderId` property of `message` object will be removed. Clients should use the Link relation `author` 
- deprecated `photo.path` property of `user` object will be removed. Clients should use the Link relation `http://schema.org/image`
- `photo.image` property of `user` object will be removed. Clients should use the Link relation `http://schema.org/image`

What's New in this release:

- use [semantic versioning](https://semver.org/) for version names of the project
    - *all the previous versions were renamed. this should **not** happen in a real life scenario of course!*
- feature list
- postman collection sample requests for simulating `edit-form` and `create-form`
- #10 [sonar cloud analisys](https://sonarcloud.io/dashboard?id=pag_isep_message-service)
- project management
    - [bitbucket issues](https://bitbucket.org/pag_isep/message-service/issues)
    - #2 [trello board](https://trello.com/b/SckHZk5Q)

Fixes:

- handle `__etag` in `edit-message.html`


## 1.1.4 / 2019.11.19

Fixes:

- issue #13 `PUT /message/:id` to fully replace resource


## 1.1.3 / 2019.12.18

Fixes:

- remove unimplemented mutations from schema


## 1.1.2 / 2019.12.16

Fixes:

- cleanup unnecessary files


## 1.1.0 / 2019.12.13

**scope:** GraphQL

What's New in this release:

- GraphQL API
    - new sublayer in `service`
    - Queries
        - `message(id)`
        - `messages(text, author, from, to, start, n)`
        - message connection to author
        - trivia factoids in message
        - `user(id)`
        - `user(name, email)`
        - user connection to messages
        - `me`
    - Mutations
        - `registerUser`
        - `postMessage`
        - `augmentMessage`
        - `postMessageForAugmentation`
    - postman collection
- trivia factoids as a map based on the date of the augmentation request 
    - MongoDB Schema changed to make `trivia` a mixed "any" type
- do not read version number from `.env` file
- limit search results pagination to avoid unnecessary stress on the server and eventual DOS attacks

Fixes:

- `trivia` link generation when using MongoDB
- checking if connection is opened in MongoDB
- search in memory persistence layer


## 1.0.6 / 2019.11.19

Fixes:

- issue #13 `PUT /message/:id` to fully replace resource
- issue #18 `PUT /user/:username` MUST fully update the resource


## 1.0.5 / 2019.12.16

Fixes:

- cleanup unnecessary files


## 1.0.0 / 2019.12.10

**scope:** restful hypermedia

**alternate name:** v11

What's New in this release:

- new link relations 
    - `http://pagsousa.net/me`
    - `http://pagsousa.net/password-reset`
    - restructure `authz/me/password`
- use link relation `describedBy` to send the json schema along with each response
    - user item - `userData.schema.json`
    - message item - `messageItem.schema.json`
- postman collection
    - test link presence in response
    - avoid code duplication in test scripts
- user `edit-form`
    - show link `alternate` to open PDF
    - send hidden field `__etag` with etag to replace the `if-match` header 
- openapi specification
    - describe headers
    - describe callbacks
- new subresource `/message/:id/trivia` to augment existing message with trivia
    - state-based link relation `http://pagsousa.net/trivia` only sent if message does not have `trivia` property
- register sucessful or unsucessful logins
- split postman collection for Authz & Signup from the User Management collection


## 0.10.5 / 2019.12.19

Fixes:

- issue #13 `PUT /message/:id` to fully replace resource
- issue #14 REST `/user` endpoint is returning mongodb document instead of DTO


## 0.10.4 / 2019.12.16

**scope:** restful hypermedia

**alternate name:** v10

Deprecated:

- `photo.path` property of `user` object will be removed. Clients should use the Link relation `http://schema.org/image` or `photo.image` property.

What's new in this release:

- A RESTfull hypermedia (level 3) API
- `POST /user` is unauthenticated and allowed to support html form enrolment
- `GET /user/:username/pdf` hack for browsers without content negotiation
- Hypermedia controls
    - `link` header
    - Hypermedia-enabled Media type, i.e., `application/vnd.pagsousa.hyper+json`, based on `Collection+JSON`
    - `edit-form` web ui
- API evolution vs versioning
    - backward compatibility
    - add new property (e.g., `user.photo.image`)
    - add a new media-type (e.g., `application/vnd.pagsousa.hyper+json`)
    - deprecate property(e.g., `user.photo.path`)
- new `authz/me` resource
- restructure and code review
- more examples in postman collection
- send `link` header
    - `authz`
    - `user/:id/pdf`
    - `user/:id/photo`
    - postback for message trivia
    - `license` link relation 
- better description

Fixes:

- photo upload storage


## 0.9.5 / 2019.12.19

Fixes:

- issue #13 `PUT /message/:id` to fully replace resource


## 0.9.4 / 2019.12.16

Fixes:

- POSTMAN collection


## 0.9.0 / 2019.11.22

**scope:** links

**alternate name:** v9

What's new in this release:

- `Link` header for each resource item (`self`, `author`, `image`) or collection (`self`, `start`, `prev`, `next`)
- pagination (filtering in the query string, and with `link` header)


## 0.8.5 / 2019.12.19

Fixes:

- issue #13 `PUT /message/:id` to fully replace resource


## 0.8.4 / 2019.12.16

Fixes:

- postman test collection


## 0.8.0 / 2019.11.22

**scope:** layers

**alternate name:** v8

What's new in this release:

- node modules with separation of concerns
    - service
    - application
    - persistence
- pagination with query string parameters `start` and `n`
- in-memory and mongo persistence
- `.env` configuration file


## 0.7.4 / 2019.12.19

Fixes:

- issue #13 `PUT /message/:id` to fully replace resource


## 0.7.3 / 2019.12.16

Fixes:

- error handling when missing file on user photo update
- path to photo in postman collection


## 0.7.0 / 2019.11.22

**scope:** async

**alternate name:** v7

What's new in this release:

- exposes `/message-trivia` resource to assynchronously post a new message
    - supports both polling and callback
    - enriches the message with trivia facts obtained from an external service (http://numbersapi.com)
- proxy settings for making the requests to outside networks


## 0.6.4 / 2019.12.16

Fixes:

- error handling when missing file on user photo update
- path to photo in postman collection
- `GET /user` must be authenticated


## 0.6.0 / 2019.11.22

**scope:** concurrency

**alternate name:** v6

What's new in this release:

- `PUT /user/:username` is both
    - unauthenticated to allow to register new users and
    - authenticated for updates
- concurrency handling with `etag` and `if-match`


## 0.5.4 / 2019.12.16

Fixes:

- error handling when missing file on user photo update
- path to photo in postman collection


## 0.5.0 / 2019.11.22

**scope:** minimal features

**alternate name:** v5

A message board service

- exposing a `message` and a `user` resource 
- separation of concerns thru node modules
- authentication with passport
- `PUT /user/:username` is unauthenticated to allow to register new users
- `user/:id/photo/` subresource with file uploads via form multipart
- PDF representation of the user resource using content negotiation
- Open API 3.0 specification
- Swagger UI


## Previous history

*Versions prior to 0.5.x are demo exercises on building an Express server and are not considered in this history*