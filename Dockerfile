FROM node:alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json .

RUN npm i
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

# define environment variables
ENV NODE_ENV development
ENV PORT 3001
ENV SIGN_SECRET=J67l7rluGfBh4onJfls0yq5thc5oqPzwc8deMeoM

# let traffic flow in on port 3000
EXPOSE 3001

# start
CMD ["node", "index.js"]