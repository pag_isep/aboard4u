# A Board 4 U #

*A [REST](https://en.wikipedia.org/wiki/Representational_state_transfer)ful Hypermedia
and [GraphQL](http://graphql.org) API for a message board service*

Copyright (c) 2013-2022 Paulo Gandra de Sousa

[MIT Licensed](https://spdx.org/licenses/MIT.html)

---

## Overview ##

A message service providing three main abstractions: *messages* and their *authors*, and *tags*.

![Domain Model](message-server/webapp/domain-model.svg)

This is a monolithic service with a set of resources for each main abstraction, providing two different API paradigms:

- RESTful Hypermedia
- GraphQL

Having the two paradigms in the same API is more of an academic exercise than a real-life driven
scenario, in which, provably, only one type of API makes sense and it should be choosen based on
the actual requirements of the API. Check [REST And Hypermedia And GraphQL And gRPC And Event-Driven](https://apievangelist.com/2019/10/01/rest-and-hypermedia-and-graphql-and-grpc-and-eventdriven/),
[Picking the right API paradigm](https://apisyouwonthate.com/blog/picking-the-right-api-paradigm),
[When to use what: REST, GraphQL, WebHooks, gRPC](https://nordicapis.com/when-to-use-what-rest-graphql-webhooks-grpc/),
[GraphQL vs REST: overview](https://phil.tech/api/2017/01/24/graphql-vs-rest-overview/),
[Is REST still a relevant API style?](https://nordicapis.com/is-rest-still-a-relevant-api-style/),
[Understanding RPC, REST and GraphQL](https://apisyouwonthate.com/blog/understanding-rpc-rest-and-graphql),
[REST vs GraphQL: A critical review](https://goodapi.co/blog/rest-vs-graphql)
for a comparision and discussion on the paradigms and when to use which.

As a short summary:

- **REST** targets long lasting (decades) APIs explorable thru [hypertext (a fundamental constraint of a REST system)](https://roy.gbiv.com/untangled/2008/rest-apis-must-be-hypertext-driven),
cacheability and multiple representations to fit different clients and uniformity in acessing each
and every resource. Eventough you can follow REST principles with any technology stack you want,
HTTP *really* provides a [powerful](https://tyk.io/power-http-rest-apis-part-1/) foundation. As
a corolary, you really need to understand [HTTP](https://tools.ietf.org/html/rfc2616) in order to
build a proper REST API.
- **GraphQL** is an easy-to-understand RPC mindset targeting clients with very different needs of shapping
the result data without the burden to learn (and mantain on the server) different endpoints. It overcomes
underfetching, overfetching and N+1 problems typically associated with other API paradigms.

The **REST** API started as an HTTP API (*restish*) and moved up in the [Richardson's Maturity Model](https://martinfowler.com/articles/richardsonMaturityModel.html)) to an hypermedia API. The API is explorable following hypermedia
principles by acessing its "billboard" root endpoint and following the provided links - check the [Domain Application
Protocol](message-server/webapp/DAP.svg). The presence of the link in the
response representation depends on the current state of the resource, for instance, `edit` links are only sent for
authenticated and authorized requests. It exposes a `message` and `user`
resource alongside a `user/photo` subresource with file uploads via form multipart. See the
[Open API](https://swagger.io/specification/) specification available at `/api/opeanapi.json`
(thru [Swagger JSDoc](https://www.npmjs.com/package/swagger-jsdoc)) or the [Swagger UI](https://www.npmjs.com/package/swagger-ui) available at `/api/api-docs`. Content negotiation is supported for returning a PDF representation of
the user as well as support both media types of the application (eventough there is a `GET /user/:username/pdf`
hack for direct link from browsers bypassing content negotiation). Pagination is handled thru the query
string (i.e., `start`, `n`) and link relations (e.g., `prev`, `next`). It also exposes a `message-trivia`
resource to assynchronously post a new message - that supports both polling and callback - and enrich the
message with trivia facts obtained from an external service [Numbers API](http://numbersapi.com).

The **GraphQL** endpoint exposes pretty much the same functionality as the REST API. The `query` root object allows to
navigate the graph `message -> user` and the `mutation` root object supports signing up and posting messages.
Check the [GraphQL Playground](https://github.com/prisma-labs/graphql-playground) (available at `/graphql`)
and the [schema definition](message-server/service/graphql/schema.graphql).

There is a set of [Postman](https://www.getpostman.com/) collections and environment to
[showcase and test the API](/message-server/assets/apitest).  

Have a look at the [feature list](FEATURES.md).

### Authentication

Most of the capabilities of the API need an authenticated user. The authentication is handled
by [Passport](http://www.passportjs.org/) using [JSON Web Tokens](https://jwt.io/) (JWT) thru [HTTP Bearer Authentication](https://tools.ietf.org/html/rfc6750). Legacy support (_deprecated, will be removed in future versions_) exists for [HTTP Basic](https://tools.ietf.org/html/rfc7617) and [HTTP Digest](https://tools.ietf.org/html/rfc7616) authentication.

In order to obtain an access token, first, the user must register via `POST /user` or `PUT /user/{username}` or via the mutation `registerUser`.

The access token is obtained via `POST /authz/login` and using the returned token in the `Authorization` header of subsequent requests.

*A note on the REST routes*

The OpenAPI specification clearly marks which routes and verbs are authenticated or not. Even the endpoints
that do not need authentication, e.g., `GET /message/:id`, support authentication and react accordingly,
for instance, by providing aditional links in the response.

The `/user/:username` resource handles `PUT` *unauthenticated* requests to allow a new user to register,
and *authenticated* requests to allow a user to update her own profile.

To create a new user, since the client is determining the id of the resource to be created, the API
should not allow `POST /user` and only allow `PUT /user/:username`. However, we support `POST /user`
(unauthenticated) for easier integration with HTML forms for user enrollment.

*A note on the GraphQL endpoint*

Mutations are authenticated except `registerUser` for user registration.

Queries are unauthenticated except `users`.

### Optimistic Concurrency Control

*REST*

Concurrency handling with `etag` and `if-match` headers.

*GraphQL*

`version` property is returned in queries and needs to be passed as parameter to mutations.

### Partial vs Full updates

The REST API supports partial as well as full updates (replace) thru its use of `PATCH` (and `POST`) and `PUT` - see also [PUT vs PATCH vs json/patch](https://apisyouwonthate.com/blog/put-vs-patch-vs-json-patch).

The GraphQL API supports partial updates only.

### Versioning

Instead of versioning the API, the general philosophy is to favour **evolution** and not versioning.
Basically keep everything **backward compatible** by:

- adding new
    - non-mandatory properties (e.g., `user.description`),
    - resources
    - media types (e.g., `application/vnd.pagsousa.hyper+json`).
    - headers (e.g., `link` header)
    - link relations
- for existing operations and/or resources or properties, do not:
    - change meaning
    - rename
    - remove

Whenever there is something that needs to be deprecated and removed, clients of the API should be
informed of deprecated methods and properties and, with time, finally remove those properties from
the API (e.g., `user.photo.path`).

Client applications must be well-behaved and ignore properties, headers or link relations received
in the response body that the client application do not understand/know.

See [API evolution for REST/HTTP APIs](https://apisyouwonthate.com/blog/api-evolution-for-rest-http-apis/) and
[API change strategy](https://nordicapis.com/api-change-strategy/) for a longer discussion on this topic.

## How do I get set up? ##

### Summary of set up

1. install prerequisites
    1. [nodejs](https://nodejs.org/en/download/) v8+  
    1. [mongodb](https://www.mongodb.com/download-center#community)
1. install dependencies by running `npm i`
1. start mongodb server, e.g., `start-db.bat` in the `scripts` folder
1. update `.env` to point to the right mongodb database url
1. run the server, e.g., `node start` or `node run-script dev`
1. open your browser and go to `http://<HOST>/api/` where `<HOST>` is the hostname and port of your local development environment, e.g., `localhost:3001`

### Configuration

Set environment variables or edit the `.env` file

- Cluster mode, e.g.,
    - `WEB_CONCURRENCY=4`
- debug level, e.g.,
    - `DEBUG=pag:*`
- public host address if you do not want to rely on `os.hostname()`, e.g.
    - `ABOARD4U_HOST=https://aboard4u.herokuapp.com`
- proxy settings (i.e., `HTTP_PROXY`, `HTTPS_PROXY` and `NO_PROXY`), e.g.,
    - `HTTP_PROXY=http://192.168.9.17:8080`
- maximum number of results in searches, e.g.,
    - `ABOARD4U_MAX_RESULTS=250`
- maximum number of requests per client per minute, e.g.
    - `ABOARD4U_RATE_LIMIT=100`
- persistence mode, i.e.: `mem` or `mongodb`, e.g.,
    - `ABOARD4U_DB_MODE=mem`
- mongoDB connection string, e.g.,
    - `ABOARD4U_DB_URL=mongodb://localhost:10050/data`
- image storage mode, i.e.: `file`
    - `ABOARD4U_IMAGE_STORE_MODE=file`
- trivia augmentation, i.e.: `static` or `web`
    - `ABOARD4U_TRIVIA_AUGMENTER_MODE=web`
- Basic and Digest HTTP authentication
    - `ALLOW_BASIC_DIGEST_AUTH=true`
- Secret for signing JWT tokens (**mandatory**)
    - `SIGN_SECRET=J7rlBh4onJuGfflq5ths0yc5zqP8dwcoeM67leoM`

### Dependencies

Install dependencies by running `npm i`

### Database configuration

If using mongodb you need to create a database and change the connection string in `.env`.
For testing purposes mainly there is also an in memory persistence layer; set `DB_MODE` to `mem` in `.env`

### How to run tests

Run **unit and component tests** with `npm test`

Run **end-to-end tests** with the script `e2e-api-test`. If running thru Postman UI, don't forget to configure
the working directory so that the stock images used for user signup are found

### Deployment instructions
    
* TBD


## Contribution guidelines ##

Feel free to fork the repo, commit, push and create pull-requests. Please follow the same approach as 
the existing code, add the necessary and meaningfull unit tests.

### Who do I talk to?

Paulo Gandra de Sousa [pag@isep.ipp.pt](emailto:pag@isep.ipp.pt) / [pagsousa@gmail.com](emailto:pagsousa@gmail.com)


## License and copyright

Copyright (c) 2013-2022 the original author or authors.

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


## References

*REST in Practice: Hypermedia and Systems Architecture* (2010) Jim Webber, Savas Parastatidis, Ian Robinson. O’Reilly Media. 

*RESTful web APIs: Services for a Changing World* (2013) Leonard Richardson, Mike Amundsen. O’Reily Media. ISBN:1449358063

[REST cook book](http://restcookbook.com/)

[How to Graphql](https://www.howtographql.com/)
