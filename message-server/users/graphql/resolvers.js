/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:11 */

"use strict";


//
// GraphQL User resolvers
//


import {
    GraphQLError
} from 'graphql';


//
// The resolvers
//

/**
 * The GraphQL User resolvers
 * 
 * @param {*} config 
 * @param {*} dependencies 
 */
export function resolvers(config, dependencies) {
    const messageController = new dependencies.MessageController(config, dependencies);
    const userController = new dependencies.UserController(config, dependencies);
    const handleError = dependencies.handleGraphQLError;

    // return the resolvers object
    return {
        //
        // Queries
        //
        Query: {
            /**
             * Get the authenticated user
             */
            me(parent, args, context, info) {
                if (!context.user) {
                    throw new GraphQLError('You are not authorized to perform this action.', {
                        extensions: {
                            code: 'UNAUTHENTICATED',
                        },
                    });
                }

                return userController
                    .getUser(context.user.username)
                    .catch(handleError);
            },

            /**
             * Get one user by username
             */
            user(parent, args, context, info) {
                return userController
                    .getUser(args.username, false)
                    .catch(handleError);
            },

            /**
             * Search users
             * 
             * users(criteria: UserCriteria={}): [User!] !
             */
            users(parent, args, context, info) {
                if (!context.user) {
                    throw new GraphQLError('You are not authorized to perform this action.', {
                        extensions: {
                            code: 'UNAUTHENTICATED',
                        },
                    });
                }
                return userController
                    .searchUsers(args.criteria.name, args.criteria.email)
                    .then(out => out.data)
                    .catch(handleError);
            },
        },

        //
        // Query resolvers for type
        //
        User: {
            /**
             * returns the version of this object for optimistic concurrency locking
             * @param {*} parent 
             */
            version(parent) {
                return parent.__v;
            },

            /**
             * Get user's messages
             * 
             * messages(paging: PagingSpecification={}): [Message!] !
             */
            messages(parent, args) {
                const paging = args.paging;
                paging.afterMode = true;

                const criteria = {
                    author: parent.username
                };
                return messageController
                    .searchMessages(criteria, paging)
                    .then(out => out.data)
                    .catch(handleError);
            },

            /**
             * Get count of user's messages
             */
            hasPostedN(parent) {
                return messageController
                    .countUserMessages(parent.username)
                    .catch(handleError);
            }
        },

        //
        // Mutations
        //
        Mutation: {
            /**
             * signup for the service
             * 
             *(username: ID!, name: string!, email: string!, description:String, newPassword: string!): User!
             */
            registerUser: function (parent, args, context, info) {
                const spec = {
                    username: args.username,
                    name: args.name,
                    email: args.email,
                    password: args.newPassword,
                    description: args.description
                };
                return userController
                    .registerUser(spec)
                    .catch(handleError);
            },

            /**
             * #17 As user I want to update my account info
             * 
             *(version: String!, name: string!, email: string!): User!
             */
            updateMyUser: function (parent, args, context, info) {
                if (!context.user) {
                    throw new GraphQLError('You are not authorized to perform this action.', {
                        extensions: {
                            code: 'UNAUTHENTICATED',
                        },
                    });
                }
                const spec = {
                    username: context.user.username,
                    name: args.name,
                    email: args.email,
                    description: args.description
                };
                return userController
                    .updateUser(context.user.username, args.version, spec, true)
                    .catch(handleError);
            },
        }
    };
}