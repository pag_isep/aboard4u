/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:10 */

//
// User commands
//


//exceptions
import {
    ConflictError,
    ConcurrencyError,
    ForbiddenError,
    NotFoundError
} from "../../exceptions.js";

/**
 * A User command
 * 
 * helps to validate business rules, schema and directs the change of the underlying models
 * 
 */
class AbstractUserCommand {

    /**
     * @typedef PhotoData
     * 
     * @property {*} path 
     * @property {*} mimetype 
     * @property {*} caption 
     */

    /**
     * @typedef UserData
     * 
     * @property {string} username 
     * @property {string} name 
     * @property {string} email 
     * @property {string} [description] 
     * @property {PhotoData} [photo] 
     */

    /**
     * constructs a new User command
     * 
     * @param {UserData} data - the user data
     */
    constructor(data) {
        //TODO check Object.defineProperty() to avoid making these public
        this.username = data.username;
        this.name = data.name;
        this.email = data.email;
        this.description = data.description;
        this.photo = data.photo;
    }

    /**
     * ensures the User invariants are met
     * 
     * @returns a promise that rejects to RangeError or resolves to this specification
     */
    holdsInvariants() {
        if (![this.username, this.name, this.email].every(e => e)) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError('Missing mandatory parameter'));
        }
        //following Postel's law let's check if the photo object really has content. if not just continue
        if (this.photo && ![this.photo.path, this.photo.mimetype, this.photo.caption].every(e => !e)) {
            if (![this.photo.path, this.photo.mimetype, this.photo.caption].every(e => e)) {
                //TODO throw BadRequestError to allow adding a json response
                return Promise.reject(new RangeError("Missing mandatory photo property"));
            }
        }
        return Promise.resolve(this);
    }

    /**
     * Validates if the user is authorized to execute this command
     * 
     * @param {string} authzUser - currently authenticated user
     */
    authorized(authzUser) {
        // authorization rule: a user can only update her own account
        if (authzUser !== this.username) {
            return Promise.reject(new ForbiddenError("You can only update your profile."));
        }
        return Promise.resolve(this);
    }

    /**
     * Applies the operation abstracted in this class to the user object
     * 
     * @param {*} user - the underlying user model
     * @param {string} rev - the expected revision of the user object (for optimistic concurrency control)
     * @returns a promise that resolves to the user object after applying the operation
     */
    applyTo(user, rev) {
        if (!user) {
            return Promise.reject(new NotFoundError("Object not found"));
        }
        if (user.username !== this.username) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError("This specification is intended for a different object than the fetched one"));
        }

        // since we just get the user from data store migth as well validate the current revision.
        // the data store will validate it again in case other instance of the server has changed the 
        // underlying data store in the meantime
        if (user.__v.toString() !== rev.toString()) {
            return Promise.reject(new ConcurrencyError("Item was updated by other user", user));
        }
        return Promise.resolve(user);
    }

    /**
     * persist the modified object. by default, this function does nothing and just resolves to the modified object
     * 
     * @param {*} user 
     * @param {*} rev 
     * @returns a promise that resolves to the modified and persisted object
     */
    persist(user, rev) {
        return Promise.resolve(user);
    }

    /**
     * Prepares the command, e.g., ensuring invariants. 
     * 
     * @param {string} authzUser - currently authenticated user
     */
    prepare(authzUser) {
        if (!this.username) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError("Missing mandatory 'username' property"));
        }
        return Promise.resolve(this);
    }

    /**
     * Executes the command by:
     * 1. prepare it by calling the object's `prepare` method
     * 2. fetch the user by invoking the `fetchUser` parameter
     * 3. apply the command to the fetched user by invoking `applyTo` with the expected `rev`ision
     * 4. persist the modified object 
     * 
     * @param {function(command):Promise<UserModel>} fetchUser - a function that returns a promise that resolves to the user model to apply the command to
     * @param {string} rev - the expected revision of the underlying user object 
     * @param {string} authzUser - currently authenticated user
     * @param {function(user, rev):Promise<UserModel>} [persist]
     */
    execute(fetchUser, rev, authzUser, persist = e => e) {
        return this.prepare(authzUser)
            .then((spec) => fetchUser(spec))
            .then((user) => this.applyTo(user, rev))
            .then((user) => persist(user, rev));
    }
}

/**
 * The commmand to perform a full update
 */
export class ReplaceUserCommand extends AbstractUserCommand {

    /**
     * 
     * @param {UserData} data 
     */
    constructor(data) {
        super(data);
    }

    prepare(authzUser) {
        return super.prepare(authzUser)
            .then(() => super.holdsInvariants())
            .then(() => super.authorized(authzUser));
    }

    /**
     * applies a full update to an underlying model based on this specification
     * 
     * @param {*} user - the underlying user model 
     * @param {string} rev - the expected revision of the user object (for optimistic concurrency control)
     * @returns a promise that resolves to the user object after applying the operation
     */
    applyTo(user, rev) {
        return super.applyTo(user, rev)
            .then((applieduser) => {
                applieduser.name = this.name;
                applieduser.email = this.email;
                applieduser.description = this.description;
                if (this.photo) {
                    applieduser.photo = {
                        ...this.photo
                    };
                }
                if (!this.photo && applieduser.photo) {
                    applieduser.photo = null;
                    delete applieduser.photo;
                }

                return applieduser;
            });
    }
}

/**
 * The commmand to perform a partial update
 */
export class UpdateUserCommand extends AbstractUserCommand {

    /**
     * 
     * @param {UserData} data 
     */
    constructor(data) {
        super(data);
    }

    prepare(authzUser) {
        return super.prepare(authzUser)
            .then(() => super.authorized(authzUser));
    }

    /**
     * applies a partial update to an underlying model based on the content of this specification
     * 
     * @param {*} user - the underlying user model
     * @param {string} rev - the expected revision of the user object (for optimistic concurrency control)
     * @returns a promise that resolves to the user object after applying the operation
     */
    applyTo(user, rev) {
        return super.applyTo(user, rev)
            .then((applieduser) => {
                if (this.name) {
                    applieduser.name = this.name;
                }
                if (this.email) {
                    applieduser.email = this.email;
                }
                if (this.description) {
                    applieduser.description = this.description;
                }
                if (this.photo) {
                    if (!applieduser.photo) {
                        applieduser.photo = {};
                    }
                    if (this.photo.mimetype) {
                        applieduser.photo.mimetype = this.photo.mimetype;
                    }
                    if (this.photo.path) {
                        applieduser.photo.path = this.photo.path;
                    }
                    if (this.photo.caption) {
                        applieduser.photo.caption = this.photo.caption;
                    }
                }
                return applieduser;
            });
    }
}

/**
 * the command to perform user registration
 */
export class RegisterUserCommand extends AbstractUserCommand {

    constructor(spec) {
        super(spec);
    }

    prepare(authzUser) {
        return super.prepare(authzUser)
            .then(() => super.holdsInvariants());
    }

    /**
     * 
     * @param {*} user 
     * @returns this user specification to pass to UserRepository.addUser
     */
    applyTo(user) {
        if (user) {
            // this is a conflict error and not a concurrency issue
            return Promise.reject(new ConflictError(`Username already in use`), {
                field: "username",
                value: `${this.username}`
            });
        }

        return Promise.resolve(this);
    }
}

export class DeleteUserCommand extends AbstractUserCommand {
    /**
     * 
     * @param {UserData} data 
     */
    constructor(data) {
        super(data);
    }

    prepare(authzUser) {
        return super.prepare(authzUser)
            .then(() => super.authorized(authzUser));
    }
}