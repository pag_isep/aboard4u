/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 9 */
/*jshint mocha: true */
/*jshint expr: true */

"use strict";

import sinon from 'sinon';
import { expect} from 'chai';

import {PassThrough} from 'stream';

// test subject 
import  {UserController} from './user-controller.js';
let subject;

// dependency stubs
let test_doubles;

// exceptions
import {
    NotFoundError,
    ConflictError,
    ForbiddenError,
    ConcurrencyError,
    BadRequestError
} from  "../../exceptions.js";

//
// helpers
//
function createSubject(userRepo, authzController) {
    const dependencies = {
        UserRepository: function () {
            return userRepo;
        },
        AuthzController: function () {
            return authzController;
        }
    };
    const config = {};
    subject = new UserController(config, dependencies);
}

import UserRepositoryStubs from  '../../test/stubs/user-repository.js';

function setupWithBasicRepo() {
    setupSubjectAndRepo(UserRepositoryStubs.basicUserRepo());
}

function setupEmptyRepo(theNewUser) {
    setupSubjectAndRepo(UserRepositoryStubs.emptyUserRepo(theNewUser));
}

function setupRepoConflict(theUser, theNewUser, deleteResult, rejectOnNewAuthz) {
    setupSubjectAndRepo(UserRepositoryStubs.conflictUserRepo(theUser, theNewUser, deleteResult, rejectOnNewAuthz));
}

function setupRepoWithOneUser(theUser, theNewUser) {
    setupSubjectAndRepo(UserRepositoryStubs.oneUserRepo(theUser, theNewUser));
}

function setupSubjectAndRepo(repos) {
    test_doubles = repos;

    createSubject(repos.UserRepository, repos.AuthzRepository);
}

// input objects
import fs from 'fs';
const marysPhoto = JSON.parse(fs.readFileSync(new URL('../../test/fixtures/marysPhoto.json', import.meta.url)).toString());
const update_photo = JSON.parse(fs.readFileSync(new URL('../../test/fixtures/update_photo.json', import.meta.url)).toString());

// repo objects
const maryWithoutPhoto = JSON.parse(fs.readFileSync(new URL('../../test/fixtures/maryWithoutPhoto.json', import.meta.url)).toString());
const maryWithPhoto = JSON.parse(fs.readFileSync(new URL('../../test/fixtures/maryWithPhoto.json', import.meta.url)).toString());
const updatedMaryWithUpdatedPhoto = JSON.parse(fs.readFileSync(new URL('../../test/fixtures/updatedMaryWithUpdatedPhoto.json', import.meta.url)).toString());
const updatedMaryWithPhotoNoUpdateOnPhoto = JSON.parse(fs.readFileSync(new URL('../../test/fixtures/updatedMaryWithPhotoNoUpdateOnPhoto.json', import.meta.url)).toString());
const updatedMaryWhitoutPhoto = JSON.parse(fs.readFileSync(new URL('../../test/fixtures/updatedMaryWhitoutPhoto.json', import.meta.url)).toString());

//
// Test Suite
//
describe("User Controller", function () {
    //
    // UserController.getUser(id)
    //
    describe("Get User", function () {
        context("Mandatory parameters", function () {
            beforeEach(setupWithBasicRepo);

            it("should reject without mandatory username parameter", function () {
                return subject.getUser().should.eventually.be.rejectedWith(RangeError);
            });
        });

        context("Existing user", function () {
            beforeEach(() => setupRepoWithOneUser(maryWithoutPhoto));

            it("should resolve with existing user", async function () {
                await subject.getUser("Mary").should.become(maryWithoutPhoto);

                test_doubles.UserRepository.getUser.should.have.been.calledOnce;
                test_doubles.UserRepository.getUser.should.always.have.been.calledWith("Mary");
            });
        });

        context("Unexisting user", function () {
            beforeEach(setupWithBasicRepo);

            it("should reject with NotFound", async function () {
                await subject.getUser("unk").should.eventually.be.rejectedWith(NotFoundError);

                test_doubles.UserRepository.getUser.should.always.have.been.calledWith("unk");
            });
        });
    });

    //
    // UserController.searchUsers(name, email, start, n)
    //
    describe("Search Users", function () {
        beforeEach(setupWithBasicRepo);

        it("should return all without any parameter", async function () {
            let result = await subject.searchUsers();

            result.should.be.an("Array");

            test_doubles.UserRepository.searchUsers.should.have.been.calledOnce;
            test_doubles.UserRepository.searchUsers.should.have.been.calledWith(undefined, undefined);
        });

        it("should search by name", async function () {
            let result = await subject.searchUsers("mary");

            result.should.be.an("Array");

            test_doubles.UserRepository.searchUsers.should.have.been.calledOnce;
            test_doubles.UserRepository.searchUsers.should.have.been.calledWith("mary", undefined);
        });

        it("should search by email", async function () {
            let result = await subject.searchUsers(null, "mary@m.org");

            result.should.be.an("Array");

            test_doubles.UserRepository.searchUsers.should.have.been.calledOnce;
            test_doubles.UserRepository.searchUsers.should.have.been.calledWith(null, "mary@m.org");
        });

        it("should search by name and email", async function () {
            let result = await subject.searchUsers("mary", "mary@m.org");

            result.should.be.an("Array");

            test_doubles.UserRepository.searchUsers.should.have.been.calledOnce;
            test_doubles.UserRepository.searchUsers.should.have.been.calledWith("mary", "mary@m.org");
        });

        it("should limit the search to N results", async function () {
            let result = await subject.searchUsers("mary", "mary@m.org", undefined, 2);

            result.should.be.an("Array");

            test_doubles.UserRepository.searchUsers.should.have.been.calledOnce;
            test_doubles.UserRepository.searchUsers.should.have.been.calledWith("mary", "mary@m.org", 1, 2);
        });

        it("should start the search at X", async function () {
            let result = await subject.searchUsers("mary", "mary@m.org", 50);

            result.should.be.an("Array");

            test_doubles.UserRepository.searchUsers.should.have.been.calledOnce;
            test_doubles.UserRepository.searchUsers.should.have.been.calledWith("mary", "mary@m.org", 50);
        });
    });

    //
    // UserController.registerUser(newID, name, email, password, storedPhoto) 
    //
    describe("Register a User", function () {
        context("Mandatory parameters", function () {
            beforeEach(setupWithBasicRepo);

            it("should reject if username parameter is missing", function () {
                return subject.registerUser({
                    username: null,
                    name: "Mary Smith",
                    email: "m@m.org",
                    password: "123"
                }).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if name parameter is missing", function () {
                return subject.registerUser({
                    username: "Mary",
                    name: null,
                    email: "m@m.org",
                    password: "123"
                }).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if email parameter is missing", function () {
                return subject.registerUser({
                    username: "Mary",
                    name: "Mary Smith",
                    email: null,
                    password: "123"
                }).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if password parameter is missing", function () {
                return subject.registerUser({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org",
                    password: null
                }).should.eventually.be.rejectedWith(RangeError);
            });

            it("should resolve if optional storedPhoto is missing", function () {
                return subject.registerUser({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org",
                    password: "123"
                }).should.eventually.be.fulfilled;
            });

            it("should reject if storedPhoto does not have filename", function () {
                return subject.registerUser({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org",
                    password: "123",
                    photo: {
                        caption: "myself",
                        mimetype: "image/jpg"
                    }
                }).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if storedPhoto does not have caption", function () {
                return subject.registerUser({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org",
                    password: "123",
                    photo: {
                        path: "a.jpg",
                        type: "image/jpg"
                    }
                }).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if storedPhoto does not have type", function () {
                return subject.registerUser({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org",
                    password: "123",
                    photo: {
                        path: "a.jpg",
                        caption: "myself",
                    }
                }).should.eventually.be.rejectedWith(RangeError);
            });
        });

        context("Username already in use", function () {
            beforeEach(() => setupRepoWithOneUser(maryWithoutPhoto));

            it("should reject if username is not available", function () {
                return subject.registerUser({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org",
                    password: "123"
                }).should.eventually.be.rejectedWith(ConflictError);
            });
        });

        context("With photo upload", function () {
            beforeEach(() => setupEmptyRepo(maryWithPhoto));

            it("should register the user", async function () {
                let result = await subject.registerUser({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org",
                    password: "123",
                    photo: marysPhoto
                });

                result.should.eql(maryWithPhoto);

                test_doubles.UserRepository.addUser.should.have.been.calledOnce;
                /*                 test_doubles.UserRepository.addUser.should.always.be.calledWith({
                                    username: "Mary",
                                    name: "Mary Smith",
                                    email: "m@m.org",
                                    photo: marysPhoto
                                });
                 */
                test_doubles.AuthzRepository.addAuthz.should.have.been.calledOnce;
                test_doubles.AuthzRepository.addAuthz.should.always.be.calledWith("Mary", "123");
            });
        });

        context("Without photo upload", function () {
            beforeEach(() => setupEmptyRepo(maryWithoutPhoto));

            it("should register the user", async function () {
                let result = await subject.registerUser({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org",
                    password: "123"
                });

                result.should.eql(maryWithoutPhoto);

                test_doubles.UserRepository.addUser.should.have.been.calledOnce;
                /*                 test_doubles.UserRepository.addUser.should.always.be.calledWith({
                                    username: "Mary",
                                    name: "Mary Smith",
                                    email: "m@m.org"
                                });
                 */
                test_doubles.AuthzRepository.addAuthz.should.have.been.calledOnce;
                test_doubles.AuthzRepository.addAuthz.should.always.be.calledWith("Mary", "123");
            });
        });

        context("Error on UserRepository", function () {
            beforeEach(() => setupRepoConflict(null));

            it("should reject", async function () {
                try {
                    await subject.registerUser({
                        username: "Mary",
                        name: "Mary Smith",
                        email: "m@m.org",
                        password: "123"
                    });
                } catch (err) {
                    expect(err.name).to.eql("ConcurrencyError");
                }

                test_doubles.UserRepository.addUser.should.have.been.calledOnce;
            });
        });

        context("Error on AuthzRepository", function () {
            beforeEach(() => setupRepoConflict(null, maryWithoutPhoto, true, true));

            it("should reject", async function () {
                try {
                    await subject.registerUser({
                        username: "Mary",
                        name: "Mary Smith",
                        email: "m@m.org",
                        password: "123"
                    });
                } catch (err) {
                    expect(err.name).to.eql("ConcurrencyError");
                }

                test_doubles.UserRepository.addUser.should.have.been.calledOnce;
                test_doubles.UserRepository.deleteUser.should.have.been.calledOnce;
            });
        });
    });

    //
    // UserController.updateUser(user, username, rev, name, email, storedPhoto, [partial])
    //
    describe("Update a User", function () {
        context("Mandatory parameters", function () {
            beforeEach(setupWithBasicRepo);

            it("should reject if authzUser parameter is missing", function () {
                return subject.updateUser(null, 1, {
                    username: "Ann",
                    name: "Ann Taylor",
                    email: "a@a.org"
                }, true).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if rev parameter is missing", function () {
                return subject.updateUser("Ann", null, {
                    username: "Ann",
                    name: "Ann Taylor",
                    email: "a@a.org"
                }, true).should.eventually.be.rejectedWith(BadRequestError);
            });
        });

        context("Optional parameters", function () {
            beforeEach(() => setupRepoWithOneUser(maryWithoutPhoto));

            it("should resolve if optional partial parameter is missing", function () {
                return subject.updateUser("Mary", maryWithoutPhoto.__v, {
                    username: "Mary",
                    name: "Ann Taylor",
                    email: "a@a.org"
                }, null).should.eventually.be.fulfilled;
            });
        });

        context("Authorizations", function () {
            beforeEach(setupWithBasicRepo);

            it("should reject if the user tries to update another account", function () {
                return subject.updateUser("Mary", 1, {
                    username: "Ann",
                    name: "Ann Taylor",
                    email: "a@a.org"
                }, true).should.eventually.be.rejectedWith(ForbiddenError);
            });
        });

        context("Conflicts", function () {
            it("should reject if the revison is stale", function () {
                setupRepoWithOneUser(maryWithoutPhoto);

                return subject.updateUser("Mary", 2, {
                    username: "Mary",
                    name: "Ann Taylor",
                    email: "a@a.org"
                }, true).should.eventually.be.rejectedWith(ConcurrencyError);
            });

            it("should reject if the revison is stale in the repository layer", function () {
                setupRepoConflict(maryWithoutPhoto);

                return subject.updateUser("Mary", 1, {
                    username: "Mary",
                    name: "Ann Taylor",
                    email: "a@a.org"
                },
                    true).should.eventually.be.rejectedWith(ConcurrencyError);
            });
        });

        context("Unexisting user", function () {
            beforeEach(() => setupEmptyRepo(null));

            it("should reject if the user does not exist", function () {
                return subject.updateUser("Mary", 2, {
                    username: "Mary",
                    name: "Ann Taylor",
                    email: "a@a.org"
                }, true).should.eventually.be.rejectedWith(NotFoundError);
            });
        });

        context("Partial update", function () {
            context("Mandatory properties", function () {
                beforeEach(setupWithBasicRepo);

                it("should reject if username property is missing", function () {
                    return subject.updateUser("Ann", 1, {
                        username: null,
                        name: "Ann Taylor",
                        email: "a@a.org"
                    }, true).should.eventually.be.rejectedWith(RangeError);
                });
            });

            context("Partial updates", function () {
                //TODO partial updates of different fields!!!!!!!!! must resolve!!!!
            });

            context("Existing user has photo", function () {
                context("Updating without photo", function () {
                    beforeEach(() => setupRepoWithOneUser(maryWithPhoto, maryWithoutPhoto));

                    it("should update own account", async function () {
                        //wait to be fulfilled
                        await subject.updateUser("Mary", 1, {
                            username: "Mary",
                            name: "new name",
                            email: "new email",
                            photo: null
                        }, true);

                        test_doubles.UserRepository.saveUser.should.have.been.calledOnce;
                        test_doubles.UserRepository.saveUser.should.have.been.calledWith(updatedMaryWithPhotoNoUpdateOnPhoto);
                    });
                });

                context("Updating with photo", function () {
                    beforeEach(() => setupRepoWithOneUser(maryWithPhoto, maryWithPhoto));

                    it("should update own account", async function () {
                        //wait to be fulfilled
                        await subject.updateUser("Mary", 1, {
                            username: "Mary",
                            name: "new name",
                            email: "new email",
                            photo: update_photo
                        }, true);

                        test_doubles.UserRepository.saveUser.should.have.been.calledOnce;
                        test_doubles.UserRepository.saveUser.should.have.been.calledWith(updatedMaryWithUpdatedPhoto);
                    });
                });
            });

            context("Existing user doesn't have photo", function () {
                context("Updating with photo", function () {
                    beforeEach(() => setupRepoWithOneUser(maryWithoutPhoto, maryWithPhoto));

                    it("should update own account", async function () {
                        //wait to be fulfilled
                        await subject.updateUser("Mary", 1, {
                            username: "Mary",
                            name: "new name",
                            email: "new email",
                            photo: update_photo
                        }, true);

                        test_doubles.UserRepository.saveUser.should.have.been.calledOnce;
                        test_doubles.UserRepository.saveUser.should.have.been.calledWith(updatedMaryWithUpdatedPhoto);
                    });
                });

                context("Updating without photo", function () {
                    beforeEach(() => setupRepoWithOneUser(maryWithoutPhoto, maryWithoutPhoto));

                    it("should update own account", async function () {
                        //wait to be fulfilled
                        await subject.updateUser("Mary", 1, {
                            username: "Mary",
                            name: "new name",
                            email: "new email",
                            photo: null
                        }, true);

                        test_doubles.UserRepository.saveUser.should.have.been.calledOnce;
                        test_doubles.UserRepository.saveUser.should.have.been.calledWith(updatedMaryWhitoutPhoto);
                    });
                });
            });
        });

        context("Full update", function () {
            context("Mandatory properties", function () {
                beforeEach(setupWithBasicRepo);

                it("should reject if username property is missing", function () {
                    return subject.updateUser("Ann", 1, {
                        username: null,
                        name: "Ann Taylor",
                        email: "a@a.org"
                    }, false).should.eventually.be.rejectedWith(RangeError);
                });

                it("should reject if photo property is provided with missing caption", function () {
                    return subject.updateUser("Ann", 1, {
                        username: "Ann",
                        name: "Ann Taylor",
                        email: "a@a.org",
                        photo: {
                            path: "ann",
                            mimetype: "image/jpeg"
                        }
                    }, false).should.eventually.be.rejectedWith(RangeError);
                });

                it("should reject if photo property is provided with missing path", function () {
                    return subject.updateUser("Ann", 1, {
                        username: "Ann",
                        name: "Ann Taylor",
                        email: "a@a.org",
                        photo: {
                            caption: "myself",
                            mimetype: "image/jpeg"
                        }
                    }, false).should.eventually.be.rejectedWith(RangeError);
                });

                it("should reject if photo property is provided with missing mimetype", function () {
                    return subject.updateUser("Ann", 1, {
                        username: "Ann",
                        name: "Ann Taylor",
                        email: "a@a.org",
                        photo: {
                            caption: "myself",
                            path: "ann"
                        }
                    }, false).should.eventually.be.rejectedWith(RangeError);
                });

                it("should reject if name property is missing", function () {
                    return subject.updateUser("Ann", 1, {
                        username: "Ann",
                        name: null,
                        email: "a@a.org"
                    }, false).should.eventually.be.rejectedWith(RangeError);
                });

                it("should reject if email property is missing", function () {
                    return subject.updateUser("Ann", 1, {
                        username: "Ann",
                        name: "Ann Taylor",
                        email: null
                    }).should.eventually.be.rejectedWith(RangeError);
                }, false);
            });

            context("Existing user has photo", function () {
                context("Updating with photo", function () {
                    beforeEach(() => setupRepoWithOneUser(maryWithPhoto, maryWithPhoto));

                    it("should update own account", async function () {
                        //wait to be fulfilled
                        await subject.updateUser("Mary", 1, {
                            username: "Mary",
                            name: "new name",
                            email: "new email",
                            description: "Mary loves Javascript and is passionate about APIs",
                            photo: update_photo
                        });

                        test_doubles.UserRepository.saveUser.should.have.been.calledOnce;
                        test_doubles.UserRepository.saveUser.should.have.been.calledWith(updatedMaryWithUpdatedPhoto);
                    });
                });

                context("Updating without photo", function () {
                    beforeEach(() => setupRepoWithOneUser(maryWithPhoto, maryWithoutPhoto));

                    it("should update own account", async function () {
                        //wait to be fulfilled
                        await subject.updateUser("Mary", 1, {
                            username: "Mary",
                            name: "new name",
                            email: "new email",
                            description: "Mary loves Javascript and is passionate about APIs",
                            photo: null
                        });

                        test_doubles.UserRepository.saveUser.should.have.been.calledOnce;
                        test_doubles.UserRepository.saveUser.should.have.been.calledWith(updatedMaryWhitoutPhoto);
                    });
                });
            });

            context("Existing user doesn't have photo", function () {
                context("Updating with photo", function () {
                    beforeEach(() => setupRepoWithOneUser(maryWithoutPhoto, maryWithPhoto));

                    it("should update own account", async function () {
                        //wait to be fulfilled
                        await subject.updateUser("Mary", 1, {
                            username: "Mary",
                            name: "new name",
                            email: "new email",
                            description: "Mary loves Javascript and is passionate about APIs",
                            photo: update_photo
                        });

                        test_doubles.UserRepository.saveUser.should.have.been.calledOnce;
                        test_doubles.UserRepository.saveUser.should.have.been.calledWith(updatedMaryWithUpdatedPhoto);
                    });
                });

                context("Updating without photo", function () {
                    beforeEach(() => setupRepoWithOneUser(maryWithoutPhoto, maryWithoutPhoto));

                    it("should update own account", async function () {
                        //wait to be fulfilled
                        await subject.updateUser("Mary", 1, {
                            username: "Mary",
                            name: "new name",
                            email: "new email",
                            description: "Mary loves Javascript and is passionate about APIs",
                            photo: null
                        });

                        test_doubles.UserRepository.saveUser.should.have.been.calledOnce;
                        test_doubles.UserRepository.saveUser.should.have.been.calledWith(updatedMaryWhitoutPhoto);
                    });
                });
            });
        });
    });

    //
    // UserController.deleteUser(currentUser, username, rev)
    //
    describe("Delete a User", function () {
        context("Mandatory parameters", function () {
            beforeEach(setupWithBasicRepo);

            it("should reject without currentUser parameter", function () {
                return subject.deleteUser(null, "Mary", 1).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject without username parameter", function () {
                return subject.deleteUser("Mary", null, 1).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject without revision parameter", function () {
                return subject.deleteUser("Mary", "Mary").should.eventually.be.rejectedWith(BadRequestError);
            });
        });

        context("Authorizations", function () {
            beforeEach(setupWithBasicRepo);

            it("should reject deleting another user's account", function () {
                return subject.deleteUser("Mary", "Ann", 1).should.eventually.be.rejectedWith(ForbiddenError);
            });
        });

        context("Conflicts", function () {
            it("should reject if the revison is stale", function () {
                setupRepoConflict(maryWithoutPhoto);

                return subject.deleteUser("Mary", "Mary", 3).should.eventually.be.rejectedWith(ConcurrencyError);
            });

            it("should reject if the revison is stale in the repository layer", function () {
                setupRepoConflict(maryWithoutPhoto, null, false);

                return subject.deleteUser("Mary", "Mary", 1).should.eventually.be.rejectedWith(ConcurrencyError);
            });
        });

        context("Ok", function () {
            beforeEach(() => setupRepoWithOneUser(maryWithoutPhoto));

            it("should delete own account", async function () {
                let result = await subject.deleteUser("Mary", "Mary", 1);

                result.should.eql(true);

                test_doubles.UserRepository.deleteUser.should.have.been.calledWith("Mary", 1);
            });
        });
    });

    //
    // UserController.setPhotoOfUser(authzUser, userId, file)
    //
    describe("Set photo of a User", function () {
        context("Mandatory parameters", function () {
            beforeEach(setupWithBasicRepo);

            it("Should reject without mandatory authzUser parameter", function () {
                return subject.setPhotoOfUser(null, "Mary", marysPhoto).should.eventually.be.rejectedWith(RangeError);
            });

            it("Should reject without mandatory userId parameter", function () {
                return subject.setPhotoOfUser("Mary", null, marysPhoto).should.eventually.be.rejectedWith(RangeError);
            });

            it("Should reject without mandatory file parameter", function () {
                return subject.setPhotoOfUser("Mary", "Mary").should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if storedPhoto does not have filename", function () {
                return subject.setPhotoOfUser("Mary", "Mary", {
                    caption: "myself",
                    mimetype: "image/jpg"
                }).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if storedPhoto does not have caption", function () {
                return subject.setPhotoOfUser("Mary", "Mary", {
                    path: "a.jpg",
                    type: "image/jpg"
                }).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if storedPhoto does not have type", function () {
                return subject.setPhotoOfUser("Mary", "Mary", {
                    path: "a.jpg",
                    caption: "myself",
                }).should.eventually.be.rejectedWith(RangeError);
            });
        });

        context("Authorizations", function () {
            beforeEach(setupWithBasicRepo);

            it("Should reject updating other user's photo", function () {
                return subject.setPhotoOfUser("Ann", "Mary", marysPhoto).should.eventually.be.rejectedWith(ForbiddenError);
            });
        });

        context("User already has a photo", function () {
            beforeEach(() => setupRepoWithOneUser(maryWithPhoto, maryWithPhoto));

            it("Should update own photo", async function () {
                let result = await subject.setPhotoOfUser("Mary", "Mary", marysPhoto);

                result.should.have.property("created", false);
                result.should.have.property("data");
                result.data.should.be.eql(maryWithPhoto);
            });
        });

        context("User doesn't yet have a photo", function () {
            beforeEach(() => setupRepoWithOneUser(maryWithoutPhoto, maryWithPhoto));

            it("Should update own photo", async function () {
                let result = await subject.setPhotoOfUser("Mary", "Mary", marysPhoto);

                result.should.have.property("created", true);
                result.should.have.property("data");
                result.data.should.be.eql(maryWithPhoto);
            });
        });
    });

    //
    // UserController.deletePhoto(authzUser, username)
    //
    describe("Delete photo of a User", function () {
        context("Mandatory parameters", function () {
            beforeEach(setupWithBasicRepo);

            it("should reject if mandatory parameter authzUser is missing", function () {
                return subject.deletePhoto(null, "Mary").should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if mandatory parameter username is missing", function () {
                return subject.deletePhoto("Mary").should.eventually.be.rejectedWith(RangeError);
            });
        });

        context("Authorizations", function () {
            beforeEach(setupWithBasicRepo);

            it("Should reject delete other user's photo", function () {
                return subject.deletePhoto("Ann", "Mary").should.eventually.be.rejectedWith(ForbiddenError);
            });
        });

        context("User has photo", function () {
            beforeEach(() => setupRepoWithOneUser(maryWithPhoto, maryWithoutPhoto));

            it("Should delete own photo", function () {
                return subject.deletePhoto("Mary", "Mary").should.become(maryWithoutPhoto);
            });
        });

        context("User doesn't have photo", function () {
            beforeEach(() => setupRepoWithOneUser(maryWithoutPhoto, maryWithoutPhoto));

            it("Should do nothing but suceed anyway", function () {
                return subject.deletePhoto("Mary", "Mary").should.become(maryWithoutPhoto);
            });
        });
    });

    //
    // UserController.generatePDFOfUser(entry, basedir, outStream)
    //
    describe("Generate PDF of user", function () {
        context("Mandatory parameters", function () {
            beforeEach(setupWithBasicRepo);

            it("should reject if mandatory user parameter is missing", function () {
                (() => subject.generatePDFOfUser(null, "", {})).should.throw(RangeError);
            });

            it("should reject if mandatory basedir parameter is missing", function () {
                (() => subject.generatePDFOfUser({}, null, {})).should.throw(RangeError);
            });

            it("should reject if mandatory outstream parameter is missing", function () {
                (() => subject.generatePDFOfUser({}, "", null)).should.throw(RangeError);
            });
        });

        context("Ok", function () {
            let stubRes;

            beforeEach(function () {
                setupWithBasicRepo();
                stubRes = sinon.stub(new PassThrough());
            });

            it("should succeed if user has photo even if file is missing", async function () {
                let result = await subject.generatePDFOfUser(maryWithPhoto, "_fake_dir", stubRes);

                result.should.be.an("Object");
                expect(stubRes.write).to.have.been.called;
            });

            it("should succeed if user has photo and file exists", async function () {
                let result = await subject.generatePDFOfUser(maryWithPhoto, process.cwd() + "/stock-photos/", stubRes);

                result.should.be.an("Object");

                expect(stubRes.write).to.have.been.called;
            });

            it("should succeed if the photo property is an empty object", async function () {
                let u = {
                    maryWithoutPhoto,
                    photo: {}
                };
                let result = await subject.generatePDFOfUser(u, process.cwd() + "/stock-photos/", stubRes);

                result.should.be.an("Object");

                expect(stubRes.write).to.have.been.called;
            });

            it("should succeed if the photo property is null or undefined", async function () {
                let result = await subject.generatePDFOfUser(maryWithoutPhoto, process.cwd() + "/stock-photos/", stubRes);

                result.should.be.an("Object");

                expect(stubRes.write).to.have.been.called;
            });
        });
    });
});