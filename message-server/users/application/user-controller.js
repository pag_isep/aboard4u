/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 9 */

"use strict";

//
// User application layer
//
// all methods return a Promise
//

import debug from 'debug';
const LOGGER = debug('pag:mserver:app');

import PDFDocument from "pdfkit";

import {
    UpdateUserCommand,
    ReplaceUserCommand,
    RegisterUserCommand,
    DeleteUserCommand
} from "./user-command.js";

//
//exceptions
//
import {
    //ConflictError,
    ForbiddenError,
    ConcurrencyError,
    buildMissingIfMatch
} from "../../exceptions.js";

//
//dependencies
//
let userRepo;
let authzController;


// maximum number of results in searches
import {
    limitPagingResults
}
from '../../util/pagination-limits.js';

//
// helper functions
//

/**
 * 
 * @param {*} user - the user object
 * @param {*} basedir 
 * @param {*} outStream 
 */
function generatePDFOfUser(user, basedir, outStream) {
    if (![user, basedir, outStream].every(e => e)) {
        //TODO throw BadRequestError to allow adding a json response
        throw new RangeError("Missing mandatory parameters");
    }

    const doc = new PDFDocument();

    // stream the content to the http response
    doc.pipe(outStream);

    // document already has one page so let's use it

    //photo
    if (user.photo && user.photo.path) {
        try {
            doc.image(basedir + user.photo.path);
            doc.moveDown();
        } catch (err) {
            LOGGER("The photo file '%s' is missing for user %s in folder %s", user.photo.path, user.name, basedir);
        }
    }

    //name
    doc.fontSize(18);
    doc.fillColor('black').text(user.name);
    doc.moveDown();

    //email
    doc.fontSize(12);
    doc.fillColor('blue').text(user.email);
    doc.moveDown();

    // close document and response stream
    doc.end();

    return doc;
}

//
// users
//

/**
 * 
 * @param {*} username 
 * @returns a promise that resolves to the user or rejects if the user does not exist
 */
function getUser(username) {
    if (!username) {
        //TODO throw BadRequestError to allow adding a json response
        return Promise.reject(new RangeError('Missing parameters.'));
    }
    return userRepo.getUser(username);
}

/**
 * Search users by name or email. paginates results
 * @param {*} [name] 
 * @param {*} [email] 
 * @param {*} [start] - pagination index (inclusive)
 * @param {*} [n] - pagination length
 */
function searchUsers(name, email, start, n) {
    const paging = limitPagingResults(start, n);
    return userRepo.searchUsers(name, email, paging.start, paging.n);
}

/**
 * 
 * @param {*} newUserSpec
 */
function registerUser(newUserSpec) {
    if (![newUserSpec, newUserSpec.password].every(e => e)) {
        //TODO throw BadRequestError to allow adding a json response
        return Promise.reject(new RangeError('Missing mandatory parameters.'));
    }
    const cmd = new RegisterUserCommand(newUserSpec);
    return cmd.execute((spec) => userRepo.userExists(spec.username), null, null, userRepo.addUser)
        .then(function (newUser) {
            //TODO decouple - we should use an event to decouple user from authz
            return authzController.addAuthz(newUserSpec.username, newUserSpec.password)
                .then(() => newUser)
                .catch(function (err) {
                    // try to remove the user
                    return userRepo.deleteUser(newUserSpec.username, newUser.__v)
                        .then(() => Promise.reject(err));
                });
        });
}

/**
 * Performs full or partial update of the user.
 * 
 * in case of partial updates, only the fields mentioned in the updateSpec will be changed.
 * for full updates, if a field is not present in the updateSpec it will be nulled in the model 
 * (unless it is a mandatory field, in which case a RangeError will be throw/rejected)
 * 
 * @param {string} authzUser - currently authenticated user
 * @param {string} rev - expected version of the model (for optimistic concurrency control) 
 * @param {*} updateSpec - the user data
 * @param {boolean} [partial] - default:false; false to indicate full replacement (PUT semantics)
 */
function updateUser(authzUser, rev, updateSpec, partial = false) {
    if (!rev) {
        return Promise.reject(buildMissingIfMatch());
    }
    if (![authzUser, updateSpec].every(e => e)) {
        //TODO throw BadRequestError to allow adding a json response
        return Promise.reject(new RangeError('Missing mandatory parameter'));
    }
    let cmd;
    if (partial) {
        cmd = new UpdateUserCommand(updateSpec);
    } else {
        cmd = new ReplaceUserCommand(updateSpec);
    }

    return cmd.execute((spec) => userRepo.getUser(spec.username), rev, authzUser, userRepo.saveUser);
}

/**
 * deletes a user account
 * 
 * @param {String} currentUser  - the currently authenticated user
 * @param {String} username     - the username of the user to delete
 * @param {String} rev          - the current revision of the client
 */
function deleteUser(currentUser, username, rev) {
    if (!rev) {
        return Promise.reject(buildMissingIfMatch());
    }
    if (![currentUser, username].every(e => e)) {
        //TODO throw BadRequestError to allow adding a json response
        return Promise.reject(new RangeError('Missing parameters.'));
    }
    const cmd = new DeleteUserCommand({
        username
    });
    return cmd.execute((spec) => userRepo.getUser(spec.username), rev, currentUser)
        .then(async function (user) {
            const deleted = await userRepo.deleteUser(username, rev);

            if (!deleted) {
                return Promise.reject(new ConcurrencyError("Item has been updated by other user already. Please refresh and retry.", user));
            }
            return deleted;
        })
        //TODO decouple - we should use an event to decouple user from authz
        .then((deleted) => authzController.deleteAuthz(username))
        //TODO decouple - we should use an event to decouple user from message
        //TODO delete user messages
        .then(e => e);
}

/**
 * TODO shouldn't we check the current revision of the user?
 * 
 * @param {*} authzUser 
 * @param {*} userId 
 * @param {*} photoFile - { caption, path, mimetype }  
 * @returns a promise that resolves to an object with {created, data}
 */
function setPhotoOfUser(authzUser, userId, photoFile) {
    if (![authzUser, userId, photoFile].every(e => e)) {
        //TODO throw BadRequestError to allow adding a json response
        return Promise.reject(new RangeError('Missing mandatory parameters.'));
    }
    if (![photoFile.caption, photoFile.mimetype, photoFile.path].every(e => e)) {
        //TODO throw BadRequestError to allow adding a json response
        return Promise.reject(new RangeError('Missing mandatory photo parameters.'));
    }

    // authorization rule: a user can only set her own photo
    if (authzUser !== userId) {
        return Promise.reject(new ForbiddenError("you can only update your own photo"));
    }

    return userRepo.getUser(userId)
        .then(function (user) {
            const created = !user.photo;
            user.photo = {
                ...photoFile
            };
            return userRepo.saveUser(user)
                .then(function (savedUser) {
                    return {
                        created,
                        data: savedUser
                    };
                });
        });
}

/**
 * 
 * TODO shouldn't we check the current revision?
 * 
 * @param {*} authzUser 
 * @param {*} username 
 * @returns a promise that resolves to the user object
 */
function deletePhoto(authzUser, username) {
    if (![authzUser, username].every(e => e)) {
        //TODO throw BadRequestError to allow adding a json response
        return Promise.reject(new RangeError('Missing parameters.'));
    }

    // authorization rule: a user can only delete her own photo
    if (authzUser !== username) {
        return Promise.reject(new ForbiddenError("you can only delete your own photo"));
    }

    return userRepo.getUser(username)
        .then(function (u) {
            if (u.photo) {
                u.photo = null;

                return userRepo.saveUser(u);
            } else {
                //nothing to do
                return u;
            }
        });
}

/////////////////////////////
// MODULE EXPORTS

export function UserController(config, dependencies) {

    //TODO decouple
    authzController = new dependencies.AuthzController(config, dependencies);
    userRepo = new dependencies.UserRepository();

    return {
        registerUser,
        updateUser,
        getUser,
        deleteUser,

        searchUsers,

        setPhotoOfUser,
        deletePhoto,
        generatePDFOfUser
    };
}