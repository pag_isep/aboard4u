/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 9 */
/*jshint mocha: true */
/*jshint expr: true */

"use strict";


// test subject 
import {
    UpdateUserCommand,
    //ReplaceUserCommand,
    RegisterUserCommand,
    DeleteUserCommand
} from "./user-command.js";

// dependency stubs
//var test_doubles;

// exceptions
import {
    NotFoundError,
    ConflictError,
    ForbiddenError,
    ConcurrencyError
} from "../../exceptions.js";

//
// helpers
//

// input objects
//const marysPhoto = require(process.cwd() + '/message-server/test/fixtures/marysPhoto');
//const update_photo = require(process.cwd() + '/message-server/test/fixtures/update_photo');

// repo objects
import fs from 'fs';
const maryWithoutPhoto = JSON.parse(fs.readFileSync(new URL('../../test/fixtures/maryWithoutPhoto.json', import.meta.url)).toString());
const maryWithPhoto = JSON.parse(fs.readFileSync(new URL('../../test/fixtures/maryWithPhoto.json', import.meta.url)).toString());
//const updatedMaryWithUpdatedPhoto = require(process.cwd() + '/message-server/test/fixtures/updatedMaryWithUpdatedPhoto');
//const updatedMaryWithPhotoNoUpdateOnPhoto = require(process.cwd() + '/message-server/test/fixtures/updatedMaryWithPhotoNoUpdateOnPhoto');
//const updatedMaryWhitoutPhoto = require(process.cwd() + '/message-server/test/fixtures/updatedMaryWhitoutPhoto');

//
// Test Suite
//
describe("User Commands", function () {
    //
    // registerUser
    //
    describe("Register a User", function () {
        context("Mandatory parameters", function () {
            it("should reject if username parameter is missing", function () {
                const subject = new RegisterUserCommand({
                    username: null,
                    name: "Mary Smith",
                    email: "m@m.org"
                });
                return subject.execute(null, null, null).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if name parameter is missing", function () {
                const subject = new RegisterUserCommand({
                    username: "Mary",
                    name: null,
                    email: "m@m.org"
                });
                return subject.execute().should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if email parameter is missing", function () {
                const subject = new RegisterUserCommand({
                    username: "Mary",
                    name: "Mary Smith",
                    email: null
                });
                return subject.execute().should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if storedPhoto does not have filename", function () {
                const subject = new RegisterUserCommand({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org",
                    photo: {
                        caption: "myself",
                        mimetype: "image/jpg"
                    }
                });
                return subject.execute().should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if storedPhoto does not have caption", function () {
                const subject = new RegisterUserCommand({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org",
                    photo: {
                        path: "a.jpg",
                        mimetype: "image/jpg"
                    }
                });
                return subject.execute().should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject if storedPhoto does not have type", function () {
                const subject = new RegisterUserCommand({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org",
                    photo: {
                        path: "a.jpg",
                        caption: "myself",
                    }
                });
                return subject.execute().should.eventually.be.rejectedWith(RangeError);
            });
        });

        context("Username already in use", function () {
            it("should reject if username is not available", function () {
                const subject = new RegisterUserCommand({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org"
                });
                const mary = {
                    username: "Mary"
                };
                return subject.execute(() => mary).should.eventually.be.rejectedWith(ConflictError);
            });
        });

        context("Ok", function () {
            it("should resolve without photo", function () {
                const subject = new RegisterUserCommand({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org"
                });
                return subject.execute((e) => null).should.eventually.be.fulfilled;
            });

            it("should resolve with empty photo", function () {
                const subject = new RegisterUserCommand({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org",
                    photo: {

                    }
                });
                return subject.execute((e) => null).should.eventually.be.fulfilled;
            });

            it("should resolve with photo", function () {
                const subject = new RegisterUserCommand({
                    username: "Mary",
                    name: "Mary Smith",
                    email: "m@m.org",
                    photo: {
                        caption: "myself",
                        path: "abc",
                        mimetype: "image/jpeg"
                    }
                });
                return subject.execute((e) => null).should.eventually.be.fulfilled;
            });
        });
    });

    //
    // updateUser
    //
    describe("Update a User", function () {
        context("Mandatory parameters", function () {
            it("should reject if authzUser parameter is missing");

            it("should reject if rev parameter is missing");

            it("Should reject if the specification and fetched user does not match", function () {
                const subject = new UpdateUserCommand({
                    username: "Ann"
                });
                return subject.execute(() => maryWithoutPhoto, 1, "Ann").should.eventually.be.rejectedWith(RangeError);
            });
        });

        context("Authorizations", function () {
            it("should reject if the user tries to update another account", function () {
                const subject = new UpdateUserCommand({
                    username: "Ann"
                });
                return subject.execute(null, 1, "Mary").should.eventually.be.rejectedWith(ForbiddenError);
            });
        });

        context("Conflicts", function () {
            it("should reject if the revison is stale", function () {
                const subject = new UpdateUserCommand({
                    username: "Mary"
                });
                return subject.execute(() => maryWithoutPhoto, 2, "Mary").should.eventually.be.rejectedWith(ConcurrencyError);
            });
        });

        context("Unexisting user", function () {
            it("should reject if the user does not exist", function () {
                const subject = new UpdateUserCommand({
                    username: "Mary"
                });
                return subject.execute(() => null, 1, "Mary").should.eventually.be.rejectedWith(NotFoundError);
            });
        });

        context("Ok", function () {
            it("Should update name", async function () {
                const subject = new UpdateUserCommand({
                    username: "Mary",
                    name: "Ann Taylor",
                });
                const mary = {
                    ...maryWithPhoto
                };

                let result = await subject.execute(() => mary, 1, "Mary");

                result.name.should.eql("Ann Taylor");
                result.email.should.eql(maryWithPhoto.email);
                result.description.should.eql(maryWithPhoto.description);
                result.photo.should.eql(maryWithPhoto.photo);
            });

            it("Should update email", async function () {
                const subject = new UpdateUserCommand({
                    username: "Mary",
                    email: "Ann Taylor",
                });
                const mary = {
                    ...maryWithPhoto
                };

                let result = await subject.execute(() => mary, 1, "Mary");

                result.name.should.eql(maryWithPhoto.name);
                result.email.should.eql("Ann Taylor");
                result.description.should.eql(maryWithPhoto.description);
                result.photo.should.eql(maryWithPhoto.photo);
            });

            it("Should update description", async function () {
                const subject = new UpdateUserCommand({
                    username: "Mary",
                    description: "Ann Taylor",
                });
                const mary = {
                    ...maryWithPhoto
                };

                let result = await subject.execute(() => mary, 1, "Mary");

                result.name.should.eql(maryWithPhoto.name);
                result.email.should.eql(maryWithPhoto.email);
                result.description.should.eql("Ann Taylor");
                result.photo.should.eql(maryWithPhoto.photo);
            });

            it("Should set description", async function () {
                const subject = new UpdateUserCommand({
                    username: "Mary",
                    description: "Ann Taylor",
                });
                const mary = {
                    ...maryWithPhoto
                };
                delete mary.description;

                let result = await subject.execute(() => mary, 1, "Mary");

                result.name.should.eql(maryWithPhoto.name);
                result.email.should.eql(maryWithPhoto.email);
                result.description.should.eql("Ann Taylor");
                result.photo.should.eql(maryWithPhoto.photo);
            });

            it("Should update caption", async function () {
                const subject = new UpdateUserCommand({
                    username: "Mary",
                    photo: {
                        caption: "Ann Taylor"
                    }
                });
                const mary = {
                    ...maryWithoutPhoto
                };

                let result = await subject.execute(() => mary, 1, "Mary");

                result.name.should.eql(maryWithoutPhoto.name);
                result.email.should.eql(maryWithoutPhoto.email);
                result.description.should.eql(maryWithoutPhoto.description);
                result.photo.caption.should.eql("Ann Taylor");
            });
        });
    });

    //
    // replaceUser
    //
    describe("Overwrite a User", function () {
        /*
                                    context("Full update", function () {
                                        context("Mandatory properties", function () {
                                            beforeEach(setupWithBasicRepo);

                                            it("should reject if username property is missing", function () {
                                                return subject.updateUser("Ann", 1, {
                                                    username: null,
                                                    name: "Ann Taylor",
                                                    email: "a@a.org"
                                                }, false).should.eventually.be.rejectedWith(RangeError);
                                            });

                                            it("should reject if photo property is provided with missing caption", function () {
                                                return subject.updateUser("Ann", 1, {
                                                    username: "Ann",
                                                    name: "Ann Taylor",
                                                    email: "a@a.org",
                                                    photo: {
                                                        path: "ann",
                                                        mimetype: "image/jpeg"
                                                    }
                                                }, false).should.eventually.be.rejectedWith(RangeError);
                                            });

                                            it("should reject if photo property is provided with missing path", function () {
                                                return subject.updateUser("Ann", 1, {
                                                    username: "Ann",
                                                    name: "Ann Taylor",
                                                    email: "a@a.org",
                                                    photo: {
                                                        caption: "myself",
                                                        mimetype: "image/jpeg"
                                                    }
                                                }, false).should.eventually.be.rejectedWith(RangeError);
                                            });

                                            it("should reject if photo property is provided with missing mimetype", function () {
                                                return subject.updateUser("Ann", 1, {
                                                    username: "Ann",
                                                    name: "Ann Taylor",
                                                    email: "a@a.org",
                                                    photo: {
                                                        caption: "myself",
                                                        path: "ann"
                                                    }
                                                }, false).should.eventually.be.rejectedWith(RangeError);
                                            });

                                            it("should reject if name property is missing", function () {
                                                return subject.updateUser("Ann", 1, {
                                                    username: "Ann",
                                                    name: null,
                                                    email: "a@a.org"
                                                }, false).should.eventually.be.rejectedWith(RangeError);
                                            });

                                            it("should reject if email property is missing", function () {
                                                return subject.updateUser("Ann", 1, {
                                                    username: "Ann",
                                                    name: "Ann Taylor",
                                                    email: null
                                                }).should.eventually.be.rejectedWith(RangeError);
                                            }, false);
                                        });

                                        context("Existing user has photo", function () {
                                            context("Updating with photo", function () {
                                                beforeEach(() => setupRepoWithOneUser(maryWithPhoto, maryWithPhoto));

                                                it("should update own account", async function () {
                                                    //wait to be fulfilled
                                                    await subject.updateUser("Mary", 1, {
                                                        username: "Mary",
                                                        name: "new name",
                                                        email: "new email",
                                                        description: "Mary loves Javascript and is passionate about APIs",
                                                        photo: update_photo
                                                    });

                                                    test_doubles.UserRepository.saveUser.should.have.been.calledOnce;
                                                    test_doubles.UserRepository.saveUser.should.have.been.calledWith(updatedMaryWithUpdatedPhoto);
                                                });
                                            });

                                            context("Updating without photo", function () {
                                                beforeEach(() => setupRepoWithOneUser(maryWithPhoto, maryWithoutPhoto));

                                                it("should update own account", async function () {
                                                    //wait to be fulfilled
                                                    await subject.updateUser("Mary", 1, {
                                                        username: "Mary",
                                                        name: "new name",
                                                        email: "new email",
                                                        description: "Mary loves Javascript and is passionate about APIs",
                                                        photo: null
                                                    });

                                                    test_doubles.UserRepository.saveUser.should.have.been.calledOnce;
                                                    test_doubles.UserRepository.saveUser.should.have.been.calledWith(updatedMaryWhitoutPhoto);
                                                });
                                            });
                                        });

                                        context("Existing user doesn't have photo", function () {
                                            context("Updating with photo", function () {
                                                beforeEach(() => setupRepoWithOneUser(maryWithoutPhoto, maryWithPhoto));

                                                it("should update own account", async function () {
                                                    //wait to be fulfilled
                                                    await subject.updateUser("Mary", 1, {
                                                        username: "Mary",
                                                        name: "new name",
                                                        email: "new email",
                                                        description: "Mary loves Javascript and is passionate about APIs",
                                                        photo: update_photo
                                                    });

                                                    test_doubles.UserRepository.saveUser.should.have.been.calledOnce;
                                                    test_doubles.UserRepository.saveUser.should.have.been.calledWith(updatedMaryWithUpdatedPhoto);
                                                });
                                            });

                                            context("Updating without photo", function () {
                                                beforeEach(() => setupRepoWithOneUser(maryWithoutPhoto, maryWithoutPhoto));

                                                it("should update own account", async function () {
                                                    //wait to be fulfilled
                                                    await subject.updateUser("Mary", 1, {
                                                        username: "Mary",
                                                        name: "new name",
                                                        email: "new email",
                                                        description: "Mary loves Javascript and is passionate about APIs",
                                                        photo: null
                                                    });

                                                    test_doubles.UserRepository.saveUser.should.have.been.calledOnce;
                                                    test_doubles.UserRepository.saveUser.should.have.been.calledWith(updatedMaryWhitoutPhoto);
                                                });
                                            });
                                        });
                                    });
                                    */
    });

    //
    // deleteUser
    //
    describe("Delete a User", function () {
        context("Mandatory parameters", function () {
            it("should reject if username parameter is missing", function () {
                const subject = new DeleteUserCommand({
                    username: null,
                    name: "Mary Smith",
                    email: "m@m.org"
                });
                return subject.execute(null, null, null).should.eventually.be.rejectedWith(RangeError);
            });

            /*
                    it("should reject without currentUser parameter", function () {
                        return subject.deleteUser(null, "Mary", 1).should.eventually.be.rejectedWith(RangeError);
                    });

                    it("should reject without username parameter", function () {
                        return subject.deleteUser("Mary", null, 1).should.eventually.be.rejectedWith(RangeError);
                    });

                    it("should reject without revision parameter", function () {
                        return subject.deleteUser("Mary", "Mary").should.eventually.be.rejectedWith(RangeError);
                    });
                    */
        });
        /*
                context("Authorizations", function () {
                    beforeEach(setupWithBasicRepo);

                    it("should reject deleting another user's account", function () {
                        return subject.deleteUser("Mary", "Ann", 1).should.eventually.be.rejectedWith(ForbiddenError);
                    });
                });

                context("Conflicts", function () {
                    it("should reject if the revison is stale", function () {
                        setupRepoConflict(maryWithoutPhoto);

                        return subject.deleteUser("Mary", "Mary", 3).should.eventually.be.rejectedWith(ConcurrencyError);
                    });
                });

                context("Ok", function () {
                    beforeEach(() => setupRepoWithOneUser(maryWithoutPhoto));

                    it("should delete own account", async function () {
                        let result = await subject.deleteUser("Mary", "Mary", 1);

                        result.should.eql(true);

                        test_doubles.UserRepository.deleteUser.should.have.been.calledWith("Mary", 1);
                    });
                });
                */
    });
});