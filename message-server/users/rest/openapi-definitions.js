/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// a documentation file to hold the common definitions of models used in the API
//

//
// schemas
//

/**
 * @swagger
 * components:
 *   schemas:
 *     attrUserUsername:
 *       type: string
 *       maxLength: 32
 *       pattern: '^[a-zA-Z]+[\-\w]*$'
 *       description: the user's username
 *       example: Mary
 *     attrUserName:
 *       type: string
 *       maxLength: 50
 *       pattern: '^[A-Z]+[a-zA-Z\- ]*$'
 *       example: Mary Ann Smith
 *     attrUserDescription:
 *       type: string
 *       maxLength: 500
 *       pattern: '^[ \w\.!?,;:-=></&%$\+\*~#\(\)]*$'
 *       description: a short note about the user
 *       example: Mary loves Javascript and his passionate about APIs
 *     attrUserRoles:
 *       type: array
 *       maxItems: 100
 *       items:
 *         type: string
 *         maxLength: 50
 *         pattern: '^[\w\-]+$'
 *         example: author
 *     attrUserPhotoDisplayImage:
 *       type: string
 *       format: binary
 *       description: user's photo
 *     attrUserPhotoCaption:
 *       description: the photo description
 *       type: string
 *       maxLength: 50
 *       pattern: '^[ \w\.!?,;:-=></&%\$\+\*~#\(\)]*$'
 *       example: young Mary
 *     userData:
 *       description: a user
 *       type: object
 *       additionalProperties: false
 *       required:
 *         - username
 *         - name
 *         - email
 *         - createdOn
 *       properties:
 *         username:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         name:
 *           $ref: '#/components/schemas/attrUserName'
 *         email:
 *           $ref: '#/components/schemas/email'
 *         description:
 *           $ref: '#/components/schemas/attrUserDescription'
 *         photo:
 *           type: object
 *           description: user's avatar
 *           required:
 *             - caption
 *             - mimetype
 *             - image
 *           properties:
 *             caption:
 *               $ref: '#/components/schemas/attrUserPhotoCaption'
 *             mimetype:
 *               type: string
 *             image:
 *               description: the URL of the user's avatar imageThis property is beeing deprecated and will be removed in future versions. Clients should follow the link relation 'https://schema.org/image'.
 *               type: string
 *               format: uri
 *         roles:
 *           $ref: '#/components/schemas/attrUserRoles'
 *         createdOn:
 *           type: string
 *           format: date-time
 *         updatedOn:
 *           type: string
 *           format: date-time
 *     userDataCollection:
 *       description: collection of users
 *       type: array
 *       maxItems: 500
 *       items:
 *         $ref: '#/components/schemas/userData'
 *     userItem:
 *       description: a user
 *       type: object
 *       additionalProperties: false
 *       properties:
 *         data:
 *           $ref: '#/components/schemas/userData'
 *         links:
 *           $ref: '#/components/schemas/links'
 *     userCollection:
 *       description: collection of user items
 *       type: object
 *       additionalProperties: false
 *       properties:
 *         data:
 *           type: array
 *           maxItems: 100
 *           items:
 *             $ref: '#/components/schemas/userItem'
 *         links:
 *           $ref: '#/components/schemas/links'
 *         template:
 *           $ref: '#/components/schemas/template'
 *         queries:
 *           $ref: '#/components/schemas/queries'
 *     newUserForm:
 *       description: the user form input
 *       additionalProperties: false
 *       type: object
 *       required:
 *         - userName
 *         - password
 *         - name
 *         - email
 *       properties:
 *         userName:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         password:
 *           $ref: '#/components/schemas/attrAuthzPassword'
 *         name:
 *           $ref: '#/components/schemas/attrUserName'
 *         description:
 *           $ref: '#/components/schemas/attrUserDescription'
 *         email:
 *           $ref: '#/components/schemas/email'
 *         displayImage:
 *           $ref: '#/components/schemas/attrUserPhotoDisplayImage'
 *         caption:
 *           $ref: '#/components/schemas/attrUserPhotoCaption'
 *     editUserForm:
 *       description: the user form input
 *       type: object
 *       additionalProperties: false
 *       properties:
 *         __etag:
 *           $ref: '#/components/schemas/attrETag'
 *         name:
 *           $ref: '#/components/schemas/attrUserName'
 *         email:
 *           $ref: '#/components/schemas/email'
 *         description:
 *           $ref: '#/components/schemas/attrUserDescription'
 *         displayImage:
 *           $ref: '#/components/schemas/attrUserPhotoDisplayImage'
 *         caption:
 *           $ref: '#/components/schemas/attrUserPhotoCaption'
 *     editUserPhotoForm:
 *       description: the user form input
 *       type: object
 *       additionalProperties: false
 *       required:
 *         - displayImage
 *         - caption
 *       properties:
 *         displayImage:
 *           $ref: '#/components/schemas/attrUserPhotoDisplayImage'
 *         caption:
 *           $ref: '#/components/schemas/attrUserPhotoCaption'
 *     editUserDataInput:
 *       description: a user
 *       type: object
 *       additionalProperties: false
 *       properties:
 *         name:
 *           $ref: '#/components/schemas/attrUserName'
 *         email:
 *           $ref: '#/components/schemas/email'
 *         description:
 *           $ref: '#/components/schemas/attrUserDescription'
 *         photo:
 *           type: object
 *           additionalProperties: false
 *           description: user's avatar
 *           properties:
 *             caption:
 *               $ref: '#/components/schemas/attrUserPhotoCaption'
 *
 */


/**
 * @swagger
 * components:
 *   responses:
 *     OkUserJson:
 *       description: Ok
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/userData'
 *     OkUserCollection:
 *       description: the user collection
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/userDataCollection'
 *         application/vnd.pagsousa.hyper+json:
 *           schema:
 *             $ref: '#/components/schemas/userCollection'
 *     CreatedUserItem:
 *       description: Created
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/userData'
 *         application/vnd.pagsousa.hyper+json:
 *           schema:
 *             $ref: '#/components/schemas/userItem'
 *     OkUserItem:
 *       description: Ok
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/userData'
 *         application/vnd.pagsousa.hyper+json:
 *           schema:
 *             $ref: '#/components/schemas/userItem'
 *     PreconditionFailedUserItem:
 *       description: if-match precondition failed
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/userData'
 *         application/vnd.pagsousa.hyper+json:
 *           schema:
 *             $ref: '#/components/schemas/userItem'
 */
