/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 11 */

"use strict";


//
// user resource and the photo subresource
//

import debug from "debug";
const LOGGER = debug("pag:mserver");

import _ from 'lodash';

// globals
let userController;
let imageStore;
let SETTINGS;
let Response;
let Hyper;

//
// hypermedia controls
//

/**
 * 
 * @param {*} user - the user resource representation 
 * @param {*} authUser - the aythenticated user of the request
 */
function itemLinks(user, authUser) {
    const links = [];
    if (!user) {
        return links;
    }

    // standard IANA rel type
    links.push({
        rel: "self",
        href: SETTINGS.server.rest.API_ROOT + "/user/" + user.username
    });

    // assumes the client of the application/vnd.pagsousa.hyper+json only needs the data schema
    // as everything else is hypermedia
    links.push({
        rel: "describedBy",
        href: SETTINGS.server.SERVER_ROOT + "/schemas/user/userData.schema.json"
    });

    // #41 : edit links are only sent for authenticated requests
    if (authUser && authUser.username === user.username) {
        links.push({
            rel: "edit",
            href: SETTINGS.server.rest.API_ROOT + "/user/" + user.username
        });

        // edit-form (from RFC 6861 https://tools.ietf.org/html/rfc6861)
        links.push({
            rel: 'edit-form',
            href: SETTINGS.server.SERVER_ROOT + "/forms/edit-user.html?id=" + encodeURIComponent(SETTINGS.server.rest.API_ROOT + "/user/" + user.username)
        });
    }

    links.push({
        rel: "collection",
        href: SETTINGS.server.rest.API_ROOT + "/user"
    });

    links.push({
        rel: "alternate",
        href: SETTINGS.server.rest.API_ROOT + "/user/" + user.username + "/pdf"
    });

    if (user.photo) {
        links.push({
            rel: "https://schema.org/image",
            href: SETTINGS.server.rest.API_ROOT + "/user/" + user.username + "/photo",
            type: user.photo.mimetype,
            title: user.photo.caption
        });
    }

    // application rel type
    links.push({
        rel: "https://aboard4u.herokuapp.com/rel/messages",
        href: SETTINGS.server.rest.API_ROOT + "user/" + user.username + "/message"
    });

    return links;
}

/**
 * 
 * @param {*} user 
 * @param {*} authUser 
 */
function photoLinks(user, authUser) {
    const links = [];
    if (!user) {
        return links;
    }

    // standard IANA rel type
    links.push({
        rel: "self",
        href: SETTINGS.server.rest.API_ROOT + "/user/" + user.username + "/photo"
    });

    // #41 : edit links are only sent for authenticated requests
    if (authUser) {
        links.push({
            rel: "edit",
            href: SETTINGS.server.rest.API_ROOT + "/user/" + user.username + "/photo"
        });
    }

    links.push({
        rel: "related",
        href: SETTINGS.server.rest.API_ROOT + "/user/" + user.username
    });

    return links;
}

/**
 * 
 * @param {*} start 
 * @param {*} n 
 * @param {*} isLast 
 * @param {*} authUser 
 */
function collectionLinks(start, n, isLast, authUser) {
    // collection links
    const links = [];

    links.push({
        rel: "self",
        href: SETTINGS.server.rest.API_ROOT + "/user"
    });

    links.push({
        rel: "start", // TODO check 'start' vs 'first' vs ...
        href: SETTINGS.server.rest.API_ROOT + "/user"
    });

    //pagination
    if (start > 1) {
        links.push({
            rel: "prev",
            href: SETTINGS.server.rest.API_ROOT + "/user?start=" + (+start - n) + "&n=" + n
        });
    }

    if (!isLast) {
        links.push({
            rel: "next",
            href: SETTINGS.server.rest.API_ROOT + "/user?start=" + (+start + n) + "&n=" + n
        });
    }

    // #41 : edit links are only sent for authenticated requests
    if (authUser) {
        // create-form & edit-form (from RFC 6861 https://tools.ietf.org/html/rfc6861)
        links.push({
            rel: 'create-form',
            href: SETTINGS.server.SERVER_ROOT + "/forms/signup.html"
        });

        //TODO to return the edit-form rel in a collection we must support templates
        links.push({
            rel: 'edit-form',
            href: SETTINGS.server.SERVER_ROOT + "/forms/edit-user.html?id={0}",
            templated: "true",
        });
    }

    return links;
}

function createTemplate() {
    // template (from Collection+json)
    return {
        data: [{
                name: 'username',
                value: '',
                prompt: "Username"
            },
            {
                name: 'name',
                value: '',
                prompt: "User's name"
            },
            {
                name: 'email',
                value: '',
                prompt: "User's email"
            },
            {
                name: 'password',
                value: '',
                prompt: "Password"
            }
        ]
    };
}

function createQueriesOfCollection() {
    // queries (from Collection+json)
    const queries = [];

    queries.push({
        "rel": "search",
        "href": SETTINGS.server.rest.API_ROOT + "/user",
        "title": "Search user", // ref #29
        "data": [{
            name: "name",
            value: "",
            prompt: "Username"
        }]
    });

    queries.push({
        "rel": "search",
        "href": SETTINGS.server.rest.API_ROOT + "/user",
        "title": "Search by email", // ref #29
        "data": [{
            name: "email",
            value: "",
            prompt: "Email"
        }]
    });

    return queries;
}

/**
 * remove internal properties, i.e., photo.path
 * @param {*} user 
 */
function cleanup1User(user) {
    const from = user.toObject ? user.toObject() : user;
    const data = _.cloneDeep(from);

    if (data.photo) {
        if (data.photo.path) {
            // remove internal properties
            delete data.photo.path;

            // add properties
            data.photo.image = SETTINGS.server.rest.API_ROOT + "/user/" + user.username + "/photo";
        } else {
            // remove empty object
            delete data.photo;
        }
    }

    return data;
}

/**
 * remove internal properties
 * @param {*[]} userArray 
 */
function cleanupUsers(userArray) {
    return userArray.map(cleanup1User);
}

/**
 * 
 * @param {*} res 
 * @param {*} authUser 
 * @param {*} entry 
 * @param {*} created 
 * @param {*} otherMediaType 
 */
function formatResponse(res, authUser, entry, created, otherMediaType = {}) {
    const response = new Response(res, SETTINGS);
    if (created) {
        return formatResponseCreated(response, entry, authUser);
    } else {
        return formatResponseOk(response, entry, otherMediaType, authUser);
    }
}

function formatResponseOk(response, entry, otherMediaType, authUser) {
    // browsers typically send a */* Accept header, so the first callback will be used
    const formatters = {
        'application/json': () => {
            return response.ok(
                cleanup1User(entry),
                itemLinks(entry, authUser)
            );
        },
        'application/vnd.pagsousa.hyper+json': () => {
            return response.ok(
                Hyper.item(entry, (i) => itemLinks(i, authUser), cleanup1User)
            );
        },
        'application/pdf': function () {
            setPdfHeadersETagAndLink(response.res(), entry);
            return userController.generatePDFOfUser(entry, imageStore.PHOTO_REPO_PATH, response.res());
        },
        'default': function () {
            // log the request and respond with 406
            return response.notAcceptable();
        },
        ...otherMediaType
    };
    response.format(formatters);
}

function formatResponseCreated(response, entry, authUser) {
    const location = SETTINGS.server.rest.API_ROOT + "/user/" + entry.username;
    response.format({
        'application/json': () => {
            return response.created(
                cleanup1User(entry),
                location,
                itemLinks(entry, authUser)
            );
        },
        'application/vnd.pagsousa.hyper+json': () => {
            return response.created(
                Hyper.item(entry, (i) => itemLinks(i, authUser), cleanup1User),
                location);
        },
        'application/pdf': function () {
            setPdfHeadersETagAndLink(response.res(), entry);
            response.res().set("Location", location);
            return userController.generatePDFOfUser(entry, imageStore.PHOTO_REPO_PATH, response.res());
        },
    });
}

function formatError(res, err, authUser) {
    const sendJsonError = () => {
        return response.error(
            err,
            (m) => cleanup1User(m),
            (m) => {
                return {
                    ...Response.etagHeader(m),
                    ...Response.linkHeader(itemLinks(m, authUser))
                };
            }
        );
    };
    const response = new Response(res, SETTINGS);
    response.format({
        'application/json': sendJsonError,
        'application/vnd.pagsousa.hyper+json': () => {
            return response.error(
                err,
                (s) => Hyper.item(s, (i) => itemLinks(i, authUser)),
                Response.etagHeader
            );
        },
        'default': sendJsonError
    });
}

//
// handling the collection
//
// GET      return all
// POST     hack for HTML forms - to create a user do a PUT to /user/:username
// PUT      not allowed
// DELETE   not allowed
//

/**
 * @swagger
 * /user:
 *   get:
 *     tags:
 *       - Users
 *     summary: Returns all users
 *     description: Returns all users
 *     operationId: getUsers
 *     parameters:
 *       - in: query
 *         name: name
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         description: the author criteria
 *       - in: query
 *         name: email
 *         description: the email criteria
 *         schema:
 *           $ref: '#/components/schemas/email'
 *       - in: query
 *         name: start
 *         schema:
 *           type: integer
 *           minimum: 0
 *           maximum: 2147483647
 *           example: 1024
 *           format: int32
 *         description: page number (for result pagination)
 *       - in: query
 *         name: n
 *         schema:
 *           type: integer
 *           minimum: 1
 *           maximum: 500
 *           default: 20
 *           format: int32
 *           example: 50
 *         description: number of results per page
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkUserCollection'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleGetUsers(req, res) {
    //TODO only users with role 'admin' should be able to use this endpoint

    const paging = handlePagingParameters(req.query);

    return userController
        .searchUsers(req.query.name, req.query.email, paging.start, paging.n)
        .then(function (out) {
            const links = collectionLinks(paging.start, paging.n, out.isLast, req.user);
            const response = new Response(res, SETTINGS);
            response.format({
                'application/json': () => {
                    return response.ok(
                        cleanupUsers(out.data),
                        links
                    );
                },
                'application/vnd.pagsousa.hyper+json': () => {
                    return response.ok(
                        Hyper.collection(
                            out.data,
                            (i) => itemLinks(i, req.user),
                            links,
                            createTemplate(),
                            createQueriesOfCollection(),
                            cleanupUsers
                        )
                    );
                }
            });
        })
        .catch((err) => formatError(res, err, req.user));
}

function handlePagingParameters(query) {
    return {
        start: parseInt(query.start) || 0,
        n: parseInt(query.n) || 25
    };
}

/**
 * @swagger
 * /user:
 *   post:
 *     tags:
 *       - Signup
 *     summary: Registers a new user
 *     operationId: signup
 *     description: Registers a new user thru an html form as a convenience for human users of the API as usually this would only be possible thru a `PUT /user/:username` as the client is assigning the resource id (i.e., username)
 *     security: []
 *     requestBody:
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/newUserForm'
 *     responses:
 *       201:
 *         $ref: '#/components/responses/CreatedUserItem'
 *       413:
 *         description: Payload Too Large
 *       415:
 *         description: Unsupported Media Type
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePostUsers(req, res) {
    return doAddUser(req, res, req.body.username);
}

function doAddUser(req, res, username) {
    return storePhotoFile(req)
        .then(function (photo) {
            const spec = {
                username,
                name: req.body.name,
                email: req.body.email,
                description: req.body.description,
                password: req.body.password,
                photo
            };
            return userController.registerUser(spec);
        })
        .then((entry) => formatResponse(res, req.user, entry, true))
        .catch((err) => formatError(res, err, req.user));
}


//
// handling individual itens in the collection
//
// GET      return specific user or 404
// POST     update existing entry or 404 (hack for HTML forms)
// PUT      overwrite existing or create new given the username
// PATCH    update existing entry or 404
// DELETE   deletes the user - message history is kept, i.e., messages of this user are not deleted
//

/**
 * @swagger
 * /user/{username}:
 *   get:
 *     summary: Returns one user
 *     description: Returns one user
 *     operationId: getUser
 *     tags:
 *       - Users
 *     security: []
 *     parameters:
 *       - in: path
 *         name: username
 *         required: true
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         description: the user's username
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/userData'
 *           application/vnd.pagsousa.hyper+json:
 *             schema:
 *               $ref: '#/components/schemas/userItem'
 *           application/pdf:
 *             schema:
 *               type: string
 *               format: binary
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleGetUserItem(req, res) {
    return doGetUserItem(req.params.username, res, req.user);
}

/**
 * 
 * @param {*} username - username to look for
 * @param {*} res - HTTP response stream
 * @param {*} authUser - current authenticated user
 */
function doGetUserItem(username, res, authUser) {
    return userController
        .getUser(username)
        .then(function (entry) {
            return formatResponse(
                res,
                authUser,
                entry,
                false
            );
        })
        .catch((err) => formatError(res, err, authUser));
}

/**
 * Return an hypermedia representation of a user
 * @param {*} username 
 * @param {*} authUser 
 */
function getUserRepresentation(username, authUser) {
    return userController
        .getUser(username)
        .then((entry) => Hyper.item(entry, (i) => itemLinks(i, authUser), cleanup1User));
}

/**
 * @swagger
 * /user/{username}/pdf:
 *   get:
 *     tags:
 *       - Users
 *     summary: Returns a pdf representation of the user
 *     description: Returns a pdf representation of the user. This endpoint is a hack for browser access without the need for content negotiation
 *     security: []
 *     parameters:
 *       - in: path
 *         name: username
 *         required: true
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         description: the user's username
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/pdf:
 *             schema:
 *               type: string
 *               format: binary
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleGetUserItemPDF(req, res) {
    return userController
        .getUser(req.params.username)
        .then(function (entry) {
            res.type("application/pdf");
            setPdfHeadersETagAndLink(res, entry);
            return userController.generatePDFOfUser(entry, imageStore.PHOTO_REPO_PATH, res);
        })
        .catch((err) => formatError(res, err, req.user));
}

function setPdfHeadersETagAndLink(res, entry) {
    return res.header("ETag", Response.etagHeader(entry).ETag)
        .links({
            related: SETTINGS.server.rest.API_ROOT + "/user/" + entry.username
        });
}

/**
 * @swagger
 * /user/{username}:
 *   post:
 *     tags:
 *       - Users
 *     summary: updates a user profile
 *     description: updates a user profile. This is a convenience for HTML forms; applications should issue a `PATCH`
 *     parameters:
 *       - in: path
 *         name: username
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         required: true
 *     requestBody:
 *       description: user
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/editUserForm'
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/editUserDataInput'
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkUserItem'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       403:
 *         description: you can only change your own user profile
 *       412:
 *         $ref: '#/components/responses/PreconditionFailedUserItem'
 *       413:
 *         description: Payload Too Large
 *       415:
 *         description: Unsupported Media Type
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePostUserItem(req, res) {
    return doUpdateUser(req, res, true);
}

/**
 * @swagger
 * /user/{username}:
 *   patch:
 *     tags:
 *       - Users
 *     summary: updates a user profile
 *     description: updates a user profile
 *     parameters:
 *       - in: path
 *         name: username
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         required: true
 *     requestBody:
 *       description: user
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/editUserDataInput'
 *         application/merge-patch+json:
 *           schema:
 *             $ref: '#/components/schemas/editUserDataInput'
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkUserItem'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       403:
 *         description: you can only change your own user profile
 *       412:
 *         $ref: '#/components/responses/PreconditionFailedUserItem'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePatchUserItem(req, res) {
    return doUpdateUser(req, res, true);
}

/**
 * Updates an existing user.
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {boolean} partial - false to indicate full replacement (PUT semantics)
 */
function doUpdateUser(req, res, partial) {
    let rev = req.headers['if-match'];
    if (!rev) {
        rev = req.body.__etag;
    }

    const response = new Response(res, SETTINGS);
    // validate in the handler because we are storing the file and only afterwards calling the application layer...
    if (!rev) {
        return response.badRequest("Missing 'if-match' header");
    }

    // we are storing the file even if the application layer will reject this update based on validation
    // rules, e.g., only the user can update her own account
    // TODO check how to avoid this
    return storePhotoFile(req)
        .then(function (stored) {
            let spec = {
                username: req.params.username,
                name: req.body.name,
                email: req.body.email,
                description: req.body.description,
                photo: stored
            };
            return userController.updateUser(req.user.username, rev, spec, partial);
        })
        .then((entry) => formatResponse(res, req.user, entry, false))
        .catch((err) => formatError(res, err, req.user));
}

/**
 * @swagger
 * /user/{username}:
 *   put:
 *     tags:
 *       - Users
 *       - Signup
 *     summary: Registers a new user or updates the authenticated user's profile
 *     description: Registers a new user or updates the authenticated user's profile
 *     parameters:
 *       - in: path
 *         required: true
 *         name: username
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *     requestBody:
 *       description: user
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/editUserForm'
 *         application/json:
 *           schema:
 *             type: object
 *             additionalProperties: false
 *             required:
 *               - name
 *               - email
 *             properties:
 *               name:
 *                 $ref: '#/components/schemas/attrUserName'
 *               email:
 *                 $ref: '#/components/schemas/email'
 *               description:
 *                 $ref: '#/components/schemas/attrUserDescription'
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkUserItem'
 *       201:
 *         $ref: '#/components/responses/CreatedUserItem'
 *       400:
 *         description: you are authenticated but missed the "if-match" header in your request
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         description: you are authenticated and tried to update a user profile other than yours
 *       409:
 *         description: you are not authenticated, trying to register a new user, but the choosen user name is already in use
 *       412:
 *         $ref: '#/components/responses/PreconditionFailedUserItem'
 *       413:
 *         description: Payload Too Large
 *       415:
 *         description: Unsupported Media Type
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePutUserItem(req, res) {
    if (!req.user) {
        // unauthenticated, we are creating a new user account
        return doAddUser(req, res, req.params.username);
    } else {
        //authenticated, we are updating a user account
        return doUpdateUser(req, res, false);
    }
}

/**
 * @swagger
 * /user/{username}:
 *   delete:
 *     tags:
 *       - Users
 *     summary: unregisters a user
 *     description: unregisters a user
 *     parameters:
 *       - in: path
 *         name: username
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         required: true
 *     responses:
 *       200:
 *         $ref: '#/components/responses/Ok'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         description: you can only delete your own user profile
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       412:
 *         $ref: '#/components/responses/PreconditionFailedUserItem'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleDeleteUserItem(req, res) {
    const rev = req.headers['if-match'];

    //TODO delete photo file
    return userController
        .deleteUser(req.user.username, req.params.username, rev)
        .then((deleted) => new Response(res, SETTINGS).ok(`User ${req.params.username} deleted.`))
        .catch((err) => formatError(res, err, req.user));
}


//
// handling photo subresource
//
// user username must be present in the request object
//
// GET      return specific user's photo or 404
// POST     upload (create or update) a user's photo or 404
// PUT      upload (create or update) a user's photo or 404
// DELETE   deletes the user's photo
//

/**
 * @swagger
 * /user/{username}/photo:
 *   get:
 *     tags:
 *       - User photo
 *     summary: Returns the photo of the user
 *     description: Returns the photo of the user
 *     security: []
 *     parameters:
 *       - in: path
 *         name: username
 *         required: true
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         description: the user's username
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/png:
 *             schema:
 *               type: string
 *               format: binary
 *           application/jpg:
 *             schema:
 *               type: string
 *               format: binary
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleGetUserItemPhoto(req, res, next) {
    return userController
        .getUser(req.params.username)
        .then(function (entry) {
            const response = new Response(res, SETTINGS);
            if (!entry.photo.path) {
                return response.notFound(`Photo of user ${req.params.username} not found`);
            }
            const options = {
                root: imageStore.PHOTO_REPO_PATH,
                dotfiles: 'deny',
                headers: {
                    'x-timestamp': Date.now(),
                    'x-sent': true,
                    "ETag": entry.__v.toString(),
                    'Content-Type': entry.photo.mimetype
                }
            };
            res.links(Response.linkArrayToObject(photoLinks(entry, req.user)));
            return res.sendFile(entry.photo.path, options, function (err) {
                if (err) {
                    LOGGER('Error', err, 'sending file', entry.photo.path);
                    next(err);
                }
            });
        })
        .catch((err) => formatError(res, err, req.user));
}

/**
 * @swagger
 * /user/{username}/photo:
 *   put:
 *     tags:
 *       - User photo
 *     summary: sets or updates a user's photo profile
 *     description: sets or updates a user's photo profile
 *     parameters:
 *       - in: path
 *         name: username
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         required: true
 *     requestBody:
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/editUserPhotoForm'
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkUserItem'
 *       201:
 *         $ref: '#/components/responses/CreatedUserItem'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         description: you can only change your own user profile
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       413:
 *         description: Payload Too Large
 *       415:
 *         description: Unsupported Media Type 
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePutUserItemPhoto(req, res) {
    return doSetOrUpdateUserPhoto(req, res);
}

function doSetOrUpdateUserPhoto(req, res) {
    //TODO check if-match header
    //const rev = req.headers['if-match'];

    const response = new Response(res, SETTINGS);
    if (!req.file) {
        return response.badRequest("File is missing.");
    }

    // authorization should be done in application layer but we do it here to avoid unnecessarily storing the file
    if (req.user.username !== req.params.username) {
        return response.forbidden('You can only change your own picture.');
    }

    return storePhotoFile(req)
        .then((stored) => userController.setPhotoOfUser(req.user.username, req.params.username, stored))
        .then((out) => formatResponse(res, req.user, out.data, out.created))
        .catch((err) => formatError(res, err, req.user));
}

/**
 * stores the photo file that was uploaded in the request. returns a promise that resolves to an 
 * object with string properties:
 * {
 *  caption
 *  filename
 *  type
 * }
 * or null if there was no file uploaded in the request
 * 
 * @param {*} req 
 * @param {*} entry 
 */
function storePhotoFile(req) {
    // #22 #23 PATCH user/:username
    const caption = req.body.caption;

    if (!req.file) {
        return Promise.resolve({
            caption
        });
    }

    const uploadedFilename = req.file.path;
    const mimetype = req.file.mimetype;
    let storeFilename = "unknown";
    if (req.params.username) {
        storeFilename = req.params.username
    } else if (req.body.username) {
        storeFilename = req.body.username
    } else if (req.user) {
        storeFilename = req.user.username;
    }
    return imageStore.storePhotoFile(uploadedFilename, storeFilename, caption, mimetype);
}

/**
 * @swagger
 * /user/{username}/photo:
 *   post:
 *     tags:
 *       - User photo
 *     summary: sets or updates a user's photo profile
 *     description: sets or updates a user's photo profile
 *     parameters:
 *       - in: path
 *         name: username
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         description: the user's username
 *         required: true
 *     requestBody:
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/editUserPhotoForm'
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkUserItem'
 *       201:
 *         $ref: '#/components/responses/CreatedUserItem'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         description: you can only change your own user profile
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       413:
 *         description: Payload Too Large
 *       415:
 *         description: Unsupported Media Type
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePostUserItemPhoto(req, res) {
    return doSetOrUpdateUserPhoto(req, res);
}

/**
 * @swagger
 * /user/{username}/photo:
 *   delete:
 *     tags:
 *       - User photo
 *     summary: deletes the photo from the user's profile
 *     description: deletes the photo from the user's profile
 *     parameters:
 *       - in: path
 *         name: username
 *         required: true
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         description: the user's username
 *     responses:
 *       200:
 *         $ref: '#/components/responses/Ok'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         description: you can only delete your own photo
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleDeleteUserItemPhoto(req, res) {
    //TODO check if-match header
    //const rev = req.headers['if-match'];
    //LOGGER("if-match", rev);

    const response = new Response(res, SETTINGS);
    return userController.deletePhoto(req.user.username, req.params.username)
        //TODO actually delete the file from storage
        .then((user) => response.ok(`Photo of User ${req.params.username} deleted.`))
        .catch((err) => formatError(response, err, req.user));
}


/////////////////////////////
// MODULE EXPORTS

export default function UserResource(config, dependencies) {
    userController = new dependencies.UserController(config, dependencies);
    imageStore = new dependencies.ImageStore(config, dependencies);

    SETTINGS = config;

    Response = dependencies.Response;
    Hyper = dependencies.Hyper;

    return {
        handleGetUsers,
        handlePostUsers,

        handleGetUserItem,
        handlePostUserItem,
        handlePatchUserItem,
        handlePutUserItem,
        handleDeleteUserItem,

        handleGetUserItemPhoto,
        handlePostUserItemPhoto,
        handlePutUserItemPhoto,
        handleDeleteUserItemPhoto,

        handleGetUserItemPDF,

        //helpers for other routes returning user objects
        doGetUserItem,
        getUserRepresentation
    };
}