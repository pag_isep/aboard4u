/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */



/*jslint node: true */
/*jshint esversion:11 */

"use strict";

//
// setting up the REST routes for the User resource
//


import {
    //PayloadTooLargeError,
    UnsupportedMediaTypeError
} from "../../exceptions.js";

import multer from 'multer';

import MessageResource from '../../messages/rest/message-handler.js';

import UserResource from "./user-handler.js";

import { readFile } from 'fs/promises';

/////////////////////////////
// MODULE EXPORTS

export async function initRoutes(config, dependencies) {
    const app = dependencies.app;
    const dualModeAuth = dependencies.dualModeAuth;
    const authenticate = dependencies.authenticate;
    const handleAsNotAllowed = dependencies.handleAsNotAllowed;
    const ensureSessionLinks = dependencies.ensureSessionLinks;
    const ensureContentType = dependencies.ensureContentType;
    const ensureJsonSchema = dependencies.ensureJsonSchema;

    //
    // HTML form posting thru encoding multipart/form-data, particularly for file upload
    //

    // #33 - limit file size and type
    function allowImagesUpToLimit(req, file, cb) {
        const isImage = file.mimetype.match(/^image\/\w*$/) != null;
        if (!isImage) {
            cb(new UnsupportedMediaTypeError(["image/*"], file.mimetype, "The uploaded file is in an unacceptable format"));
        } else {
            cb(null, isImage);
        }
    }

    const uploadDisplayImage = multer({
        dest: './uploads/',
        fileFilter: allowImagesUpToLimit,
        limits: {
            fileSize: 32 * 1024 // 32 Kb 
        }
    }).single('displayImage');

    //
    // load resource modules
    //
    const messageResource = new MessageResource(config, dependencies);

    const userResource = new UserResource(config, dependencies);

    const API_PATH = config.server.rest.API_PATH;

    // B4U-37 json schema validation
    /*
    const newUserSchema = JSON.parse(await readFile(new URL("file://" + process.cwd() + "/message-server/assets/schemas/user/newUserDataInput.schema.json", import.meta.url)));
    const ensureNewUserSchema = ensureJsonSchema({
        body: newUserSchema
    });
    */
    const newUserFormSchema = JSON.parse(await readFile(new URL("file://" + process.cwd() + "/message-server/assets/schemas/user/newUserForm.schema.json",
        import.meta.url)));
    const ensureNewUserFormSchema = ensureJsonSchema({
        body: newUserFormSchema
    });
    const editUserFormSchema = JSON.parse(await readFile(new URL("file://" + process.cwd() + "/message-server/assets/schemas/user/editUserForm.schema.json",
        import.meta.url)));
    const ensureEditUserFormSchema = ensureJsonSchema({
        body: editUserFormSchema
    });
    const userDataInputSchema = JSON.parse(await readFile(new URL("file://" + process.cwd() + "/message-server/assets/schemas/user/userDataInput.schema.json",
        import.meta.url)));
    const ensureUserDataInputSchema = ensureJsonSchema({
        body: userDataInputSchema
    });

    //
    // USER resource
    //
    // GET /user is authenticated to avoid a guest user to download the user collection, eventough GET /user/:username is unauthenticated
    // POST is unauthenticated to allow an html form to "post" a new user registration, because typically POST to the user collection would not be allowed
    app.route(API_PATH + "/user")
        .get(authenticate, ensureSessionLinks, userResource.handleGetUsers)
        .put(handleAsNotAllowed)
        .post(ensureSessionLinks, ensureContentType(['multipart/form-data', 'application/x-www-form-urlencoded']), uploadDisplayImage, ensureNewUserFormSchema, userResource.handlePostUsers)
        .patch(handleAsNotAllowed)
        .delete(handleAsNotAllowed);

    // GET /user/:username is dual mode authenticated to hide "private" data/links from unathenticated users
    // PUT /user/:username is both unauthenticated to allow to register new users and authenticated for updates
    app.route(API_PATH + "/user/:username")
        .get(dualModeAuth, ensureSessionLinks, userResource.handleGetUserItem)
        .put(dualModeAuth, ensureSessionLinks, ensureContentType(['application/json', 'multipart/form-data', 'application/x-www-form-urlencoded']), uploadDisplayImage, /* ensureNewUserSchema,*/ userResource.handlePutUserItem)
        .post(authenticate, ensureSessionLinks, ensureContentType(['multipart/form-data', 'application/x-www-form-urlencoded']), uploadDisplayImage, ensureEditUserFormSchema, userResource.handlePostUserItem)
        .patch(authenticate, ensureSessionLinks, ensureContentType(['application/merge-patch+json', 'application/json']), ensureUserDataInputSchema, userResource.handlePatchUserItem)
        .delete(authenticate, ensureSessionLinks, userResource.handleDeleteUserItem);

    // PDF hack due to limitation to explicitly download PDF thru content negotiation from browser
    app.route(API_PATH + "/user/:username/pdf")
        .get(ensureSessionLinks, userResource.handleGetUserItemPDF)
        .put(handleAsNotAllowed)
        .post(handleAsNotAllowed)
        .patch(handleAsNotAllowed)
        .delete(handleAsNotAllowed);

    //
    // Photo subresource
    //
    // GET /user/:username/photo is public unauthenticated
    app.route(API_PATH + "/user/:username/photo")
        .get(ensureSessionLinks, userResource.handleGetUserItemPhoto)
        .put(authenticate, ensureSessionLinks, ensureContentType(['multipart/form-data']), uploadDisplayImage, userResource.handlePutUserItemPhoto)
        .post(authenticate, ensureSessionLinks, ensureContentType(['multipart/form-data']), uploadDisplayImage, userResource.handlePostUserItemPhoto)
        .patch(handleAsNotAllowed)
        .delete(authenticate, ensureSessionLinks, userResource.handleDeleteUserItemPhoto);

    //
    // User's messages subresource
    //
    // GET /user/:username/message is public unauthenticated
    app.route(API_PATH + "/user/:username/message")
        .get(ensureSessionLinks, messageResource.handleGetUserMessages)
        .put(handleAsNotAllowed)
        .post(handleAsNotAllowed)
        .patch(handleAsNotAllowed)
        .delete(handleAsNotAllowed);
    // GET /user/:username/message/count is public unauthenticated
    app.route(API_PATH + "/user/:username/message/summary")
        .get(ensureSessionLinks, messageResource.handleGetUserMessagesSummary)
        .put(handleAsNotAllowed)
        .patch(handleAsNotAllowed)
        .post(handleAsNotAllowed)
        .delete(handleAsNotAllowed);
}