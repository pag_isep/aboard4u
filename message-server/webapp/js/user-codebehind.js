/*
 * A Board 4 U
 *
 * Copyright (c) 2016-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

function createUserItemEditForm(user, etag) {
    // If-match
    $('#etag').text(etag);
    $('#__etag').attr('value', etag);

    // TODO show other meaningful attributes
    $('#username').attr('value', user.data.username);
    $('#email').attr('value', user.data.email);
    $('#name').attr('value', user.data.name);

    const imageLink = getLinkObject(user.links, 'https://schema.org/image');
    $('#photo').attr('src', imageLink.href);
    if (imageLink) {
        $('#caption').attr('value', imageLink.title);
    }

    // link to PDF representation
    const userPDF = getLink(user.links, 'alternate');
    $("#userpdf").attr("href", userPDF);

    //form URI
    const userURI = getLink(user.links, 'self');
    $('#edit-user').attr('action', userURI);
}

function loadUserForEdit() {
    loadItemForEdit(createUserItemEditForm);
}