/*
 * A Board 4 U
 *
 * Copyright (c) 2016-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

function createMessageItemEditForm(msg, etag) {
    // If-match
    $('#etag').text(etag);
    $('#__etag').attr('value', etag);

    // TODO show other meaningfull attributes
    $('#messageID').attr('value', msg.data.id);
    $('#title').attr('value', msg.data.title);
    $('#text').text(msg.data.text);

    //form URI
    const msgURI = getLink(msg.links, 'self');
    $('#edit-message').attr('action', msgURI);
}

function loadMessageForEdit() {
    loadItemForEdit(createMessageItemEditForm);
}

$().ready(function () {
    loadMessageForEdit();
    getVersion();
});