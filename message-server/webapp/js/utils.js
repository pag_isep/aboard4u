/*
 * A Board 4 U
 *
 * Copyright (c) 2016-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:11 */

"use strict";

/**
 * source: http: //www.joezimjs.com/javascript/3-ways-to-parse-a-query-string-in-a-url/
 * 
 * @param {*} queryString 
 */
function parseQueryString(queryString) {
    let params = {},
        queries, temp, i, l;

    // Split into key/value pairs
    queries = queryString.split("&");

    // Convert the array of strings into an object
    for (i = 0, l = queries.length; i < l; i++) {

        temp = queries[i].split('=');
        params[temp[0]] = decodeURIComponent(temp[1]);
    }

    return params;
}

/**
 * Returns the value of a parameter from the query string
 * @param {String} uriParm - the name of the parameter. defaults to 'id'
 */
function getUriFromQS(uriParm) {
    const qstring = window.location.search.substring(1);
    const qs = parseQueryString(qstring);
    if (!uriParm) {
        uriParm = "id";
    }
    return qs[uriParm];
}

/**
 * Returns the target URI of a link relation
 * @param {LinkDefinition[]} links 
 * @param {String} rel 
 */
function getLink(links, rel) {
    if (!links.length) {
        return null;
    }

    const link = links.find((l) => l.rel === rel);

    if (link) { 
        return link.href;
    }
    return null; // TODO better error handling
}

function getLinkObject(links, rel) {
    if (!links.length) {
        return null;
    }

    const link = links.find((l) => l.rel === rel);
    return link;
}

/**
 * fetchs an item from the API based on the id in query string
 * @param {*} createItem - callback to handle the fetched item
 * @returns the item URI
 */
function loadItemForEdit(createItem) {
    const itemURI = getUriFromQS();
    $.get({
        url: itemURI,
        accepts: {
            "pagsousahyper": "application/vnd.pagsousa.hyper+json"
        },
        converters: {
            "* pagsousahyper": jQuery.parseJSON
        },
        dataType: "pagsousahyper",
        success: function (body, status, xhr) {
            let etag = xhr.getResponseHeader('etag');
            createItem(body, etag);
        }
    });
    return itemURI;
}