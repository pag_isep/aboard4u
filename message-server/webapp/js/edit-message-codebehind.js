/*
 * A Board 4 U
 *
 * Copyright (c) 2016-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

$().ready(function () {
    loadMessageForEdit();
    getVersion();
});