/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

function getVersion() {
    $.get({
        url: '/api/version',
        headers: {
            Accept: "application/json",
        },
        success: function (body, status, xhr) {
            $('#version').text(xhr.getResponseHeader('X-API-Version'));
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#version').text("--");
        }
    });
}