/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:10 */

//
// message application layer
//

import debug from "debug";
const LOGGER = debug("pag:mserver:app");

// maximum number of results in searches
import {
    limitMaxResults
} from "../../util/pagination-limits.js";


//
// tags
//

/**
 * Tag controller
 * 
 * all methods return a Promise
 */
export class TagController {

    constructor(config, dependencies) {
        // private by convention
        this._tagRepo = new dependencies.TagRepository();

        // public

    }

    /**
     * Searches tags by text. paginates results.
     * 
     * @param {MessageSearchCriteria} criteria
     * @param {*} [paging={}] - pagination 
     */
    searchTags(criteria, paging = {}) {
        paging.first = limitMaxResults(paging.first);
        return this._tagRepo.searchTagsAfter(paging.first, paging.after, criteria);
    }


    _percentage(num, total) {
        return total ? Math.round(((num / total * 100) + Number.EPSILON) * 100) / 100 : 0;
    }
    
    _countTags(a, t) {
        return a + (t.tag ? t.count : 0);
    }

    /**
     * builds a tag cloud of the most used tags in percentage
     * 
     * @param {*} freq 
     */
    _buildCloud(freq) {
        const N_TOP_TAGS = 10;

        let withTag = 0;
        let withNoTag = 0;
        let cloud = {};

        if (freq.length) {
            //inefficient as we loop several times thru the array
            withTag = freq.reduce(this._countTags, 0);

            freq.sort((a, b) => a.count > b.count ? -1 : 1);

            let top = 0;
            let otherCount = 0;
            freq.forEach(e => {
                if (!e.tag) {
                    withNoTag = e.count;
                } else {
                    if (top < N_TOP_TAGS) {
                        cloud[e.tag] = this._percentage(e.count, withTag);
                        top++;
                    } else {
                        otherCount += e.count;
                    }
                }
            });
            cloud.$other = this._percentage(otherCount, withTag);
        }

        let total = withTag + withNoTag;

        return {
            cloud,
            total,
            withTag,
            withNoTag
        };
    }

    /**
     * 
     * @param {*} [criteria] 
     */
    getTagCloud(criteria = {}) {
        return this._tagRepo.getTagCloud(criteria)
            .then((freq) => {
                LOGGER('tag cloud returned', freq.length, 'results.');

                let cloud = {
                    frequencies: freq,
                    ...this._buildCloud(freq)
                };

                if (criteria.author) {
                    cloud.scope = {
                        author: criteria.author
                    };
                }
                return cloud;
            });
    }
}