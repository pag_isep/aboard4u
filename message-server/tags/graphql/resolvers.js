/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// GraphQL Message
//


import {handleError} from '../../util/apollo-error-helper.js';

//
// The resolvers
//

/**
 * The GraphQL resolvers
 * 
 * @param {*} config 
 * @param {*} dependencies 
 */
export function resolvers(config, dependencies) {
    const tagController = new dependencies.TagController(config, dependencies);

    // return the resolvers object
    return {
        TagCloud: {
            cloud: (parent) => {
                return Object.keys(parent.cloud).map(function (k) {
                    return {
                        tag: k,
                        frequency: parent.cloud[k]
                    };
                });
            }
        },

        //
        // Queries
        //
        Query: {
            /**
             * Search tags
             * 
             * tags(criteria: TagCriteria = {}, paging: PagingSpecification = {}): [String!] !
             */
            tags: (parent, args, context, info) => {
                const spec = {
                    text: args.criteria.text,
                };
                const page = {
                    ...args.paging
                };
                return tagController
                    .searchTags(spec, page)
                    .then(out => out.data)
                    .catch(handleError);
            },

            /**
             * tag cloud
             */
            tagCloud: (parent, args, context, info) => {
                const spec = {
                    author: args.author,
                };
                return tagController
                    .getTagCloud(spec)
                    .then(out => out)
                    .catch(handleError);
            }
        },
    };
}
