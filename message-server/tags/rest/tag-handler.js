/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// Tag resource
//

//these will be passed to the module thru the configure() function
let tagController;
let Response;

let SETTINGS;
let DEPENDENCIES;

//
// hypermedia controls
//

function buildHref(paging, criteria) {
    let href = SETTINGS.server.rest.API_ROOT + "/tags?";

    if (paging.first) {
        href = href + "first=" + paging.first;
    }
    if (paging.after) {
        href = href + "&after=" + paging.after;
    }

    if (criteria.text) {
        href += "&text=" + criteria.text;
    }

    return href;
}

/**
 * Creates links element of the collection.
 * follows Collection+JSON specification
 * 
 * @param {Paging} paging 
 * @param {*} out  
 * @param {MessageSearchCriteria} criteria 
 */
function collectionLinks(paging, out, criteria) {
    const links = [];

    // collection links
    links.push({
        rel: "self",
        href: buildHref(paging, criteria),
    });

    links.push({
        rel: "start",
        href: buildHref({}, criteria)
    });

    if (paging.after) {
        //TODO how to construct the prev link ?
    }

    if (!out.isLast) {
        const nextPage = {
            first: paging.first,
            after: out.data[out.data.length - 1]
        };
        links.push({
            rel: "next",
            href: buildHref(nextPage, criteria)
        });
    }

    return links;
}

/**
 * Performs content negotiation and sends the response in the appropriate format.
 *  
 * @param {*} res
 * @param {*} authzUser
 * @param {*} out 
 * @param {MessageSearchCriteria} criteria 
 * @param {Paging} paging 
 */
function formatResponseCollection(res, out, criteria, paging) {
    // LOGGER("Formatting collection: N=", out.data.length, "start=", paging.start, "n=", paging.n, "isLast:", out.isLast);
    const links = collectionLinks(paging, out, criteria);
    const response = new Response(res, SETTINGS);
    return response.ok(out.data, links);
}

/**
 * 
 * @param {*} res 
 * @param {*} err 
 * @param {*} authzUser 
 */
function formatError(res, err) {
    const response = new Response(res, SETTINGS);
    return response.error(err);
}

//
// handling the collection
//

/**
 * @swagger
 * /tags:
 *   get:
 *     tags:
 *       - Tags
 *     summary: Returns all tags
 *     description: Returns all tags. Optionally filters tags by starting text.
 *     operationId: getTags
 *     security: []
 *     parameters:
 *       - in: query
 *         name: text
 *         schema:
 *           $ref: '#/components/schemas/attrMessageTag'
 *         description: the starting text of a tag
 *       - in: query
 *         name: first
 *         schema:
 *           type: integer
 *           default: 5
 *           minimum: 1
 *           maximum: 500
 *           format: int32
 *         description: number of results
 *       - in: query
 *         name: after
 *         schema:
 *           $ref: '#/components/schemas/attrMessageTag'
 *         description: tag name to start the pagination (exclusive)
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkTagCollection'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleGetTags(req, res) {
    let paging = handlePagingParameters(req.query);

    let criteria = {
        text: req.query.text,
    };
    return tagController
        .searchTags(criteria, paging)
        .then((out) => formatResponseCollection(res, out, criteria, paging))
        .catch((err) => formatError(res, err));
}

// TODO duplicated in message-handler
function handlePagingParameters(query) {
    let paging = {};
    if (query.first || query.after) {
        paging.first = parseInt(query.first);
        paging.after = query.after;
    }
    return paging;
}

/**
 * @swagger
 * /tag-cloud:
 *   get:
 *     tags:
 *       - Tags
 *     summary: Returns the frequency of each tag
 *     description: Returns the frequency of each tag, i.e., percentage of messages with a certain tag
 *     operationId: getTagCloud
 *     security: []
 *     parameters:
 *       - in: query
 *         name: author
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         description: the author's tag cloud
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkTagCloud'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleGetTagCloud(req, res) {
    let criteria = {};
    if (req.query.author) {
        criteria.author = req.query.author;
    }
    return tagController
        .getTagCloud(criteria)
        .then((out) => res.json(out))
        .catch((err) => formatError(res, err));
}

/////////////////////////////
// MODULE EXPORTS

export default function TagResource(config, dependencies) {
    tagController = new dependencies.TagController(config, dependencies);

    SETTINGS = config;
    DEPENDENCIES = dependencies;

    Response = DEPENDENCIES.Response;

    return {
        handleGetTags,
        handleGetTagCloud
    };
}