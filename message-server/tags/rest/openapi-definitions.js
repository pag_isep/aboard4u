/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// a documentation file to hold the common definitions of models used in the API
//


//
// schemas
//


/**
 * @swagger
 * components:
 *   responses:
 *     OkTagCollection:
 *       description: the tags
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/attrMessageTags'
 *     OkTagCloud:
 *       description: the frequency of each tag
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             additionalProperties: false
 *             required:
 *               - frequencies
 *             properties:
 *               scope:
 *                 type: object
 *                 properties:
 *                   author:
 *                     $ref: '#/components/schemas/attrUserUsername'
 *               total:
 *                 type: integer
 *                 format: int32
 *                 minimum: 0
 *                 maximum: 2147483647
 *                 description: total number of messages
 *               withTag:
 *                 type: integer
 *                 format: int32
 *                 minimum: 0
 *                 maximum: 2147483647
 *                 description: number of messages with at least a tag
 *               withNoTag:
 *                 type: integer
 *                 format: int32
 *                 minimum: 0
 *                 maximum: 2147483647
 *                 description: number of messages with no tag
 *               cloud:
 *                 type: object
 *                 description: an hash (tag -> percentage). Only the top tags will be present
 *                 additionalProperties: true
 *                 properties:
 *                   $other:
 *                     type: number
 *                     format: float
 *                     minimum: 0.0
 *                     maximum: 100.0
 *                     description: frequency of messages with other tags besides the ones present in the hash
 *               frequencies:
 *                 type: array
 *                 maxItems: 1024
 *                 description: an array of hashes (tag -> absolute frequency).
 *                 items:
 *                   type: object
 *                   additionalProperties: false
 *                   properties:
 *                     tag:
 *                       $ref: '#/components/schemas/attrMessageTag'
 *                     count:
 *                       type: integer
 *                       format: int32
 *                       minimum: 0
 *                       maximum: 2147483647
 *                       description: absolute frequency of messages with this tag
 *
 */