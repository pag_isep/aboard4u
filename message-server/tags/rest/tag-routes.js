/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */



/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// setting up the REST routes for the Message resource
//

import TagResource from "./tag-handler.js";

/////////////////////////////
// MODULE EXPORTS

export function initRoutes(config, dependencies) {
    const app = dependencies.app;
    const handleAsNotAllowed = dependencies.handleAsNotAllowed;
    const ensureSessionLinks = dependencies.ensureSessionLinks;

    //
    // load resource modules
    //
    const tagResource = new TagResource(config, dependencies);

    const API_PATH = config.server.rest.API_PATH;

    //
    // TAGS resource
    //
    app.route(API_PATH + "/tags")
        .get(ensureSessionLinks, tagResource.handleGetTags)
        .put(handleAsNotAllowed)
        .post(handleAsNotAllowed)
        .patch(handleAsNotAllowed)
        .delete(handleAsNotAllowed);

    //
    // TAG-CLOUD resource
    //
    app.route(API_PATH + "/tag-cloud")
        .get(ensureSessionLinks, tagResource.handleGetTagCloud)
        .put(handleAsNotAllowed)
        .post(handleAsNotAllowed)
        .patch(handleAsNotAllowed)
        .delete(handleAsNotAllowed);
}
