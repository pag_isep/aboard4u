/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

/**
 * An operation tried to use an unexisting resource
 */
function NotFoundError(message, requestedId) {
    this.name = 'NotFoundError';
    this.message = message || 'Requested Object does not exist';
    this.requestedId = requestedId;
    this.stack = (new Error()).stack;
}
NotFoundError.prototype = Object.create(Error.prototype);
NotFoundError.prototype.constructor = NotFoundError;


/**
 * An operation needs the user to be authenticated, but there is no currently logged in user
 */
function UnauthenticatedError(message) {
    this.name = 'UnauthenticatedError';
    this.message = message || 'You must be authenticated to perform this operation';
    this.stack = (new Error()).stack;
}
UnauthenticatedError.prototype = Object.create(Error.prototype);
UnauthenticatedError.prototype.constructor = UnauthenticatedError;

/**
 * The currently authenticated user does not have permission (authorization) to execute an operation
 */
function ForbiddenError(message, currentState) {
    this.name = 'ForbiddenError';
    this.message = message || 'You are not authorized to perform this operation';
    this.currentState = currentState;
    this.stack = (new Error()).stack;
}
ForbiddenError.prototype = Object.create(Error.prototype);
ForbiddenError.prototype.constructor = ForbiddenError;


/**
 * state inconsistencies due to Concurrency issues.
 * 
 * in the service layer it will generate a 412
 */
function ConcurrencyError(message, currentState) {
    this.name = 'ConcurrencyError';
    this.message = message || 'Item has been updated after your previous query';
    this.currentState = currentState;
    this.stack = (new Error()).stack;
}
ConcurrencyError.prototype = Object.create(Error.prototype);
ConcurrencyError.prototype.constructor = ConcurrencyError;


/**
 * state inconsistencies, e.g., userame already in use.
 * 
 * in the service layer it will generate a 409
 */
function ConflictError(message, currentState) {
    this.name = 'ConflictError';
    this.message = message || 'There is a conflict between the server resource state and the cliente state';
    this.currentState = currentState;
    this.stack = (new Error()).stack;
}
ConflictError.prototype = Object.create(Error.prototype);
ConflictError.prototype.constructor = ConflictError;

/**
 * The client has send an unsupported media format, e.g., during file upload - will generate a 415
 * 
 * see #33 - limit file size and type
 * 
 * @param {string[]} [expected]
 * @param {string} [actual]
 * @param {string} [message]
 */
function UnsupportedMediaTypeError(expected, actual, message = "The uploaded file is in an unacceptable format") {
    this.name = 'UnsupportedMediaTypeError';
    this.message = message;
    this.expected = expected;
    this.actual = actual;
    this.stack = (new Error()).stack;
}
UnsupportedMediaTypeError.prototype = Object.create(Error.prototype);
UnsupportedMediaTypeError.prototype.constructor = ConflictError;


/**
 * The client has send a payload that is too large for the server to handle, e.g., during file upload - will generate a 413
 * 
 * see #33 - limit file size and type
 */
function PayloadTooLargeError(message, currentState) {
    this.name = 'PayloadTooLargeError';
    this.message = message || "The uploaded file is above the allowed size";
    this.currentState = currentState;
    this.stack = (new Error()).stack;
}
PayloadTooLargeError.prototype = Object.create(Error.prototype);
PayloadTooLargeError.prototype.constructor = ConflictError;


/**
 * The client has send a wrong input, bad request, malformed input
 */
function BadRequestError(message, currentState) {
    this.name = 'BadRequestError';
    this.message = message || "Bad Request";
    this.currentState = currentState;
    this.stack = (new Error()).stack;
}
BadRequestError.prototype = Object.create(Error.prototype);
BadRequestError.prototype.constructor = BadRequestError;

/**
 * 
 */
function buildMissingIfMatch() {
    return new BadRequestError('Missing "if-match" header', {
        headers: {
            keyword: "required",
            dataPath: "",
            schemaPath: "#/required",
            params: {
                missingProperty: "if-match"
            },
            message: "should have required property 'if-match'"
        }
    });
}

/**
 * 
 * @param {*} dataPath 
 * @param {*} schemaPath 
 * @param {*} pattern 
 */
function buildWrongFormat(dataPath, schemaPath, pattern) {
    return new BadRequestError('Wrong format', {
        body: {
            keyword: "pattern",
            dataPath,
            schemaPath,
            params: {
                pattern
            },
            message: `should match pattern "${pattern}"`
        }
    });
}

/**
 * 
 * @param {*} dataPath 
 * @param {*} schemaPath 
 * @param {*} pattern 
 */
function buildWrongFormatInQuery(queryParam, pattern) {
    return new BadRequestError('Wrong format', {
        query: {
            keyword: "pattern",
            dataPath: `.${queryParam}`,
            schemaPath: `#/properties/${queryParam}`,
            params: {
                pattern
            },
            message: `should match pattern "${pattern}"`
        }
    });
}

/////////////////////////////
// MODULE EXPORTS

export {
    NotFoundError,
    UnauthenticatedError,
    ConflictError,
    ConcurrencyError,
    ForbiddenError,
    UnsupportedMediaTypeError,
    PayloadTooLargeError,
    BadRequestError,
    buildMissingIfMatch,
    buildWrongFormat,
    buildWrongFormatInQuery
};