/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// in memory persistence data layer implementation
//
// all methods return a Promise
// this way we design the API of these functions in an assynchronus way to ease the portability to
// real DB systems
//


import {
    ConcurrencyError,
    ConflictError,
    NotFoundError
    //UnauthenticatedError,
    //ForbiddenError
} from "../exceptions.js";


//
// Messages
//

// data
const messages = {};

/**
 * pagination & transformation to array
 *
 * @param {*} mes
 * @param {*} start 
 * @param {*} n 
 * @returns object[]
 */
function pageResults(mes, start, n) {
    return Object.values(mes)
        .sort((a, b) => a.publishedOn > b.publishedOn ? -1 : 1)
        .slice(start - 1, start - 1 + n);
}

/**
 * 
 * @param {*} message 
 * @param {*} username 
 */
function comparatorUser(message, username) {
    return message.authorUsername === username;
}

/**
 * 
 * @param {*} message 
 * @param {*} text 
 */
function comparatorText(message, text) {
    return message.text.indexOf(text) >= 0;
}

function comparatorTag(message, text) {
    return message.tags.some(e => e.indexOf(text[0]) >= 0);
}

function comparatorTitle(message, text) {
    return message.title.indexOf(text) >= 0;
}

function comparatorDates(message, {
    createdFrom,
    createdTo
}) {
    let ret = true;
    if (createdFrom) {
        ret &= message.createdOn >= createdFrom;
    }
    if (ret && createdTo) {
        ret &= message.createdOn <= createdTo;
    }

    return ret;
}


/**
 * Filter messages
 * 
 * @param {object} mes - a javascript object
 * @param {*} param 
 * @param {*} comparator - a comparator function f(element, param)
 * @returns object
 */
function filterMessages(mes, param, comparator) {
    let out = {};
    if (param) {
        Object.keys(mes).forEach(function (m) {
            if (comparator(mes[m], param)) {
                out[mes[m].id] = mes[m];
            }
        });
    } else {
        out = mes;
    }
    return out;
}

/**
 * 
 * @param {Array} mes - message array
 * @param {String} text - text to searcg for
 */
function filterMessagesByUser(mes, user) {
    return filterMessages(mes, user, comparatorUser);
}

/**
 * 
 * @param {Array} mes - message array
 * @param {String} text - text to searcg for
 */
function filterMessagesByText(mes, text) {
    // TODO should do a URL decode before comparing
    return filterMessages(mes, text, comparatorText);
}

function filterMessagesByTag(mes, tag) {
    // TODO should do a URL decode before comparing
    return filterMessages(mes, tag, comparatorTag);
}

/**
 * 
 * @param {Array} mes - message array
 * @param {String} text - text to searcg for
 */
function filterMessagesByTitle(mes, text) {
    // TODO should do a URL decode before comparing
    return filterMessages(mes, text, comparatorTitle);
}

function filterMessagesByDate(mes, createdFrom, createdTo) {
    if (!createdFrom && !createdTo) {
        return mes;
    }

    return filterMessages(mes, {
        createdFrom,
        createdTo
    }, comparatorDates);
}

//
// messages
//

/**
 * 
 * @param {*} newID 
 * @param {*} msg
 * @param {*} authorUsername 
 */
function addMessage(newID, msg, authorUsername) {
    const now = new Date();
    messages[newID] = {
        id: newID,
        title: msg.title,
        text: msg.text,
        authorUsername,
        createdOn: now,
        updatedOn: now,
        publishedOn: now,
        tags: msg.tags || [],
        trivia: msg.trivia || undefined,
        "__v": "0"
    };
    return Promise.resolve(messages[newID]);
}

/**
 * 
 * @param {*} m 
 */
function saveMessage(m) {
    m.__v = (parseInt(m.__v) + 1).toString();
    return Promise.resolve(m);
}

/**
 * 
 * @param {*} messageId 
 */
function getMessage(messageId, reject = true) {
    if (messages[messageId]) {
        return Promise.resolve(messages[messageId]);
    } else {
        if (reject) {
            return Promise.reject(new NotFoundError(null, messageId));
        } else {
            return Promise.resolve(null);
        }
    }
}

function sort(filtered) {
    return Object.values(filtered).sort((a, b) => a.publishedOn + a.id > b.publishedOn + b.id ? -1 : 1);
}

function filter(spec) {
    let out = filterMessagesByUser(messages, spec.author);
    out = filterMessagesByText(out, spec.text);
    out = filterMessagesByTitle(out, spec.title);
    out = filterMessagesByDate(out, spec.createdFrom, spec.createdTo);
    out = filterMessagesByTag(out, spec.tags);
    return out;
}

/**
 * searches messages by text, author or creation date. paginates results.
 * 
 * @param {Number} first - pagination - number of elements to return
 * @param {*} [after] - pagination index (exclusive)
 * @param {MessageSearchCriteria} [spec] 
 */
function searchMessagesAfter(first, after, spec = {}) {
    if (!first) {
        //TODO throw BadRequestError to allow adding a json response
        throw new RangeError("Missing mandatory parameters");
    }

    // filtering & sorting
    const filtered = filter(spec);
    const sorted = sort(filtered);

    // pagination
    const startIdx = sorted.findIndex(e => e.id === after) + 1;
    const res = sorted.slice(startIdx, startIdx + first);
    const length = Object.keys(filtered).length;
    return Promise.resolve({
        isLast: first >= length,
        data: res
    });
}

/**
 * 
 * @param {*} messageId 
 * @param {*} rev 
 */
function deleteMessage(messageId, rev) {
    if (messages[messageId].__v !== rev) {
        return Promise.reject(new ConcurrencyError("Item has been updated by other user already. Please refresh and retry.", messages[messageId]));
    }

    delete messages[messageId];
    return Promise.resolve(true);
}

function countOfMessages(spec = {}) {
    //TODO only allows counting by author
    const authored = filterMessagesByUser(messages, spec.author);
    return Object.keys(authored).length;
}

//
// Tags
//

function tagPipeline(source) {
    const allTags = Object.keys(source).map(m => source[m].tags);
    const flattenTags = [].concat(...allTags);
    const uniqueTags = flattenTags.filter((value, index, self) => self.indexOf(value) === index);

    let tags = uniqueTags;
    tags.sort();
    return tags;
}

/**
 * searches tags by text. paginates results.
 * 
 * @param {*} first - pagination length
 * @param {*} [after] - pagination index (exclusive)
 * @param {TagSearchCriteria} [spec] 
 */
function searchTagsAfter(first, after, spec = {}) {
    if (!first) {
        //TODO throw BadRequestError to allow adding a json response
        throw new RangeError("Missing mandatory parameters");
    }

    //TODO decouple tags from message
    let tags = tagPipeline(messages);

    if (spec.text) {
        const regex = new RegExp('^' + spec.text + '.*');
        tags = tags.filter(e => e.match(regex));
    }

    if (after) {
        tags = tags.filter(e => e > after);
    }

    const length = tags.length;

    tags = tags.slice(0, first);

    return Promise.resolve({
        isLast: first > length,
        data: tags
    });
}

function tagCloudPipeline(source) {
    if (Object.keys(source).length === 0) {
        return [];
    }

    let accum = {
        $none: 0
    };

    Object.keys(source).forEach(m => {
        if (source[m].tags) {
            source[m].tags.forEach(t => {
                accum[t] = accum[t] ? accum[t] + 1 : 1;
            });
        } else {
            accum.$none += 1;
        }
    });

    let tags = Object.keys(accum).map(a => {
        return {
            tag: a === '$none' ? null : a,
            count: accum[a]
        };
    });

    return tags;
}

/**
 * build a tag cloud.
 * 
 * @param {Object} [criteria] 
 */
function getTagCloud(criteria = {}) {

    let source;
    if (criteria.author) {
        //TODO decouple tags from message
        source = filterMessagesByUser(messages, criteria.author);
    } else {
        //TODO decouple tags from message
        source = messages;
    }
    const tags = tagCloudPipeline(source);

    return Promise.resolve(tags);
}

//
// users
//

// data
const users = {};

/**
 * 
 * @param {*} username 
 */
function getUser(username, reject = true) {
    if (users[username]) {
        return Promise.resolve(users[username]);
    } else {
        if (reject) {
            return Promise.reject(new NotFoundError(null, username));
        } else {
            return Promise.resolve(null);
        }
    }
}

function userExists(username) {
    return getUser(username, false)
        .then((user) => user ? true : false);
}

/**
 * filter out a map based on a string field containing a substring.
 * 
 * @param {*} obj 
 * @param {*} field 
 * @param {*} value 
 */
function filterInObj(obj, field, value) {
    const out = {};
    Object.keys(obj).forEach(k => {
        if (obj[k][field].includes(value)) {
            out[k] = obj[k];
        }
    });
    return out;
}

/**
 * Search users by name or email.paginates results.
 * 
 * @param {*} [name] 
 * @param {*} [email] 
 * @param {*} [start] - pagination index (inclusive)
 * @param {*} [n] - pagination length
 */
function searchUsers(name, email, start, n) {
    const length = Object.keys(users).length;
    if (!start) {
        start = 1;
    }
    if (!n) {
        n = length;
    }
    let data = users;
    if (name) {
        data = filterInObj(data, "name", name);
    }
    if (email) {
        data = filterInObj(data, "email", email);
    }
    return Promise.resolve({
        isLast: start + n >= length,
        data: pageResults(data, start, n)
    });
}

/**
 * 
 * @param {*} u 
 */
function saveUser(u) {
    u.__v = (parseInt(u.__v) + 1).toString();
    u.updatedOn = new Date();
    return Promise.resolve(u);
}

/**
 * 
 * @param {*} user 
 */
function addUser(user) {
    const now = new Date();
    //user
    const entry = {
        username: user.username,
        'name': user.name,
        'email': user.email,
        description: user.description,
        createdOn: now,
        updatedOn: now,
        "__v": "0"
    };
    if (user.photo) {
        entry.photo = {
            ...user.photo
        };
    }

    users[entry.username] = entry;
    return Promise.resolve(users[entry.username]);
}

/**
 * 
 * @param {*} username 
 */
function deleteUser(username) {
    delete users[username];
    return Promise.resolve(true);
}


//
// authentication
//

// data
const users_pwd = {};

function getAuthz(username) {
    if (users_pwd[username]) {
        const user = users_pwd[username];
        return Promise.resolve(user);
    } else {
        return Promise.resolve(false);
    }
}

/**
 * 
 * @param {*} username 
 */
function deleteAuthz(username) {
    delete users_pwd[username];
    return Promise.resolve(true);
}

/**
 * returns a promise that resolves to an object
 * {
 *   data:    // the user
 *   created: // boolean flag
 * }
 * 
 * @param {*} newID 
 */
function upsertAuthz(newID, password, roles) {
    if (users_pwd[newID]) {
        // update
        users_pwd[newID].password = password;
        users_pwd[newID].roles = roles ? roles : users_pwd[newID].roles;
        users_pwd[newID].updatedOn = new Date();
        return saveAuthz(users_pwd[newID])
            .then(u => Promise.resolve({
                data: u,
                created: false
            }));
    } else {
        // insert
        return addAuthz(newID, password, roles);
    }
}

/**
 * returns a promise that resolves to an object
 * {
 *   data:    // the user
 *   created: // boolean flag
 * }
 * 
 * @param {*} username 
 */
function addAuthz(username, password, roles) {
    if (users_pwd[username]) {
        return Promise.reject(new ConflictError(`Username "${username}" is already in use`));
    }
    const now = new Date();
    users_pwd[username] = {
        username: username,
        password: password,
        roles: roles,
        createdOn: now,
        updatedOn: now,
        "__v": "0",
        failedLogins: 0,
        resetToken: null,
        lastLogin: null
    };
    return Promise.resolve({
        data: users_pwd[username],
        created: true
    });
}

/**
 * returns a promise that resolves to the authz object
 * 
 * @param {*} u 
 */
function saveAuthz(u) {
    const now = new Date();
    u.updatedOn = now;
    u.__v = (parseInt(u.__v) + 1).toString();
    return Promise.resolve(u);
}

/////////////////////////////
// MODULE EXPORTS

export function MessageRepository() {
    return {
        addMessage,
        getMessage,
        deleteMessage,
        saveMessage,
        searchMessagesAfter,

        countOfMessages
    };
}

export function TagRepository() {
    return {
        searchTagsAfter,
        getTagCloud
    };
}

export function UserRepository() {
    return {
        addUser,
        getUser,
        userExists,
        deleteUser,
        saveUser,
        searchUsers
    };
}

export function AuthzRepository() {
    return {
        addAuthz,
        getAuthz,
        upsertAuthz,
        saveAuthz,
        deleteAuthz
    };
}