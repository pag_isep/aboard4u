/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// MongoDB persistence data layer implementation
//
// all methods return a Promise
//

import debug from "debug";
const LOGGER=debug("pag:mserver:dal");

import {
    ConcurrencyError,
    NotFoundError,
    //UnauthenticatedError,
    //ConflictError,
    //ForbiddenError
} from "../exceptions.js";

import mongoose from "mongoose";


// Optimistic concurrency control for moongose
import {
    updateIfCurrentPlugin
} from 'mongoose-update-if-current';

/* Optimistic Concurrency Control: set as Global plugin */
mongoose.plugin(updateIfCurrentPlugin);

/**
 * Checks if the MongoDB connection is opened and the models/schemas are compiled
 */
function isConnectionOpen() {
    return mongoose.connection.readyState == 1 && User && Message && Authz;
}


/**
 * connect to the database.
 * @returns a promise
 */
function connectMongo(dbUrl) {
    // connect to mongodb
    const promise = mongoose.connect(
            dbUrl, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
        .then(() => LOGGER("Sucessfull connection to DB"))
        .catch((err) => LOGGER("Error connecting to database", err));

    // build models and schemas without waiting for the connection to be established
    User = buildUserSchemaAndModel();
    Message = buildMessageSchemaAndModel();
    Authz = buildAuthzSchemaAndModel();

    LOGGER("MongoDB models compiled.");

    // setup global error handler
    const db = mongoose.connection;
    db.on("error", function () {
        console.error.bind(console, "Error on mongodb connection\n");
    });

    return promise;
}

/**
 * translates MongoDB errors to the application layer 
 * 
 * @param {*} err 
 */
function handleMongoError(err) {
    // just pass thru NotFoundError to application layer
    if (err.name === "NotFoundError") {
        throw err;
    }

    // handle mongodb errors
    LOGGER("Error on persistence layer", err);
    if (err.name === "VersionError") {
        return Promise.reject(new ConcurrencyError(`Conflict on fields: ${err.modifiedPaths}`));
    } else {
        throw err;
    }
}


//
// messages
//

// model
let Message;

/**
 * build the Message schema
 */
function buildMessageSchemaAndModel() {
    const messageSchema = mongoose.Schema({
        id: {
            type: String,
            index: {
                unique: true,
                dropDups: true
            }
        },
        title: {
            type: String,
            required: true
        },
        text: {
            type: String,
            required: true
        },
        authorUsername: String,
        createdOn: Date,
        publishedOn: Date,
        updatedOn: {
            type: Date,
            default: Date.now
        },
        tags: [String],
        trivia: {
            // empty object marks this as a Mixed "any" type to handle dynamic properties
            //createdOn: String
        }
    });

    // on every save, add the date
    messageSchema.pre("save", function (next) {
        const currentDate = new Date();
        this.updatedOn = currentDate;
        if (!this.createdOn) {
            this.createdOn = currentDate;
        }

        // issue #94 Messages should be sorted by publishing date descending order
        // issue #7 message's publishing date attribute (with the same value as creation date)
        this.publishedOn = this.createdOn;

        next();
    });

    return mongoose.model("Message", messageSchema);
}

/**
 * 
 * @param {*} newID 
 * @param {object} msg 
 * @param {*} authorUsername 
 */
function addMessage(newID, msg, authorUsername) {
    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    const now = new Date();
    const m = new Message({
        id: newID,
        title: msg.title,
        text: msg.text,
        authorUsername: authorUsername,
        createdOn: now,
    });
    if (msg.trivia) {
        m.trivia = msg.trivia;
    }
    if (msg.tags) {
        m.tags = msg.tags;
    }
    return m.save()
        .catch(handleMongoError);
}

/**
 * 
 * @param {*} id 
 * @param {boolean} [reject] - indicates if the function should resolve to null or reject if the message does not exist
 * @returns a promise that resolves to the message. 
 */
function getMessage(id, reject = true) {
    //LOGGER("get message", id);
    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    return Message
        .find({
            id
        })
        .exec()
        .then(function (messages) {
            if (messages.length > 0) {
                return messages[0];
            } else {
                if (reject) {
                    return Promise.reject(new NotFoundError(`Unknown message ${id}`, id));
                } else {
                    return null;
                }
            }
        })
        .catch(handleMongoError);
}

/**
 * searches messages by text, author or creation date. paginates results.
 * 
 * @param {*} first - pagination length
 * @param {*} [after] - pagination index (exclusive)
 * @param {MessageSearchCriteria} [spec] 
 */
function searchMessagesAfter(first, after, spec = {}) {
    if (!first) {
        //TODO throw BadRequestError to allow adding a json response
        throw new RangeError("Missing mandatory parameters");
    }

    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    const criteria = _createSearchCriteria(spec);
    if (after) {
        criteria.id = {
            '$gt': after
        };
    }

    return Message.find(criteria)
        .sort({
            publishedOn: -1,
            id: 1
        })
        .limit(first)
        .exec()
        .then((messages) => {
            LOGGER('search messages with criteria', criteria, 'returned ', messages.length, 'results.');
            const length = messages.length;
            return Promise.resolve({
                isLast: first > length,
                data: messages
            });
        })
        .catch(handleMongoError);
}

function _createSearchCriteria(spec = {}) {
    const criteria = {};
    if (spec.author) {
        criteria.authorUsername = spec.author;
    }
    if (spec.title) {
        criteria.title = new RegExp('.*' + spec.title + '.*');
    }
    if (spec.text) {
        criteria.text = new RegExp('.*' + spec.text + '.*');
    }
    if (spec.createdFrom || spec.createdTo) {
        criteria.createdOn = {};
        if (spec.createdFrom) {
            criteria.createdOn.$gte = spec.createdFrom;
        }
        if (spec.createdTo) {
            criteria.createdOn.$lte = spec.createdTo;
        }
    }
    if (spec.tags) {
        criteria.tags = {
            "$all": spec.tags
        };
    }
    return criteria;
}

/**
 * 
 * @param {*} m 
 */
function saveMessage(m) {
    if (!isConnectionOpen() || !m.save) {
        throw new Error("Problem during db connection startup. Or object passed is not a model");
    }

    return m.save()
        .catch(handleMongoError);
}

/**
 * Deletes one message by id. 
 * 
 * @param {*} id the message to delete 
 * @param {*} rev the expected revision of the message in the data store
 * @returns a Promise that resolves to true if the message was deleted. If the message was not found or was in a diferent revision than the expected one, the promise resolves to false
 */
function deleteMessage(id, rev) {
    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    return Message
        .deleteOne({
            id,
            __v: rev
        })
        .then(mongooseDeleteResult => mongooseDeleteResult.deletedCount == 1)
        .catch(handleMongoError);
}

/**
 * returns the number of messages
 * 
 * @param {*} spec 
 */
function countOfMessages(spec = {}) {
    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    const criteria = _createSearchCriteria(spec);
    return Message.countDocuments(criteria)
        .catch(handleMongoError);
}

//
// Tags
//

const TAG_AGGREGATE_PIPELINE = [{
        "$unwind": {
            path: "$tags"
        }
    },
    {
        "$group": {
            _id: "$tags"
        }
    },
    {
        "$sort": {
            "_id": 1
        }
    }
];

/**
 * searches tags by text. paginates results.
 * 
 * @param {*} first - pagination length
 * @param {*} [after] - pagination index (exclusive)
 * @param {TagSearchCriteria} [spec] 
 */
function searchTagsAfter(first, after, spec = {}) {
    if (!first) {
        //TODO throw BadRequestError to allow adding a json response
        throw new RangeError("Missing mandatory parameters");
    }

    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    // build pipeline from template
    const pipeline = TAG_AGGREGATE_PIPELINE.map(e => e);

    if (after) {
        //FIXME
        let stage = {
            "$project": {
                tags: {
                    "$filter": {
                        input: "$tags",
                        "as": "tag",
                        cond: {
                            "$gt": ["$$tag", after]
                        }
                    }
                }
            }
        };

        pipeline.unshift(stage);
    }

    if (spec.text) {
        const ignoreMessagesStage = {
            "$match": {
                tags: new RegExp('^' + spec.text + '.*')
            }
        };

        pipeline.unshift(ignoreMessagesStage);

        const ignoreTagsStage = {
            "$match": {
                _id: new RegExp('^' + spec.text + '.*')
            }
        };

        pipeline.push(ignoreTagsStage);
    }

    if (first) {
        pipeline.push({
            "$limit": first
        });
    }

    //TODO decouple tags from message
    return Message.aggregate(pipeline)
        .exec()
        .then(counts => counts.map(t => t._id))
        .then((tags) => {
            //LOGGER('search tags with criteria', criteria, 'returned ', tags.length, 'results.');
            const length = tags.length;
            return {
                isLast: first > length,
                data: tags
            };
        })
        .catch(handleMongoError);
}

const TAG_CLOUD_AGGREGATE_PIPELINE = [{
        "$unwind": {
            path: "$tags",
            preserveNullAndEmptyArrays: true
        }
    },
    {
        "$group": {
            _id: "$tags",
            count: {
                "$sum": 1
            }
        }
    },
    {
        "$project": {
            _id: false,
            tag: "$_id",
            count: true
        }
    },
    {
        "$sort": {
            "tag": 1
        }
    }
];

/**
 * build a tag cloud.
 * 
 * @param {Object} [criteria] 
 */
function getTagCloud(criteria = {}) {
    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    // build pipeline from template
    const pipeline = TAG_CLOUD_AGGREGATE_PIPELINE.map(e => e);

    if (criteria.author) {
        pipeline.unshift({
            "$match": {
                authorUsername: {
                    "$eq": criteria.author
                }
            }
        });
    }

    //TODO decouple tags from message
    return Message.aggregate(pipeline)
        .exec()
        .catch(handleMongoError);
}

//
// users
//

// model
let User;

/**
 * build the User schema
 */
function buildUserSchemaAndModel() {
    const schema = mongoose.Schema({
        username: {
            type: String,
            index: {
                unique: true,
                dropDups: true
            }
        },
        name: String,
        email: String,
        description: String,
        photo: {
            caption: String,
            path: String,
            mimetype: String,
        },
        createdOn: Date,
        updatedOn: {
            type: Date,
            default: Date.now
        }
    });

    // on every save, add the date
    schema.pre("save", function (next) {
        // get the current date
        const currentDate = new Date();

        // change the updated_at field to current date
        this.updatedOn = currentDate;

        // if created_at doesn't exist, add to that field
        if (!this.createdOn) {
            this.createdOn = currentDate;
        }
        next();
    });

    return mongoose.model("User", schema);
}

/**
 * 
 * @param {*} user 
 */
function addUser(user) {
    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    const u = new User({
        username: user.username,
        name: user.name,
        email: user.email,
        description: user.description,
    });
    if (user.photo) {
        u.photo = {
            ...user.photo
        };
    }

    return u.save()
        .catch(handleMongoError);
}

/**
 * 
 * @param {*} u 
 */
function saveUser(u) {
    if (!isConnectionOpen() || !u.save) {
        throw new Error('Problem during db connection startup. Or object passed is not a model.');
    }

    return u.save()
        .catch(handleMongoError);
}

/**
 * 
 * @param {*} username 
 * @param {boolean} [reject] - indicates if the function should resolve to null or reject if the user does not exist
 * @returns a promise that resolves to the user. 
 */
function getUser(username, reject = true) {
    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    return User
        .find({
            username
        })
        .then(function (users) {
            if (users.length > 0) {
                return users[0];
            } else {
                if (reject) {
                    return Promise.reject(new NotFoundError(`Unknown user ${username}`, username));
                } else {
                    return null;
                }
            }
        })
        .catch(handleMongoError);
}

/**
 * checks if a username is already in use
 * @param {*} username 
 */
function userExists(username) {
    return getUser(username, false)
        .then((user) => user ? true : false);
}

/**
 * Search users by name or email. paginates results
 * @param {*} [name] 
 * @param {*} [email] 
 * @param {*} start - pagination index (inclusive)
 * @param {*} n - pagination length
 */
function searchUsers(name, email, start, n) {
    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    const criteria = {};
    if (name) {
        criteria.name = new RegExp('.*' + name + '.*');
    }
    if (email) {
        criteria.email = email;
    }
    return User.find(criteria)
        .skip(start - 1)
        .limit(n)
        .exec()
        .then(function (users) {
            LOGGER('search users with criteria', criteria, 'returned', users.length, 'results.');
            const length = users.length;
            return Promise.resolve({
                isLast: n > length,
                data: users
            });
        })
        .catch(handleMongoError);
}

/**
 * Deletes one user. 
 * 
 * @param {*} username the user to delete 
 * @param {*} rev the expected revision of the user in the data store
 * @returns a Promise that resolves to true if the user was deleted. If the user was not found or was in a diferent revision than the expected one, the promise resolves to false
 */
function deleteUser(username, rev) {
    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    return User
        .deleteOne({
            username,
            __v: rev
        })
        .then((mongooseDeleteResult) => mongooseDeleteResult.deletedCount === 1)
        .catch(handleMongoError);
}

//
// authentication
//

// model
let Authz;

/**
 * build the Authz schema
 */
function buildAuthzSchemaAndModel() {
    const schema = mongoose.Schema({
        username: {
            type: String,
            index: {
                unique: true,
                dropDups: true
            }
        },
        password: {
            type: String,
            required: true
        },
        failedLogins: Number,
        resetToken: String,
        roles: [String],
        lastLogin: Date,
        createdOn: Date,
        updatedOn: {
            type: Date,
            default: Date.now
        }
    });
    // on every save, add the date
    schema.pre("save", function (next) {
        const currentDate = new Date();
        this.updatedOn = currentDate;
        if (!this.createdOn) {
            this.createdOn = currentDate;
        }

        next();
    });
    return mongoose.model("Authz", schema);
}

/**
 * 
 * @param {*} username 
 */
function getAuthz(username) {
    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    return Authz
        .find({
            username
        })
        .then(function (user) {
            if (user.length > 0) {
                return user[0];
            } else {
                return false;
            }
        })
        .catch(handleMongoError);
}

/**
 * 
 * @param {*} username 
 */
function deleteAuthz(username) {
    return Authz
        .deleteOne({
            username
        })
        .then((mongooseDeleteResult) => mongooseDeleteResult.n === 1)
        .catch(handleMongoError);
}

/**
 * 
 * @param {*} username 
 * @param {*} password 
 * @param {*} roles 
 */
function upsertAuthz(username, password, roles) {
    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    const now = new Date();
    return Authz
        .find({
            username
        })
        .then(function (authz) {
            if (authz.length > 0) {
                // update
                authz[0].password = password;
                authz[0].roles = roles || authz.roles;
                authz[0].updatedOn = now;
                return authz[0]
                    .save()
                    .then((u) => Promise.resolve({
                        data: u,
                        created: false
                    }));
            } else {
                // insert
                return addAuthz(username, password, roles);
            }
        })
        .catch(handleMongoError);
}

/**
 * 
 * @param {*} username 
 * @param {*} password 
 * @param {*} roles 
 */
function addAuthz(username, password, roles) {
    if (!isConnectionOpen()) {
        throw new Error("Problem during db connection startup.");
    }

    const now = new Date();
    const u = new Authz({
        username,
        password,
        roles,
        createdOn: now,
        updatedOn: now,
        failedLogins: 0,
        resetToken: null,
        lastLogin: null
    });
    return u.save()
        .then((nu) => Promise.resolve({
            data: nu,
            created: true
        }))
        .catch(handleMongoError);
}

/**
 * 
 * @param {*} u 
 */
function saveAuthz(u) {
    return saveUser(u);
}


/////////////////////////////
// MODULE EXPORTS

export function MessageRepository() {
    return {
        addMessage,
        getMessage,
        deleteMessage,
        saveMessage,
        searchMessagesAfter,

        countOfMessages,
    };
}

export function TagRepository() {
    return {
        searchTagsAfter,
        getTagCloud
    };
}

export function UserRepository() {
    return {
        addUser,
        getUser,
        userExists,
        deleteUser,
        saveUser,
        searchUsers
    };
}

export function AuthzRepository() {
    return {
        addAuthz,
        getAuthz,
        upsertAuthz,
        saveAuthz,
        deleteAuthz
    };
}

// start the connection to MongoDB and compile the models
(async function start() {
    //TODO should this setting come from the general settings module or should it really be local just to this module?
    const DB_URL = process.env.ABOARD4U_DB_URL || "mongodb://localhost:10050/data";

    await connectMongo(DB_URL);
})();