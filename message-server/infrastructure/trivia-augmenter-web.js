/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

import debug from 'debug';
const LOGGER = debug('pag:mserver');
// http client
import { fetch } from  '../util/fetch-with-proxy.js';

/**
 * returns a map containing the trivia factoid for the date. The key is a string in the format "MM/DD"
 * 
 * @param {*} today 
 * @returns an object with keys in the format "MM/DD" and string values
 */
function triviaOfDay(today) {
    const month = today.getMonth() + 1;
    const day = today.getDate();
    LOGGER("augmenting with: http://numbersapi.com/%s/%s/date", month, day);

    return fetch(`http://numbersapi.com/${month}/${day}/date`)
        .then(body => body.text())
        .then(function (body) {
            const dateKey = `${month}/${day}`;
            const trivia = {};
            trivia[dateKey] = body;
            return trivia;
        });
}

/////////////////////////////
// MODULE EXPORTS

export function TriviaAugmenter(config, dependencies) {
    return {
        triviaOfDay
    };
}