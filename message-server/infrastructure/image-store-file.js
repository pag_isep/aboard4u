/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:11 */

"use strict";

//
// file folder for user's photo storage
// files are uploaded to the upload folder and then copied to this repo
// to distinguish from other uploaded files
//
const PHOTO_REPO_PATH = process.cwd() + "/photo-repo/";

import fs from "fs";
import fsp from 'fs/promises';

/**
 * 
 * @param {*} uploadedFilename 
 * @param {*} storeFilename 
 * @param {*} caption 
 * @param {*} mimetype 
 */
function storePhotoFile(uploadedFilename, storeFilename, caption, mimetype) {
    const ext = mimetype.split("/")[1];
    const path = storeFilename + "." + ext;
    const photoPath = PHOTO_REPO_PATH + path;

    return fsp.rename(uploadedFilename, photoPath)
        .then(function () {
            return {
                caption,
                path,
                mimetype
            };
        });
}


// automatically create the photo repo folder on module loading, if it does not exist
(function () {
    if (!fs.existsSync(PHOTO_REPO_PATH)) {
        fs.mkdirSync(PHOTO_REPO_PATH);
    }
})();

/////////////////////////////
// MODULE EXPORTS

export function ImageStore(config, dependencies) {
    return {
        PHOTO_REPO_PATH,
        storePhotoFile
    };
}