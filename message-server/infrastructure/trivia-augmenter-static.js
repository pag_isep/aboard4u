/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:11 */

"use strict";

const THE_STATIC_TRIVIA_FACTOID = "The knowledge base we have is empty, so we know nothing about this day...";


/**
 * returns a static factoid text no matter what day
 * 
 * @param {Date} today 
 * @returns a Promise to an object with keys in the format "MM/DD" and string values
 */
async function triviaOfDay(today) {
    const month = today.getMonth() + 1;
    const day = today.getDate();

    const dateKey = `${month}/${day}`;
    const trivia = {};
    trivia[dateKey] = THE_STATIC_TRIVIA_FACTOID;
    return trivia;
}

/////////////////////////////
// MODULE EXPORTS

export function TriviaAugmenter(config, dependencies) {
    return {
        triviaOfDay
    };
}