/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */



/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// setting up the REST routes for the Message resource
//

import multer from 'multer';
import { readFile } from 'fs/promises';
import MessageResource from "./message-handler.js";

/////////////////////////////
// MODULE EXPORTS

export async function initRoutes(config, dependencies) {
    const app = dependencies.app;
    const dualModeAuth = dependencies.dualModeAuth;
    const authenticate = dependencies.authenticate;
    const handleAsNotAllowed = dependencies.handleAsNotAllowed;
    const ensureSessionLinks = dependencies.ensureSessionLinks;
    const ensureContentType = dependencies.ensureContentType;
    const ensureJsonSchema = dependencies.ensureJsonSchema;

    //
    // HTML form posting thru encoding multipart/form-data
    //
    const formPost = multer({
        limits: {
            fileSize: 10000
        }
    }).none();

    //
    // load resource modules
    //
    const messageResource = new MessageResource(config, dependencies);

    const API_PATH = config.server.rest.API_PATH;

    // B4U-37 json schema validation
    const newMessageInputSchema = JSON.parse(await readFile(new URL("file://" + process.cwd() + "/message-server/assets/schemas/message/newMessageData.schema.json", import.meta.url)));
    const ensureNewMessageSchema = ensureJsonSchema({
        body: newMessageInputSchema
    });
    const messageInputSchema = JSON.parse(await readFile(new URL("file://" + process.cwd() +"/message-server/assets/schemas/message/messageDataInput.schema.json")));
    const ensureMessageSchema = ensureJsonSchema({
        body: messageInputSchema
    });
    const newMessageInputCallbackSchema = JSON.parse(await readFile(new URL("file://" + process.cwd() +"/message-server/assets/schemas/message/newMessageDataWithCallback.schema.json")));
    const ensureNewMessageCallbackSchema = ensureJsonSchema({
        body: newMessageInputCallbackSchema
    });
    const callbackSchema = JSON.parse(await readFile(new URL("file://" + process.cwd() +"/message-server/assets/schemas/callback.schema.json")));
    const ensureCallbackSchema = ensureJsonSchema({
        body: callbackSchema
    });

    //
    // MESSAGE resource
    //
    // GET messages is dual mode authenticated to hide "private" data/links from unathenticated users
    app.route(API_PATH + "/message")
        .get(dualModeAuth, ensureSessionLinks, messageResource.handleGetMessages)
        .put(handleAsNotAllowed)
        .post(authenticate, ensureSessionLinks, ensureContentType(['multipart/form-data', 'application/x-www-form-urlencoded', 'application/json']), formPost, ensureNewMessageSchema, messageResource.handlePostMessages)
        .patch(handleAsNotAllowed)
        .delete(handleAsNotAllowed);

    // GET message/:id is dual mode authenticated to hide "private" data/links from unathenticated users
    app.route(API_PATH + "/message/:id")
        .get(dualModeAuth, ensureSessionLinks, messageResource.handleGetMessageItem)
        .put(authenticate, ensureSessionLinks, ensureContentType(['application/json']), ensureNewMessageSchema, messageResource.handlePutMessageItem)
        .post(authenticate, ensureSessionLinks, ensureContentType(['multipart/form-data', 'application/x-www-form-urlencoded']), formPost, ensureMessageSchema, messageResource.handlePostMessageItem)
        .patch(authenticate, ensureSessionLinks, ensureContentType(['application/merge-patch+json', 'application/json']), ensureMessageSchema, messageResource.handlePatchMessageItem)
        .delete(authenticate, ensureSessionLinks, messageResource.handleDeleteMessageItem);


    //
    // MESSAGE augmentation with trivia resource
    //
    app.route(API_PATH + "/message-trivia")
        .post(authenticate, ensureSessionLinks, ensureContentType(['application/json']), ensureNewMessageCallbackSchema, messageResource.handlePostMessagesForAugmentation)
        .get(handleAsNotAllowed)
        .put(handleAsNotAllowed)
        .patch(handleAsNotAllowed)
        .delete(handleAsNotAllowed);
    app.route(API_PATH + "/message/:id/trivia")
        .post(authenticate, ensureSessionLinks, ensureContentType(['application/json']), ensureCallbackSchema, messageResource.handlePostMessageItemForAugmentation)
        .get(handleAsNotAllowed)
        .put(handleAsNotAllowed)
        .patch(handleAsNotAllowed)
        .delete(handleAsNotAllowed);
}
