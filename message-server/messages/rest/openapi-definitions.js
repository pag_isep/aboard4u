/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// a documentation file to hold the common definitions of models used in the API
//


//
// schemas
//


/**
 * @swagger
 * components:
 *   schemas:
 *     attrMessageId:
 *       type: string
 *       maxLength: 64
 *       pattern: '^[a-zA-Z]+[\-\.\w]*$'
 *       example: ab3D4f
 *     attrMessageTitle:
 *       type: string
 *       maxLength: 50
 *       pattern: '^[- \w\.!?,;:-=><\/()&%$+*~#]*$'
 *       example: Hello
 *     attrMessageText:
 *       type: string
 *       maxLength: 2000
 *       pattern: '^[- \w\.!?,;:-=><\/()&%$+*~#]*$'
 *       example: Hello everyone. My name is Franz.
 *     attrMessageTag:
 *       type: string
 *       maxLength: 20
 *       pattern: '^[a-zA-Z]+[\-\w]*$'
 *       example: greetings
 *     attrMessageTags:
 *       type: array
 *       maxItems: 10
 *       items:
 *         $ref: '#/components/schemas/attrMessageTag'
 *       uniqueItems: true
 *       example: ["greetings", "introduction"]
 *     attrMessageTrivia:
 *       type: object
 *       additionalProperties: true
 *       properties:
 *         createdOn:
 *           description: a trivia fact about the date the message was posted
 *           type: string
 *           maxLength: 2000
 *           pattern: '^[ \w\.!?,;:\-=><\/&%\$\+\*~#]*$'
 *           example: "December 17th is the day in 1935 that First flight of the Douglas DC-3 airplane."
 *     messageData:
 *       type: object
 *       additionalProperties: false
 *       description: a message
 *       required:
 *         - id
 *         - title
 *         - text
 *         - authorUsername
 *         - createdOn
 *         - updatedOn
 *       properties:
 *         id:
 *           $ref: '#/components/schemas/attrMessageId'
 *         title:
 *           $ref: '#/components/schemas/attrMessageTitle'
 *         text:
 *           $ref: '#/components/schemas/attrMessageText'
 *         authorUsername:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         createdOn:
 *           type: string
 *           format: date-time
 *         updatedOn:
 *           type: string
 *           format: date-time
 *         tags:
 *           $ref: '#/components/schemas/attrMessageTags'
 *         trivia:
 *           $ref: '#/components/schemas/attrMessageTrivia'
 *     messageDataCollection:
 *       description: collection of messages
 *       type: array
 *       maxItems: 500
 *       items:
 *         $ref: '#/components/schemas/messageData'
 *     messageDataCollectionSummary:
 *       description: summary of a collection of messages
 *       type: object
 *       additionalProperties: false
 *       properties:
 *         count:
 *           type: integer
 *           format: int32
 *           minimum: 0
 *           maximum: 2147483647
 *           description: total number of messages
 *     messageItem:
 *       description: a message
 *       type: object
 *       additionalProperties: false
 *       properties:
 *         data:
 *           $ref: '#/components/schemas/messageData'
 *         links:
 *           $ref: '#/components/schemas/links'
 *     messageCollection:
 *       description: collection of message items
 *       type: object
 *       additionalProperties: false
 *       properties:
 *         data:
 *           type: array
 *           maxItems: 500
 *           items:
 *             $ref: '#/components/schemas/messageItem'
 *         links:
 *           $ref: '#/components/schemas/links'
 *         template:
 *           $ref: '#/components/schemas/template'
 *         queries:
 *           $ref: '#/components/schemas/queries'
 *     messageCollectionSummary:
 *       description: summary of a collection of message items
 *       type: object
 *       additionalProperties: false
 *       properties:
 *         data:
 *           type: object
 *           properties:
 *             count:
 *               type: integer
 *               format: int32
 *               minimum: 0
 *               maximum: 2147483647
 *               description: total number of messages
 *         links:
 *           $ref: '#/components/schemas/links'
 *     editMessageForm:
 *       description: message data input for partial update thru an HTML form
 *       type: object
 *       additionalProperties: false
 *       required:
 *         - __etag
 *       properties:
 *         __etag:
 *           $ref:  '#/components/schemas/attrETag'
 *         title:
 *           $ref: '#/components/schemas/attrMessageTitle'
 *         text:
 *           $ref: '#/components/schemas/attrMessageText'
 *         tags:
 *           $ref: '#/components/schemas/attrMessageTags'
 *     messageDataInput:
 *       description: message data input for partial update
 *       type: object
 *       additionalProperties: false
 *       properties:
 *         title:
 *           $ref: '#/components/schemas/attrMessageTitle'
 *         text:
 *           $ref: '#/components/schemas/attrMessageText'
 *         tags:
 *           $ref: '#/components/schemas/attrMessageTags'
 *     newMessageDataInput:
 *       description: a new message to post
 *       type: object
 *       additionalProperties: false
 *       required:
 *         - title
 *         - text
 *       properties:
 *         title:
 *           $ref: '#/components/schemas/attrMessageTitle'
 *         text:
 *           $ref: '#/components/schemas/attrMessageText'
 *         tags:
 *           $ref: '#/components/schemas/attrMessageTags'
 *     messageWithCallbackDataInput:
 *       description: a message
 *       type: object
 *       additionalProperties: false
 *       required:
 *         - title
 *         - text
 *       properties:
 *         title:
 *           $ref: '#/components/schemas/attrMessageTitle'
 *         text:
 *           $ref: '#/components/schemas/attrMessageText'
 *         tags:
 *           $ref: '#/components/schemas/attrMessageTags'
 *         callback:
 *           type: string
 *           format: uri
 *           example: https://example.org/postback-trivia
 *         callbackRef:
 *           $ref: '#/components/schemas/correlationRef'
 *     messageDataPostback:
 *       description: a message
 *       type: object
 *       additionalProperties: false
 *       required:
 *         - id
 *         - title
 *         - text
 *         - tags
 *         - authorUsername
 *         - createdOn
 *         - updatedOn
 *         - correlation
 *       properties:
 *         id:
 *           $ref: '#/components/schemas/attrMessageId'
 *         title:
 *           $ref: '#/components/schemas/attrMessageTitle'
 *         text:
 *           $ref: '#/components/schemas/attrMessageText'
 *         authorUsername:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         createdOn:
 *           type: string
 *           format: date-time
 *         updatedOn:
 *           type: string
 *           format: date-time
 *         tags:
 *           $ref: '#/components/schemas/attrMessageTags'
 *         trivia:
 *           $ref: '#/components/schemas/attrMessageTrivia'
 *         correlation:
 *           type: object
 *           required:
 *             - myRef
 *             - yourRef
 *           properties:
 *             myRef:
 *               $ref: '#/components/schemas/correlationRef'
 *               description: a correlation id
 *             yourRef:
 *               $ref: '#/components/schemas/correlationRef'
 *               description: the correlation id sent on the original request
 *
 */

/**
 * @swagger
 * components:
 *   responses:
 *     OkMessageCollection:
 *       description: the message collection
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/messageDataCollection'
 *         application/vnd.pagsousa.hyper+json:
 *           schema:
 *             $ref: '#/components/schemas/messageCollection'
 *     CreatedMessageItem:
 *       description: Created
 *       headers:
 *         Location:
 *           schema:
 *             type: string
 *             format: uri
 *           description: the id (location) of the newly created message
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/messageData'
 *         application/vnd.pagsousa.hyper+json:
 *           schema:
 *             $ref: '#/components/schemas/messageItem'
 *     OkMessageItem:
 *       description: Ok
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/messageData'
 *         application/vnd.pagsousa.hyper+json:
 *           schema:
 *             $ref: '#/components/schemas/messageItem'
 *       links:
 *         authorOfMessage:
 *           operationId: getUser
 *           description: GET the message's author
 *           parameters:
 *             id: '$response.body#/authorUsername'
 *         deleteMessage:
 *           operationId: deleteMessage
 *           description: DELETE this message by following the `edit` relation
 *           parameters:
 *             id: '$response.body#/id'
 *         editMessage:
 *           operationId: editMessage
 *           description: update this message by PUTing to the `edit` relation's target
 *           parameters:
 *             id: '$response.body#/id'
 *     PreconditionFailedMessageItem:
 *       description: if-match header caused conflict
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/messageData'
 *         application/vnd.pagsousa.hyper+json:
 *           schema:
 *             $ref: '#/components/schemas/messageItem'
 */