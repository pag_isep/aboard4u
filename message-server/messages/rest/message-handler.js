/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

const PAGSOUSA_HYPER_MIMETYPE = "application/vnd.pagsousa.hyper+json";

//
// Message resource
//

import debug from "debug";
const LOGGER = debug("pag:mserver");

import _ from 'lodash';

// http client
import {
    fetch
} from '../../util/fetch-with-proxy.js';


//these will be passed to the module thru the configure() function
let messageController;
let Response;
let Hyper;

let SETTINGS;
let DEPENDENCIES;

//
// hypermedia controls
//

function buildHref(paging, criteria) {
    let href = SETTINGS.server.rest.API_ROOT + "/message?";

    if (paging.first) {
        href = href + "first=" + paging.first;
    }
    if (paging.after) {
        href = href + "&after=" + paging.after;
    }

    if (criteria.author) {
        href += "&author=" + criteria.author;
    }
    if (criteria.text) {
        href += "&text=" + criteria.text;
    }
    if (criteria.title) {
        href += "&title=" + criteria.title;
    }
    if (criteria.createdFrom) {
        href += "&created_from=" + criteria.createdFrom;
    }
    if (criteria.createdTo) {
        href += "&created_to=" + criteria.createdTo;
    }

    return href;
}

/**
 * Creates links element of the collection.
 * follows Collection+JSON specification
 * 
 * @param {Paging} paging 
 * @param {*} out  
 * @param {MessageSearchCriteria} criteria 
 * @param {*} authUser - the authenticated user
 */
function collectionLinks(paging, out, criteria, authUser) {
    const links = [];

    // collection links
    links.push({
        rel: "self",
        href: buildHref(paging, criteria),
    });

    links.push({
        rel: "start",
        href: buildHref({}, criteria)
    });

    if (paging.after) {
        //TODO how to construct the prev link ?
    }

    if (!out.isLast) {
        const nextPage = {
            first: paging.first,
            after: out.data[out.data.length - 1].id
        };
        links.push({
            rel: "next",
            href: buildHref(nextPage, criteria)
        });
    }

    // #41 : edit links are only sent for authenticated requests
    if (authUser) {
        // create-form & edit-form (RFC 6861 https://tools.ietf.org/html/rfc6861)
        links.push({
            rel: "create-form",
            href: SETTINGS.server.SERVER_ROOT + "/forms/post-message.html"
        });

        //TODO to return edit-form in a collection we must support templates
        links.push({
            rel: "edit-form",
            href: SETTINGS.server.SERVER_ROOT + "/forms/edit-message.html?id={0}",
            template: "true"
        });
    }

    return links;
}

/**
 * Creates the link element of an item. Follows Collection + JSON link element.
 * 
 * @param {*} message - the message resource representation
 * @param {*} authUser - the authenticated user
 * @returns {Link[]} the links of this message
 */
function itemLinks(message, authUser) {
    const links = [];
    if (!message) {
        return links;
    }

    const SELF = `${SETTINGS.server.rest.API_ROOT}/message/${message.id}`;

    // standard IANA rel types
    links.push({
        rel: "self",
        href: SELF
    });

    // assumes the client of the application/vnd.pagsousa.hyper+json only needs the data schema
    // as everything else is hypermedia
    links.push({
        rel: "describedBy",
        href: SETTINGS.server.SERVER_ROOT + "/schemas/message/messageData.schema.json"
    });

    // #41 : edit links are only sent for authenticated requests
    if (authUser && authUser.username === message.authorUsername) {
        links.push({
            rel: "edit",
            href: SELF
        });

        // edit-form (RFC 6861 https://tools.ietf.org/html/rfc6861)
        links.push({
            rel: 'edit-form',
            href: SETTINGS.server.SERVER_ROOT + "/forms/edit-message.html?id=" + encodeURIComponent(SELF)
        });
    }

    links.push({
        rel: "collection",
        href: SETTINGS.server.rest.API_ROOT + "/message"
    });

    links.push({
        rel: "author",
        href: SETTINGS.server.rest.API_ROOT + "/user/" + message.authorUsername,
        title: message.authorUsername
    });

    //
    // state-based links
    // - the link is only sent if the message is in a certain object state
    //
    const hasTrivia = message.toObject ? message.toObject().trivia : message.trivia;
    if (!hasTrivia) {
        links.push({
            rel: 'https://aboard4u.herokuapp.com/rel/trivia',
            href: SELF + "/trivia"
        });
    }

    return links;
}

/**
 * Creates template element of a message.
 * Follows Collection+JSON specification
 */
function createTemplateOfMessage() {
    return {
        data: [{
            name: "title",
            value: "",
            prompt: "Title"
        },
        {
            name: "text",
            value: "",
            prompt: "Message"
        }
        ]
    };
}

/**
 * A data template.
 * 
 * @typedef {Object} DataTemplate
 * @property {string} name     - the name of the parameter
 * @property {string} [title] - a text to present to a user
 * @property {*} value         - the default value for the parameter
 */

/**
 * A query template specification.
 * 
 * @typedef {Object} Query
 * @property {string}       rel      - a relationship type
 * @property {uri}          href     - the target URL of the relation
 * @property {string}       [name]   - a unique identifier of the query
 * @property {string}       [title] - a text to present to a user representing this query's intention
 * @property {DataTemplate} [data]   - an array of template parameters for this query 
 */

/**
 * Creates queries element of the message collection.
 * follows Collection+JSON specification
 * 
 * @returns {Query[]} an array of the queries that are allowed to perform on the collection
 */
function createQueriesOfCollection() {
    const queries = [];

    queries.push({
        rel: "search",
        href: SETTINGS.server.rest.API_ROOT + "/message",
        name: "search",
        title: "Search messages:", // ref #29
        data: [{
            name: "user",
            value: "",
            prompt: "Username"
        },
        {
            name: "text",
            value: "",
            prompt: "Text"
        },
        {
            name: "created_from",
            value: "",
            prompt: "From"
        },
        {
            name: "created_to",
            value: "",
            prompt: "To"
        }
        ]
    });

    queries.push({
        rel: "search",
        href: SETTINGS.server.rest.API_ROOT + "/message",
        name: "search-by-user",
        title: "Messages of user", // ref #29
        data: [{
            name: "user",
            value: "",
            prompt: "Username"
        }]
    });

    queries.push({
        rel: "search",
        href: SETTINGS.server.rest.API_ROOT + "/message",
        name: "search-by-date",
        title: "Search messages by date", // ref #29
        data: [{
            name: "created_from",
            value: "",
            prompt: "From"
        },
        {
            name: "created_to",
            value: "",
            prompt: "To"
        }
        ]
    });

    queries.push({
        rel: "search",
        href: SETTINGS.server.rest.API_ROOT + "/message?created_from=-7d",
        name: "search-last-7-days",
        title: "Last week's messages", // ref #29
        data: []
    });

    return queries;
}


/**
 * cleanup one message to remove attributes that must not be exposed in the API.
 * 
 * @param {*} message 
 */
function cleanup1Message(message) {
    const from = message.toObject ? message.toObject() : message;
    const data = _.cloneDeep(from);

    //delete data.authorUsername;

    return data;
}

/**
 * cleanup an array of messages by cleaning up each message.
 * 
 * @param {*} messageArray 
 */
function cleanupMessages(messageArray) {
    return messageArray.map(cleanup1Message);
}

/**
 * Performs content negotiation and sends the response in the appropriate format.
 *  
 * @param {*} res
 * @param {*} authzUser
 * @param {*} out 
 * @param {MessageSearchCriteria} criteria 
 * @param {Paging} paging 
 */
function formatResponseCollection(res, authzUser, out, criteria, paging) {
    LOGGER("Formatting collection: N=", out.data.length, "start=", paging.start, "n=", paging.n, "isLast:", out.isLast);
    const links = collectionLinks(paging, out, criteria, authzUser);
    const response = new Response(res, SETTINGS);
    return response.format({
        'application/json': () => {
            return response.ok(
                cleanupMessages(out.data),
                links
            );
        },
        'application/vnd.pagsousa.hyper+json': () => {
            return response.ok(
                Hyper.collection(
                    out.data,
                    (i) => itemLinks(i, authzUser),
                    links,
                    createTemplateOfMessage(),
                    createQueriesOfCollection(),
                    cleanupMessages
                )
            );
        }
    });
}

/**
 * 
 * @param {*} res
 * @param {object} entry - an object with a data property and a created flag 
 */
function formatResponse(res, entry, user) {
    const response = new Response(res, SETTINGS);

    // if the accept header is not sent, the first callback is used
    // if the desired content is not present the default callback is used
    // if there is no 'default', the server will reply with 406 Not Acceptable
    return response.format({
        'application/json': () => {
            const msg = cleanup1Message(entry.data);
            const links = itemLinks(entry.data, user);
            if (entry.created) {
                return response.created(
                    msg,
                    SETTINGS.server.rest.API_ROOT + "/message/" + entry.data.id,
                    links
                );
            } else {
                return response.ok(msg, links);
            }
        },
        'application/vnd.pagsousa.hyper+json': () => {
            const msg = Hyper.item(entry.data, (i) => itemLinks(i, user), cleanup1Message);
            if (entry.created) {
                return response.created(msg, SETTINGS.server.rest.API_ROOT + "/message/" + entry.data.id);
            } else {
                return response.ok(msg);
            }
        }
    });
}

/**
 * 
 * @param {*} res 
 * @param {*} err 
 * @param {*} authzUser 
 */
function formatError(res, err, authzUser) {
    const response = new Response(res, SETTINGS);

    return response.format({
        'application/json': () => {
            return response.error(
                err,
                cleanup1Message,
                (m) => {
                    return {
                        ...Response.etagHeader(m),
                        ...Response.linkHeader(itemLinks(m, authzUser))
                    };
                }
            );
        },
        'application/vnd.pagsousa.hyper+json': () => {
            return response.error(
                err,
                (s) => Hyper.item(s, (i) => itemLinks(i, authzUser), cleanup1Message),
                Response.etagHeader
            );
        }
    });
}

//
// handling the collection
//

/**
 * @swagger
 * /message:
 *   get:
 *     tags:
 *       - Messages
 *     summary: Returns all messages
 *     description: Returns all messages.Optionally filters messages by author, title or text particle as well as creation date.
 *     operationId: getMessages
 *     security: []
 *     parameters:
 *       - in: query
 *         name: author
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         description: the author of the messages
 *       - in: query
 *         name: title
 *         schema:
 *           $ref: '#/components/schemas/attrMessageTitle'
 *         description: a text particle in the message's title
 *       - in: query
 *         name: text
 *         schema:
 *           $ref: '#/components/schemas/attrMessageText'
 *         description: a text particle in the message's text
 *       - in: query
 *         name: tags
 *         schema:
 *           $ref: '#/components/schemas/attrMessageTags'
 *         description: a list of tags that the messages must possess (all inclusive)
 *       - in: query
 *         name: created_from
 *         description: starting date range of message creation
 *         schema:
 *           type: string
 *           format: date
 *       - in: query
 *         name: created_to
 *         description: end date range of message creation
 *         schema:
 *           type: string
 *           format: date
 *       - in: query
 *         name: first
 *         schema:
 *           type: integer
 *           default: 5
 *           minimum: 1
 *           maximum: 500
 *           format: int32
 *         description: number of results
 *       - in: query
 *         name: after
 *         schema:
 *           $ref: '#/components/schemas/attrMessageId'
 *         description: message id to start the pagination (exclusive)
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkMessageCollection'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleGetMessages(req, res) {
    // issue #61 `first` and `after` paging query parameters 
    const paging = handlePagingParameters(req.query);

    const asArray = (tags) => Array.isArray(tags) ? tags : new Array(tags);

    const criteria = {
        author: req.query.author,
        title: req.query.title,
        text: req.query.text,
        createdFrom: req.query.created_from,
        createdTo: req.query.created_to,
        tags: !req.query.tags ? undefined : asArray(req.query.tags)
    };
    return messageController
        .searchMessages(criteria, paging)
        .then((out) => formatResponseCollection(res, req.user, out, criteria, paging))
        .catch((err) => formatError(res, err, req.user));
}

function handlePagingParameters(query) {
    let paging = {};
    if (query.first || query.after) {
        paging.first = parseInt(query.first);
        paging.after = query.after;
    }
    return paging;
}

/**
 * @swagger
 * /message:
 *   post:
 *     summary: Post a new message.
 *     description: Post a new message.
 *     operationId: postMessage
 *     tags:
 *       - Messages
 *     requestBody:
 *       description: new message
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/newMessageDataInput'
 *     responses:
 *       201:
 *         $ref: '#/components/responses/CreatedMessageItem'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       400:
 *         description: bad input
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePostMessages(req, res) {
    return messageController
        .addMessage(req.body, req.user.username)
        .then(function (entry) {
            const r = {
                data: entry,
                created: true
            };
            return formatResponse(res, r, req.user);
        })
        .catch((err) => formatError(res, err, req.user));
}


//
// handling message-trivia "controller" resource
//
// POST      accepts the creation of a new message (returns 202)
//

function doPostBack(callbackUrl, message, myRef, senderRef, contentBuilder, etagHeaderBuilder, linkHeaderBuilder, contentType) {

    const postbackMessage = contentBuilder(message);
    postbackMessage.correlation = {
        yourRef: senderRef,
        myRef: myRef
    };

    const fetchOptions = {
        method: 'POST',
        body: JSON.stringify(postbackMessage),
        headers: {
            'User-Agent': 'ABoard4U/Request-Promise',
            "Content-Type": contentType,
            ...etagHeaderBuilder(message),
            //we are generating the links as if it were an unauthenticated session 
            ...linkHeaderBuilder(message)
        }
    };

    return fetch(callbackUrl, fetchOptions)
        .then((res) => {
            LOGGER(`Posted back to ${callbackUrl} in regarding to ref# ${postbackMessage.correlation.yourRef} and got ${res.status}`, postbackMessage);
            if (res.status !== 200 && res.status !== 204) {
                return Promise.reject({
                    statusCode: res.status
                });
            } else {
                return Promise.resolve(res.body || {});
            }
        })
        .catch((err) => LOGGER(`Posted to ${callbackUrl} in regarding to ref# ${postbackMessage.correlation.yourRef} and got ERROR ${err.statusCode}`));
}

function whichPostBackError(accept) {
    if (accept === PAGSOUSA_HYPER_MIMETYPE) {
        return (callback, message, myRef, senderRef) => doPostBack(
            callback,
            message,
            myRef,
            senderRef,
            Hyper.error,
            Response.etagHeader,
            (m) => Object.freeze({ }),
            PAGSOUSA_HYPER_MIMETYPE
        );
    }

    //fallback to application/json
    const errorBuilder = (m) => {
        return {
            error: m
        };
    };
    return (callback, message, myRef, senderRef) => doPostBack(
        callback,
        message,
        myRef,
        senderRef,
        errorBuilder,
        Response.etagHeader,
        (m) => Object.freeze({ }),
        "application/json"
    );
}

function whichPostBack(accept) {
    if (accept === PAGSOUSA_HYPER_MIMETYPE) {
        return (callback, message, myRef, senderRef) => doPostBack(
            callback,
            message,
            myRef,
            senderRef,
            (m) => Response.cleanupContent(Hyper.item(m, itemLinks, cleanup1Message)),
            Response.etagHeader,
            (m) => Object.freeze({ }),
            PAGSOUSA_HYPER_MIMETYPE
        );
    }

    //fallback to application/json
    return (callback, message, myRef, senderRef) => doPostBack(
        callback,
        message,
        myRef,
        senderRef,
        (m) => Response.cleanupContent(cleanup1Message(m)),
        Response.etagHeader,
        (m) => Response.linkHeader(itemLinks(m)),
        "application/json"
    );
}

/**
 * @swagger
 * /message-trivia:
 *   post:
 *     summary: Creates a new message (assynchronously) by first augmenting it with trivia factoids.
 *     description: Asynchronously creates a message augmenting it with trivia factoids. If a callback url is passed in the request body, the newly created message will be posted to that url as content-type `application/json` and schema `messageData`
 *     tags:
 *       - Message trivia
 *     requestBody:
 *       description: new message
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/messageWithCallbackDataInput'
 *           examples:
 *             pooling:
 *               value:
 *                 title: Hello world
 *                 text: First message! Hurray!
 *             postback:
 *               value:
 *                 title: Hello world
 *                 text: First message! Hurray!
 *                 callback: https://example.org/postback-trivia
 *                 callbackRef: A123-R
 *     callbacks:
 *       created:
 *         '{$request.body#/callback}':
 *           post:
 *             security: []
 *             requestBody:
 *               required: true
 *               content:
 *                 application/json:
 *                   schema:
 *                     $ref: '#/components/schemas/messageDataPostback'
 *             responses:
 *               200:
 *                 $ref: '#/components/responses/Ok'
 *     responses:
 *       202:
 *         headers:
 *           Location:
 *             schema:  
 *               type: string
 *               format: uri
 *             description: the id and location where the message will be available
 *         description: Accepted. The caller must pool the resource identified in the `Location` header to obtain the newly created (augmented) message, or provide a `callback` and `callbackRef` in the request body where the server will later POST the new message
 *         content:
 *           text/plain:
 *             schema:
 *               $ref: '#/components/schemas/serverResponseText'
 *       400:
 *         description: Missing `title` or `text` property, or `callback` is provided but not `callbackRef`
 *         content:
 *           text/plain:
 *             schema:
 *               $ref: '#/components/schemas/serverResponseText'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePostMessagesForAugmentation(req, res) {
    const authUser = req.user.username;
    return messageController
        .addMessageForAugmentation(authUser, req.body, req.body.callback, req.body.callbackRef, whichPostBack(req.headers.accept), whichPostBackError(req.headers.accept))
        .then((newID) => new Response(res, SETTINGS).accepted({}, SETTINGS.server.rest.API_ROOT + "/message/" + newID))
        .catch((err) => formatError(res, err, req.user));
}

/**
 * @swagger
 * /message/{id}/trivia:
 *   post:
 *     summary: assynchronously augments a message with trivia factoids.
 *     description: Asynchronously augments a message with trivia factoids. If a callback url is passed in the request body, the newly created message will be posted to that url as content-type `application/json` and schema `messageData`
 *     tags:
 *       - Message trivia
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           $ref: '#/components/schemas/attrMessageId'
 *         description: the message id
 *     requestBody:
 *       description: callback info
 *       required: false
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             additionalProperties: false
 *             properties:
 *               callback:
 *                 type: string
 *                 format: uri
 *                 example: https://example.org/postback-trivia
 *               callbackRef:
 *                 type: string
 *                 maxLength: 64
 *                 pattern: '^[a-zA-Z0-9]+(-\w)*$'
 *                 example: A123-R
 *                 description: this property is mandatory if a callback url is present
 *           examples:
 *             postback:
 *               value:
 *                 callback: https://example.org/postback-trivia
 *                 callbackRef: A123-R
 *     responses:
 *       202:
 *         headers:
 *           Location:
 *             schema:  
 *               type: string
 *               format: uri
 *             description: the id and location where the message will be available
 *         description: Accepted. The caller must pool the resource identified in the `Location` header to obtain the newly created (augmented) message or provide a `callback` and `callbackRef` in the request body where the server will later POST the new message
 *         content:
 *           text/plain:
 *             schema:
 *               $ref: '#/components/schemas/serverResponseText'
 *       400:
 *         description: missing `callbackRef` or `callbackRef` in body
 *         content:
 *           text/plain:
 *             schema:
 *               $ref: '#/components/schemas/serverResponseText'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 *     callbacks:
 *       created:
 *         '{$request.body#/callback}':
 *           post:
 *             security: []
 *             requestBody:
 *               required: true
 *               content:
 *                 application/json:
 *                   schema:
 *                     $ref: '#/components/schemas/messageDataPostback'
 *             responses:
 *               200:
 *                 $ref: '#/components/responses/Ok'
 */
function handlePostMessageItemForAugmentation(req, res) {
    const authUser = req.user.username;
    return messageController
        .augmentMessage(authUser, req.params.id, req.body.callback, req.body.callbackRef, whichPostBack(req.headers.accept), whichPostBackError(req.headers.accept))
        .then((newID) => new Response(res, SETTINGS).accepted({}, SETTINGS.server.rest.API_ROOT + "/message/" + newID))
        .catch((err) => formatError(res, err, req.user));
}

//
// handling individual itens in the collection
//
// message id must be present in the request object
//
// GET      return specific message or 404
// POST     update existing entry or 404
// PUT      overwrite existing or create new given the id
// DELETE   deletes the message
//

/**
 * @swagger
 * /message/{id}:
 *   get:
 *     tags:
 *       - Messages
 *     summary: Returns one message
 *     description: Returns one message
 *     security: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           $ref: '#/components/schemas/attrMessageId'
 *         description: the message id
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkMessageItem'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleGetMessageItem(req, res) {
    return messageController
        .getMessage(req.params.id)
        .then(function (entry) {
            const r = {
                data: entry,
                created: false
            };
            return formatResponse(res, r, req.user);
        })
        .catch((err) => formatError(res, err, req.user));
}

/**
 * @swagger
 * /message/{id}:
 *   put:
 *     summary: Creates or replaces a message.
 *     description: Creates or replaces a message (full update).
 *     operationId: editMessage
 *     tags:
 *       - Messages
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           $ref: '#/components/schemas/attrMessageId'
 *         required: true
 *         description: the message id
 *     requestBody:
 *       description: message
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/newMessageDataInput' 
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkMessageItem'
 *       201:
 *         $ref: '#/components/responses/CreatedMessageItem'
 *       400:
 *         description: bad input
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         description: You cannot update other user's messages
 *       412:
 *         $ref: '#/components/responses/PreconditionFailedMessageItem'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePutMessageItem(req, res) {
    const rev = req.headers['if-match'];
    return messageController.upsertMessage(req.user.username, req.params.id, req.body, rev)
        .then((entry) => formatResponse(res, entry, req.user))
        .catch((err) => formatError(res, err, req.user));
}

/**
 * @swagger
 * /message/{id}:
 *   post:
 *     summary: Updates a message.
 *     description: Partially updates a message. This endpoint is a convenience hack for HTML forms. Clients should use 'PATCH'.
 *     tags:
 *       - Messages
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           $ref: '#/components/schemas/attrMessageId'
 *         required: true
 *         description: the message id
 *     requestBody:
 *       description: message
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/editMessageForm'
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/messageDataInput'
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkMessageItem'
 *       400:
 *         description: bad input.
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         description: You cannot update other user's messages
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       412:
 *         $ref: '#/components/responses/PreconditionFailedMessageItem'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePostMessageItem(req, res) {
    return doPartialUpdate(req, res);
}

function doPartialUpdate(req, res) {
    let rev = req.headers['if-match'];
    if (!rev) {
        // fallback, see if we are getting here from the HTML form
        rev = req.body.__etag;
    }

    return messageController
        .updateMessage(req.user.username, req.params.id, rev, req.body)
        .then(function (entry) {
            const r = {
                data: entry
            };
            return formatResponse(res, r, req.user);
        })
        .catch((err) => formatError(res, err, req.user));
}

/**
 * @swagger
 * /message/{id}:
 *   patch:
 *     summary: Updates a message.
 *     description: Partially updates a message. 
 *     tags:
 *       - Messages
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           $ref: '#/components/schemas/attrMessageId'
 *         required: true
 *         description: the message id
 *     requestBody:
 *       description: message
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/messageDataInput'
 *         application/merge-patch+json:
 *           schema:
 *             $ref: '#/components/schemas/messageDataInput'
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkMessageItem'
 *       400:
 *         description: bad input.
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         description: You cannot update other user's messages
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       412:
 *         $ref: '#/components/responses/PreconditionFailedMessageItem'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePatchMessageItem(req, res) {
    return doPartialUpdate(req, res);
}

/**
 * @swagger
 * /message/{id}:
 *   delete:
 *     summary: removes a message
 *     description: removes a message
 *     operationId: deleteMessage
 *     tags:
 *       - Messages
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           $ref: '#/components/schemas/attrMessageId'
 *         description: the message id
 *     responses:
 *       200:
 *         $ref: '#/components/responses/Ok'
 *       400:
 *         description: missing "if-match" header
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/messageItem'
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       403:
 *         description: cannot delete messages of another user
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       412:
 *         $ref: '#/components/responses/PreconditionFailedMessageItem'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleDeleteMessageItem(req, res) {
    const rev = req.headers["if-match"];
    return messageController
        .deleteMessage(req.user.username, req.params.id, rev)
        .then((deleted) => new Response(res, SETTINGS).ok(`Message ${req.params.id} deleted.`))
        .catch((err) => formatError(res, err, req.user));
}


//
// user's message collection
//
// GET      return user's messages
// POST     not allowed
// PUT      not allowed
// DELETE   not allowed
//

/**
 * @swagger
 * /user/{username}/message:
 *   get:
 *     tags:
 *       - Messages
 *       - Users
 *     summary: Returns all messages of a user
 *     security: []
 *     description: Returns all messages of the user. 
 *     parameters:
 *       - in: path
 *         name: username
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         required: true
 *       - in: query
 *         name: first
 *         schema:
 *           type: integer
 *           default: 5
 *           minimum: 1
 *           maximum: 500
 *           format: int32
 *         description: number of results
 *       - in: query
 *         name: after
 *         schema:
 *           $ref: '#/components/schemas/attrMessageId'
 *         description: message id to start the pagination (exclusive)
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkMessageCollection'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleGetUserMessages(req, res) {
    // issue #61 `first` and `after` paging query parameters 
    const paging = handlePagingParameters(req.query);
    const author = req.params.username;
    const criteria = {
        author
    };
    return messageController.findUserMessages(author, paging)
        .then((out) => formatResponseCollection(res, req.user, out, criteria, paging))
        .catch((err) => formatError(res, err, req.user));
}

/**
 * @swagger
 * /user/{username}/message/summary:
 *   get:
 *     tags:
 *       - Messages
 *       - Users
 *     summary: Returns the total number of published messages by a user
 *     security: []
 *     description: Returns the total number of published messages by a user
 *     parameters:
 *       - in: path
 *         name: username
 *         schema:
 *           $ref: '#/components/schemas/attrUserUsername'
 *         required: true
 *     responses:
 *       200:
 *         description: the message count
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/messageDataCollectionSummary'
 *           application/vnd.pagsousa.hyper+json:
 *             schema:
 *               $ref: '#/components/schemas/messageCollectionSummary'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleGetUserMessagesSummary(req, res) {
    const author = req.params.username;
    return messageController.countUserMessages(author)
        .then(function (count) {
            return {
                count
            };
        })
        .then((out) => formatResponseSummary(res, author, out))
        .catch((err) => formatError(res, err, req.user));
}

function formatResponseSummary(res, author, out) {
    const response = new Response(res, SETTINGS);
    return response.format({
        'application/json': () => {
            return response.ok(
                out, {
                "related": SETTINGS.server.rest.API_ROOT + "/user/" + author + "/message"
            }
            );
        },
        'application/vnd.pagsousa.hyper+json': () => {
            return response.ok(
                Hyper.item(
                    out,
                    function (i) {
                        return {
                            "related": SETTINGS.server.rest.API_ROOT + "/user/" + author + "/message"
                        };
                    }
                )
            );
        }
    });
}

/////////////////////////////
// MODULE EXPORTS

export default function MessageResource(config, dependencies) {
    messageController = new dependencies.MessageController(config, dependencies);

    SETTINGS = config;
    DEPENDENCIES = dependencies;

    Response = DEPENDENCIES.Response;
    Hyper = DEPENDENCIES.Hyper;

    return {
        handleGetMessages,
        handlePostMessages,

        handlePostMessagesForAugmentation,
        handlePostMessageItemForAugmentation,

        handleGetMessageItem,
        handlePostMessageItem,
        handlePatchMessageItem,
        handlePutMessageItem,
        handleDeleteMessageItem,

        handleGetUserMessages,
        handleGetUserMessagesSummary
    };
}