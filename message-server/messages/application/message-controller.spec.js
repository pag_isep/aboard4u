/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 9 */
/*jshint mocha: true */
/*jshint expr: true */

"use strict";


import chai from 'chai';

chai.should();

// test subject 
import {MessageController} from './message-controller.js';
let subject;

// dependency stubs
let test_doubles = {};

// exceptions
import {
    NotFoundError,
    //ConflictError,
    //ForbiddenError,
    //ConcurrencyError
} from "../../exceptions.js";

//
// helpers
//
function createSubject(msgRepo, userRepo, triviaAugmenter) {
    const dependencies = {
        MessageRepository: function () {
            return msgRepo;
        },
        UserRepository: function () {
            return userRepo;
        },
        TriviaAugmenter: function () {
            return triviaAugmenter;
        }
    };
    const config = {};
    subject = new MessageController(config, dependencies);
}

import UserRepoStubs from '../../test/stubs/user-repository.js';
import MessageRepoStubs from'../../test/stubs/message-repository.js';

function setupWithExistingUserNoMessages(addResult) {
    Object.assign(test_doubles, UserRepoStubs.oneUserRepo(maryWithoutPhoto));

    test_doubles.MessageRepository = MessageRepoStubs.emptyMessageRepo(addResult);

    createSubject(test_doubles.MessageRepository, test_doubles.UserRepository, null);
}

function setupWithNoUsersNoMessages() {
    Object.assign(test_doubles, UserRepoStubs.emptyUserRepo());

    test_doubles.MessageRepository = MessageRepoStubs.emptyMessageRepo(marysMessage);

    createSubject(test_doubles.MessageRepository, test_doubles.UserRepository, null);
}

//repo objects
import fs from 'fs';
const maryWithoutPhoto = fs.readFileSync(new URL('../../test/fixtures/maryWithoutPhoto.json', import.meta.url)).toString();
const marysMessage = {
    id: "a11",
    title: "title",
    text: "text",
};

const marysMessageWithTrivia = {
    id: "a11",
    title: "title",
    text: "text",
    trivia: {
        "1/1": "something"
    }
};

//
// Test Suite
//
describe("Message Controller", function () {
    //
    // MessageController.addMessage(title, text, author, extension) 
    //
    describe("Add message", function () {
        context("Ok", function () {
            beforeEach(() => setupWithExistingUserNoMessages(marysMessage));

            it("should work by delegating to addMessage");
        });
    });

    //
    // MessageController.addMessageWithID
    //
    describe("Add message with ID", function () {
        context("Mandatory parameters", function () {
            beforeEach(() => setupWithExistingUserNoMessages());

            it("should reject with missing newID mandatory parameter", function () {
                return subject.addMessageWithID(null, { title: "title", text: "text" }, "Mary", null).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject with empty newID mandatory parameter", function () {
                return subject.addMessageWithID("", { title: "title", text: "text" }, "Mary", null).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject without title mandatory parameter", function () {
                return subject.addMessageWithID("id", { text: "text" }, "Mary", null).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject without mandatory text parameter", function () {
                return subject.addMessageWithID("id", { title: "title" }, "Mary", null).should.eventually.be.rejectedWith(RangeError);
            });

            it("should reject without mandatory author parameter", function () {
                return subject.addMessageWithID("id", { title: "title", text: "text" }, null, null).should.eventually.be.rejectedWith(RangeError);
            });
        });

        context("Optional parameters", function () {
            beforeEach(() => setupWithExistingUserNoMessages(marysMessage));

            it("should resolve without extension optional parameter", function () {
                return subject.addMessageWithID("id", { title: "title", text: "text" }, "Mary").should.eventually.be.fulfilled;
            });

            it("should resolve with extension optional parameter", function () {
                return subject.addMessageWithID("id", { title: "title", text: "text" }, "Mary", {
                    trivia: ""
                }).should.eventually.be.fulfilled;
            });
        });

        context("Conflicts", function () { });

        context("Unexisting author", function () { });

        context("Ok", function () {
            beforeEach(() => setupWithExistingUserNoMessages(marysMessage));

            it("without extension", function () {
                return subject.addMessageWithID("a11", { title: "title", text: "text" }, "Mary").should.become(marysMessage);
            });
        });

        context("Ok", function () {
            beforeEach(() => setupWithExistingUserNoMessages(marysMessageWithTrivia));

            it("with extension", function () {
                return subject.addMessageWithID("a11", { title: "title", text: "text" }, "Mary", {
                    trivia: {
                        "1/1": "something"
                    }
                }).should.become(marysMessageWithTrivia);
            });
        });
    });

    //
    // MessageController.getMessage
    //
    describe("Get a message", function () {
        context("Mandatory parameters", function () { });

        context("Conflicts", function () { });

        context("Ok", function () { });
    });

    //
    // MessageController.upsertMessage
    //
    describe("Upsert a message", function () {
        describe("Update", function () {
            context("Mandatory parameters", function () { });

            context("Authorizations", function () { });

            context("Conflicts", function () { });

            context("Unexisting message", function () { });

            context("Ok", function () { });
        });

        describe("Insert", function () {
            context("Mandatory parameters", function () { });

            context("Unexisting author", function () { });

            context("Ok", function () { });
        });
    });

    //
    // MessageController.updateMessage
    //
    describe("Update a message", function () {
        context("Mandatory parameters", function () { });

        context("Authorizations", function () { });

        context("Conflicts", function () { });

        context("Unexisting message", function () { });

        context("Ok", function () { });
    });

    //
    // MessageController.deleteMessage
    //
    describe("Delete a message", function () {
        context("Mandatory parameters", function () { });

        context("Authorizations", function () { });

        context("Conflicts", function () { });

        context("Unexisting message", function () { });

        context("Ok", function () { });
    });

    //
    // MessageController.addMessageForAugmentation
    //
    describe("Add message for augmentation", function () {
        context("Mandatory parameters", function () { });

        context("Conflicts", function () { });

        context("Unexisting author", function () { });

        context("Ok", function () { });
    });

    //
    // MessageController.augmentMessage
    //
    describe("Augment existing message", function () {
        context("Mandatory parameters", function () { });

        context("Authorizations", function () { });

        context("Conflicts", function () { });

        context("Unexisting message", function () { });

        context("Ok", function () { });
    });

    //
    // MessageController.searchMessages
    //
    describe("Search messages", function () {
        context("Mandatory parameters", function () { });

        describe("pagination", function () {
            context("first 2", function () { });
            context("first 2 after X", function () { });
            context("after X without specifying 'first'", function () { });
        });

        describe("pagination with deprecated start/n", function () {
            context("start=1 n=2", function () { });
            context("n=2", function () { });
            context("start=3", function () { });
        });

        context("by title", function () { });
        context("by text", function () { });
        context("by author", function () { });
        context("by creation date", function () { });
    });

    //
    // MessageController.findUserMessages
    //
    describe("Get a user's messages", function () {
        context("Mandatory parameters", function () { });

        context("Unexisting user", function () { });

        context("Ok", function () { });
    });

    //
    // MessageController.countUserMessages(author)
    //
    describe("Get total number of posted messages by a user", function () {
        context("Mandatory parameters", function () {
            beforeEach(setupWithNoUsersNoMessages);

            it("should reject without author mandatory parameter", function () {
                return subject.countUserMessages().should.eventually.be.rejectedWith(RangeError);
            });
        });

        context("Unexisting user", function () {
            beforeEach(setupWithNoUsersNoMessages);

            it("should reject with unknown author", function () {
                return subject.countUserMessages("__fake__").should.eventually.be.rejectedWith(NotFoundError);
            });
        });

        context("Ok", function () {
            beforeEach(() => setupWithExistingUserNoMessages());

            it("should resolve with known author", function () {
                return subject.countUserMessages("Mary").should.become(0);
            });
        });
    });
});