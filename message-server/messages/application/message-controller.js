/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */

/*jslint node: true */
/*jshint esversion: 11*/

//
// message application layer
//

import debug from "debug";
const LOGGER=debug("pag:mserver:app");

import {
    createID
} from '../../util/id-token-generator.js';

//exceptions
import {
    NotFoundError,
    ConflictError,
    ForbiddenError,
    ConcurrencyError,
    buildMissingIfMatch,
    buildWrongFormatInQuery
} from "../../exceptions.js";

// maximum number of results in searches
import {
    limitMaxResults
} from "../../util/pagination-limits.js";


//
// messages
//

/**
 * Message controller
 * 
 * all methods return a Promise
 */
export class MessageController {

    constructor(config, dependencies) {
        // private by convention
        this._messageRepo = new dependencies.MessageRepository();
        this._userRepo = new dependencies.UserRepository();
        this._triviaAugmenter = new dependencies.TriviaAugmenter(config, dependencies);

        // public

    }

    /**
     * 
     * @param {*} messageId
     * @returns {Promise<Message>} 
     */
    getMessage(messageId) {
        if (!messageId) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError('Missing mandatory parameters'));
        }
        return this._messageRepo.getMessage(messageId);
    }

    _cleanupTags(tags = []) {
        /*
         * to follow Postel's law we will gracefully ignore malformed tags
         * if we wanted to be strict we would return a 400 Bad Request 
         *
         * if (msgIn.tags && !msgIn.tags.every(e => e)) {
         *   return Promise.reject(new RangeError('Malformed parameters'));
         * }
         */
        return tags.filter(e => e);
    }

    _buildMessage(msgIn) {
        let msg = {
            title: msgIn.title,
            text: msgIn.text,
        };
        if (msgIn.tags) {
            msg.tags = this._cleanupTags(msgIn.tags);
        } else {
            msg.tags = [];
        }
        // TODO trivia should only be present for internal augmentation calls... 
        if (msgIn.trivia) {
            msg.trivia = msgIn.trivia;
        }
        return msg;
    }

    /**
     * 
     * @param {*} newID 
     * @param {Object} msgIn
     * @param {*} authorUsername 
     */
    addMessageWithID(newID, msgIn, authorUsername) {
        if (![newID, msgIn, msgIn.title, msgIn.text, authorUsername].every(e => e)) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError('Missing mandatory parameters'));
        }
        let msg = this._buildMessage(msgIn);

        // validate if user exists
        return this._userRepo.userExists(authorUsername)
            .then((exists) => {
                if (!exists) {
                    //TODO throw BadRequestError to allow adding a json response
                    return Promise.reject(new RangeError(`Unknown user ${authorUsername}`));
                }
                return this._messageRepo.addMessage(newID, msg, authorUsername);
            });
    }

    /**
     * 
     * @param {*} msg 
     * @param {*} authorUsername 
     */
    addMessage(msg, authorUsername) {
        return this.addMessageWithID(createID(), msg, authorUsername);
    }

    /**
     * 
     * @param {*} newID 
     * @param {*} fnAfterFetch 
     * @param {*} callback 
     * @param {*} callbackRef 
     * @param {*} postBack 
     * @param {*} postBackError 
     */
    _doAugmentation(newID, fnAfterFetch, callback, callbackRef, postBack, postBackError) {
        const today = new Date();

        // async fetch external data
        this._triviaAugmenter.triviaOfDay(today)
            .then((trivia) => fnAfterFetch(trivia))
            .then(function (entry) {
                LOGGER("new augmented message:", entry);

                // assync callback
                if (callback) {
                    LOGGER("Posting back to %s relating to ref# %s", callback, callbackRef);
                    return postBack(callback, entry, newID, callbackRef);
                }
                return entry;
            })
            .catch((reason) => {
                LOGGER("Error '%s' while augmenting. message not created", reason);

                // assync callback
                if (callback) {
                    LOGGER("Posting back to %s relating to ref# %s", callback, callbackRef);
                    postBackError(callback, reason, newID, callbackRef);
                }
            });
    }

    /**
     * Creates new message but first augmenting it with trivia
     * 
     * @param {*} user 
     * @param {object} msg 
     * @param {*} [callback] 
     * @param {*} [callbackRef] 
     * @param {*} postBack 
     * @param {*} postBackError 
     */
    addMessageForAugmentation(user, msg, callback, callbackRef, postBack, postBackError) {
        if (![user, msg, msg.title, msg.text].every(e => e)) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError('Missing mandatory parameters'));
        }
        if (callback && !callbackRef) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError("Missing 'callbackRef' property"));
        }

        const newID = createID("augz");
        this._doAugmentation(
            newID,
            (factoid) => {
                msg.trivia = {
                    createdOn: factoid[Object.keys(factoid)[0]],
                    ...factoid
                };
                return this.addMessageWithID(newID, msg, user);
            },
            callback,
            callbackRef,
            postBack,
            postBackError
        );

        return Promise.resolve(newID);
    }

    /**
     * Augments an existing message
     * 
     * @param {*} authzUsername 
     * @param {*} messageId 
     * @param {*} [callback] 
     * @param {*} [callbackRef] - mandatory if callback is passed
     * @param {*} postBack 
     * @param {*} postBackError 
     */
    augmentMessage(authzUsername, messageId, callback, callbackRef, postBack, postBackError) {
        if (!authzUsername || !messageId) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError('Missing mandatory parameters'));
        }
        if (callback && !callbackRef) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError("Missing 'callbackRef' property"));
        }
        return this._messageRepo.getMessage(messageId)
            .then((m) => {
                if (!m) {
                    return Promise.reject(new NotFoundError(`Message ${messageId} not found.`, messageId));
                }
                // authorization rule: a user can only update her own messages
                if (authzUsername !== m.authorUsername) {
                    return Promise.reject(new ForbiddenError("you can only update your own messages"));
                }

                this._doAugmentation(
                    messageId,
                    (factoid) => {
                        const ext = {
                            trivia: {
                                ...factoid
                            }
                        };
                        m = Object.assign(m, ext);
                        return this._messageRepo.saveMessage(m);
                    },
                    callback,
                    callbackRef,
                    postBack,
                    postBackError
                );

                return messageId;
            });
    }

    /**
     * Fully replaces an existing message or creates a new one if it doesn't exist.
     * 
     * @param {*} authzUser 
     * @param {*} newID 
     * @param {*} msg 
     * @param {*} rev 
     */
    upsertMessage(authzUser, newID, msg, rev) {
        //LOGGER('upsertMessage', newID, msg, authzUser);
        if (![newID, msg, msg.title, msg.text, authzUser].every(e => e)) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError('Missing mandatory parameter'));
        }
        /*
         * to follow Postel's law we will gracefully ignore malformed tags
         * if we wanted to be strict we would return a 400 Bad Request 
         *
         * if (msg.tags && !msg.tags.every(e => e)) {
         *   return Promise.reject(new RangeError('Malformed parameters'));
         * }
         */

        // first check if the message exists to update or insert
        return this._messageRepo.getMessage(newID, false)
            .then((messageExists) => {
                if (messageExists) {
                    if (!rev) {
                        return Promise.reject(buildMissingIfMatch());
                    }
                    // authorization rule: a user can only delete her own account
                    if (authzUser !== messageExists.authorUsername) {
                        return Promise.reject(new ForbiddenError("you can only update your own messages"));
                    }
                    if (rev !== messageExists.__v.toString()) {
                        return Promise.reject(new ConcurrencyError("Object has been updated by another user", messageExists));
                    }

                    // PUT MUST replace the full representation
                    messageExists.title = msg.title;
                    messageExists.text = msg.text;
                    messageExists.tags = this._cleanupTags(msg.tags);
                    messageExists.trivia = undefined; // since trivia is supposed to be a derived property the client cannot update it
                    delete messageExists.trivia;

                    return this._messageRepo.saveMessage(messageExists)
                        .then(function (m) {
                            return {
                                data: m,
                                created: false
                            };
                        });
                } else {
                    return this._messageRepo.addMessage(newID, this._buildMessage(msg), authzUser)
                        .then(function (m) {
                            return {
                                data: m,
                                created: true
                            };
                        });
                }
            });
    }

    /**
     * Partially updates an existing message.
     * 
     * @param {string} authzUsername 
     * @param {string} messageId 
     * @param {string} rev 
     * @param {object} msg 
     */
    updateMessage(authzUsername, messageId, rev, msg) {
        //LOGGER('updateMessage', messageId, msg, authzUsername);
        if (!rev) {
            return Promise.reject(buildMissingIfMatch());
        }
        if (![messageId, authzUsername, msg].every(e => e)) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError('Missing mandatory parameters'));
        }
        /*
         * to follow Postel's law we will gracefully ignore malformed tags
         * if we wanted to be strict we would return a 400 Bad Request 
         *
         * if (msg.tags && !msg.tags.every(e => e)) {
         *   return Promise.reject(new RangeError('Malformed parameters'));
         * }
         */
        // we first load the message to perform authorization
        return this._messageRepo.getMessage(messageId)
            .then((message) => {
                if (authzUsername !== message.authorUsername) {
                    return Promise.reject(new ForbiddenError("you can only update your own messages"));
                }
                if (rev !== message.__v.toString()) {
                    return Promise.reject(new ConflictError("Item has been updated by other user already. Please refresh and retry."));
                }

                if (msg.title) {
                    message.title = msg.title;
                }
                if (msg.text) {
                    message.text = msg.text;
                }
                if (msg.tags) {
                    message.tags = this._cleanupTags(msg.tags);
                }
                return this._messageRepo.saveMessage(message);
            });
    }

    /**
     * @typedef MessageSearchCriteria
     * @property {String} [author] 
     * @property {String} [title] 
     * @property {String} [text] 
     * @property {String} [createdFrom] - date of creation minimum boundary (inclusive)
     * @property {String} [createdTo] - date of creation maximum boundary (inclusive)
     */

    /**
     * Searches messages by title, text, author or creation date. paginates results.
     * 
     * @param {MessageSearchCriteria} criteria
     * @param {*} [paging={}] - pagination 
     */
    searchMessages(criteria, paging = {}) {
        /**
         * 
         * @param {*} dt 
         */
        function parseDates(dt, adjustmentToEndOfDay, queryParam) {
            // relative date
            if (/^[\-\+]\d+d$/.test(dt)) {
                const dir = dt.charAt(0) === '-' ? -1 : 1;
                const magnitude = parseInt(dt.substr(1, dt.length - 1));
                let ref = new Date();
                ref.setDate(ref.getDate() + dir * magnitude);

                LOGGER('Parsed date', dt, 'to', ref);
                return ref;
            }

            // absolute date
            let abs = new Date(dt);
            if (isNaN(abs)) {
                //TODO throw BadRequestError to allow adding a json response
                throw buildWrongFormatInQuery(queryParam, "YYYY-MM-DD");
            }
            if (adjustmentToEndOfDay) {
                abs.setHours(23);
                abs.setMinutes(59);
                abs.setSeconds(59);
                abs.setMilliseconds(999);
            }
            return abs;
        }

        try {
            if (criteria.createdFrom) {
                criteria.createdFrom = parseDates(criteria.createdFrom, false, "createdFrom");
            }
            if (criteria.createdTo) {
                criteria.createdTo = parseDates(criteria.createdTo, true, "createdTo");
            }
        } catch (e) {
            return Promise.reject(e);
        }

        paging.first = limitMaxResults(paging.first);
        return this._messageRepo.searchMessagesAfter(paging.first, paging.after, criteria);
    }

    /**
     * deletes a message
     *
     * @param {String} authzUsername current logged in username
     * @param {String} messageId message to delete
     * @param {String} rev specified revision from the client (e.g., if-match header)
     */
    deleteMessage(authzUsername, messageId, rev) {
        if (!rev) {
            return Promise.reject(buildMissingIfMatch());
        }
        if (![messageId, authzUsername].every(e => e)) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError('Missing mandatory parameters'));
        }

        // first check if the message exists to distinguish between a not found (404) and a concurrency issue (412)
        // provably we could optimize this with a single call to the data store
        // however, we need to get the message anyway in order to perform the authorization rule that a user 
        // can only delete her own messages
        return this._messageRepo.getMessage(messageId)
            .then((messageExists) => {
                //LOGGER("User", user, "\n»» Lookedup ", messageExists);
                if (authzUsername !== messageExists.authorUsername) {
                    return Promise.reject(new ForbiddenError("you can only delete your own messages"));
                }
                // since we just get the message from data store migth as well validate the current revision
                // the data store will validate it again in case other instance of the server has changed the 
                // underlying data store in the meantime
                if (rev !== messageExists.__v.toString()) {
                    return Promise.reject(new ConcurrencyError("Item has been updated by other user already. Please refresh and retry.", messageExists));
                }

                return this._messageRepo.deleteMessage(messageId, rev)
                    .then(function (deleted) {
                        if (!deleted) {
                            return Promise.reject(new ConcurrencyError("Item has been updated by other user already. Please refresh and retry.", messageExists));
                        }
                        return Promise.resolve(true);
                    });
            });
    }

    /**
     * 
     * @param {*} author - username of the author to look for
     * @param {*} [paging] 
     */
    findUserMessages(author, paging) {
        if (!author) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError('Missing mandatory parameters'));
        }

        // get user will return NotFound if the user does not exist
        return this._userRepo.getUser(author)
            .then((user) => this.searchMessages({
                author
            }, paging));
    }

    /**
     * Returns the total number of messages posted by a user
     * 
     * @param {String} author - the username to look for
     * @returns {Number} the total number of messages posted by a user
     */
    countUserMessages(author) {
        if (!author) {
            //TODO throw BadRequestError to allow adding a json response
            return Promise.reject(new RangeError('Missing mandatory parameters'));
        }

        // getUser will throw NotFound if the user does not exist
        return this._userRepo.getUser(author)
            .then((user) => this._messageRepo.countOfMessages({
                author
            }));
    }

}