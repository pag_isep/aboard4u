/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// GraphQL Message
//

import { GraphQLError } from 'graphql';

import { handleError } from '../../util/apollo-error-helper.js';

//
// The resolvers
//

/**
 * The GraphQL resolvers
 * 
 * @param {*} config 
 * @param {*} dependencies 
 */
export function resolvers(config, dependencies) {
    const messageController = new dependencies.MessageController(config, dependencies);
    const userController = new dependencies.UserController(config, dependencies);

    // return the resolvers object
    return {
        //
        // Queries
        //
        Query: {
            /**
             * Get one message by id
             */
            message: (parent, args, context, info) => {
                return messageController
                    .getMessage(args.id)
                    .catch(handleError);
            },

            /**
             * Search messages
             * 
             * messages(criteria: MessageCriteria = {}, paging: PagingSpecification = {}): [Message!] !
             */
            messages: (parent, args, context, info) => {
                const spec = {
                    author: args.criteria.authorName,
                    title: args.criteria.title,
                    text: args.criteria.text,
                    createdFrom: args.criteria.created ? args.criteria.created.from : null,
                    createdTo: args.criteria.created ? args.criteria.created.to : null,
                    tags: args.criteria.tags
                };
                const page = {
                    afterMode: true,
                    ...args.paging
                };
                return messageController
                    .searchMessages(spec, page)
                    .then(out => out.data)
                    .catch(handleError);
            }
        },

        //
        // Query resolvers for type
        //
        Message: {
            /**
             * returns the version of this object for optimistic concurrency locking
             * @param {*} parent 
             */
            version(parent) {
                return parent.__v;
            },

            /**
             * Get the author of a message
             */
            author(parent) {
                return userController
                    .getUser(parent.authorUsername, false)
                    .catch(handleError);
            }
        },

        Trivia: {
            /**
             * transform the trivias map to an array to match the GraphQL type definition
             */
            trivias(parent) {
                return Object.keys(parent).map(function (k) {
                    return {
                        date: k,
                        factoid: parent[k]
                    };
                });
            }
        },

        //
        // Mutations
        //
        Mutation: {
            /**
             * Post a message
             * 
             *(title: String!, text: String!): Message!
             */
            postMessage: function (parent, args, context, info) {
                if (!context.user) {
                    throw new GraphQLError('You are not authorized to perform this action.', {
                        extensions: {
                          code: 'UNAUTHENTICATED',
                        },
                      });
                }

                //the following validation is not really necessary as the GraphQL type system will 
                //force the existence of the parameter since it is marked as "not null"
                if (!args.title || !args.text) {
                    throw new GraphQLError('Please check your input. Must provide "title" and "text".', {
                        extensions: {
                          code: 'BAD_USER_INPUT',
                        },
                      });
                }

                return messageController
                    .addMessage(args, context.user.username)
                    .catch(handleError);
            },

            /**
             * Augment an existing message
             * 
             *(id: ID!): ID!
             */
            augmentMessage: function (parent, args, context, info) {
                if (!context.user) {
                    throw new GraphQLError('You are not authorized to perform this action.', {
                        extensions: {
                          code: 'UNAUTHENTICATED',
                        },
                      });
                }

                return messageController
                    .augmentMessage(context.user.username, args.id)
                    .catch(handleError);
            },

            /**
             * Post a message for augmentation
             * 
             *(title: String!, text: String!): ID!
             */
            postMessageForAugmentation: function (parent, args, context, info) {
                if (!context.user) {
                    throw new GraphQLError('You are not authorized to perform this action.', {
                        extensions: {
                          code: 'UNAUTHENTICATED',
                        },
                      });
                }

                return messageController
                    .addMessageForAugmentation(context.user.username, args)
                    .catch(handleError);
            },

            /**
             * Update a message
             * 
             *(id: ID!, version: String!, title: String!, text: String!): Message!
             */
            updateMessage: function (parent, args, context, info) {
                if (!context.user) {
                    throw new GraphQLError('You are not authorized to perform this action.', {
                        extensions: {
                          code: 'UNAUTHENTICATED',
                        },
                      });
                }

                return messageController
                    .updateMessage(context.user.username, args.id, args.version, args)
                    .catch(handleError);
            },

            /**
             * Delete a message
             * 
             *(id: ID!, version: String!): boolean!
             */
            deleteMessage: function (parent, args, context, info) {
                if (!context.user) {
                    throw new GraphQLError('You are not authorized to perform this action.', {
                        extensions: {
                          code: 'UNAUTHENTICATED',
                        },
                      });
                }

                return messageController
                    .deleteMessage(context.user.username, args.id, args.version)
                    .catch(handleError);
            }
        }
    };
}
