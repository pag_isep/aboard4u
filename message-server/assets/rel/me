<!DOCTYPE html>
<html lang="en">

<!--
/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */
-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>A Board 4 U Message Service Custom Relationship Type</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
        </script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="container">
        <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="/">ABoard4U</a>
        </nav>
        <br/>
        <div class="row">
            <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="card mb-3">
                    <div class="card-body">
                        <h2 class="card-title"><em class="fa fa-book"></em> Custom Relationship Type</h2>
                        <hr />
                        <div>
                                        <p> <code>https://aboard4u.herokuapp.com/rel/me</code></p>
                                        <p>The target IRI of this relation can be used to obtain the authenticated
                                            user's profile by issuing an authenticated <code>GET</code>. The response
                                            body MUST contain a representation of the authenticated user. </p>
                                        <p>Allows to obtain the authenticated user's info. Usefull for client
                                            applications
                                            to get the set of allowed actions (links) of the user. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="card  mb-3">
                    <div class="card-body">
                        <h2 class="card-title"><em class="fa fa-code"></em> Usefull Links</h2>
                        <ul class='contents list-group list-group-flush'>
                            <li class="list-group-item"><a href="/">API home</a></li>
                            <li class="list-group-item"><a rel="help" href="/api/api-docs">Swagger UI</a></li>
                            <li class='list-group-item'><a href="/graphql">GraphQL Playground</a></li>
                            <li class="list-group-item"><a rel="describedBy" href="/api/openapi.json">OpenAPI
                                    Specification</a> <small class="text-muted">describedBy</small>
                            </li>
                            <li class="list-group-item"><a rel="describedBy" href="/schemas/">Schemas</a> <small class="text-muted">describedBy</small>
                            </li>
                            <li class="list-group-item"><a rel="license"
                                    href="https://spdx.org/licenses/MIT.html">License</a> <small
                                    class="text-muted">license</small></li>
                            <li class="list-group-item"><a rel="https://aboard4u.herokuapp.com/rel/login"
                                    href="/rel/login">Obtain
                                    an access token</a> <small
                                    class="text-muted">https://aboard4u.herokuapp.com/rel/login</small>
                            </li>
                            <li class="list-group-item"><a rel="https://aboard4u.herokuapp.com/rel/me"
                                    href="/rel/me">Obtain
                                    authenticated user's profile</a> <small
                                    class="text-muted">https://aboard4u.herokuapp.com/rel/me</small>
                            </li>
                            <li class="list-group-item"><a rel="https://aboard4u.herokuapp.com/rel/password"
                                    href="/rel/password">Change password</a> <small
                                    class="text-muted">https://aboard4u.herokuapp.com/rel/password</small>
                            </li>
                            <li class="list-group-item"><a rel="https://aboard4u.herokuapp.com/rel/password-reset"
                                    href="/rel/password-reset">Reset password of user's account</a> <small
                                    class="text-muted">https://aboard4u.herokuapp.com/rel/password-reset</small>
                            </li>
                            <li class="list-group-item"><a rel="https://aboard4u.herokuapp.com/rel/users"
                                    href="/rel/users">User collection</a> <small
                                    class="text-muted">https://aboard4u.herokuapp.com/rel/users</small></li>
                            <li class="list-group-item"><a rel="https://aboard4u.herokuapp.com/rel/messages"
                                    href="/rel/messages">Message collection</a> <small
                                    class="text-muted">https://aboard4u.herokuapp.com/rel/messages</small></li>
                            <li class="list-group-item"><a rel="https://aboard4u.herokuapp.com/rel/trivia"
                                    href="/rel/trivia">Message Trivia</a> <small
                                    class="text-muted">https://aboard4u.herokuapp.com/rel/trivia</small></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <div class="text-center">
            <hr />
            <small class="text-muted">Copyright <em class="fa fa-copyright"></em> 2013-2022 <span class='author'>Paulo Gandra
                    Sousa</span> &nbsp;<em class="fa fa-ellipsis-v"></em>
                &nbsp;<a href="https://bitbucket.org/pag_isep/aboard4u/issues/new">Report an
                    issue</a></small>
        </div>
    </div>
</body>

</html>