/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// the inmemory data store. also serves as bootstrap sample data
//


// some helper constants for sample data
const now = new Date();
const yesterday = now.getDate() + 1;
const jan1st2014 = new Date(2014, 1, 1);
const may2nd2014 = new Date(2014, 5, 2);


//
// DATA STORE
//

const messages = {};
const users = {};
const users_pwd = {};


//
// MESSAGES SAMPLE DATA
//


// this is the data store, so 'id' is the primary key, not the URI of the resource
// also, 'authorUsernameUsername' is the foreign key to the 'user' table not the URI of the user resource
messages.a11 = {
    id: "a11",
    "title": "one",
    "text": "Sample one",
    authorUsername: "Mary",
    createdOn: now,
    updatedOn: now,
    publishedOn: now,
    "__v": "0"
};
messages.a12 = {
    id: "a12",
    "title": "five",
    "text": "Sample one 2",
    authorUsername: "Mary",
    createdOn: now,
    updatedOn: now,
    publishedOn: now,
    "__v": "0"
};
messages.a13 = {
    id: "a13",
    "title": "one 3",
    "text": "Sample one 3",
    authorUsername: "Mary",
    createdOn: now,
    updatedOn: now,
    publishedOn: now,
    "__v": "0"
};
messages.a13b = {
    id: "a13b",
    "title": "five",
    "text": "Sample one 3b",
    authorUsername: "Mary",
    createdOn: now,
    updatedOn: now,
    publishedOn: now,
    "__v": "0"
};
messages.a13c = {
    id: "a13c",
    "title": "five",
    "text": "Sample one 3c",
    authorUsername: "Mary",
    createdOn: now,
    updatedOn: now,
    publishedOn: now,
    "__v": "0"
};
messages.a13d = {
    id: "a13d",
    "title": "five",
    "text": "Sample one 3d",
    authorUsername: "Mary",
    createdOn: now,
    updatedOn: now,
    publishedOn: now,
    "__v": "0"
};


messages.b23d = {
    id: "b23d",
    "title": "five",
    "text": "Sample two 3d",
    authorUsername: "Ann",
    createdOn: yesterday,
    publishedOn: yesterday,
    updatedOn: now,
    "__v": "0"
};
messages.b23c = {
    id: "b23c",
    "title": "five",
    "text": "Sample two 3c",
    authorUsername: "Ann",
    createdOn: yesterday,
    publishedOn: yesterday,

    updatedOn: now,
    "__v": "0"
};
messages.b23b = {
    id: "b23b",
    "title": "five",
    "text": "Sample two 3b",
    authorUsername: "Ann",
    createdOn: yesterday,
    publishedOn: yesterday,

    updatedOn: now,
    "__v": "0"
};
messages.b23 = {
    id: "b23",
    "title": "five",
    "text": "Sample two 3",
    authorUsername: "Ann",
    createdOn: yesterday,
    publishedOn: yesterday,

    updatedOn: now,
    "__v": "0"
};
messages.b21 = {
    id: "b21",
    "title": "two",
    "text": "Sample two",
    authorUsername: "Ann",
    createdOn: yesterday,
    publishedOn: yesterday,

    updatedOn: now,
    "__v": "0"
};
messages.b22 = {
    id: "b22",
    "title": "two 2",
    "text": "Sample two 2",
    authorUsername: "Ann",
    createdOn: yesterday,
    publishedOn: yesterday,

    updatedOn: now,
    "__v": "0"
};


messages.c31 = {
    id: "c31",
    "title": "three",
    "text": "Sample three",
    authorUsername: "Tom",
    createdOn: jan1st2014,
    publishedOn: jan1st2014,

    updatedOn: now,
    "__v": "0"
};
messages.c32 = {
    id: "c32",
    "title": "three 2",
    "text": "Sample three 2",
    authorUsername: "Tom",
    createdOn: jan1st2014,
    publishedOn: jan1st2014,

    updatedOn: now,
    "__v": "0"
};
messages.c33 = {
    id: "c33",
    "title": "five",
    "text": "Sample three 3",
    authorUsername: "Tom",
    createdOn: jan1st2014,
    publishedOn: jan1st2014,

    updatedOn: now,
    "__v": "0"
};
messages.c33d = {
    id: "c33d",
    "title": "five",
    "text": "Sample three 3d",
    authorUsername: "Tom",
    createdOn: jan1st2014,
    publishedOn: jan1st2014,

    updatedOn: now,
    "__v": "0"
};
messages.c33c = {
    id: "c33c",
    "title": "five",
    "text": "Sample three 3c",
    authorUsername: "Tom",
    createdOn: jan1st2014,
    publishedOn: jan1st2014,

    updatedOn: now,
    "__v": "0"
};
messages.c33b = {
    id: "c33b",
    "title": "five",
    "text": "Sample three 3b",
    authorUsername: "Tom",
    createdOn: jan1st2014,
    publishedOn: jan1st2014,

    updatedOn: now,
    "__v": "0"
};

messages.d43d = {
    id: "d43d",
    "title": "five",
    "text": "Sample four 3d",
    authorUsername: "Carl",
    createdOn: may2nd2014,
    publishedOn: may2nd2014,

    updatedOn: now,
    "__v": "0"
};
messages.d43c = {
    id: "d43c",
    "title": "five",
    "text": "Sample four 3c",
    authorUsername: "Carl",
    createdOn: may2nd2014,
    publishedOn: may2nd2014,

    updatedOn: now,
    "__v": "0"
};
messages.d43b = {
    id: "d43b",
    "title": "five",
    "text": "Sample four 3b",
    authorUsername: "Carl",
    createdOn: may2nd2014,
    publishedOn: may2nd2014,

    updatedOn: now,
    "__v": "0"
};
messages.d43 = {
    id: "d43",
    "title": "five",
    "text": "Sample four 3",
    authorUsername: "Carl",
    createdOn: may2nd2014,
    publishedOn: may2nd2014,
    updatedOn: now,
    "__v": "0"
};
messages.d41 = {
    id: "d41",
    "title": "four",
    "text": "Sample four",
    authorUsername: "Carl",
    createdOn: may2nd2014,
    publishedOn: may2nd2014,
    updatedOn: now,
    "__v": "0"
};
messages.d42 = {
    id: "d42",
    "title": "four 2",
    "text": "Sample four 2",
    authorUsernameUsername: "Carl",
    createdOn: may2nd2014,
    publishedOn: may2nd2014,
    updatedOn: now,
    "__v": "0"
};


//
// USERS SAMPLE DATA
//

// this is the data store, 'username' is the primary key, not the URI of the resource
users.Mary = {
    username: "Mary",
    name: "Mary Smith",
    email: "mary@contoso.com",
    description: "Mary loves Javascript and his passionate about APIs",
    createdOn: now,
    updatedOn: now,
    photo: {
        caption: "Mary's avatar",
        path: "Mary.jpg"
    },
    "__v": "0"
};
users.Ann = {
    username: "Ann",
    name: "Ann Belle",
    email: "ann@contoso.com",
    createdOn: yesterday,
    updatedOn: now,
    photo: {
        path: "Ann.jpg"
    },
    "__v": "0"
};
users.Tom = {
    username: "Tom",
    name: "Tom Clancy",
    email: "tom@contoso.com",
    createdOn: jan1st2014,
    updatedOn: now,
    "__v": "0"
};
users.Carl = {
    username: "Carl",
    name: "Carl Guttenberg",
    email: "carl@contoso.com",
    createdOn: may2nd2014,
    updatedOn: now,
    "__v": "0"
};


//
// AUTHZ SAMPLE DATA
//

users_pwd.Mary = {
    username: "Mary",
    password: '123',
    roles: [],
    failedLogins: 0,
    lastLogin: yesterday,
    resetToken: null
};
users_pwd.Ann = {
    username: "Ann",
    password: '123',
    roles: ['admin'],
    failedLogins: 0,
    lastLogin: null,
    resetToken: null
};
users_pwd.Tom = {
    username: "Tom",
    password: '123',
    roles: [],
    failedLogins: 0,
    lastLogin: null,
    resetToken: null
};
users_pwd.Carl = {
    username: "Carl",
    password: '123',
    roles: [],
    failedLogins: 0,
    lastLogin: null,
    resetToken: null
};


//
// exports
//
exports.data = {
    messages,
    users,
    users_pwd
};