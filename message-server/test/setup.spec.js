/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 9 */
/*jshint mocha: true */
/*jshint expr: true */

"use strict";

// test-setup.spec.js
import chai from 'chai';
import sinonChai from'sinon-chai';
import chaiPromise from 'chai-as-promised';

before(function () {
    chai.use(sinonChai);
    chai.use(chaiPromise);
    chai.should();
});

beforeEach(function () { });

afterEach(function () { });