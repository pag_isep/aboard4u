/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 9 */
/*jshint mocha: true */
/*jshint expr: true */

"use strict";

import sinon from 'sinon';

//
// stubs
// 
export function mockSendResponse() {
    const res = {};
    const mockResponse = Object.assign(res, {
        set: sinon.stub().returns(res),
        get: sinon.stub(),
        status: sinon.stub().returns(res),
        links: sinon.stub().returns(res),
        send: sinon.stub(),
        json: sinon.stub(),
    });
    return mockResponse;
}

export function neitherEtagNorLinkResponse() {
    const mockResponse = sinon.stub({
        set: function () { },
        get: function () { }
    });
    mockResponse.get.withArgs("Link").returns(undefined);
    mockResponse.get.withArgs("ETag").returns(undefined);
    return mockResponse;
}

export function withEtagButNoLinkResponse(ETAG) {
    const mockResponse = sinon.stub({
        set: function () { },
        get: function () { }
    });
    mockResponse.get.withArgs("Link").returns(undefined);
    mockResponse.get.withArgs("ETag").returns(ETAG);
    return mockResponse;
}

export function withLinkButNoETagResponse(ETAG) {
    const mockResponse = sinon.stub({
        set: function () { },
        get: function () { }
    });
    mockResponse.get.withArgs("Link").returns('<https://acme.org>; rel="next"');
    mockResponse.get.withArgs("ETag").returns(ETAG);
    return mockResponse;
}
