/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 9 */

"use strict";

import _ from 'lodash';

/**
 * configures a stub to return a promise.
 * if result is an instance of Error the promise will reject
 * if result is null, the promise will resolve to null, otherwise the promise will resolve to a copy of the result value
 * @param {*} stub 
 * @param {*} result - 
 */
export function setupStubPromise(stub, result) {
    if (result instanceof Error) {
        stub.rejects(result);
    } else {
        let obj = null;
        if (result !== undefined) {
            obj = _.cloneDeep(result);
        }
        stub.resolves(obj);
    }
}