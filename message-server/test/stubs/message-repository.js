/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 9 */
/*jshint mocha: true */
/*jshint expr: true */

"use strict";

import sinon from 'sinon';
import {setupStubPromise} from './util.js';

function emptyMessageRepo(addResult) {
    let repo = sinon.stub({
        addMessage: function () { },
        getMessage: function () { },
        deleteMessage: function () { },
        saveMessage: function () { },
        searchMessages: function () { },
        countOfMessages: function () { },
    });

    setupStubPromise(repo.addMessage, addResult);
    setupStubPromise(repo.countOfMessages, 0);

    return repo;
}

/////////////////////////
// exports

export default {
    emptyMessageRepo
};