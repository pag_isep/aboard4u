/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 11 */
/*jshint mocha: true */
/*jshint expr: true */

"use strict";

import sinon from 'sinon';
import {setupStubPromise} from './util.js';

// exceptions
import {
    NotFoundError,
    //ConflictError,
    //ForbiddenError,
    ConcurrencyError
} from "../../exceptions.js";

//
// helpers
//
function basicUserRepo() {
    return setupUserRepo(null, null, [], false, null);
}

/**
 * 
 * @param {*} theNewUser - value to return from addUser()
 */
function emptyUserRepo(theNewUser) {
    return setupUserRepo(null, theNewUser, [], false, theNewUser);
}

/**
 * 
 * @param {*} theUser - value to return from getUser()
 * @param {*} theNewUser - null will reject with ConcurrencyError
 * @param {Boolean} deleteResult - value to return from deleteUser()
 * @param {Boolean} rejectOnNewAuthz
 */
function conflictUserRepo(theUser, theNewUser, deleteResult = true, rejectOnNewAuthz = false) {
    return setupUserRepo(
        theUser,
        theNewUser ? theNewUser : new ConcurrencyError(),
        [theUser],
        deleteResult,
        theNewUser ? theNewUser : new ConcurrencyError(),
        rejectOnNewAuthz ? new ConcurrencyError() : theUser
    );
}

/**
 * 
 * @param {*} theUser - value to return from getUser()
 * @param {*} theNewUser - value to return from addUser() and saveUser()
 */
function oneUserRepo(theUser, theNewUser) {
    return setupUserRepo(theUser, theNewUser, [theUser], true, theNewUser);
}

/**
 * 
 * @param {*} theUser - value to return from getUser()
 * @param {*} theNewUser - value to return from addUser()
 * @param {*} searchResult - value to return from searchUsers()
 * @param {*} deleteResult - value to return from deleteUser()
 * @param {*} updateResult -
 * @param {*} theNewAuthz -
 */
function setupUserRepo(theUser, theNewUser, searchResult, deleteResult, updateResult, theNewAuthz) {
    let userRepo = sinon.stub({
        getUser: function () { },
        userExists: function () { },
        deleteUser: function () { },
        addUser: function () { },
        saveUser: function () { },
        searchUsers: function () { }
    });
    setupStubPromise(userRepo.getUser, theUser ? theUser : new NotFoundError());
    setupStubPromise(userRepo.userExists, theUser ? true : false);
    setupStubPromise(userRepo.addUser, theNewUser);
    setupStubPromise(userRepo.deleteUser, deleteResult);
    setupStubPromise(userRepo.saveUser, updateResult);
    setupStubPromise(userRepo.searchUsers, searchResult);

    let authzRepo = sinon.stub({
        addAuthz: function () { },
        getAuthz: function () { },
        upsertAuthz: function () { },
        saveAuthz: function () { },
        deleteAuthz: function () { }
    });
    setupStubPromise(authzRepo.addAuthz, theNewAuthz || theNewUser);
    setupStubPromise(authzRepo.deleteAuthz, true);

    return {
        UserRepository: userRepo,
        AuthzRepository: authzRepo
    };
}

/////////////////////////
// exports

export default {
    basicUserRepo,
    emptyUserRepo,
    oneUserRepo,
    conflictUserRepo,
    setupUserRepo
}