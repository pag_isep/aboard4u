/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// GraphQL helper
//

import { GraphQLError } from 'graphql';
import { ApolloServerErrorCode } from '@apollo/server/errors';

/**
 * helper
 */
export function handleError(err) {
    if (err.name === "RangeError" || err.name === "BadRequestError") {
        throw new GraphQLError(err.message, {
            extensions: { code: ApolloServerErrorCode.BAD_USER_INPUT},
          });
    } else if (err.name === "NotFoundError") {
        // return null to the data element
        return null;
    } else if (err.name === "ConflictError" || err.name === "ConcurrencyError") {
        throw new GraphQLError(err.message, {
            extensions: { code: 'CONFLICT'},
          });
    } else {
        throw err;
    }
}