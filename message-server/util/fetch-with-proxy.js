/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// Wrapper layer around node-fetch that properly handles proxies using the environment variables
//

// http client
import nodefetch from "node-fetch";
import { HttpProxyAgent } from 'hpagent';
import { getProxyForUrl } from 'proxy-from-env';


/////////////////////////////
// MODULE EXPORTS

export function fetch (uri, options) {
    options = options || {};
    if (!options.agent) {
        const proxy_url = getProxyForUrl(uri);
        if (proxy_url) {
            const httpAgent = new HttpProxyAgent({
                keepAlive: true,
                keepAliveMsecs: 1000,
                maxSockets: 256,
                maxFreeSockets: 256,
                proxy: proxy_url
            });

            options.agent = httpAgent;
        }
    }
    return nodefetch(uri, options);
}