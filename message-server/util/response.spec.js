/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 9 */
/*jshint mocha: true */
/*jshint expr: true */

"use strict";

import {expect} from 'chai';

// stubs
import {
    mockSendResponse,
    neitherEtagNorLinkResponse,
    withEtagButNoLinkResponse,
    withLinkButNoETagResponse
} from '../test/stubs/express-response.js';

// subject
import {Response} from './response.js';



describe("Response module", function () {
    describe("contructor", function () {
        it("constructs the object with no settings", function () {
            const res = {
            };
            const subject = new Response(res);
            subject.res().should.be.eql(res);
            subject.settings().should.be.eql({});
        });

        it("constructs the object with settings", function () {
            const res = {
            };
            const settings = {
                a: "a"
            };
            const subject = new Response(res, settings);
            subject.res().should.be.eql(res);
            subject.settings().should.be.eql(settings);
        });
    });

    describe("helper methods", function () {
        describe("eTagHeader", function () {
            it("Returns empty object on null input", function () {
                const result = Response.etagHeader();
                result.should.eql({});
            });

            it("Returns empty object on empty input", function () {
                const result = Response.etagHeader({});
                result.should.eql({});
            });

            it("Returns empty object on string input", function () {
                const result = Response.etagHeader("abab");
                result.should.eql({});
            });

            it("Returns empty object on empty string input", function () {
                const result = Response.etagHeader("");
                result.should.eql({});
            });

            it("Returns empty object on input without __v", function () {
                const result = Response.etagHeader({
                    id: 1
                });
                result.should.eql({});
            });

            it("should return empty object if there is no __v property even if there is a data property", function () {
                Response.etagHeader({
                    something: "abc",
                    data: {
                        something: "abc",
                    }
                }).should.be.eql({});
            });

            it("Returns etag object on input with __v", function () {
                const ETAG = "wxy22.56/";
                const input = {
                    __v: ETAG,
                    name: "mary"
                };
                const expected = {
                    ETag: ETAG
                };
                const result = Response.etagHeader(input);
                result.should.eql(expected);
            });

            it("Returns etag object on input with non-string __v", function () {
                const input = {
                    __v: 1,
                    name: "mary"
                };
                const expected = {
                    ETag: "1"
                };
                const result = Response.etagHeader(input);
                result.should.eql(expected);
            });

            it("should return ETag if there is a data.__v property (number)", function () {
                Response.etagHeader({
                    something: "abc",
                    data: {
                        something: "abc",
                        __v: 22
                    }
                }).should.have.property("ETag", "22");
            });

            it("should return ETag if there is a data.__v property (string)", function () {
                Response.etagHeader({
                    something: "abc",
                    data: {
                        something: "abc",
                        __v: "-w?fd45"
                    }
                }).should.have.property("ETag", "-w?fd45");
            });
        });

        describe("linkHeaderElement1", function () {
            it("href + rel", function () {
                const result = Response.linkHeaderElement1("next", "https://acme.org/");
                result.should.eql('<https://acme.org/>; rel="next"');
            });

            // the following test cases make no sense for this method as they are not real expectations
            it("empty input href", function () {
                const result = Response.linkHeaderElement1("next", null);
                result.should.eql('<null>; rel="next"');
            });

            it("empty input rel", function () {
                const result = Response.linkHeaderElement1(null, "https://acme.org/");
                result.should.eql('<https://acme.org/>; rel="null"');
            });

            it("undefined input rel", function () {
                const result = Response.linkHeaderElement1(undefined, "https://acme.org/");
                result.should.eql('<https://acme.org/>; rel="home"');
            });

            it("empty object input rel", function () {
                const result = Response.linkHeaderElement1({}, "https://acme.org/");
                result.should.eql('<https://acme.org/>; rel="[object Object]"');
            });

            it("empty input", function () {
                const result = Response.linkHeaderElement1();
                result.should.eql('</>; rel="home"');
            });
        });

        describe("linkHeaderElement", function () {
            it("Returns on missing mandatory properties", function () {
                const result = Response.linkHeaderElement({});
                result.should.eql('</>; rel="home"');
            });

            it("Returns on missing input", function () {
                const result = Response.linkHeaderElement();
                result.should.eql('</>; rel="home"');
            });

            it("Throws on null input", function () {
                expect(Response.linkHeaderElement.bind(Response, null)).to.throw(TypeError);
            });

            it("Returns on invalid input", function () {
                const result = Response.linkHeaderElement("abc");
                result.should.eql('</>; rel="home"');
            });

            it("href + rel", function () {
                const input = {
                    rel: "next",
                    href: "https://acme.org/"
                };
                const result = Response.linkHeaderElement(input);
                result.should.eql('<https://acme.org/>; rel="next"');
            });

            it("href + rel + name", function () {
                const input = {
                    rel: "next",
                    href: "https://acme.org/",
                    name: "proximo"
                };
                const result = Response.linkHeaderElement(input);
                result.should.eql('<https://acme.org/>; rel="next"; name="proximo"');
            });

            it("href + rel + type", function () {
                const input = {
                    rel: "next",
                    href: "https://acme.org/",
                    type: "text/html"
                };
                const result = Response.linkHeaderElement(input);
                result.should.eql('<https://acme.org/>; rel="next"; type="text/html"');
            });

            it("href + rel + title", function () {
                const input = {
                    rel: "next",
                    href: "https://acme.org/",
                    title: "proximo resultado"
                };
                const result = Response.linkHeaderElement(input);
                result.should.eql('<https://acme.org/>; rel="next"; title="proximo resultado"');
            });

            it("href + rel + name + title", function () {
                const input = {
                    rel: "next",
                    href: "https://acme.org/",
                    title: "proximo resultado",
                    name: "proximo"
                };
                const result = Response.linkHeaderElement(input);
                result.should.eql('<https://acme.org/>; rel="next"; name="proximo"; title="proximo resultado"');
            });

            it("href + rel + name + type", function () {
                const input = {
                    rel: "next",
                    href: "https://acme.org/",
                    name: "proximo",
                    type: "text/html"
                };
                const result = Response.linkHeaderElement(input);
                result.should.eql('<https://acme.org/>; rel="next"; name="proximo"; type="text/html"');
            });

            it("href + rel + name + type + title", function () {
                const input = {
                    rel: "next",
                    href: "https://acme.org/",
                    title: "proximo resultado",
                    name: "proximo",
                    type: "text/html"
                };
                const result = Response.linkHeaderElement(input);
                result.should.eql('<https://acme.org/>; rel="next"; name="proximo"; type="text/html"; title="proximo resultado"');
            });

            it("href + rel + type + title", function () {
                const input = {
                    rel: "next",
                    href: "https://acme.org/",
                    title: "proximo resultado",
                    type: "text/html"
                };
                const result = Response.linkHeaderElement(input);
                result.should.eql('<https://acme.org/>; rel="next"; type="text/html"; title="proximo resultado"');
            });

            it("ignores extra properties", function () {
                const input = {
                    rel: "next",
                    href: "https://acme.org/",
                    title: "proximo resultado",
                    name: "proximo",
                    type: "text/html",
                    abc: "abc"
                };
                const result = Response.linkHeaderElement(input);
                result.should.eql('<https://acme.org/>; rel="next"; name="proximo"; type="text/html"; title="proximo resultado"');
            });
        });

        describe("linkHeader", function () {
            describe("with object", function () {
                it("empty", function () {
                    const expected = {
                        Link: ""
                    };
                    const result = Response.linkHeader({});
                    result.should.eql(expected);
                });

                it("empty but with previous", function () {
                    const previous = '<https://one.net>; rel="https://acme.org/one"';
                    const expected = {
                        Link: previous
                    };
                    const result = Response.linkHeader({}, previous);
                    result.should.eql(expected);
                });

                it("one entry", function () {
                    const input = {
                        "next": "https://acme.org"
                    };
                    const expected = {
                        Link: '<https://acme.org>; rel="next"'
                    };
                    const result = Response.linkHeader(input);
                    result.should.eql(expected);
                });

                it("several entries", function () {
                    const input = {
                        "next": "https://acme.org",
                        "prev": "https://sample.net"
                    };
                    const expected = {
                        Link: '<https://acme.org>; rel="next", <https://sample.net>; rel="prev"'
                    };
                    const result = Response.linkHeader(input);
                    result.should.eql(expected);
                });

                it("with previous header", function () {
                    const previous = '<https://one.net>; rel="https://acme.org/one"';
                    const input = {
                        "next": "https://acme.org",
                        "prev": "https://sample.net"
                    };
                    const expected = {
                        Link: '<https://one.net>; rel="https://acme.org/one", <https://acme.org>; rel="next", <https://sample.net>; rel="prev"'
                    };
                    const result = Response.linkHeader(input, previous);
                    result.should.eql(expected);
                });
            });

            describe("with LDO array", function () {
                it("empty", function () {
                    const expected = {
                        Link: ""
                    };
                    const result = Response.linkHeader([]);
                    result.should.eql(expected);
                });

                it("empty but with previous", function () {
                    const previous = '<https://one.net>; rel="https://acme.org/one"';
                    const expected = {
                        Link: previous
                    };
                    const result = Response.linkHeader([], previous);
                    result.should.eql(expected);
                });

                it("one entry", function () {
                    const input = [{
                        rel: "next",
                        href: "https://acme.org"
                    }];
                    const expected = {
                        Link: '<https://acme.org>; rel="next"'
                    };
                    const result = Response.linkHeader(input);
                    result.should.eql(expected);
                });

                it("several entries", function () {
                    const input = [{
                        rel: "next",
                        href: "https://acme.org"
                    },
                    {
                        rel: "prev",
                        href: "https://sample.net"
                    }
                    ];
                    const expected = {
                        Link: '<https://acme.org>; rel="next", <https://sample.net>; rel="prev"'
                    };
                    const result = Response.linkHeader(input);
                    result.should.eql(expected);
                });

                it("with aditional link properties", function () {
                    const input = [{
                        rel: "next",
                        href: "https://acme.org",
                        title: "proximo"
                    },
                    {
                        rel: "prev",
                        href: "https://sample.net",
                        name: "anterior"
                    }
                    ];
                    const expected = {
                        Link: '<https://acme.org>; rel="next"; title="proximo", <https://sample.net>; rel="prev"; name="anterior"'
                    };
                    const result = Response.linkHeader(input);
                    result.should.eql(expected);
                });

                it("with previous header", function () {
                    const previous = '<https://one.net>; rel="https://acme.org/one"';
                    const input = [{
                        rel: "next",
                        href: "https://acme.org"
                    },
                    {
                        rel: "prev",
                        href: "https://sample.net"
                    }
                    ];
                    const expected = {
                        Link: '<https://one.net>; rel="https://acme.org/one", <https://acme.org>; rel="next", <https://sample.net>; rel="prev"'
                    };
                    const result = Response.linkHeader(input, previous);
                    result.should.eql(expected);
                });
            });
        });

        describe("linkArrayToObject", function () {
            it("throws if input is string", function () {
                expect(Response.linkArrayToObject.bind(Response, "abc")).to.throw(TypeError);
            });

            it("throws if input is object", function () {
                expect(Response.linkArrayToObject.bind(Response, {})).to.throw(TypeError);
            });

            it("throws if input is number", function () {
                expect(Response.linkArrayToObject.bind(Response, 1)).to.throw(TypeError);
            });

            it("returns empty object on empty array", function () {
                const result = Response.linkArrayToObject([]);
                result.should.eql({});
            });

            it("returns empty object on null/missing parameter", function () {
                const result = Response.linkArrayToObject();
                result.should.eql({});
            });

            it("works ok with 1 object", function () {
                const input = [{
                    rel: "next",
                    href: "https://acme.org"
                }];
                const expected = {
                    "next": "https://acme.org"
                };
                const result = Response.linkArrayToObject(input);
                result.should.eql(expected);
            });

            it("works ok with 2 object", function () {
                const input = [{
                    rel: "next",
                    href: "https://acme.org"
                }, {
                    rel: "prev",
                    href: "https://sample.net"
                }];
                const expected = {
                    "next": "https://acme.org",
                    "prev": "https://sample.net"
                };
                const result = Response.linkArrayToObject(input);
                result.should.eql(expected);
            });

            it("ignores extra properties", function () {
                const input = [{
                    rel: "next",
                    href: "https://acme.org",
                    title: "proximo"
                }];
                const expected = {
                    "next": "https://acme.org"
                };
                const result = Response.linkArrayToObject(input);
                result.should.eql(expected);
            });
        });

        describe("linkHashToArray", function () {
            it("returns empty array on empty input", function () {
                const result = Response.linkHashToArray({});
                result.should.eql([]);
            });

            it("returns empty array on missing input", function () {
                const result = Response.linkHashToArray();
                result.should.eql([]);
            });

            it("returns array", function () {
                const input = {
                    "next": "https://acme.org",
                    "prev": "https://sample.net"
                };
                const expected = [{
                    rel: "next",
                    href: "https://acme.org"
                }, {
                    rel: "prev",
                    href: "https://sample.net"
                }];
                const result = Response.linkHashToArray(input);
                result.should.eql(expected);
            });
        });

        describe("linkHeaderObject", function () {
            it("empty object on empty array", function () {
                const result = Response.linkHeaderObject([]);
                result.should.eql({});
            });

            it("empty object on null/missing parameter", function () {
                const result = Response.linkHeaderObject();
                result.should.eql({});
            });

            it("same object if object", function () {
                const linkHash = {
                    "next": "https://acme.org"
                };
                const result = Response.linkHeaderObject(linkHash);
                result.should.eql(linkHash);
            });

            it("ok with array ignoring extra properties", function () {
                const input = [{
                    rel: "next",
                    href: "https://acme.org",
                    title: "proximo"
                }, {
                    rel: "prev",
                    href: "https://sample.net"
                }];
                const expected = {
                    "next": "https://acme.org",
                    "prev": "https://sample.net"
                };
                const result = Response.linkHeaderObject(input);
                result.should.eql(expected);
            });
        });

        describe("cleanupContent", function () {
            // avoid Sonar warnings regarding hardcoded credentials
            process.env.DEMO_PVALUE = "abc";
            const DEMO_PVALUE = process.env.DEMO_PVALUE;

            describe("object", function () {
                it("handles empty objects", function () {
                    const result = Response.cleanupContent({});
                    result.should.eql({});
                });

                it("default filter", function () {
                    const input = {
                        _id: "1",
                        __v: "1",
                        password: DEMO_PVALUE,
                        name: "Acme"
                    };
                    const expected = {
                        name: "Acme"
                    };
                    const result = Response.cleanupContent(input);
                    result.should.eql(expected);
                });

                it("same object if empty filter", function () {
                    const input = {
                        _id: "1",
                        __v: "1",
                        password: DEMO_PVALUE,
                        name: "Acme"
                    };
                    const expected = {
                        _id: "1",
                        __v: "1",
                        password: DEMO_PVALUE,
                        name: "Acme"
                    };
                    const result = Response.cleanupContent(input, []);
                    result.should.eql(expected);
                });

                it("same object if no property in filter", function () {
                    const input = {
                        _id: "1",
                        __v: "1",
                        password: DEMO_PVALUE,
                        name: "Acme"
                    };
                    const result = Response.cleanupContent(input, ["title"]);
                    result.should.eql(input);
                });

                it("filter property", function () {
                    const input = {
                        name: "Acme",
                        email: "acme@acme.org"
                    };
                    const expected = {
                        name: "Acme"
                    };
                    const result = Response.cleanupContent(input, ["email"]);
                    result.should.eql(expected);
                });

                it("works with toObject", function () {
                    const input = {
                        _id: "1",
                        __v: "1",
                        password: DEMO_PVALUE,
                        name: "Acme",
                        toObject: () => {
                            return {
                                _id: "1",
                                __v: "1",
                                password: DEMO_PVALUE,
                                name: "Acme obj"
                            };
                        }
                    };
                    const expected = {
                        name: "Acme obj"
                    };
                    const result = Response.cleanupContent(input);
                    result.should.eql(expected);
                });
            });

            describe("hyper object", function () {
                it("default filter", function () {
                    const input = {
                        _id: "1",
                        __v: "1",
                        password: DEMO_PVALUE,
                        data: {
                            _id: "2",
                            __v: "2",
                            password: DEMO_PVALUE + "2",
                            name: "Acme"
                        }
                    };
                    const expected = {
                        _id: "1",
                        __v: "1",
                        password: DEMO_PVALUE,
                        data: {
                            name: "Acme"
                        }
                    };
                    const result = Response.cleanupContent(input);
                    result.should.eql(expected);
                });

                it("same object if empty filter", function () {
                    const input = {
                        links: {
                            "next": "acme.org"
                        },
                        data: {
                            _id: "1",
                            __v: "1",
                            password: DEMO_PVALUE,
                            name: "Acme"
                        }
                    };
                    const expected = {
                        links: {
                            "next": "acme.org"
                        },
                        data: {
                            _id: "1",
                            __v: "1",
                            password: DEMO_PVALUE,
                            name: "Acme"
                        }
                    };
                    const result = Response.cleanupContent(input, []);
                    result.should.eql(expected);
                });

                it("same object if no property in filter", function () {
                    const input = {
                        links: {
                            "next": "acme.org"
                        },
                        data: {
                            _id: "1",
                            __v: "1",
                            password: DEMO_PVALUE,
                            name: "Acme"
                        }
                    };
                    const result = Response.cleanupContent(input, ["title"]);
                    result.should.eql(input);
                });

                it("filter property", function () {
                    const input = {
                        links: {
                            "next": "acme.org"
                        },
                        data: {
                            name: "Acme",
                            email: "acme@acme.org"
                        }
                    };
                    const expected = {
                        links: {
                            "next": "acme.org"
                        },
                        data: {
                            name: "Acme"
                        }
                    };
                    const result = Response.cleanupContent(input, ["email"]);
                    result.should.eql(expected);
                });

                it("cleans data array", function () {
                    const input = {
                        template: {
                            abc: "abc"
                        },
                        data: [{
                            links: {
                                "next": "acme.org"
                            },
                            data: {
                                _id: "1",
                                "__v": "1",
                                password: DEMO_PVALUE,
                                name: "Acme 1",
                                email: "acme@acme.org"
                            }
                        }, {
                            links: {
                                "next": "acme.org"
                            },
                            data: {
                                _id: "2",
                                "__v": "2",
                                password: DEMO_PVALUE,
                                name: "Acme 2",
                                email: "acme@acme.org"
                            }
                        }]
                    };
                    const expected = {
                        template: {
                            abc: "abc"
                        },
                        data: [{
                            links: {
                                "next": "acme.org"
                            },
                            data: {
                                name: "Acme 1",
                                email: "acme@acme.org"
                            }
                        }, {
                            links: {
                                "next": "acme.org"
                            },
                            data: {
                                name: "Acme 2",
                                email: "acme@acme.org"
                            }
                        }]
                    };
                    const result = Response.cleanupContent(input);
                    result.should.eql(expected);
                });
            });

            describe("array of objects", function () {
                it("handles empty arrays", function () {
                    const result = Response.cleanupContent([]);
                    result.should.eql([]);
                });

                it("cleans up each entry", function () {
                    const input = [{
                        _id: "1",
                        __v: "1",
                        password: DEMO_PVALUE,
                        name: "Acme 1"
                    }, {
                        _id: "2",
                        __v: "2",
                        password: DEMO_PVALUE,
                        name: "Acme 2"
                    }, {
                        _id: "3",
                        __v: "3",
                        password: DEMO_PVALUE,
                        name: "Acme 3"
                    }];
                    const expected = [{
                        name: "Acme 1"
                    }, {
                        name: "Acme 2"
                    }, {
                        name: "Acme 3"
                    }];
                    const result = Response.cleanupContent(input);
                    result.should.eql(expected);
                });
            });

            describe("array of hyper objects", function () {
                it("cleans up each entry", function () {
                    const input = [{
                        data: {
                            _id: "1",
                            __v: "1",
                            password: DEMO_PVALUE,
                            name: "Acme 1"
                        }
                    }, {
                        data: {
                            _id: "2",
                            __v: "2",
                            password: DEMO_PVALUE,
                            name: "Acme 2"
                        }
                    }, {
                        data: {
                            _id: "3",
                            __v: "3",
                            password: DEMO_PVALUE,
                            name: "Acme 3"
                        }
                    }];
                    const expected = [{
                        data: {
                            name: "Acme 1"
                        }
                    }, {
                        data: {
                            name: "Acme 2"
                        }
                    }, {
                        data: {
                            name: "Acme 3"
                        }
                    }];
                    const result = Response.cleanupContent(input);
                    result.should.eql(expected);
                });
            });
        });

        describe("ensureHeaders", function () {
            it("adds ETag from content", function () {
                const ETAG = "acme";
                const input = {
                    __v: ETAG,
                    name: "Anton"
                };

                const mockResponse = neitherEtagNorLinkResponse();

                Response.ensureHeaders(mockResponse, input);

                mockResponse.set.withArgs("ETag").callCount.should.eql(1);
                mockResponse.set.calledWith("ETag", ETAG).should.eql(true);
            });

            it("adds ETag from content as string", function () {
                const ETAG = 1;
                const input = {
                    __v: ETAG,
                    name: "Anton"
                };

                const mockResponse = neitherEtagNorLinkResponse();

                Response.ensureHeaders(mockResponse, input);

                mockResponse.set.withArgs("ETag").callCount.should.eql(1);
                mockResponse.set.calledWith("ETag", "1").should.eql(true);
            });

            it("does not add ETag if unexistent in content", function () {
                const input = {
                    name: "Anton"
                };

                const mockResponse = neitherEtagNorLinkResponse();

                Response.ensureHeaders(mockResponse, input);

                mockResponse.set.withArgs("ETag").callCount.should.eql(0);
            });

            it("adds ETag from hyper content", function () {
                const ETAG = "acme";
                const input = {
                    data: {
                        __v: ETAG,
                        name: "Anton"
                    }
                };

                const mockResponse = neitherEtagNorLinkResponse();

                Response.ensureHeaders(mockResponse, input);

                mockResponse.set.withArgs("ETag").callCount.should.eql(1);
                mockResponse.set.calledWith("ETag", ETAG).should.eql(true);
            });

            it("adds ETag from hyper content as string", function () {
                const ETAG = 1;
                const input = {
                    data: {
                        __v: ETAG,
                        name: "Anton"
                    }
                };

                const mockResponse = neitherEtagNorLinkResponse();

                Response.ensureHeaders(mockResponse, input);

                mockResponse.set.withArgs("ETag").callCount.should.eql(1);
                mockResponse.set.calledWith("ETag", "1").should.eql(true);
            });

            it("does not change pre-existing ETag", function () {
                const ETAG = "acme";
                const input = {
                    __v: "12345",
                    name: "Anton"
                };

                const mockResponse = withEtagButNoLinkResponse(ETAG);

                Response.ensureHeaders(mockResponse, input);

                mockResponse.set.withArgs("ETag").callCount.should.eql(0);
            });

            it("does not change pre-existing ETag from explicit header", function () {
                const ETAG = "acme";
                const content = {
                    __v: "12345",
                    name: "Anton"
                };
                const headers = {
                    "ETag": ETAG
                };

                const mockResponse = withEtagButNoLinkResponse(ETAG);

                Response.ensureHeaders(mockResponse, content, [], headers);

                mockResponse.set.withArgs("ETag").callCount.should.eql(0);
            });

            it("does not change pre-existing ETag (hyper)", function () {
                const ETAG = "acme";
                const input = {
                    data: {
                        __v: "12345",
                        name: "Anton"
                    }
                };

                const mockResponse = withEtagButNoLinkResponse(ETAG);

                Response.ensureHeaders(mockResponse, input);

                mockResponse.set.withArgs("ETag").callCount.should.eql(0);
            });

            it("adds Link from content.links", function () {
                const input = {
                    links: [{
                        rel: "next",
                        href: "https://acme.org"
                    }],
                    name: "Anton"
                };

                const mockResponse = neitherEtagNorLinkResponse();

                Response.ensureHeaders(mockResponse, input);

                mockResponse.set.calledWith({
                    "Link": '<https://acme.org>; rel="next"'
                }).should.eql(true);
            });

            it("adds Link from content._links", function () {
                const input = {
                    _links: [{
                        rel: "next",
                        href: "https://acme.org"
                    }],
                    name: "Anton"
                };

                const mockResponse = neitherEtagNorLinkResponse();

                Response.ensureHeaders(mockResponse, input);

                mockResponse.set.calledWith({
                    "Link": '<https://acme.org>; rel="next"'
                }).should.eql(true);
            });

            it("adds explicit Links", function () {
                const input = {
                    name: "Anton"
                };
                const links = [{
                    rel: "next",
                    href: "https://acme.org"
                }];

                const mockResponse = neitherEtagNorLinkResponse();

                Response.ensureHeaders(mockResponse, input, links);

                mockResponse.set.calledWith({
                    "Link": '<https://acme.org>; rel="next"'
                }).should.eql(true);
            });

            it("merges explicit Links with content links", function () {
                const input = {
                    name: "Anton",
                    links: [{
                        rel: "prev",
                        href: "https://sample.org"
                    }]
                };
                const links = [{
                    rel: "next",
                    href: "https://acme.org"
                }];

                const mockResponse = neitherEtagNorLinkResponse();

                Response.ensureHeaders(mockResponse, input, links);

                mockResponse.set.calledWith({
                    "Link": '<https://sample.org>; rel="prev", <https://acme.org>; rel="next"'
                }).should.eql(true);
            });

            it("preserves existing links", function () {
                const input = {
                    name: "Anton"
                };

                const mockResponse = withLinkButNoETagResponse();

                Response.ensureHeaders(mockResponse, input);

                mockResponse.set.calledWith({
                    "Link": '<https://acme.org>; rel="next"'
                }).should.eql(true);
            });

            it("adds explicit headers", function () {
                const content = {
                    name: "Anton"
                };
                const headers = {
                    "accept": "text/html"
                };

                const mockResponse = neitherEtagNorLinkResponse();

                Response.ensureHeaders(mockResponse, content, [], headers);

                mockResponse.set.withArgs(headers).callCount.should.eql(1);
            });
        });
    });

    describe("helper send methods", function () {
        describe("sendJSON", function () {
            it("works", function () {
                const content = {
                    name: "Anton",
                    __v: 1
                };
                const clean_content = {
                    name: "Anton"
                };
                const headers = {
                    "accept": "text/html"
                };
                const links = [{
                    rel: "next",
                    href: "https://acme.org"
                }];
                const LINK_HEADER = '<https://acme.org>; rel="next"';
                const CODE = 201;
                const mockResponse = mockSendResponse();

                Response.sendJSON(mockResponse, CODE, content, links, headers);

                mockResponse.status.withArgs(CODE).should.have.been.called;
                mockResponse.set.withArgs("ETag").should.have.been.called;
                mockResponse.set.withArgs(headers).should.have.been.called;
                mockResponse.set.withArgs({
                    "Link": LINK_HEADER
                }).should.have.been.called;
                mockResponse.json.withArgs(clean_content).should.have.been.called;
            });

            it("works with missing optional parameters", function () {
                const content = {
                    name: "Anton",
                    __v: 1
                };
                const clean_content = {
                    name: "Anton"
                };
                const CODE = 201;
                const mockResponse = mockSendResponse();

                Response.sendJSON(mockResponse, CODE, content);

                mockResponse.status.withArgs(CODE).should.have.been.called;
                mockResponse.set.withArgs("ETag").should.have.been.called;
                mockResponse.json.withArgs(clean_content).should.have.been.called;
            });
        });

        describe("sendTxt", function () {
            it("works", function () {
                const content = {
                    name: "Anton"
                };
                const headers = {
                    "accept": "text/html"
                };
                const links = {
                    "next": "https://acme.org"
                };
                const CODE = 201;
                const mockResponse = mockSendResponse();

                Response.sendTxt(mockResponse, CODE, content, links, headers);

                mockResponse.status.withArgs(CODE).should.have.been.called;
                mockResponse.set.withArgs(headers).should.have.been.called;
                mockResponse.links.withArgs(links).should.have.been.called;
                mockResponse.send.withArgs(content).should.have.been.called;
            });

            it("works with missing optional parameters", function () {
                const content = {
                    name: "Anton"
                };
                const CODE = 201;
                const mockResponse = mockSendResponse();

                Response.sendTxt(mockResponse, CODE, content);

                mockResponse.status.withArgs(CODE).should.have.been.called;
                mockResponse.set.withArgs({}).should.have.been.called;
                mockResponse.links.withArgs({}).should.have.been.called;
                mockResponse.send.withArgs(content).should.have.been.called;
            });
        });

        describe("handleAsNotAllowed", function () {
            it("works", function () {
                const mockResponse = mockSendResponse();

                Response.handleAsNotAllowed({}, mockResponse);

                mockResponse.status.withArgs(405).should.have.been.called;
                mockResponse.send.should.have.been.called;
            });
        });
    });

    describe("send methods", function () {
        describe("send", function () {
            describe("objects", function () {
                it("works for single object", function () {
                    const content = {
                        name: "Anton",
                        __v: 1
                    };
                    const clean_content = {
                        name: "Anton"
                    };
                    const headers = {
                        "accept": "text/html"
                    };
                    const links = [{
                        rel: "next",
                        href: "https://acme.org"
                    }];
                    const LINK_HEADER = '<https://acme.org>; rel="next"';
                    const CODE = 201;
                    const settings = {
                        API_VERSION: "1.0.0"
                    };
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse, settings);
                    subject.send(CODE, content, links, headers, ["__v"]);

                    mockResponse.status.withArgs(CODE).should.have.been.called;
                    mockResponse.set.withArgs("X-API-Version", "1.0.0").should.have.been.called;
                    mockResponse.set.withArgs("ETag", "1").should.have.been.called;
                    mockResponse.set.withArgs(headers).should.have.been.called;
                    mockResponse.set.withArgs({
                        "Link": LINK_HEADER
                    }).should.have.been.called;
                    mockResponse.json.withArgs(clean_content).should.have.been.called;
                });

                it("works for arrays", function () {
                    const content = [{
                        name: "Anton",
                        __v: 1
                    }];
                    const clean_content = [{
                        name: "Anton"
                    }];
                    const headers = {
                        "accept": "text/html"
                    };
                    const links = [{
                        rel: "next",
                        href: "https://acme.org"
                    }];
                    const LINK_HEADER = '<https://acme.org>; rel="next"';
                    const CODE = 201;
                    const settings = {
                        API_VERSION: "1.0.0"
                    };
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse, settings);
                    subject.send(CODE, content, links, headers, ["__v"]);

                    mockResponse.status.withArgs(CODE).should.have.been.called;
                    mockResponse.set.withArgs("X-API-Version", "1.0.0").should.have.been.called;
                    mockResponse.set.withArgs(headers).should.have.been.called;
                    mockResponse.set.withArgs({
                        "Link": LINK_HEADER
                    }).should.have.been.called;
                    mockResponse.json.withArgs(clean_content).should.have.been.called;
                });

                it("works with missing optional parameters", function () {
                    const content = {
                        name: "Anton",
                        __v: 1
                    };
                    const clean_content = {
                        name: "Anton"
                    };
                    const CODE = 201;
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.send(CODE, content);

                    mockResponse.status.withArgs(CODE).should.have.been.called;
                    mockResponse.set.withArgs("ETag", "1").should.have.been.called;
                    mockResponse.set.withArgs({}).should.have.been.called;
                    mockResponse.set.withArgs({
                        "Link": ""
                    }).should.have.been.called;
                    mockResponse.json.withArgs(clean_content).should.have.been.called;
                });

                it("works for Error objects", function () {
                    const MESSAGE = "This is an error";
                    const content = new RangeError(MESSAGE);
                    const CODE = 400;
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.send(CODE, content);

                    mockResponse.status.withArgs(CODE).should.have.been.called;
                    mockResponse.set.withArgs({}).should.have.been.called;
                    mockResponse.set.withArgs({
                        "Link": ""
                    }).should.have.been.called;
                    mockResponse.send.withArgs(MESSAGE).should.have.been.called;
                });

                it("works for other types", function () {
                    const MESSAGE = "This is an error";
                    const CODE = 400;
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.send(CODE, MESSAGE);

                    mockResponse.status.withArgs(CODE).should.have.been.called;
                    mockResponse.set.withArgs({}).should.have.been.called;
                    mockResponse.set.withArgs({
                        "Link": ""
                    }).should.have.been.called;
                    mockResponse.send.withArgs(MESSAGE).should.have.been.called;
                });
            });

            describe("return codes helper", function () {
                const content = {
                    name: "Anton",
                    __v: 1
                };
                const clean_content = {
                    name: "Anton"
                };

                it("works for 200 Ok", function () {
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.ok(content);

                    mockResponse.status.withArgs(200).should.have.been.called;
                    mockResponse.set.withArgs("ETag", "1").should.have.been.called;
                    mockResponse.json.withArgs(clean_content).should.have.been.called;
                });

                it("works for 201 Created", function () {
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.created(content, "https://acme.org");

                    mockResponse.status.withArgs(201).should.have.been.called;
                    mockResponse.set.withArgs("ETag", "1").should.have.been.called;
                    mockResponse.set.withArgs("Location", "https://acme.org").should.have.been.called;
                    mockResponse.json.withArgs(clean_content).should.have.been.called;
                });

                it("works for 202 Accepted", function () {
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.accepted(content, "https://acme.org");

                    mockResponse.status.withArgs(202).should.have.been.called;
                    mockResponse.set.withArgs("ETag", "1").should.have.been.called;
                    mockResponse.set.withArgs("Location", "https://acme.org").should.have.been.called;
                    mockResponse.json.withArgs(clean_content).should.have.been.called;
                });

                it("works for 400 Bad Request", function () {
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.badRequest(content);

                    mockResponse.status.withArgs(400).should.have.been.called;
                    mockResponse.json.withArgs(clean_content).should.have.been.called;
                });

                it("works for 401 Unauthorized", function () {
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.unauthorized("content");

                    mockResponse.status.withArgs(401).should.have.been.called;
                    mockResponse.send.withArgs("content").should.have.been.called;
                });

                it("works for 403 Forbidden", function () {
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.forbidden("content");

                    mockResponse.status.withArgs(403).should.have.been.called;
                    mockResponse.send.withArgs("content").should.have.been.called;
                });

                it("works for 404 Not Found", function () {
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.notFound("content");

                    mockResponse.status.withArgs(404).should.have.been.called;
                    mockResponse.send.withArgs("content").should.have.been.called;
                });

                it("works for 405 Not Allowed", function () {
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.notAllowed("content");

                    mockResponse.status.withArgs(405).should.have.been.called;
                    mockResponse.send.withArgs("content").should.have.been.called;
                });

                it("works for 406 Not Acceptable", function () {
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.notAcceptable("content");

                    mockResponse.status.withArgs(406).should.have.been.called;
                    mockResponse.send.withArgs("content").should.have.been.called;
                });

                it("works for 409 Conflict", function () {
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.conflict("content");

                    mockResponse.status.withArgs(409).should.have.been.called;
                    mockResponse.send.withArgs("content").should.have.been.called;
                });

                it("works for 413 Payload Too Large", function () {
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.payloadTooLarge("content");

                    mockResponse.status.withArgs(413).should.have.been.called;
                    mockResponse.send.withArgs("content").should.have.been.called;
                });

                it("works for 415 Unsupported Media Type", function () {
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.unsupportedMediaType("content");

                    mockResponse.status.withArgs(415).should.have.been.called;
                    mockResponse.send.withArgs("content").should.have.been.called;
                });

                it("works for 500 Server Error", function () {
                    const mockResponse = mockSendResponse();

                    const subject = new Response(mockResponse);
                    subject.serverError("content");

                    mockResponse.status.withArgs(500).should.have.been.called;
                    mockResponse.send.withArgs("content").should.have.been.called;
                });
            });

            describe("errors", function () {
                it("works");
                it("works with missing optional parameters");
            });

            describe("text", function () {
                it("works");
                it("works with missing optional parameters");
            });
        });
    });
});