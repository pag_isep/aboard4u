/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 9 */
/*jshint mocha: true */
/*jshint expr: true */

"use strict";

import sinon from 'sinon';
import {
    expect
} from 'chai';

// test subject
import {
    limitMaxResults,
    limitPagingResults,
    getMaxResults
} from './pagination-limits.js';

describe("Pagination Limiter module", function () {
    describe("limitMaxResults", function () {
        it("provides default n", function () {
            limitMaxResults(null).should.be.a("Number");
        });

        it("respects MAX_RESULTS", function () {
            limitMaxResults(10000).should.be.eql(getMaxResults());
        });

        it("respects parameter if lower than MAX_RESULTS", function () {
            let v = getMaxResults() - 1;

            limitMaxResults(v).should.be.eql(v);
        });
    });

    describe("limitPagingResults", function () {
        context("process.env.ABOARD4U_MAX_RESULTS", function () {
            let envStub;
            let oldEnvValue;

            before(function () {
                oldEnvValue = process.env.ABOARD4U_MAX_RESULTS;
                process.env.ABOARD4U_MAX_RESULTS = "";
            });

            after(function () {
                process.env.ABOARD4U_MAX_RESULTS = oldEnvValue;
            });

            beforeEach(function () {
                envStub = sinon.stub(process.env, "ABOARD4U_MAX_RESULTS");
            });

            afterEach(function () {
                envStub.restore();
            });

            it("respects env.MAX_RESULTS", function () {
                const max_test = 50;
                envStub.value(max_test.toString());

                const result = limitPagingResults(0, 1000);

                expect(result.n.toString()).to.eql(process.env.ABOARD4U_MAX_RESULTS);
            });

            it("respects internal max if env.MAX_RESULTS is not defined", function () {
                envStub.value(null);

                const result = limitPagingResults(0, 10000);

                result.n.should.be.eql(getMaxResults());
            });

            it("respects internal max if MAX_RESULTS is not parseable to a number", function () {
                envStub.value("abc");
                const result = limitPagingResults(0, 10000);

                result.n.should.be.eql(getMaxResults());
            });
        });

        it("provides default 'n'", function () {
            const result = limitPagingResults(0, null);

            result.n.should.exist;
        });

        it("provides default 'start'", function () {
            const result = limitPagingResults(null, 10);

            result.start.should.exist;
        });

        it("Ok", function () {
            const result = limitPagingResults(1, 100);

            result.start.should.be.eql(1);
            result.n.should.be.eql(100);
        });
    });

    context("currentMaxResults", function () {
        it("has current MAX_RESULTS", function () {
            getMaxResults().should.be.a("Number");
        });
    });
});