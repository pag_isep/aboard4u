/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 9 */
/*jshint mocha: true */
/*jshint expr: true */

"use strict";

// test subject
import {createID, createToken} from './id-token-generator.js';

describe("ID/token Generator module", function () {
    describe("createdID", function () {
        it("generates the id without suffix", function () {
            const result = createID();

            result.should.be.a("String");
            result.should.have.length.above(1);
        });

        it("generates the id with suffix", function () {
            const result = createID("ABC");

            result.should.be.a("String");
            result.should.have.length.above(4);
            result.should.satisfy(e => e.endsWith("ABC"));
        });
    });

    describe("createToken", function () {
        it("generates a token", function () {
            const result = createToken();

            result.should.be.a("String");
            result.should.have.length.above(1);
        });
    });
});