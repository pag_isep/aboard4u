/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

import {
	//PayloadTooLargeError,
	UnsupportedMediaTypeError
} from  "../exceptions.js";

/////////////////////////////
// MODULE EXPORTS

/**
 * Express middleware to validate the right `content-type` header is passed to a route. Uses `req.is` behind the scenes.
 * 
 * Inpired on https://github.com/NatLibFi/express-validate-content-type-js/blob/master/src/index.js 
 * 
 * @param {string[]} types - array of media types 
 */
export function ensureContentType(types) {
	return (req, res, next) => {
		if (types.some(type => req.is(type))) {
			next();
		} else {
			next(new UnsupportedMediaTypeError(types, req.get('Content-Type')));
		}
	};
}