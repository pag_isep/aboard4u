/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

import _ from "lodash";

//
// Utilities for the application layer
//


/**
 * returns a string which can be used as id for a message
 * @param {String} [sufix] 
 */
export function createID(sufix = "") {
    const prefix1 = String.fromCharCode('A'.charCodeAt(0) + _.random(25));
    const prefix2 = String.fromCharCode('Z'.charCodeAt(0) - _.random(25));
    const infix = _.random(1, true).toString().substr(1);

    return `${prefix1}${prefix2}${infix}${sufix}`;
}


/**
 * creates a token
 * @returns {String} a password reset token
 */
export function createToken() {
    const prefix = () => String.fromCharCode("a".charCodeAt(0) + _.random(26));
    const r = () => _.random(10000).toString();
    const t = () => (new Date()).getTime();

    const parts = [
        prefix() + r() + t(),
        prefix() + r() + t(),
        prefix() + r() + t()
    ];
    return parts.reduce((memo, part) => memo + part);
}