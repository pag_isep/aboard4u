/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 9 */
/*jshint mocha: true */
/*jshint expr: true */

"use strict";

import sinon from 'sinon';
import {expect} from 'chai';

// test subject
import {Hyper} from './pag-hyper.js';

describe("Hypermedia format 'application/vnd.pagsousa.hyper+json' helper module", function () {
    describe("item", function () {
        it("should return data and links", function () {
            const obj = {
                one: "1"
            };
            const link = {
                rel: ""
            };
            const builder = (e) => link;

            const result = Hyper.item(obj, builder);

            result.should.have.property("data", obj);
            result.should.have.property("links", link);
        });

        it("Should cleanup content", function () {
            const obj = {
                one: "1"
            };
            const link = {
                rel: ""
            };
            const builder = (e) => link;
            const clean = {
                two: "2"
            };
            const cleanup = (e) => clean;

            const result = Hyper.item(obj, builder, cleanup);

            result.should.have.property("data", clean);
            result.should.have.property("links", link);
        });

        it("should call linkbuilder", function () {
            const obj = {
                one: "1"
            };
            const link = {
                "self": "this/is/me"
            };

            function builder(e) {
                return link;
            }

            const spy = sinon.spy(builder);

            const result = Hyper.item(obj, spy);

            result.should.have.property("data");
            result.should.have.property("links", link);

            expect(spy).to.have.been.calledOnce;
            expect(spy).to.have.been.calledWith(obj);
        });
    });

    describe("error", function () {
        it("should return error and links #1", function () {
            const obj = {};
            const link = {};

            const result = Hyper.error(obj, link);

            result.should.have.property("error", obj);
            result.should.have.property("links", link);
        });

        it("should return error and links #2", function () {
            const obj = {
                one: "1"
            };
            const link = {
                "self": "this/is/me"
            };

            const result = Hyper.error(obj, link);

            result.should.have.property("error", obj);
            result.should.have.property("links", link);
        });
    });

    describe("collection", function () {
        it("should return data, links, template and queries", function () {
            const emptyCollection = [];
            const links = {};
            const template = {};
            const queries = {};

            const result = Hyper.collection(emptyCollection, null, links, template, queries);

            result.should.have.property("data");
            result.data.should.have.length(0);
            result.should.have.property("links");
            result.links.should.be.eql({});
            result.should.have.property("template", template);
            result.should.have.property("queries", queries);
        });

        it("should not build element links if there are no elements", function () {
            const spy = sinon.spy();

            Hyper.collection([], spy, {}, {}, {});

            spy.should.have.not.been.called;
        });

        it("Should cleanup content", function () {
            const clean = ["a", "b", "c"];
            const cleanup = (c) => clean;

            const result = Hyper.collection([], null, {}, {}, {}, cleanup);

            result.should.have.property("data", clean);
        });

        it("should build as many elements as source elements", function () {
            const elements = [1, 2, 3, 4, 5];
            const builder = function (e) {
                return e;
            };

            const spy = sinon.spy(builder);

            const result = Hyper.collection(elements, spy, {}, {}, {});

            sinon.assert.callCount(spy, elements.length);

            expect(result.data).to.have.length(elements.length);

            result.data.forEach(function (e) {
                expect(e).to.have.property("data");
                expect(e).to.have.property("links");
            });
        });
    });
});