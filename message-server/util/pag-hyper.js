/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// Hypermedia manipulation according to media type: "application/vnd.pagsousa.hyper+json"
//


/////////////////////////////
// MODULE EXPORTS

//
// hypermedia helper
//
export const Hyper = {
    /**
     * 
     * @param {Object} data 
     * @param {function(data:Object)} linksBuilder - function 
     * @param {function} cleanup
     */
    item: function (dataObj, linksBuilder, cleanup = (i) => i) {
        return {
            data: cleanup(dataObj),
            links: linksBuilder(dataObj),
        };
    },

    /**
     * 
     * @param {*} data 
     * @param {*} links 
     */
    error: function (data, links) {
        return {
            error: data,
            links: links
        };
    },

    /**
     * 
     * @param {Object[]} data               - array of objects
     * @param {function} itemLinksBuilder   - 
     * @param {Object[]} Links              - array (according to collection+json)
     * @param {Object} template             - object (according to collection+json)
     * @param {*} queries                   -
     * @param {function} cleanup            - a cleanup function
     */
    collection: function (data, itemLinksBuilder, links, template, queries, cleanup = (i) => i) {
        const hyperData = data.map((i) => this.item(i, itemLinksBuilder));

        return {
            data: cleanup(hyperData),
            links,
            template,
            queries
        };
    }
};