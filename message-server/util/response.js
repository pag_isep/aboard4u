/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// Response manipulation
//


import _ from "lodash";

import {
    NotFoundError,
    UnauthenticatedError,
    ForbiddenError,
    ConcurrencyError,
    ConflictError,
    UnsupportedMediaTypeError,
    PayloadTooLargeError,
    BadRequestError
} from "../exceptions.js";

/**
 * Response wrapper around an existing http response object.
 * Provides helper static methods 
 * 
 * @param {*} resp - the http response object to wrapp
 */
export class Response {

    constructor(resp, SETTINGS = {}) {
        const _res = resp;
        const _SETTINGS = SETTINGS;

        this.res = function () {
            return _res;
        };

        this.settings = function () {
            return _SETTINGS;
        };
    }

    //
    // Response helper
    //

    /**
     * creates ETag header based on the existence of "__v" property or "data.__v"
     * @param {Object} entry
     * @returns an object with the etag header if there is an "__v" property in entry 
     */
    static etagHeader(entry) {
        let etag;
        if (entry) {
            if (entry.data) {
                etag = entry.data.__v ? entry.data.__v.toString() : null;
            } else {
                etag = entry.__v ? entry.__v.toString() : null;
            }
        }
        if (etag) {
            return {
                "ETag": etag
            };
        } else {
            return {};
        }
    }

    /**
     * Builds the string fragment for a link header element with just the url and relationship type
     * @param {string} rel 
     * @param {string} href 
     */
    static linkHeaderElement1(rel = "home", href = "/") {
        return `<${href}>; rel="${rel}"`;
    }

    /**
     * Builds the string fragment for a link header element from a Link Description Object. Adds all properties to the fragment
     *  
     * @param {LinkDescriptionObject} ldo 
     * @returns {string}
     */
    static linkHeaderElement(ldo = {}) {
        let element = Response.linkHeaderElement1(ldo.rel, ldo.href);
        if (ldo.name) {
            element = element + `; name="${ldo.name}"`;
        }
        if (ldo.type) {
            element = element + `; type="${ldo.type}"`;
        }
        if (ldo.title) {
            element = element + `; title="${ldo.title}"`;
        }
        return element;
    }

    /**
     * Builds the link header object to pass to res.set()
     * 
     * @param {*} data - either an array of link description objects or an object which key is the rel and value is the href of a link
     * @param {string} previous - current link header value to append
     * @returns {object} an object wich key is "Link" and value is the link header
     */
    static linkHeader(data = [], previous = "") {
        let ldos;
        if (Array.isArray(data)) {
            ldos = data;
        } else {
            ldos = Response.linkHashToArray(data);
        }
        const elements = ldos.map((ldo) => Response.linkHeaderElement(ldo));

        if (previous) {
            elements.unshift(previous);
        }
        const linkstring = elements.join(", ");
        return {
            "Link": linkstring
        };
    }

    /**
     * A Link Description Object (LDO)
     * @typedef {Object} LinkDescriptionObject
     * @property {string} rel      - a relationship type
     * @property {uri}    href     - the target URL of the relation
     * @property {string} [name]   - a unique identifier of the query
     * @property {string} [title] - a text to present to a user representing this query's intention
     */

    /**
     * Builds a link object that can be passed to res.links()
     *     
     * for example, 
     *      [
     *          {rel:"self", href:"/m/3", name:"me"}, 
     *          {rel:"next", href="/m/4"}
     *      ]
     * 
     * will become
     *      {
     *          self: {href:"/m/3", name:"me"},
     *          next: {href="/m/4"}
     *      }
     * 
     * @param {LinkDescriptionObject[]} data - link array 
     * @returns {object} an object which key is the link relation and value is the url
     */
    static linkArrayToObject(data = []) {
        let links = {};
        data.forEach(l => links[l.rel] = l.href);
        return links;
    }

    /**
     * builds an array of LDOs
     * @param {Object} data 
     * @returns {LinkDescriptionObject[]}
     */
    static linkHashToArray(data = {}) {
        let links = [];
        Object.keys(data).forEach(rel => links.push({
            rel,
            href: data[rel]
        }));
        return links;
    }

    /**
     * Generates the link header.
     * 
     * @param {*} data - either an array of link description objects or an object which key is the rel and value is the href of a link
     */
    static linkHeaderObject(data = {}) {
        if (Array.isArray(data)) {
            return Response.linkArrayToObject(data);
        } else {
            return data;
        }
    }

    /**
     * cleanup array entries recursively
     * @param {*} content 
     * @param {*} filter 
     */
    static _cleanupArray(content, filter) {
        if (content.length > 0 && content[0] instanceof Object) {
            let out = [];
            content.forEach(e => out.push(this.cleanupContent(e, filter)));
            return out;
        } else {
            return content;
        }
    }

    /**
     * creates a copy of the content but removing certain properties. properly handle objects and object arrays 
     * as well as "data" properties. 
     * 
     * @param {Object | Object[]} content - the content to cleanup
     * @param {String[]} [filter]         - the properties to remove
     * @returns {object}
     */
    static cleanupContent(content, filter = ["_id", "__v", "password"]) {
        // filter out implementation properties
        if (Array.isArray(content)) {
            return Response._cleanupArray(content, filter);
        } else {
            if (content.data) {
                content.data = Response.cleanupContent(content.data, filter);
            } else {
                content = _.omit(content.toObject ? content.toObject() : content, filter);
            }
            return content;
        }
    }

    /**
     * Ensure the response will contain the necessary headers. Add the "etag" and "Link" header if they are not present
     * 
     * @param {HttpResponse} res - the response object
     * @param {object} content
     * @param {Object[]} [links]          - link object to send as Link header
     * @param {object[]} [headers] 
     */
    static ensureHeaders(res, content, links = {}, headers = {}) {
        res.set(headers);

        // ensure Etag header, handle "hypermedia" data property
        if (!res.get("ETag")) {
            // since 0 is a valid value for "__v" do not shortcut the following if statement by using `if (!content.__v)`
            if (content.__v != null) {
                res.set("ETag", content.__v.toString());
            } else if (content.data && content.data.__v != null) {
                res.set("ETag", content.data.__v.toString());
            }
        }

        // ensure Link header based on hypermedia "links" or "_links" property preserving current links
        const linkPreviousAndContent = Response.linkHeader(content._links || content.links, res.get("Link"));

        // add explicit links
        const linkExplicit = Response.linkHeader(links, linkPreviousAndContent.Link);
        res.set(linkExplicit);
    }

    /**
     * sends a JSON response.
     * 
     * @param {*} res 
     * @param {*} code 
     * @param {*} content 
     * @param {Object} [links]          - link object to send as Link header
     * @param {*} [headers] 
     */
    static sendJSON(res, code, content, links = {}, headers = {}) {
        res.status(code);
        Response.ensureHeaders(res, content, links, headers);
        res.json(Response.cleanupContent(content));
    }

    /**
     * sends a text response.
     * 
     * @param {*} res 
     * @param {*} code 
     * @param {*} content 
     * @param {Object} [links]          - link object to send as Link header
     * @param {*} [headers] 
     */
    static sendTxt(res, code, content, links = {}, headers = {}) {
        res.status(code).set(headers).links(Response.linkHeaderObject(links));
        return res.send(content);
    }

    /**
     * generic handler for "405 Not Allowed" paths.
     * 
     * @param {*} req 
     * @param {*} res 
     */
    static handleAsNotAllowed(req, res) {
        const response = new Response(res);
        return response.notAllowed("Method is not allowed on this resource");
    }

    /**
     * prepares an http response and sends it.
     * 
     * @param {Number} code http code
     * @param {*} content response body
     * @param {Object} [links]          - link object to send as Link header
     * @param {Object} [headers] http header array
     * @param {string[]} [filter] 
     */
    send(code, content, links = {}, headers = {}, filter = ["_id", "__v", "password"]) {
        let exres = this.res();
        if (this.settings().API_VERSION) {
            exres.set('X-API-Version', this.settings().API_VERSION);
        }
        exres.status(code);

        Response.ensureHeaders(exres, content, links, headers);
        if (content instanceof Error) {
            exres.send(content.message);
        } else if (content instanceof Object) {
            return exres.json(Response.cleanupContent(content, filter));
        } else {
            return exres.send(content);
        }
    }

    /**
     * 200 Ok.
     * 
     * @param {Object | Object[]} [data]  - object or array to use as response body
     * @param {Object} [links]          - link object to send as Link header
     * @param {Object} [headers]        - http headers  
     */
    ok(data = {}, links = {}, headers = {}) {
        return this.send(200, data, links, headers);
    }

    /**
     * 201 Created,
     * 
     * @param {Object | Object[]} data  - object or array to use as response body
     * @param {String} location         - location header 
     * @param {Object} [links]          - link object to send as Link header
     * @param {Object} [headers]        - http headers  
     */
    created(data, location, links = {}, headers = {}) {
        headers.Location = location;
        this.res().set("Location", location);
        return this.send(201, data, links, headers);
    }

    /**
     * 202 Accepted.
     * 
     * @param {Object | Object[]} data  - object or array to use as response body
     * @param {String} location         - location header 
     * @param {Object} [links]          - link object to send as Link header
     * @param {Object} [headers]        - http headers  
     */
    accepted(data, location, links = {}, headers = {}) {
        headers.Location = location;
        this.res().set("Location", location);
        return this.send(202, data, links, headers);
    }

    /**
     * sends a text/html response back to the client.
     * 
     * @param {String} content 
     */
    html(content) {
        return this.send(200, content, {}, {
            'content-type': 'text/html'
        });
    }

    /**
     * Performs content negotiation.
     * 
     * @param {*} options 
     */
    format(options) {
        return this.res().format(options);
    }

    /**
     * Redirects the client agent to a new url.
     * 
     * @param {String} url 
     */
    redirect(url) {
        return this.res().redirect(url);
    }

    /**
     * 400 Bad Request.
     * 
     * @param {Object | String} [text]  -  
     * @param {Object} [links]          - link object to send as Link header
     * @param {*} [headers]             -
     */
    badRequest(text = "Bad Request", links = {}, headers = {}) {
        return this.send(400, text, links, headers);
    }

    /**
     * 401 Unauthorized. 
     * 
     * In reality this code should be called "Unauthenticated"; see 403 for authorizations
     * @param {*} text 
     * @param {Object} [links]    - link object to send as Link header
     * @param {*} [headers] 
     */
    unauthorized(text = "Unauthorized", links = {}, headers = {}) {
        return this.send(401, text, links, headers);
    }

    /**
     * 403 Forbidden.
     * 
     * @param {*} text 
     * @param {Object} [links]    - link object to send as Link header
     * @param {*} [headers] 
     */
    forbidden(text = "Forbidden", links = {}, headers = {}) {
        return this.send(403, text, links, headers);
    }

    /**
     * 404 Not Found.
     * 
     * @param {*} text 
     * @param {Object} [links]     - link object to send as Link header
     * @param {*} [headers] 
     */
    notFound(text = "Not Found", links = {}, headers = {}) {
        return this.send(404, text, links, headers);
    }

    /**
     * 405 Not Allowed.
     * 
     * @param {*} text            - content
     * @param {Object} [links]    - link object to send as Link header
     * @param {Object} [headers]  - http headers  
     */
    notAllowed(text = 'Method is not allowed on this resource.', links = {}, headers = {}) {
        return this.send(405, text, links, headers);
    }

    /**
     * 406 Not Acceptable.
     * 
     * @param {*} [text]          - content
     * @param {Object} [links]    - link object to send as Link header
     * @param {Object} [headers]  - http headers  
     */
    notAcceptable(text = "The requested format in the 'accept' header is unacceptable.", links = {}, headers = {}) {
        return this.send(406, text, links, headers);
    }

    /**
     * 409 Conflict.
     * 
     * @param {*} text            - content
     * @param {Object} [links]    - link object to send as Link header
     * @param {Object} [headers]  - http headers  
     */
    conflict(text = "Conflict", links = {}, headers = {}) {
        return this.send(409, text, links, headers);
    }

    /**
     * 412 Precondition Failed.
     * 
     * @param {*} currentState 
     * @param {Object} [links]    - link object to send as Link header
     * @param {Object} [headers]  - http headers  
     */
    preconditionFailed(currentState, links = {}, headers = {}) {
        return this.send(412, currentState, links, headers);
    }

    /**
     * 413 Payload Too Large.
     * 
     * @param {*} text 
     * @param {Object} [links]          - link object to send as Link header
     * @param {*} [headers] 
     */
    payloadTooLarge(text = "Payload Too Large", links = {}, headers = {}) {
        return this.send(413, text, links, headers);
    }

    /**
     * 415 Unsupported Media Type.
     * 
     * @param {*} text 
     * @param {Object} [links]          - link object to send as Link header
     * @param {*} [headers] 
     */
    unsupportedMediaType(text = "Unsupported Media Type", links = {}, headers = {}) {
        return this.send(415, text, links, headers);
    }

    /**
     * 500 Server Error.
     * 
     * @param {*} text 
     * @param {Object} [links]          - link object to send as Link header
     * @param {*} [headers] 
     */
    serverError(text = "Internal Server Error", links = {}, headers = {}) {
        return this.send(500, text, links, headers);
    }

    /**
     * Builds an error response body based on the type of Error that was thrown.
     * 
     * @param {*} err 
     * @param {function(currentState:object)} [contentBuilder] - a function that builds the content to return in the response
     * @param {function(currentState:object)} [headerBuilder] - a function that buils the headers to attach to the response
     */
    error(err, contentBuilder, headerBuilder) {
        const currentStateContent = () => contentBuilder ? contentBuilder(err.currentState) : err.currentState;
        const content = err.currentState ? currentStateContent() : err.message;
        const headers = headerBuilder ? headerBuilder(err.currentState) : undefined;

        if (err instanceof RangeError || err instanceof BadRequestError) {
            return this.badRequest(content, headers);
        } else if (err instanceof UnauthenticatedError) {
            return this.unauthorized(content, headers);
        } else if (err instanceof ForbiddenError) {
            return this.forbidden(content, headers);
        } else if (err instanceof NotFoundError) {
            const payload = {
                requestedId : err.requestedId,
                message: err.message
            };
            return this.notFound(payload, headers);
        } else if (err instanceof ConflictError) {
            return this.conflict(content, headers);
        } else if (err instanceof ConcurrencyError) {
            return this.preconditionFailed(content, headers);
        } else if (err instanceof UnsupportedMediaTypeError) {
            return this.unsupportedMediaType(content, headers);
        } else if (err instanceof PayloadTooLargeError) {
            return this.payloadTooLarge(content, headers);
        } else {
            //fallback
            return this.serverError(content, headers);
        }
    }
}
