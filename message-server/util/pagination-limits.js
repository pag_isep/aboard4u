/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// Utilities for the application layer
//

// maximum number of results in searches
export const getMaxResults = () => parseInt(process.env.ABOARD4U_MAX_RESULTS) || 200;

/**
 * 
 * @param {Number} start 
 * @param {Number} n 
 */
 export function limitPagingResults(start, n) {
    if (!start) {
        start = 1;
    }
    if (!n) {
        n = 5;
    }
    const currentMax = getMaxResults();
    if (n > currentMax) {
        n = currentMax;
    }

    return {
        start,
        n
    };
}

export function limitMaxResults(first) {
    return Math.min(first || Number.MAX_SAFE_INTEGER, getMaxResults());
}
