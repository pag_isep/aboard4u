/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */

/*
 * A Board 4 U
 * A service for messages authored by users with a REST and a GraphQL endpoint
 *
 * Node modules with separation of concerns, organized by features
 *   - users
 *      - application
 *      - rest
 *      - graphql
 *   - messages
 *      - application
 *      - rest
 *      - graphql
 *   - authz
 *      - application
 *      - rest
 *      - graphql
 *   - service
 *      - rest
 *      - graphql
 *   - persistence
 *      - inmemory
 *      - mongo
 *
 * .env configuration file
 *
 */

/*jslint node: true */
/*jshint esversion:9 */

"use strict";


// configuration
import {
    SETTINGS
} from './config/settings.js';
import debug from "debug";
const LOGGER = debug("pag:mserver");

// module dependency injection
import {
    buildServices
} from './config/dependency-factory.js';

// API
import {
    buildApplication
} from "./service/api-server.js";


/**
 * cluster master function
 * 
 */
function master() {
    LOGGER("Booting with Server Configuration:", SETTINGS);

    const banner = `
A Board 4 U 

RESTful Hypermedia and GraphQL message service
API version: ${SETTINGS.API_VERSION}

* REST
    * Billboard API: ${SETTINGS.server.rest.API_ROOT}/
    * OpenAPI 3.0 specification: ${SETTINGS.server.rest.API_ROOT}/openapi.json
    * Swagger UI: ${SETTINGS.server.rest.API_ROOT}/api-docs

* GraphQL
    * GraphQL endpoint: ${SETTINGS.server.graphql.API_ROOT}
    * GraphQL Playground: ${SETTINGS.server.graphql.API_ROOT}

Go to ${SETTINGS.server.rest.API_ROOT} to browse the API
`;

    console.log(banner);

    process.once('beforeExit', () => {
        LOGGER('Master cleanup.');
    });
}

/**
 * cluster worker function
 * @param {*} id 
 * @param {*} disconnect 
 */
async function worker(id, disconnect) {
    LOGGER(`Started worker ${id}`);
    process.once('SIGTERM', shutdown);
    process.once('SIGINT', shutdown);

    function shutdown() {
        LOGGER(`Worker ${id} cleanup.`);
        disconnect();
    }

    // module dependency injection
    const SERVICE_REGISTRY = await buildServices(SETTINGS);

    //
    // API
    //
    const app = await buildApplication(SETTINGS, SERVICE_REGISTRY);


    /////////////////////////////
    // STARTING ...

    app.listen(SETTINGS.server.PORT, function () {
        const banner = `Listening on worker ${id}`;
        console.log(banner);
    });
}

//
// clustering
//
import throng from 'throng';

const WORKERS = process.env.WEB_CONCURRENCY || 1;

// since "clinic doctor" does not support clustering we execute without clustering if the desired number of workers is 1 
if (WORKERS !== 1) {
    throng({
        master, // Fn to call in master process (can be async)
        worker, // Fn to call in cluster workers (can be async)
        count: WORKERS, // Number of workers (cpu count)
        lifetime: Infinity, // Min time to keep cluster alive (ms)
        grace: 5000, // Grace period between signal and hard shutdown (ms)
        signals: ['SIGTERM', 'SIGINT'] // Signals that trigger a shutdown (proxied to workers)
    });
} else {
    master();
    worker(0, () => LOGGER("Disconnecting"));
}