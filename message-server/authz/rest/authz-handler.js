/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// API's authentication features
//

import jwt from "jsonwebtoken";

import {
    UnauthenticatedError,
} from '../../exceptions.js';

// these will be passed to the module thru the configure() function
let authzController;
let Response;
let SETTINGS;

/**
 * @swagger
 * /authz/login:
 *   post:
 *     summary: authenticates a user
 *     description: returns the authentication ID token (JWT) to use in further API calls.
 *     tags:
 *       - Authz
 *     security: []
 *     requestBody:
 *       description: login data
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/loginForm'
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/loginForm'
 *     responses:
 *       200:
 *         description: a JSON web token (JWT) required to login.
 *         content:
 *           application/jwt:
 *             schema:
 *               type: string
 *               maxLength: 1024
 *               pattern: '^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$'
 *               example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwczovL3NwYWNlYXBpLmNvbS9ncmFwaHFsIjp7InJvbGVzIjpbImFzdHJvbmF1dCJdLCJwZXJtaXNzaW9ucyI6WyJyZWFkOm93bl91c2VyIl19LCJpYXQiOjE1OTQyNTI2NjMsImV4cCI6MTU5NDMzOTA2Mywic3ViIjoiNjc4OTAifQ.Z1JPE53ca1JaxwDTlnofa3hwpS0PGdRLUMIrC7M3FCI
 *       401:
 *         description: wrong username and/or password
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleLogin(req, res) {

    return authzController
        .login(req.body.username, req.body.password)
        .then(authz => {
            if (!authz) {
                throw new UnauthenticatedError('Invalid username/password.');
            }
            return Promise.resolve(authz);
        })
        .then(authz => {
            const payload = {
                links: buildMeLink(authz.username),
                scope: authz.roles
            };
            //TODO switch to RSA algorithm
            const token = jwt.sign(payload, process.env.SIGN_SECRET, {
                algorithm: "HS256",
                subject: authz.username,
                audience: SETTINGS.server.rest.API_ROOT,
                issuer: SETTINGS.server.rest.API_ROOT,
                expiresIn: "15m"
            });
            //return the proper link header to the user
            return res.links(buildMeLink(authz.username))
                .type('application/jwt')
                .send(token);
        })
        .catch((err) => {
            const response = new Response(res, SETTINGS);
            return response.error(err);
        });
}

/**
 * @swagger
 * /authz/me/password:
 *   put:
 *     summary: changes a user's password
 *     description: changes a user 's password
 *     tags:
 *       - Authz
 *     requestBody:
 *       description: message
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             additionalProperties: false
 *             required:
 *               - password
 *             properties:
 *               password:
 *                 $ref: '#/components/schemas/attrAuthzPassword'
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkAuthzJson'
 *       400:
 *         description: malformed input
 *       401:
 *         $ref: '#/components/responses/Unauthorized'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePutChangePassword(req, res) {
    //TODO should we check for if-match header or simply allow to change the password?

    const response = new Response(res, SETTINGS);
    return authzController
        .changePasswordOfAuthenticated(req.user, req.body.password)
        .then((out) => sendAuthz(response, req.user))
        .catch((err) => response.error(err));
}

function buildMeLink(username) {
    //TODO the logic to build the "me" link should not be duplicated in this module and the user module
    return {
        "https://aboard4u.herokuapp.com/rel/me": SETTINGS.server.rest.API_ROOT + "/user/" + username,
        "https://aboard4u.herokuapp.com/rel/password": SETTINGS.server.rest.API_ROOT + "/authz/me/password"
    };
}

function sendAuthz(response, user) {
    response.ok({
        username: user.username,
        lastLogin: user.lastLogin,
        createdOn: user.createdOn
    },
        buildMeLink(user.username)
    );
}


/**
 * @swagger
 * /authz/password-reset:
 *   post:
 *     summary: resets a user's password
 *     description: Initiates a request for reseting a user's password. If the user sucessfully logins in the meantime, the request is ignored.
 *     security: []
 *     tags:
 *       - Authz
 *     requestBody:
 *       description: message
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             additionalProperties: false
 *             properties:
 *               username:
 *                 $ref: '#/components/schemas/attrUserName'
 *               email:
 *                 $ref: '#/components/schemas/email'
 *     responses:
 *       200:
 *         description: Ok. An email has been sent with a reset link.
 *         content:
 *           text/plain:
 *             schema:
 *               $ref: '#/components/schemas/serverResponseText'
 *       400:
 *         description: malformed input
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePostResetPassword(req, res) {
    const response = new Response(res, SETTINGS);
    return authzController
        .resetPassword(req.body.username, req.body.email)
        .then((user) => response.ok("An email has been sent for confirmation"))
        .catch((err) => response.error(err));
}

/**
 * @swagger
 * /authz/password-reset-confirmation:
 *   post:
 *     summary: confirms the "password reset" request
 *     description: confirms the "password reset" request and sets a new user's password
 *     security: []
 *     tags:
 *       - Authz
 *     requestBody:
 *       description: password reset confirmation token
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             additionalProperties: false
 *             required:
 *               - username
 *               - token
 *               - newPassword
 *             properties:
 *               username:
 *                 $ref: '#/components/schemas/attrUserName'
 *               token:
 *                 type: string
 *                 maxLength: 128
 *                 pattern: '^[\w\-\.]*$'
 *               newPassword:
 *                 $ref: '#/components/schemas/attrAuthzPassword'
 *     responses:
 *       200:
 *         $ref: '#/components/responses/OkAuthzJson'
 *       400:
 *          description: wrong username and/or token
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handlePostConfirmResetPassword(req, res) {
    const response = new Response(res, SETTINGS);
    return authzController
        .confirmResetPassword(req.body.username, req.body.token, req.body.newPassword)
        .then((out) => sendAuthz(response, req.user))
        .catch((err) => response.error(err));
}

/////////////////////////////
// MODULE EXPORTS

export default function AuthzResource(config, dependencies) {
    SETTINGS = config;

    authzController = new dependencies.AuthzController(config, dependencies);

    Response = dependencies.Response;

    return {
        handlePutChangePassword,
        handlePostResetPassword,
        handlePostConfirmResetPassword,

        handleLogin
    };
}