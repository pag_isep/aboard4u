/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// a documentation file to hold the common definitions of models used in the API
//

//
// schemas
//


/**
 * @swagger
 * components:
 *   schemas:
 *     attrAuthzPassword:
 *       type: string
 *       example: pr@ga!98
 *       maxLength: 30
 *       pattern: '^[\w\-!#\$?\+\*\.,;&\/:@]{8,}$'
 *     loginForm:
 *       description: the user form input
 *       type: object
 *       additionalProperties: false
 *       required:
 *         - username
 *         - password
 *       properties:
 *         username:
 *           $ref: '#/components/schemas/attrUserName'
 *         password:
 *           $ref: '#/components/schemas/attrAuthzPassword'
 *     authzData:
 *       description: a user profile
 *       type: object
 *       additionalProperties: false
 *       required:
 *         - username
 *         - createdOn
 *       properties:
 *         username:
 *           $ref: '#/components/schemas/attrUserName'
 *         createdOn:
 *           type: string
 *           format: date-time
 *         lastLogin:
 *           type: string
 *           format: date-time
 */

/**
* @swagger
* components:
*   responses:
*     OkAuthzJson:
*       description: Ok
*       content:
*         application/json:
*           schema:
*             $ref: '#/components/schemas/authzData'
*/