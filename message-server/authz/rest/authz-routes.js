/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */



/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// setting up the REST routes for the Authz resource
//

import multer from 'multer';
import AuthzResource from '../../authz/rest/authz-handler.js';

export function initRoutes(config, dependencies) {
    const app = dependencies.app;
    const authenticate = dependencies.authenticate;
    const ensureSessionLinks = dependencies.ensureSessionLinks;
    const ensureContentType = dependencies.ensureContentType;

    //
    // load resource modules
    //
    const authzResource = new AuthzResource(config, dependencies);

    const API_PATH = config.server.rest.API_PATH;

    //
    // HTML form posting thru encoding multipart/form-data
    //
    const formPost = multer({
        limits: {
            fileSize: 10000
        }
    }).none();

    //
    // AUTHZ login
    //
    app.route(API_PATH + "/authz/login")
        .post(formPost, authzResource.handleLogin);

    //
    // AUTHZ "me" resource
    //
    app.route(API_PATH + "/authz/me/password")
        .put(authenticate, ensureSessionLinks, ensureContentType(['application/json']), authzResource.handlePutChangePassword);

    //
    // AUTHZ "password" resource
    //
    app.route(API_PATH + "/authz/password-reset")
        .post(ensureSessionLinks, ensureContentType(['application/json']), authzResource.handlePostResetPassword);
    app.route(API_PATH + "/authz/password-reset-confirmation")
        .post(ensureSessionLinks, ensureContentType(['application/json']), authzResource.handlePostConfirmResetPassword);
}
