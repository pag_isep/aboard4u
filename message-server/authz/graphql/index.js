/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 11*/

"use strict";

//
// GraphQL Authz module
//
import {resolvers} from './resolvers.js';
import fs from "fs";
const schema = fs.readFileSync(new URL('schema.graphql', import.meta.url)).toString();

export default {
    schema,
    resolvers
};