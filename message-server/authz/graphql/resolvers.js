/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// GraphQL Authz
//


import { GraphQLError } from 'graphql';

import {handleError} from '../../util/apollo-error-helper.js';

//
// The resolvers
//

/**
 * The GraphQL resolvers
 * 
 * @param {*} config 
 * @param {*} dependencies 
 */
export function resolvers(config, dependencies) {
    const authzController = new dependencies.AuthzController(config, dependencies);

    // return the resolvers object
    return {
        //
        // Queries
        //
        Query: {

        },

        //
        // Mutations
        //
        Mutation: {
            /**
             * issue #16 : As user I want to change my password
             * 
             *(newPassword: string!)
             */
            changeMyPassword: function (parent, args, context, info) {
                if (!context.user) {
                    throw new GraphQLError('You are not authorized to perform this action.', {
                        extensions: {
                          code: 'UNAUTHENTICATED',
                        },
                      });
                }

                return authzController
                    .changePasswordOfAuthenticated(context.user, args.newPassword)
                    .then((user) => {
                        return {
                            username: user.username,
                            lastLogin: user.lastLogin,
                            createdOn: user.createdOn
                        };
                    })
                    .catch(handleError);
            }
        }
    };
}
