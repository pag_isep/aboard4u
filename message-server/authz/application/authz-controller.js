/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// authorizations application layer
//


import debug from 'debug';
const LOGGER = debug('pag:mserver:app');
const LOGGER_AUDIT = debug('pag:audit:login');

// Password hashing
import bcrypt from 'bcrypt';

import  {
    ForbiddenError
} from  '../../exceptions.js';

import  {
    createToken
} from '../../util/id-token-generator.js';

let SETTINGS;

/**
 * the Authorization repository
 */
let authzRepo;


/**
 * Validates the login of a user.
 * 
 * @param {*} username 
 * @param {*} password 
 * @returns {Promise} a promise that resolves to the authz object if the login succeeds or null otherwise
 */
async function login(username, password) {
    if (!username || !password) {
        LOGGER_AUDIT("Malformed attempted login", username);
        //TODO throw BadRequestError to allow adding a json response
        return Promise.reject(new RangeError("Missing 'username' and/or 'password' parameter"));
    }

    return authzRepo.getAuthz(username)
        .then(async function (user) {
            if (!user) {
                LOGGER_AUDIT("Invalid username attempted login", username);
                return Promise.resolve(null);
            }
            //#116 hash password
            const isSamePass = await bcrypt.compare(password, user.password);
            if (isSamePass) {
                LOGGER_AUDIT("Sucessful login", username);

                sucessfulLogin(user);

                return Promise.resolve(user);
            } else {
                LOGGER_AUDIT("Wrong password attempted login", username);

                unsucessfulLogin(user);

                return Promise.resolve(null);
            }
        })
        .catch(function (err) {
            LOGGER("Error", err, "on login");
            throw err;
        });
}

/**
 * Register login & clear reset token if any.
 * 
 * @param {*} user 
 */
function sucessfulLogin(user) {
    user.resetToken = null;
    user.lastLogin = new Date();
    user.failedLogins = 0;

    return authzRepo.saveAuthz(user)
        .catch(function (err) {
            LOGGER("Error while updating login registry with sucessful login", err);
        });
}

/**
 * Increase login attempt number to check  if it needs to lock the account.
 * 
 * @param {*} user 
 */
function unsucessfulLogin(user) {
    user.failedLogins = user.failedLogins + 1;

    return authzRepo.saveAuthz(user)
        .catch(function (err) {
            LOGGER("Error while updating login registry with unsucessful login", err);
        });
}

/**
 * Helper method to obtain the user authorization from the datastore in order 
 * to perform authentication by HTTP digest.
 * 
 * @param {String} username 
 */
function loginByDigest(username) {
    if (!username) {
        LOGGER_AUDIT("Malformed attempted login", username);
        //TODO throw BadRequestError to allow adding a json response
        return Promise.reject(new RangeError("Missing 'username' parameter"));
    }

    return authzRepo.getAuthz(username);
}

// #116 hash passwords
async function hashPassword(password) {
    const hashed = await bcrypt.hash(password, 10);
    return hashed;
}

//
// authz resource
//

/**
 * Changes the password of a user, assuming the user has already been authenticated and her current password validated.
 * 
 * @param {*} authzUsername 
 * @param {*} username 
 * @param {*} newPassword 
 */
function changePassword(authzUsername, username, newPassword) {
    if (![authzUsername, username, newPassword].every(e => e)) {
        //TODO throw BadRequestError to allow adding a json response
        return Promise.reject(new RangeError("Missing mandatory parameters"));
    }

    if (authzUsername !== username) {
        //TODO admin users could have permission to change other user's password
        return Promise.reject(new ForbiddenError("You can only change your own password"));
    }

    return authzRepo
        .getAuthz(username)
        .then(async function (entry) {
            if (!entry) {
                // to avoid security breaches we return 403 instead of 404
                return Promise.reject(new ForbiddenError("You can only change your own password"));
            }
            // #116 hash passwords
            entry.password = await hashPassword(newPassword);
            return entry;
        })
        .then(authzRepo.saveAuthz);
}

/**
 * Changes the password of a user, assuming the user has already been authenticated and her current password validated
 * 
 * @param {*} who - the User object of the authenticated user
 * @param {*} password 
 */
async function changePasswordOfAuthenticated(who, password) {
    if (![who, password].every(e => e)) {
        //TODO throw BadRequestError to allow adding a json response
        return Promise.reject(new RangeError("Missing mandatory parameters"));
    }

    // #116 hash passwords
    who.password = await hashPassword(password);

    return authzRepo.saveAuthz(who);
}

/**
 * 
 * @param {*} username 
 */
function resetPassword(username) {
    //TODO also support email property
    if (![username].every(e => e)) {
        //TODO throw BadRequestError to allow adding a json response
        return Promise.reject(new RangeError("Missing mandatory parameters"));
    }

    return authzRepo
        .getAuthz(username)
        .then(function (entry) {
            if (!entry) {
                // to avoid security breaches we won't return a 404 but pretend everything is ok
                return Promise.resolve({});
            }
            entry.resetToken = createToken();
            return entry;
        })
        .then(authzRepo.saveAuthz);
}

/**
 * 
 * @param {*} username 
 * @param {*} token 
 * @param {*} newPassword 
 */
function confirmResetPassword(username, token, newPassword) {
    if (![username, token, newPassword].every(e => e)) {
        //TODO throw BadRequestError to allow adding a json response
        return Promise.reject(new RangeError("Missing mandatory parameters"));
    }

    return authzRepo.getAuthz(username)
        .then(async function (entry) {
            if (!entry || entry.resetToken !== token) {
                //TODO throw BadRequestError to allow adding a json response
                return Promise.reject(new RangeError('Invalid token'));
            }
            // #116 hash passwords
            entry.password = await hashPassword(newPassword);
            return entry;
        })
        .then(authzRepo.saveAuthz);
}

/**
 * 
 * @param {*} user 
 */
async function addAuthz(username, password) {

    const hashed = await hashPassword(password);
    return authzRepo.addAuthz(username, hashed);
}

/**
 * 
 * @param {*} username 
 */
function deleteAuthz(username) {
    //TODO should we check against rev?
    return authzRepo.deleteAuthz(username);
}

/**
 * Builds the links related to authentication
 * 
 * @param {*} authUser
 * @returns {LinkDescriptionObject[]} 
 */
function buildSessionLinks(authUser) {
    const links = [];

    if (authUser) {
        // links for authenticated user:
        // - get user profile (me)
        // - change password
        links.push({
            rel: "https://aboard4u.herokuapp.com/rel/me",
            href: SETTINGS.server.rest.API_ROOT + "/user/" + authUser.username
        });

        links.push({
            rel: "https://aboard4u.herokuapp.com/rel/password",
            href: SETTINGS.server.rest.API_ROOT + "/authz/me/password"
        });
    } else {
        // links for unauthenticated user:
        // - login
        // - reset password
        links.push({
            rel: "https://aboard4u.herokuapp.com/rel/login",
            href: SETTINGS.server.rest.API_ROOT + "/authz/login"
        });
        links.push({
            rel: "https://aboard4u.herokuapp.com/rel/password-reset",
            href: SETTINGS.server.rest.API_ROOT + "/authz/password-reset"
        });
    }
    return links;
}

/////////////////////////////
// MODULE EXPORTS

export function AuthzController(config, dependencies) {
    SETTINGS = config;

    authzRepo = new dependencies.AuthzRepository();

    
        /*
        in the future we might need some permission based functions, e.g.:
        
        canDeleteMessage
        canCreateMessage
        canUpdateMessage
        */

    return Object.freeze({
        login,
        loginByDigest,
        sucessfulLogin,
        unsucessfulLogin,

        changePassword,
        changePasswordOfAuthenticated,
        resetPassword,
        confirmResetPassword,

        addAuthz,
        deleteAuthz,

        buildSessionLinks

    });
}