/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// GraphQL server
//

import {
    mergeTypeDefs,
    mergeResolvers
} from '@graphql-tools/merge';

import {
    ApolloServer,
} from '@apollo/server';

import {
    expressMiddleware
} from '@apollo/server/express4';
import cors from 'cors';

import {
    ApolloServerPluginLandingPageProductionDefault,
    ApolloServerPluginLandingPageLocalDefault
} from '@apollo/server/plugin/landingPage/default';

import fs from 'fs';
import {
    resolvers as standardResolvers
} from './resolvers-standard.js';


/**
 * Initialize the GraphQL endpoint
 *  
 * @param {*} configuration
 * @param {*} dependencies
 * @param {*} app 
 * @param {*} dualModeAuth 
 * @param {*[]} modules 
 */
export async function initGraphQL(configuration, dependencies, modules) {
    const app = dependencies.app;
    const dualModeAuth = dependencies.dualModeAuth;

    const schemas = modules.map((m) => m.schema);
    const schema = fs.readFileSync(new URL('schema-standard.graphql',
        import.meta.url)).toString();
    schemas.push(schema.toString());

    const resolvers = modules.map((m) => m.resolvers(configuration, dependencies));
    resolvers.push(standardResolvers(configuration, dependencies));

    // The GraphQL endpoint
    const server = new ApolloServer({
        typeDefs: mergeTypeDefs(schemas),
        resolvers: mergeResolvers(resolvers),
        plugins: [
            process.env.NODE_ENV === "production" ?
            ApolloServerPluginLandingPageProductionDefault({
                embed: true,
            }) :
            ApolloServerPluginLandingPageLocalDefault({
                embed: true
            })
        ]
    });

    await server.start();

    //setup GraphQL thru express
    app.use(
        '/graphql',
        dualModeAuth,
        cors(), expressMiddleware(server, {
            context: async function ({
                req
            }) {
                return {
                    user: req.user
                };
            }
        }, )
    );
}