/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// GraphQL
//

import {
    GraphQLScalarType
} from "graphql";

//
// The resolvers
//

/**
 * The GraphQL resolvers
 * 
 * @param {*} config 
 * @param {*} dependencies 
 */
export function resolvers(config, dependencies) {
    // return the resolvers object
    return {
        //
        // Query resolvers for type
        //
        DateTime: new GraphQLScalarType({
            name: "DateTime",
            description: "A valid date time value.",
            parseValue: (value) => new Date(value),
            serialize: (value) => new Date(value).toISOString(),
            parseLiteral: (ast) => ast.value
        })
    };
}