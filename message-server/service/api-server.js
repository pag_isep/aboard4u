/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:11 */

"use strict";

//
// setting up the server
//
import debug from "debug";
const LOGGER = debug("pag:mserver");
const LOGGER_AUDIT = debug('pag:audit:login');

import {
    //PayloadTooLargeError,
    UnsupportedMediaTypeError
} from "../exceptions.js";

// Express & plugins
import passport from 'passport';
import passportHtpp from 'passport-http';
import passportJWT from "passport-jwt";
import express from 'express';
import serveIndex from 'serve-index';
import express_status_monitor from 'express-status-monitor';
import helmet from "helmet";
import morgan from "morgan";
import compression from 'compression';
import rateLimit from 'express-rate-limit';
import {
    Validator,
    ValidationError
} from 'express-json-validator-middleware';
import {
    ensureContentType
} from '../util/ensure-content-type.js';
import addFormats from "ajv-formats"


// RESTful routes
import {
    initRoutes as messageRoutes
} from '../messages/rest/message-routes.js';
import {
    initRoutes as userRoutes
} from '../users/rest/user-routes.js';
import {
    initRoutes as authzRoutes
} from '../authz/rest/authz-routes.js';
import {
    initRoutes as tagsRoutes
} from '../tags/rest/tag-routes.js';
import {
    initRoutes as restDocsRoutes
} from './rest/index.js';

// GraphQL
import authzGraphQLModule from '../authz/graphql/index.js';
import userGraphQLModule from '../users/graphql/index.js';
import messageGraphQLModule from '../messages/graphql/index.js';
import tagsGraphQLModule from '../tags/graphql/index.js';
import {
    initGraphQL as initGraphQLServer
} from './graphql/graphql-init.js';


/**
 * Authentication via HTTP Digest or JWT.
 * 
 * @param {*} username 
 * @param {*} done 
 */
function fetchUserOnLogin(authz, username, done) {
    authz.loginByDigest(username)
        .then(function (user) {
            if (!user) {
                LOGGER_AUDIT("Invalid attempted login by ", username);
                return done(null, false);
            } else {
                LOGGER_AUDIT("Login attempt by ", username);
                return done(null, user, user.password);
            }
        })
        .catch(function (err) {
            LOGGER("Error", err, "while authenticating");
            return done(err);
        });
}

//
// authentication
//
function initAuthentication(app, authz, config) {

    // HTTP authentication
    const BasicStrategy = passportHtpp.BasicStrategy;
    const DigestStrategy = passportHtpp.DigestStrategy;
    // JWT
    const JWTStrategy = passportJWT.Strategy;
    const ExtractJWT = passportJWT.ExtractJwt;

    app.use(passport.initialize());

    let AUTH_STRATEGY = [];

    // JWT
    passport.use(new JWTStrategy({
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.SIGN_SECRET,
            issuer: config.server.rest.API_ROOT,
            audience: config.server.rest.API_ROOT,
            algorithms: ["HS256"]
        },
        (jwtPayload, done) => fetchUserOnLogin(authz, jwtPayload.sub, done)
    ));
    AUTH_STRATEGY.push('jwt');

    // HTTP Basic & Digest
    if (config.service.ALLOW_BASIC_DIGEST_AUTH) {
        // HTTP Basic
        passport.use(new BasicStrategy(
            function (username, password, done) {
                authz.login(username, password)
                    .then((user) => done(null, !user ? false : user))
                    .catch(function (err) {
                        LOGGER("Error", err, "while authenticating");
                        return done(err);
                    });
            }
        ));
        AUTH_STRATEGY.push('basic');

        // HTTP Digest
        passport.use(new DigestStrategy({
                qop: 'auth'
            },
            (username, done) => fetchUserOnLogin(authz, username, done),
            function (params, done) {
                // validate nonces as necessary
                done(null, true);
            }
        ));
        AUTH_STRATEGY.push('digest');
    }

    /**
     * authentication middleware to support both authenticated and unauthenticated endpoints
     * 
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    function dualModeAuth(req, res, next) {
        if (req.header('authorization')) {
            return passport.authenticate(AUTH_STRATEGY, {
                session: false
            })(req, res, next);
        } else {
            return next();
        }
    }

    const authenticate = passport.authenticate(AUTH_STRATEGY, {
        session: false
    });

    return Object.freeze({
        AUTH_STRATEGY,
        passport,
        dualModeAuth,
        authenticate
    });
}

// #57 optimize for security - use HTTPS
/**
 * Forces a redirect to HTTPS on deployed hosts, e.g., heroku.
 * 
 * extracted from: https://stackoverflow.com/questions/8605720/how-to-force-ssl-https-in-express-js
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function requireHTTPS(req, res, next) {
    // The 'x-forwarded-proto' check is for Heroku
    if (!req.secure && req.get('x-forwarded-proto') !== 'https' && process.env.NODE_ENV !== "development") {
        return res.redirect('https://' + req.get('host') + req.url);
    }
    next();
}


//
// Express app
//
function initWebapp(config) {
    const app = express();

    // monitoring the service status: GET /status
    app.use(express_status_monitor());

    // #57 optimize for security - use HTTPS
    if (process.env.NODE_ENV !== "development") {
        // javascript:S5689 Recovering fingerprints from web application technologies should not be possible
        //app.disable("x-powered-by");
        app.use(helmet({
            contentSecurityPolicy: {
                directives: {
                    ...helmet.contentSecurityPolicy.getDefaultDirectives(),
                    "script-src": ["'self'", "cdnjs.cloudflare.com", "cdn.jsdelivr.net", "code.jquery.com"],
                }
            }
        }));

        // #57 optimize for security - use HTTPS
        app.use(requireHTTPS);
    }

    // TODO should only be applied to paths that need to support html forms
    // form upload - application/x-www-urlencoded
    app.use(express.urlencoded({
        extended: true,
        limit: "2mb"
    }));

    // json content
    app.use(express.json({
        //limit: '100kb', 
        type: ["application/json", "application/vnd.pagsousa.hyper+json", "application/merge-patch+json"]
    }));

    // static assets
    const setHeaders = (res, path, stat) => {
        res.type("text/html");
    };
    const ASSET_FOLDER = process.cwd() + '/message-server/assets';
    app.use("/rel", express.static(ASSET_FOLDER + "/rel", {
        setHeaders
    }), serveIndex(ASSET_FOLDER + "/rel", {
        'icons': true
    }));
    app.use("/schemas", express.static(ASSET_FOLDER + "/schemas"), serveIndex(ASSET_FOLDER + "/schemas", {
        'icons': true
    }));
    app.use("/apitest", express.static(ASSET_FOLDER + "/apitest"), serveIndex(ASSET_FOLDER + "/apitest", {
        'icons': true
    }));

    // static HTML and client-side javascript content
    app.use("/", express.static(process.cwd() + '/message-server/webapp'));

    // logging : DEBUG
    app.use(morgan("dev"));

    // #54 enable compression
    app.use(compression());

    // #54 set up rate limiter
    // maximum of 100 requests per minute
    const limiter = rateLimit({
        windowMs: 1 * 60 * 1000, // 1 minute
        max: config.service.RATE_LIMIT,
        standardHeaders: true,
        legacyHeaders: false
    });
    app.use(limiter);

    return app;
}

/////////////////////////////
// MODULE EXPORTS

export async function buildApplication(config, depend) {
    const Response = depend.Response;

    let dependencies = {
        ...depend
    };

    let authz = new dependencies.AuthzController(config, dependencies);

    //
    // express app
    //
    const app = initWebapp(config);
    dependencies.app = app;

    // authentication
    const {
        dualModeAuth,
        authenticate
    } = initAuthentication(app, authz, config);
    dependencies.dualModeAuth = dualModeAuth;
    dependencies.authenticate = authenticate;

    dependencies.ensureSessionLinks = function (req, res, next) {
        const links = authz.buildSessionLinks(req.user);
        res.links(Response.linkArrayToObject(links));
        next();
    };

    // B4U-37 JSON-Schema validator
    const validator = new Validator({
        allErrors: true
    });
    addFormats(validator.ajv)
    dependencies.ensureJsonSchema = validator.validate;
    dependencies.ValidationError = ValidationError;
    
    // #124 ensure right Content-Type on each route
    dependencies.ensureContentType = ensureContentType;

    //
    // REST
    // 

    dependencies.handleAsNotAllowed = dependencies.Response.handleAsNotAllowed;

    // setup RESTful routes
    await messageRoutes(config, dependencies);
    await userRoutes(config, dependencies);
    authzRoutes(config, dependencies);
    tagsRoutes(config, dependencies);
    restDocsRoutes(config, dependencies);

    //
    // GraphQL
    //
    initGraphQLServer(config, dependencies, [authzGraphQLModule, userGraphQLModule, messageGraphQLModule, tagsGraphQLModule]);

    //
    // error handling middleware
    //

    // Error handler for validation errors
    app.use(function (err, req, res, next) {
        if (err instanceof ValidationError) {
            res.status(400).json(err.validationErrors);
            next();
        } else if ((err.name === "MulterError" && err.code === "LIMIT_FILE_SIZE") || err.name === "PayloadTooLargeError") {
            res.status(413).json({
                field: err.field,
                message: err.message,
                limit: err.limit,
                actual: err.expected
            });
            next();
        } else if (err instanceof UnsupportedMediaTypeError) {
            res.status(415).send({
                message: err.message,
                expected: err.expected,
                actual: err.actual
            });
            next();
        } else {
            next(err);
        }
    });

    //
    // return built app
    //
    return app;
}