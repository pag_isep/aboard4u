*A RESTful Hypermedia (level 3 of Richardson's Maturity Model) API for a message board.*

---

A message service providing three main abstractions: *messages* and their *authors*, and *tags*. 

![Domain Model](/domain-model.svg)

Following hypermedia principles, the API is explorable thru its "billboard" endpoint `/` which will present an html introductory page as well as a JSON table of contents with the entry points of the API. 

Besides serving "plain JSON" representations using `application/json`, the API follows hypermedia principles and sends the HTTP `Link` header as well as supports its proprietary Hypermedia-enabled media type `application/vnd.pagsousa.hyper+json` inspired on 
[Collection+JSON](http://amundsen.com/media-types/collection/) by Mike Amudsen.

## Domain Application Protocol (DAP)

The following diagram depicts the Domain Application Protocol (DAP) of the service.

![DAP](/DAP.svg)

The HTTP `Link` header is sent for each resource item (with `rel` set to, e.g.,  `self`, `author`, `https://Schema.org/image`, 
`edit-form`, `alternate`) or collection (with, e.g., `self`, `start`, `prev`, `next`, `create-form`) or subresource like 
`user/:username/photo` and `user/:username/pdf` (with, e.g., `related`). The presence of the link in the response representation depends
on the current state of the resource, for instance, `edit` links are only sent for authenticated and authorized requests

## Custom Link Relations

The following list presents the Custom Link Relations used in the media type `application/vnd.pagsousa.hyper+json`

- `https://aboard4u.herokuapp.com/rel/login` - The target IRI of this relation can be used to obtain an access token to the API. 

- `https://aboard4u.herokuapp.com/rel/me` - The target IRI of this relation can be used to obtain the authenticated  
            user's profile by issuing an authenticated `GET`. The response
            body MUST contain a representation of the authenticated user. 
            Allows to obtain the authenticated user's info. Usefull for client applications 
            to get the set of allowed actions (links) of the user.

- `https://aboard4u.herokuapp.com/rel/password` - The target IRI of this relation can be used to change the password of the
                authenticated user by issuing a `PUT` with a request body containing a
                `password` property with the value to set as the new password of the user.

- `https://aboard4u.herokuapp.com/rel/password-reset` - The target IRI of this relation can be used to reset a user's password in
                case the user has forgoten it, by issuing a `POST` with a request body
                containing a `id` property with the user's username or an
                `email` property. Handling the password reset and consequent confirmation is an out of band
                process, e.g., an email with a confirmation form URL.
                Allows for a user that has forgotten her password to issue an unathenticated request to 
                reset her password. An out-of-band mechanism (e.g., email) is used to comunicate with the 
                user and ensure the request is authentic. It is still possible to use the authenticated 
                endpoints if the user remembers the password and the password reset request will be ignored.

- `https://aboard4u.herokuapp.com/rel/users` - The target IRI of this relation represents a collection of users. The
                target resource MUST allow `GET` and `PUT` to a
                subresource to add a new member to the user collection, i.e., signup.
                `POST` MAY be allowed to register users from an HTML form.

- `https://aboard4u.herokuapp.com/rel/messages` -
            The target IRI of this relation represents a collection of messages. The
                target resource MUST allow `GET` and `POST`. 

- `https://aboard4u.herokuapp.com/rel/trivia` - The target IRI of this relation MUST allow to POST an empty request body in
                order to augment the context IRI resource with trivia factoids. Alternatively, the
                request body MAY contain a `callback` and a `callbackRef`
                property indicating a postback url and correlation id. The response is a `202
                    Accepted`. The caller may issue a GET to the url returned in the
                `Location` header in order to pool the updated message or wait
                for a callback.

## Standard Link Relations

Besides the custom link relations, the following Standard Link Relations are also used from
[IANA Link Relations] and [Schema.org]

`self`, `author`, `https://schema.org/image`, `related`, `alternate`, `describedBy`

`start`, `prev`, `next`, `collection`, `item`  

`search`, `edit`, `edit-form`, `create-form`

`help`, `license`, `https://schema.org/softwareVersion`

For further information on these standard rel types visit [IANA Link Relations] and [Schema.org]

[Collection+JSON]: http://amundsen.com/media-types/collection/
[Schema.org]: https://schema.org/docs/full.html
[IANA Link Relations]: https://www.iana.org/assignments/link-relations/link-relations.xml

---