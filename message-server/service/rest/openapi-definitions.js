/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// a documentation file to hold the common definitions of models used in the API
//

/**
 * @swagger
 * components:
 *   responses:
 *     Ok:
 *       description: Ok.
 *       content:
 *         text/plain:
 *           schema:
 *             $ref: '#/components/schemas/serverResponseText'
 *     NotFound:
 *       description: Not Found.
 *       content:
 *         text/plain:
 *           schema:
 *             $ref: '#/components/schemas/serverResponseText'
 *     TooManyRequests:
 *       description: Too Many Requests.
 *       content:
 *         text/plain:
 *           schema:
 *             $ref: '#/components/schemas/serverResponseText'
 *     Unauthorized:
 *       description: Unauthorized. You must first authenticate in order to perform this request.
 *       content:
 *         text/plain:
 *           schema:
 *             $ref: '#/components/schemas/serverResponseText'
 *     Forbidden:
 *       description: Forbidden. You do not have enough permissions to perform this request.
 *       content:
 *         text/plain:
 *           schema:
 *             $ref: '#/components/schemas/serverResponseText'
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     correlationRef:
 *       type: string
 *       description: a correlation id
 *       maxLength: 64
 *       pattern: '^[a-zA-Z0-9]+[\-\w]*$'
 *       example: asF4-3d7x
 *     email:
 *       type: string
 *       format: email
 *       maxLength: 128
 *       pattern: '^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$'
 *       example: mary@contoso.com
 *     serverResponseText:
 *       type: string
 *       maxLength: 2000
 *       pattern: '^[ \w\.!?,;:\-=><\/&%\$\+\*~#]*$'
 *       example: Server Busy.
 *     attrETag:
 *       type: string
 *       description: convenience field for replacing 'if-match' header for html form submission
 *       maxLength: 128
 *       pattern: '^[a-zA-Z0-9]+[\-\w]*$'
 *       example: A34f-sd32-22bb
 *     attrLinkTitle:
 *       type: string
 *       maxLength: 250
 *       pattern: '^[\w\- \.!?\$%#]+$'
 *       description: a user friendly label
 *       example: Next page
 *     attrLinkName:
 *       type: string
 *       maxLength: 250
 *       pattern: '^[\w\-]+$'
 *       description: a unique identifier for this link. Can be used to distinguish multiple links with the same `rel` value
 *       example: next
 *     attrDataPrompt:
 *       type: string
 *       maxLength: 250
 *       pattern: '^[\w\- \.!?\$%#]+$'
 *       description: a user friendly label
 *       example: Date of birth
 *     attrDataName:
 *       type: string
 *       maxLength: 250
 *       pattern: '^[\w\-]+$'
 *       description: a unique identifier for this data element
 *       example: dob
 */

//
// hypermedia elements
//


/**
 * @swagger
 * components:
 *   schemas:
 *     link:
 *       description: a link relation element
 *       type: object
 *       additionalProperties: false
 *       required:
 *         - rel
 *         - href
 *       properties:
 *         rel:
 *           type: string
 *           format: uri
 *           example: next
 *         href:
 *           type: string
 *           format: uri
 *           example: https://aboard4u.herokuapp.com/api/message?first=20&after=asd-345
 *         title:
 *           $ref: '#/components/schemas/attrLinkTitle'
 *         name:
 *           $ref: '#/components/schemas/attrLinkName'
 *         type:
 *           type: string
 *           pattern: '^\w+/[-\.\w]+(?:\+[-\.\w]+)?$'
 *           maxLength: 128
 *           description: the media type of the target IRI. Can be used to distinguish multiple links with the same `rel` value
 *           example: application/json
 *     links:
 *       description: the set of available link relations for this resource in the current context
 *       type: array
 *       maxItems: 100
 *       items:
 *         $ref: '#/components/schemas/link'
 *     dataElement:
 *       description: a generic element template
 *       type: object
 *       additionalProperties: false
 *       properties:
 *         data:
 *           type: object
 *           additionalProperties: false
 *           required:
 *             - name
 *           properties:
 *             name:
 *               $ref: '#/components/schemas/attrDataName'
 *             value:
 *               type: string
 *               maxLength: 4096
 *               pattern: '^.*$'
 *             prompt:
 *               $ref: '#/components/schemas/attrDataPrompt'
 *     dataTemplate:
 *       description: a set of templates
 *       type: array
 *       maxItems: 100
 *       items:
 *         $ref: '#/components/schemas/dataElement'
 *     template:
 *       description: an item creation form template
 *       type: array
 *       maxItems: 100
 *       items:
 *         $ref: '#/components/schemas/dataElement'
 *     query:
 *       description: a predefined search template
 *       type: object
 *       additionalProperties: false
 *       required:
 *         - rel
 *         - href
 *       properties:
 *         rel:
 *           type: string
 *           format: uri
 *           example: next
 *         href:
 *           type: string
 *           format: uri
 *           example: https://aboard4u.herokuapp.com/message?first=20&after=asd-345
 *         name:
 *           $ref: '#/components/schemas/attrLinkName'
 *         title:
 *           $ref: '#/components/schemas/attrLinkTitle'
 *         data:
 *           $ref: '#/components/schemas/dataTemplate'
 *     queries:
 *       description: the set of predefined queries for this collection
 *       type: array
 *       maxItems: 100
 *       items:
 *         $ref: '#/components/schemas/query'
 */
