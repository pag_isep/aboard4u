/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// setting up the openapi plugins
//

import { readFile } from 'fs/promises';
import billboardRoutes from './billboard-routes.js';
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';


/**
 * Billboard API endpoint
 * @param {*} config 
 * @param {*} dependencies 
 */
function initBillboard(config, dependencies) {
    billboardRoutes(config, dependencies);
}

/**
 * swagger API documentation
 * 
 * @param {*} config 
 * @param {*} dependencies 
 * @param {*} app 
 */
 async function initApiDocs(config, dependencies) {
    const app = dependencies.app;

    // A Git flavored Markdown file describing the API. visit https://github.github.com/gfm/ for sintax
    const descriptionContent = await readFile(new URL("openapi-description.md", import.meta.url), 'utf8');

    let securitySchemes = {
        bearerAuth: {
            type: "http",
            scheme: "bearer",
            bearerFormat: "JWT"
        }
    };

    let security = [{
        bearerAuth: []
    }];
    if (config.service.ALLOW_BASIC_DIGEST_AUTH) {
        securitySchemes.basicAuth = {
            type: "http",
            scheme: "Basic"
        };
        securitySchemes.digestAuth = {
            type: "http",
            scheme: "Digest"
        };

        security.push({
            basicAuth: []
        });
        security.push({
            digestAuth: [],
        });
    }

    const swaggerJSOptions = {
        swaggerDefinition: {
            openapi: "3.0.0",
            info: {
                title: 'A Board 4 U Message Service',
                version: config.API_VERSION,
                description: descriptionContent,
                contact: {
                    name: "Paulo Gandra de Sousa",
                    email: "pagsousa@gmail.com"
                },
                license: {
                    name: "MIT",
                    url: "https://spdx.org/licenses/MIT.html"
                },
            },
            components: {
                securitySchemes
            },
            security,
            servers: [{
                url: config.server.rest.API_ROOT
            }]
        },
        // path to the API docs - pass all in array
        apis: ['message-server/**/rest/*.js'],
    };
    const swaggerSpec = swaggerJSDoc(swaggerJSOptions);

    app.use(config.server.rest.API_PATH + '/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

    app.get(config.server.rest.API_PATH + '/openapi.json', function (req, res) {
        res.json(swaggerSpec);
    });
}

/////////////////////////////
// MODULE EXPORTS

export function initRoutes(config, dependencies) {
    initApiDocs(config, dependencies);
    initBillboard(config, dependencies);
}