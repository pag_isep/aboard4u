/*
 * A Board 4 U
 *
 * Copyright (c) 2019-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */



/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// setting up the REST routes
//


import BillboardResource from "./billboard-handler.js";

export default function initRoutes(config, dependencies) {
    const app = dependencies.app;
    const ensureSessionLinks = dependencies.ensureSessionLinks;

    //
    // load resource modules
    //
    const billboardResource = new BillboardResource(config, dependencies);

    const API_PATH = config.server.rest.API_PATH;

    //
    // billboard
    //
    app.route(API_PATH)
        .get(ensureSessionLinks, billboardResource.handleGetTOC);

    app.route(API_PATH + "/version")
        .get(ensureSessionLinks, billboardResource.handleGetVersion);
}
