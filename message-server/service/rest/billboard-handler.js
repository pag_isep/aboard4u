/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// billboard API
//


// these will be passed to the module thru the configure() function
let SETTINGS;
let Response;
let DEPENDENCIES;

/**
 * @swagger
 * /:
 *   get:
 *     tags:
 *       - Billboard
 *     summary: API's Table of contents
 *     description: API's table of contents, as a large road-side billboard announcing the API capabilities
 *     security: []
 *     responses:
 *       200:
 *         description: the table of contents in the form of links to navigate and explore the API
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               format: html
 *           application/json:
 *             schema:
 *               type: object
 *               additionalProperties: false
 *               properties:
 *                 links:
 *                   $ref: '#/components/schemas/links'
 *         links:
 *           GetMessages:
 *             operationId: getMessages
 *             description: follow the `https://aboard4u.herokuapp.com/rel/messages` relation to GET all messages
 *           GetUsers:
 *             operationId: getUsers
 *             description: follow the `https://aboard4u.herokuapp.com/rel/users` relation to GET all users
 *           PostMessage:
 *             operationId: postMessage
 *             description: create a new member in the message collection by POSTing to the `https://aboard4u.herokuapp.com/rel/messages` relation
 *           Signup:
 *             operationId: signup
 *             description: create a new member in the user collection by POSTing to the `https://aboard4u.herokuapp.com/rel/users` relation
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 * 
 */
function handleGetTOC(req, res) {
    //content
    const toc = {
        links: [{
            rel: "help",
            href: SETTINGS.server.rest.API_ROOT + '/api-docs'
        },
        {
            rel: "describedBy",
            href: SETTINGS.server.rest.API_ROOT + '/openapi.json'
        },
        {
            rel: "license",
            href: "https://spdx.org/licenses/MIT.html"
        },
        {
            rel: "self",
            href: SETTINGS.server.rest.API_ROOT
        },
        {
            rel: "https://schema.org/softwareVersion",
            href: SETTINGS.server.rest.API_ROOT + "/version"
        },
        {
            rel: "https://aboard4u.herokuapp.com/rel/login",
            href: SETTINGS.server.rest.API_ROOT + "/authz/login"
        },
        {
            rel: "https://aboard4u.herokuapp.com/rel/password-reset",
            href: SETTINGS.server.rest.API_ROOT + "/authz/password-reset"
        },
        {
            rel: "https://aboard4u.herokuapp.com/rel/users",
            href: SETTINGS.server.rest.API_ROOT + "/user"
        },
        {
            rel: "https://aboard4u.herokuapp.com/rel/messages",
            href: SETTINGS.server.rest.API_ROOT + "/message"
        }
        ]
    };

    // 1 day cache - it could be even bigger...
    res.set('Cache-Control', 'public, max-age=86400');
    const response = new Response(res, SETTINGS);
    response.format({
        'text/html': function () {
            // redirect to the webapp that serves as autoexplorable and self-documented API
            response.redirect("/");
        },
        'application/json': function () {
            response.ok(toc);
        },
        'application/vnd.pagsousa.hyper+json': function () {
            response.ok(toc);
        }
    });
}

/**
 * @swagger
 * /version:
 *   get:
 *     tags:
 *       - Billboard
 *     summary: returns information on the API version
 *     description: returns information on the API version
 *     security: []
 *     responses:
 *       200:
 *         description: information on the API version
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               format: html
 *           application/json:
 *             schema:
 *               type: object
 *               additionalProperties: false
 *               properties:
 *                 version:
 *                   description: semantic versioning
 *                   type: string
 *                   maxLength: 50
 *                   pattern: '^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$'
 *                 title:
 *                   type: string
 *                   maxLength: 250
 *                   pattern: '^\w+$'
 *                 author:
 *                   type: string
 *                   maxLength: 250
 *                   pattern: '^[\w ,]+$'
 *                 copyright:
 *                   type: string
 *                   maxLength: 250
 *                   pattern: '^[\w ,]+$'
 *       404:
 *         $ref: '#/components/responses/NotFound'
 *       429:
 *         $ref: '#/components/responses/TooManyRequests'
 */
function handleGetVersion(req, res) {
    // 1 day cache - it could be even bigger...
    res.set('Cache-Control', 'public, max-age=86400');
    const response = new Response(res, SETTINGS);
    response.format({
        'text/html': function () {
            response.html(
                `
                <body>
                <h1>A Board 4 U</h1>
                <p>v<span class='https://schema.org/softwareVersion'>${SETTINGS.API_VERSION}</span></p>
                <p>Copyright (c) 2013-2022 <span class='author'>Paulo Gandra Sousa</span></p>
                </body>
                `
            );
        },
        'application/json': function () {
            response.ok({
                version: SETTINGS.API_VERSION,
                title: 'A Board 4 U message service',
                author: 'Paulo Gandra de Sousa',
                copyright: '2013-2022'
            });
        }
    });
}


/////////////////////////////
// MODULE EXPORTS

export default function BillboardResource (config, dependencies) {
    SETTINGS = config;
    DEPENDENCIES = dependencies;
    Response = dependencies.Response;

    return {
        handleGetTOC,
        handleGetVersion
    };
}