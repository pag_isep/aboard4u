/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion: 11 */

"use strict";

import debug from "debug";
const LOGGER = debug("pag:mserver");

// version numbers
import { readFileSync } from 'fs';
const packageJson = JSON.parse( readFileSync(new URL("file://" + process.cwd() + '/package.json', import.meta.url)));
const API_VERSION = packageJson.version;

//
// configuration
//
import dotenv from "dotenv";
const result = dotenv.config();

// server
import os from "os";

const PORT = parseInt(process.env.PORT || process.argv[2] || 3001);
const LOCALHOST = "http://" + os.hostname() + ":" + PORT;
//TODO validate URL if read from .env
const SERVER_ROOT = process.env.ABOARD4U_HOST || LOCALHOST;

//service
const RATE_LIMIT = parseInt(process.env.ABOARD4U_RATE_LIMIT) || 200;
const ALLOW_BASIC_DIGEST_AUTH = process.env.ALLOW_BASIC_DIGEST_AUTH === 'true' || false;

// API paths
const REST_PATH = "/api";
const GRAPHQL_PATH = "/graphql";

// strategies
const DB_MODE = process.env.ABOARD4U_DB_MODE || "mongodb";
const IMAGE_STORE_MODE = process.env.ABOARD4U_IMAGE_STORE_MODE || "file";
const TRIVIA_AUGMENTER_MODE = process.env.ABOARD4U_TRIVIA_AUGMENTER_MODE || "static";

// check mandatory config
if (!process.env.SIGN_SECRET) {
    LOGGER('Missing mandatory configuration SIGN_SECRET for signing JWT tokens');
    throw new Error('Missing mandatory configuration SIGN_SECRET for signing JWT tokens');
}

//
// the settings
//
export const SETTINGS = Object.freeze({
    API_VERSION,

    server: {
        SERVER_ROOT,
        PORT,
        rest: {
            API_ROOT: SERVER_ROOT + REST_PATH,
            API_PATH: REST_PATH
        },
        graphql: {
            API_ROOT: SERVER_ROOT + GRAPHQL_PATH,
            API_PATH: GRAPHQL_PATH
        }
    },

    service: {
        RATE_LIMIT,
        ALLOW_BASIC_DIGEST_AUTH
    },

    service_mode: {
        PERSISTENCE: DB_MODE,
        IMAGE_STORE: IMAGE_STORE_MODE,
        TRIVIA_AUGMENTER: TRIVIA_AUGMENTER_MODE
    }
});
