/*
 * A Board 4 U
 *
 * Copyright (c) 2013-2022 Paulo Gandra de Sousa
 * MIT Licensed
 */


/*jslint node: true */
/*jshint esversion:11 */

"use strict";

import debug from 'debug';
const LOGGER = debug('pag:mserver');

//
// controllers
//
import {
    MessageController
} from '../messages/application/message-controller.js';
import {
    UserController
} from '../users/application/user-controller.js';
import {
    AuthzController
} from '../authz/application/authz-controller.js';
import {
    TagController
} from '../tags/application/tag-controller.js';

//
// Response
//
import {
    Hyper
} from '../util/pag-hyper.js';
import {
    Response
} from '../util/response.js';

//
// error handlers
//
import {
    handleError as handleGraphQLError
} from '../util/apollo-error-helper.js';

/////////////////////////////
// MODULE EXPORTS

export async function buildServices(settings) {

    //
    // repositories
    //
    const {
        MessageRepository,
        UserRepository,
        AuthzRepository,
        TagRepository
    } = await import("file://" + process.cwd() + '/message-server/persistence/data-layer-' + settings.service_mode.PERSISTENCE + '.js');
    LOGGER(`Starting persistence layer in ${settings.service_mode.PERSISTENCE} mode.`);

    //
    // infrastructure
    //
    const moduleImageStore = await import("file://" + process.cwd() + '/message-server/infrastructure/image-store-' + settings.service_mode.IMAGE_STORE + '.js');
    const ImageStore = moduleImageStore.ImageStore;
    LOGGER(`Starting image store service in ${settings.service_mode.IMAGE_STORE} mode.`);

    const moduleTriva = await import("file://" + process.cwd() + '/message-server/infrastructure/trivia-augmenter-' + settings.service_mode.TRIVIA_AUGMENTER + '.js');
    const TriviaAugmenter = moduleTriva.TriviaAugmenter;
    LOGGER(`Starting trivia augmentation service in ${settings.service_mode.TRIVIA_AUGMENTER} mode.`);


    //
    //dependency registry
    //
    return Object.freeze({
        MessageRepository,
        UserRepository,
        AuthzRepository,
        TagRepository,

        UserController,
        MessageController,
        AuthzController,
        TagController,

        ImageStore,
        TriviaAugmenter,

        Hyper,
        Response,

        handleGraphQLError
    });
}